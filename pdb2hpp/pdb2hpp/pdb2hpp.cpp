﻿// This file is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include <iostream>
#include <string>
#include <codecvt>
#include <vector>
#include <assert.h>
#include <atlbase.h>
#include <comdef.h>
#include <dia2.h>

class LinePrinter {
    public:
        std::ostream& stream = std::cout;
        std::ostream& new_line() {
            for(size_t i = 0; i < this->indent_level; i++) {
                this->stream << "\t";
            }
            return this->stream;
        }

        void operator +=(std::ostream& stream) {
            stream << std::endl;
        };

        void indent() {
            this->indent_level++;
        }

        void dedent() {
            this->indent_level--;
        }
    private:
        int indent_level = 0;
};

LinePrinter line_printer;

#define LINE line_printer += line_printer.new_line()
#define PARTIAL_LINE line_printer.new_line()
#define OUT line_printer.stream

class SymbolIterator {
    public:
        SymbolIterator(CComPtr<IDiaSymbol> base_sym, enum SymTagEnum tag) {
            assert(SUCCEEDED(base_sym->findChildrenEx(
                tag, NULL, nsNone, &this->dia_enum)));
            this->operator++();
        }

        SymbolIterator(
            CComPtr<IDiaEnumSymbols> dia_enum, CComPtr<IDiaSymbol> sym)
            : dia_enum(dia_enum), current_sym(sym) { }

        SymbolIterator& begin() {
            return *this;
        }

        SymbolIterator end() {
            return SymbolIterator(this->dia_enum, NULL);
        }

        bool operator==(SymbolIterator& other) {
            return this->dia_enum == other.dia_enum
                && this->current_sym == other.current_sym;
        }
        bool operator!=(SymbolIterator& other) {
            return !(*this == other);
        }

        SymbolIterator& operator++() {
            unsigned long count;
            this->current_sym.Release();
            assert(SUCCEEDED(this->dia_enum->Next(
                1, &this->current_sym, &count)));
            if(count != 1) {
                this->current_sym = NULL;
            }
            return *this;
        }

        CComPtr<IDiaSymbol> operator*() {
            return this->current_sym;
        }
    private:
        CComPtr<IDiaEnumSymbols> dia_enum;
        CComPtr<IDiaSymbol> current_sym;
};

std::string join_vec(std::vector<std::string> vec, std::string sep) {
    std::string result;
    for(size_t i = 0; i < vec.size(); i++) {
        result += vec[i];
        if(i != vec.size() - 1) {
            result += sep;
        }
    }

    return result;
}

std::string sym_name(CComPtr<IDiaSymbol> sym) {
    bstr_t name_bstr;
    assert(SUCCEEDED(sym->get_name(name_bstr.GetAddress())));
    std::string name =
        std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(
            name_bstr.GetBSTR(), name_bstr.GetBSTR() + name_bstr.length());
    return name;
}

std::string type_value_str(CComPtr<IDiaSymbol> type, std::string name = "");

std::string function_sym_params(
        CComPtr<IDiaSymbol> func_type,
        std::vector<std::string> param_names = {}) {
    std::vector<std::string> params;
    size_t i = 0;
    for(auto param : SymbolIterator(func_type, SymTagFunctionArgType)) {
        CComPtr<IDiaSymbol> param_type;
        assert(SUCCEEDED(param->get_type(&param_type)));

        DWORD tag;
        param->get_symTag(&tag);

        std::string name;
        if (param_names.size() > i) {
            name = param_names[i];
        }

        params.push_back(type_value_str(param_type, name));
        i++;
    }

    return "(" + join_vec(params, ", ") + ")";
}

std::string type_value_str(CComPtr<IDiaSymbol> type, std::string name) {
    DWORD tag;
    assert(SUCCEEDED(type->get_symTag(&tag)));

    std::string space_name;
    if(name != "") {
        space_name = " " + name;
    }

    std::string mod_str;
    BOOL is_const;
    assert(SUCCEEDED(type->get_constType(&is_const)));
    if (is_const) {
        mod_str = "const ";
    }

    if(tag == SymTagUDT || tag == SymTagTypedef || tag == SymTagEnum) {
        return mod_str + sym_name(type) + space_name;
    }
    if(tag == SymTagBaseType) {
        DWORD base_type;
        assert(SUCCEEDED(type->get_baseType(&base_type)));
        ULONGLONG length;
        assert(SUCCEEDED(type->get_length(&length)));

        std::string name;
        // TODO: Varargs manifest as btNoType, fix
        switch(base_type) {
            // This is just a partial switch for the types likely to turn up
            case btVoid:
                name = "void";
                break;
            case btBool:
                name = "bool";
                break;
            case btChar:
                name = "char";
                break;
            case btWChar:
                name = "wchar_t";
                break;
            case btInt:
            case btLong:
                name = "int" + std::to_string(length * 8) + "_t";
                break;
            case btUInt:
            case btULong:
                name = "uint" + std::to_string(length * 8) + "_t";
                break;
            case btFloat:
                if(length == 4) {
                    name = "float";
                } else if(length == 8) {
                    name = "double";
                } else {
                    name = "float" + length;
                }
                break;
            case btBSTR:
                name = "BSTR";
                break;
            case btHresult:
                name = "HRESULT";
                break;
            default:
                name = "UnkBT" + std::to_string(base_type);
        }

        return mod_str + name + space_name;
    }
    if(tag == SymTagPointerType) {
        CComPtr<IDiaSymbol> inner_type;
        assert(SUCCEEDED(type->get_type(&inner_type)));
        BOOL is_reference;
        assert(SUCCEEDED(type->get_reference(&is_reference)));
        std::string prefix;
        if (is_reference) {
            prefix = "&";
        }
        else {
            prefix = "*";
        }
        if (is_const) {
            if (name != "") {
                prefix += " const ";
            }
            else {
                prefix += " const";
            }
        }
        BOOL is_volatile;
        assert(SUCCEEDED(type->get_volatileType(&is_volatile)));
        if (is_volatile) {
            if (name != "") {
                prefix += " volatile ";
            }
            else {
                prefix += " volatile";
            }
        }
        return type_value_str(inner_type, prefix + name);
    }
    if(tag == SymTagArrayType) {
        CComPtr<IDiaSymbol> inner_type;
        assert(SUCCEEDED(type->get_type(&inner_type)));
        DWORD length;
        assert(SUCCEEDED(type->get_count(&length)));
        return type_value_str(
            inner_type,
            name + "[" + std::to_string(length) + "]");
    }
    if(tag == SymTagFunctionType) {
        CComPtr<IDiaSymbol> ret_type;
        assert(SUCCEEDED(type->get_type(&ret_type)));

        return type_value_str(ret_type) + "(" + name + ")"
            + function_sym_params(type);
    }
    
    return "Unk" + std::to_string(tag) + space_name;
}

bool sym_in_class(CComPtr<IDiaSymbol> sym) {
    DWORD class_parent = 0;
    assert(SUCCEEDED(sym->get_classParentId(&class_parent)));
    return class_parent != 0;
}

bool sym_constructor_of(CComPtr<IDiaSymbol> func, CComPtr<IDiaSymbol> cls) {
    auto func_name = sym_name(func);
    auto cls_name = sym_name(cls);
    if (func_name == cls_name) {
        return true;
    }
    auto colon_pos = cls_name.find_last_of(':');
    if (colon_pos != std::string::npos) {
        auto cls_name_no_ns = cls_name.substr(colon_pos + 1);
        if (func_name == cls_name_no_ns) {
            return true;
        }
    }
    return false;
}

void process_sym(CComPtr<IDiaSymbol> sym, std::vector<std::string> udt_stack = {}) {
    BOOL is_compiler_generated;
    assert(SUCCEEDED(sym->get_compilerGenerated(&is_compiler_generated)));
    if (is_compiler_generated) {
        return;
    }

    DWORD tag;
    assert(SUCCEEDED(sym->get_symTag(&tag)));

    if(tag == SymTagEnum) {
        LINE << "enum " << sym_name(sym) << " {";
        line_printer.indent();

        for(auto member : SymbolIterator(sym, SymTagData)) {
            LINE << sym_name(member) << ",";
        }

        line_printer.dedent();
        LINE << "};";
    } else if(tag == SymTagUDT) {
        if(std::find(udt_stack.begin(), udt_stack.end(), sym_name(sym)) != udt_stack.end()) {
            LINE << "// Elided recursive listing of " << sym_name(sym);
            return;
        }

        DWORD kind;
        assert(SUCCEEDED(sym->get_udtKind(&kind)));

        if(kind == UdtStruct) {
            PARTIAL_LINE << "struct ";
        } else if(kind == UdtClass) {
            PARTIAL_LINE << "class ";
        } else if(kind == UdtUnion) {
            // XXX: Not handled properly for now
            PARTIAL_LINE << "union ";
        }

        OUT << sym_name(sym);

        std::vector<std::string> base_class_names;
        for(auto base_class : SymbolIterator(sym, SymTagBaseClass)) {
            base_class_names.push_back("public " + sym_name(base_class));
        }
        if(base_class_names.size() > 0) {
            OUT << ": " << join_vec(base_class_names, ", ");
        }

        OUT << " {" << std::endl;
        line_printer.indent();
        if (kind == UdtClass) {
            // TODO: Also support writing the actual access modifiers
            LINE << "public:";
        }

        // TODO: Will mutating the variable break anything?
        udt_stack.push_back(sym_name(sym));
        for(auto member : SymbolIterator(sym, SymTagNull)) {
            process_sym(member, udt_stack);
        }
        udt_stack.pop_back();

        line_printer.dedent();
        LINE << "};";
    } else if(tag == SymTagData) {
        CComPtr<IDiaSymbol> type;
        assert(SUCCEEDED(sym->get_type(&type)));

        DWORD location_type;
        assert(SUCCEEDED(sym->get_locationType(&location_type)));
        std::string static_mod;
        if(location_type == LocIsStatic) {
            static_mod = "static ";
        }

        PARTIAL_LINE << static_mod
            << type_value_str(type, sym_name(sym)) << ";";

        DWORD addr;
        assert(SUCCEEDED(sym->get_relativeVirtualAddress(&addr)));
        if(addr != 0) {
            OUT << " // addr=0x" << std::hex << addr << std::dec;
        }
        OUT << std::endl;
    } else if(tag == SymTagFunction) {
        CComPtr<IDiaSymbol> func_type;
        assert(SUCCEEDED(sym->get_type(&func_type)));

        std::string name = sym_name(sym);

        CComPtr<IDiaSymbol> parent;
        assert(SUCCEEDED(sym->get_classParent(&parent)));

        CComPtr<IDiaSymbol> ret_type;
        assert(SUCCEEDED(func_type->get_type(&ret_type)));

        bool print_ret_type =
            name[0] != '~'
            && (parent == NULL || !sym_constructor_of(sym, parent))
            && ret_type != NULL
            // No type for conversion operators
            && !(name.rfind("operator ") == 0
                && name != "operator new"
                && name != "operator new[]"
                && name != "operator delete"
                && name != "operator delete[]"
                && name.rfind("operator \"\"") != 0);

        std::vector<std::string> param_names;
        for (auto datasym : SymbolIterator(sym, SymTagData)) {
            DWORD kind;
            assert(SUCCEEDED(datasym->get_dataKind(&kind)));

            if (kind == DataIsParam) {
                // Ordering seems to be kept
                auto name = sym_name(datasym);
                // XXX: For some reason there seem to be duplicates, hence
                // this ugly workaround
                if (std::find(param_names.begin(), param_names.end(), name)
                        == param_names.end()) {
                    param_names.push_back(sym_name(datasym));
                }
            }
        }

        PARTIAL_LINE;

        BOOL is_virtual;
        assert(SUCCEEDED(sym->get_virtual(&is_virtual)));
        if(is_virtual) {
            OUT << "virtual ";
        }

        if(sym_in_class(sym)) {
            BOOL is_static;
            assert(SUCCEEDED(sym->get_isStatic(&is_static)));
            if(is_static) {
                OUT << "static ";
            }
        }

        if (print_ret_type) {
            OUT << type_value_str(ret_type, name);
        }
        else {
            OUT << name;
        }
        OUT << function_sym_params(func_type, param_names);

        // TODO: Doesn't seem to work
        BOOL is_const;
        assert(SUCCEEDED(sym->get_constType(&is_const)));
        if (is_const) {
            OUT << " const";
        }

        BOOL is_volatile;
        assert(SUCCEEDED(sym->get_volatileType(&is_volatile)));
        if (is_volatile) {
            OUT << " volatile";
        }
            
        OUT << ";";

        DWORD addr;
        assert(SUCCEEDED(sym->get_relativeVirtualAddress(&addr)));
        if(addr != 0) {
            OUT << " // addr=0x" << std::hex << addr << std::dec;
        }
        OUT << std::endl;
    }
}

int main(int argc, char *argv[])
{
    if(argc != 2) {
        std::cerr << "Usage: <pdb file>" << std::endl;
        exit(1);
    }

    CoInitialize(NULL);

    CComPtr<IDiaDataSource> data_source;
    assert(SUCCEEDED(CoCreateInstance(
        _uuidof(DiaSource), NULL, CLSCTX_INPROC_SERVER,
        IID_PPV_ARGS(&data_source))));

    std::wstring pdb_path(argv[1], argv[1] + strlen(argv[1]));
    assert(SUCCEEDED(data_source->loadDataFromPdb(pdb_path.c_str())));

    CComPtr<IDiaSession> session;
    assert(SUCCEEDED(data_source->openSession(&session)));

    CComPtr<IDiaSymbol> global_sym;
    assert(SUCCEEDED(session->get_globalScope(&global_sym)));

    for(auto sym : SymbolIterator(global_sym, SymTagNull)) {
        DWORD tag;
        assert(SUCCEEDED(sym->get_symTag(&tag)));

        if(tag != SymTagUDT) {
            if(sym_in_class(sym)) {
                continue;
            }
        }

        process_sym(sym);
    }
}