`pdb2hpp` is a rudimentary C++ utility to generate C++ headers from .pdb files.

Before use, make sure to register the MS DIA DLL (run this **as admin**):

```
regsvr32.exe $env:VSINSTALLDIR/"DIA SDK/bin/amd64/msdia140.dll"
```