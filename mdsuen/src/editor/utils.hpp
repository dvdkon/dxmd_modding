// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2021 David Koňařík

#pragma once
#include <exception>
#include <string>
#include <filesystem>
#include <optional>
#include <Windows.h>
#include <GL/glew.h>

class OpenGLErrorException : public std::exception {
    public:
    OpenGLErrorException(GLenum errnum) {
        this->msg = std::string("OpenGL error: " + std::to_string(errnum));
    }
    const char *what() const noexcept override {
        return this->msg.c_str();
    }
    
    private:
    std::string msg;
};

void catch_opengl_errors();

class WindowsErrorException : public std::exception {
    public:
    WindowsErrorException(HRESULT errnum) {
        this->msg = std::string("Windows error: " + std::to_string(errnum));
    }
    const char *what() const noexcept override {
        return this->msg.c_str();
    }
    
    private:
    std::string msg;
};

void assert_hresult(HRESULT res);

enum class FileDialogType {
    OPEN_FILE,
    SAVE_FILE
};
std::optional<std::filesystem::path> open_file_dialog(FileDialogType type);