// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2021 David Koňařík

#include "resourcebrowser.hpp"
#include <imgui.h>
#include <imgui_stdlib.h>
#include "common/imgui_extra.hpp"
#include "editor/gui.hpp"
#include "editor/gui/resourceviewer.hpp"

void ResourceBrowser::draw() {
    ImGui::Begin("Resource browser");

    ImGui::BeginActionBar("");
    if(ImGui::ActionBarButton("Load index")) {
        editor_backend.read_resource_index();
    }
    if(ImGui::ActionBarButton("Write index")) {
        editor_backend.write_resource_index();
    }
    ImGui::EndActionBar();

    ImGui::InputText("", &this->search_query);
    ImGui::SameLine();
    if(ImGui::Button("Search")) {
        if(this->search_query == "") {
            this->filtered_tree = {};
        } else {
            this->filtered_tree = std::move(filter_resource_tree(
                this->search_query, editor_backend.resource_tree_root));
        }
    }

    ImGui::BeginChild("resource_tree");
    if(this->filtered_tree) {
        this->draw_node(**this->filtered_tree);
    } else {
        this->draw_node(editor_backend.resource_tree_root);
    }
    ImGui::EndChild();
    ImGui::End();
}

void ResourceBrowser::draw_node(ResourceTreeNode &node) {
    auto name = node.name;
    auto children = &node.children;
    auto rrid = node.rrid;
    auto end_node = &node;
    while(children->size() == 1 && !rrid) {
        auto &child = *(*children)[0];
        name += "/" + child.name;
        children = &child.children;
        rrid = child.rrid;
        end_node = &child;
    }

    if(children->size() > 0) {
        if(ImGui::TreeNode(name.c_str())) {
            for(auto& child : *children) {
                this->draw_node(*child);
            }
            ImGui::TreePop();
        }
    } else {
        if(rrid) {
            ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.5, .7, 1., 1.));
            if(ImGui::Selectable(name.c_str(), false)) {
                gui.windows.add_window(std::make_unique<ResourceViewer>(*end_node));
            }
            ImGui::PopStyleColor(1);
        } else {
            ImGui::Text("%s", name.c_str());
        }
    }
}
