// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2021 David Koňařík
#include "resourceviewer.hpp"
#include <assert.h>
#include <fstream>
#include <functional>
#include <pahil/logging.hpp>
#include "editor/gui.hpp"
#include <GL/glew.h>
#include "editor/utils.hpp"

static void save_inner(std::function<void (std::ofstream &)> writer) {
    auto path = open_file_dialog(FileDialogType::SAVE_FILE);
    if(path) {
        std::ofstream outfile(*path, std::ios::binary | std::ios::out);
        writer(outfile);
    }
}

TextureViewer::TextureViewer(ZTextureMap text) : text(text) {
    this->gl_tex_ids.resize(text.GetNumMipLevels());
    glGenTextures(this->gl_tex_ids.size(), this->gl_tex_ids.data());

    for(uint32_t i = 0; i < text.GetNumMipLevels(); i++) {
        ZTextureMap::SMipLevel mip;
        text.GetMipLevel(&mip, i);
        ImGui::Text("Mip level %u: %ux%u", i, mip.nWidth, mip.nHeight);

        glBindTexture(GL_TEXTURE_2D, this->gl_tex_ids[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
        if(text.GetFormat() == RENDER_FORMAT_R8G8B8A8_UNORM) {
            glTexImage2D(
                GL_TEXTURE_2D, 0, GL_RGBA8, mip.nWidth, mip.nHeight, 0,
                GL_RGBA, GL_UNSIGNED_BYTE, mip.pData);
        } else if(text.GetFormat() == RENDER_FORMAT_R16G16_FLOAT) {
            // XXX: Looks different in external viewer
            glTexImage2D(
                GL_TEXTURE_2D, 0, GL_RG16F, mip.nWidth, mip.nHeight, 0,
                GL_RG, GL_HALF_FLOAT, mip.pData);
        } else if(text.GetFormat() == RENDER_FORMAT_BC1_UNORM) {
            glCompressedTexImage2D(
                GL_TEXTURE_2D, 0, GL_COMPRESSED_RGBA_S3TC_DXT1_EXT,
                mip.nWidth, mip.nHeight, 0, mip.nSizeInBytes, mip.pData);
        } else if(text.GetFormat() == RENDER_FORMAT_BC3_UNORM) {
            glCompressedTexImage2D(
                GL_TEXTURE_2D, 0, GL_COMPRESSED_RGBA_S3TC_DXT5_EXT,
                mip.nWidth, mip.nHeight, 0, mip.nSizeInBytes, mip.pData);
        } else if(text.GetFormat() == RENDER_FORMAT_BC4_UNORM) {
            glCompressedTexImage2D(
                GL_TEXTURE_2D, 0, GL_COMPRESSED_RED_RGTC1,
                mip.nWidth, mip.nHeight, 0, mip.nSizeInBytes, mip.pData);
        } else if(text.GetFormat() == RENDER_FORMAT_BC6H_UF16) {
            glCompressedTexImage2D(
                GL_TEXTURE_2D, 0, GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB,
                mip.nWidth, mip.nHeight, 0, mip.nSizeInBytes, mip.pData);
        } else if(text.GetFormat() == RENDER_FORMAT_BC7_UNORM) {
            glCompressedTexImage2D(
                GL_TEXTURE_2D, 0, GL_COMPRESSED_RGBA_BPTC_UNORM_ARB,
                mip.nWidth, mip.nHeight, 0, mip.nSizeInBytes, mip.pData);
        }
    }
    catch_opengl_errors();
}

TextureViewer::~TextureViewer() {
    // TODO: Some kind of refcounting
    //glDeleteTextures(this->gl_tex_ids.size(), this->gl_tex_ids.data());
}

void TextureViewer::draw() {
    ImGui::Text("Total size: %u", text.GetTotalSize());
    ImGui::Text("Width: %u", text.GetWidth());
    ImGui::Text("Height: %u", text.GetHeight());
    ImGui::Text("Depth: %u", text.GetDepth());
    ImGui::Text("Number of mip levels: %u", text.GetNumMipLevels());
    ImGui::Text("Default mip level: %u", text.GetDefaultMipLevel());
    ImGui::Text("Mip interpolation: %x", text.GetMipInterpolation());
    ImGui::Text("Flags: %x", text.GetFlags());

    std::string format = std::string("[unhandled ") + std::to_string(text.GetFormat()) + "]";
    if(text.GetFormat() == RENDER_FORMAT_UNKNOWN) { format = "UNKNOWN"; }
    else if(text.GetFormat() == RENDER_FORMAT_R8G8B8A8_UNORM) { format = "R8G8B8A8_UNORM"; }
    else if(text.GetFormat() == RENDER_FORMAT_R16G16_FLOAT) { format = "R16G16_FLOAT"; }
    else if(text.GetFormat() == RENDER_FORMAT_BC1_UNORM) { format = "BC1_UNORM"; }
    else if(text.GetFormat() == RENDER_FORMAT_BC3_UNORM) { format = "BC3_UNORM"; }
    else if(text.GetFormat() == RENDER_FORMAT_BC4_UNORM) { format = "BC4_UNORM"; }
    else if(text.GetFormat() == RENDER_FORMAT_BC6H_UF16) { format = "BC6H_UF16"; }
    else if(text.GetFormat() == RENDER_FORMAT_BC7_UNORM) { format = "BC7_UNORM"; }
    ImGui::Text("Render format: %s", format.c_str());

    std::string interpret_as = std::string("[unhandled ") + std::to_string(text.GetInterpretAs()) + "]";;
    if(text.GetInterpretAs() == ZTextureMap::INTERPRET_AS_COLOR) { interpret_as = "COLOR"; }
    else if(text.GetInterpretAs() == ZTextureMap::INTERPRET_AS_NORMAL) { interpret_as = "NORMAL"; }
    else if(text.GetInterpretAs() == ZTextureMap::INTERPRET_AS_HEIGHT) { interpret_as = "HEIGHT"; }
    else if(text.GetInterpretAs() == ZTextureMap::INTERPRET_AS_CONE) { interpret_as = "CONE"; }
    ImGui::Text("Interpret as: %s", interpret_as.c_str());

    std::string dimensions = std::string("[unhandled ") + std::to_string(text.GetDimensions()) + "]";;
    if(text.GetDimensions() == ZTextureMap::DIMENSIONS_2D) { dimensions = "2D"; }
    else if(text.GetDimensions() == ZTextureMap::DIMENSIONS_CUBE) { dimensions = "CUBE"; }
    else if(text.GetDimensions() == ZTextureMap::DIMENSIONS_VOLUME) { dimensions = "VOLUME"; }
    ImGui::Text("Dimensions: %s", dimensions.c_str());

    ImGui::Spacing();

    ImGui::BeginActionBar("texture actions");
    if(ImGui::ActionBarButton("Save as .DDS")) {
        save_inner([this](std::ofstream &outfile) {
            auto data = texture_as_dds(this->text);
            outfile.write(data.data(), data.size());
        });
    }
    ImGui::EndActionBar();

    ImGui::Spacing();

    for(uint32_t i = text.GetNumMipLevels(); i-- > 0; ) {
        ZTextureMap::SMipLevel mip;
        text.GetMipLevel(&mip, i);
        ImGui::Text("Mip level %u: %ux%u", i, mip.nWidth, mip.nHeight);
        ImGui::Image(
            reinterpret_cast<void *>(static_cast<uintptr_t>(this->gl_tex_ids[i])),
            ImVec2(mip.nWidth, mip.nHeight));
    }
}

ResourceViewer::ResourceViewer(ResourceTreeNode &node) : rrid(*node.rrid) {
    // TODO: Figure out how to load individual resources without loading the
    // whole resourcelib first.
    // How does the game do it? Trace it?
    // UPDATE: The game doesn't do it :D Loading a resourcelib just loads all of
    // the contained resources. We probably don't have to load a whole
    // *headerlib* first, though
    editor_backend.load_headerlib(*node.hlib_name);

    auto lrptr = GetResourceManager()->GetResourcePtrInternal(*node.library_rrid, High, EResourceLoadStatus::LOAD_STATUS_HEADER, false);

    this->name = static_cast<std::string>(rrid.ToDebugString());

    // TODO: ZStreamedLibraryResourceStub seems to have some "file
    // offset"/"length" data, so maybe this whole long diversion with manually
    // loading HLIBs was pointless?
    auto hlib = load_headerlib_raw(
        static_cast<std::string>(editor_backend.lrf_mediator->m_RuntimePath)
        + *node.hlib_name);
    assert(headerlib_raw_find_resource(hlib, *node.rrid, &this->locator));
    free(hlib);

    this->data_hex_view.ReadOnly = true;
    this->data_hex_view.Open = false;
    this->header_hex_view.ReadOnly = true;
    this->header_hex_view.Open = false;
}

std::vector<char> &ResourceViewer::load_raw_data() {
    if(!this->resdata) {
        this->resdata = this->locator.load_data();
    }
    return *this->resdata;
}

std::string ResourceViewer::get_title() {
    return "Resource viewer: " + this->name;
}

void ResourceViewer::draw() {
    ImGui::TextWrapped("Resource ID: %s", this->name.c_str());

    auto header_header = reinterpret_cast<SResourceHeaderHeader *>(
        this->locator.header_bytes.data());
    auto tag = tag_to_string(header_header->m_type);
    ImGui::Text("Tag: %s", tag.c_str());

    ImGui::BeginActionBar("resource data");
    if(ImGui::ActionBarButton("Show raw data hex view")) {
        this->data_hex_view.Open = true;
    }
    if(ImGui::ActionBarButton("Show header hex view")) {
        this->header_hex_view.Open = true;
    }
    if(ImGui::ActionBarButton("Save raw data")) {
        save_inner([this](std::ofstream &outfile) {
            outfile.write(this->load_raw_data().data(), this->load_raw_data().size());
        });
    }
    if(ImGui::ActionBarButton("Save header")) {
        save_inner([this](std::ofstream &outfile) {
            outfile.write(
                reinterpret_cast<char *>(this->locator.header_bytes.data()),
                this->locator.header_bytes.size());
        });
    }
    if(ImGui::ActionBarButton("Save header and raw data")) {
        save_inner([this](std::ofstream &outfile) {
            outfile.write(
                reinterpret_cast<char *>(this->locator.header_bytes.data()),
                this->locator.header_bytes.size());
            outfile.write(this->load_raw_data().data(), this->load_raw_data().size());
        });
    }
    ImGui::EndActionBar();

    if(this->data_hex_view.Open) {
        auto data = this->load_raw_data();
        this->data_hex_view.DrawWindow(this->get_title().c_str(), data.data(), data.size());
    }
    if(this->header_hex_view.Open) {
        auto data = this->locator.header_bytes;
        this->header_hex_view.DrawWindow(this->get_title().c_str(), data.data(), data.size());
    }

    try {
        if(tag == "TELN") {
            ImGui::Text("Type: ZTextLineData");
            ImGui::Separator();
            this->draw_TELN(this->load_and_install<ZTextLineData>());
        } else if(tag == "TELI") {
            ImGui::Text("Type: ZTextListData");
            ImGui::Separator();
            this->draw_TELI(this->load_and_install<ZTextListData>());
        } else if(tag == "XREF") {
            ImGui::Text("Type: ZExternReferenceMap");
            ImGui::Separator();
            this->draw_XREF(this->load_and_install<ZExternReferenceMap>());
        } else if(tag == "LOCR") {
            ImGui::Text("Type: ZTextListData");
            ImGui::Text("Only data for current locale is shown!");
            ImGui::Separator();
            this->draw_TELI(this->load_and_install<ZTextListData>());
        } else if(tag == "TEXT") {
            ImGui::Text("Type: ZTextureMap");
            ImGui::Separator();
            this->draw_TEXT_raw();
        }
    } catch(ResourceViewer::ResLoadFailedException e) {
        ImGui::Text("Loading failed/not finished! Status %d", e.status);
    }
}

void ResourceViewer::draw_TELN(TResourcePtr<ZTextLineData> teln) {
    ImGui::TextWrapped("%s", teln->GetText().ToCString());
    ImGui::Spacing();

    ImGui::Text("Part of TextList:");
    ImGui::Indent();
    this->draw_TELI(teln->m_pTextListResourcePtr);
    ImGui::Unindent();
}

void ResourceViewer::draw_TELI(TResourcePtr<ZTextListData> teli) {
    auto map = teli->GetMap();
    for(auto &kv : map) {
        ImGui::TextWrapped("%x = %s", kv.Key(), kv.Value().ToCString());
    }
}

void ResourceViewer::draw_XREF(TResourcePtr<ZExternReferenceMap> xref) {
    for(auto it = xref->m_xrefMap.Begin(); it != xref->m_xrefMap.End(); ++it) {
        ImGui::Text("- Entity ID: %d", it->entityId);
        ImGui::Text("  Property index: %d", it->propertyIndex);
        ImGui::Text("  Scene: %s", it->sceneRid.ToDebugString().ToCString());
    }
}

void ResourceViewer::draw_TEXT_raw() {
    if(!this->texture_viewer) {
        // To get the data we need without digging into the whole rendering
        // subsystem, we load the raw data into an intermediary type
        auto texmap = ZTextureMap(
            reinterpret_cast<uint8_t *>(this->load_raw_data().data() + 4));
        this->texture_viewer = TextureViewer(ZTextureMap(texmap));
    }
    this->texture_viewer->draw();
}
