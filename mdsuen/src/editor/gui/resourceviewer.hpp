// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2021 David Koňařík

#pragma once
#include <optional>
#include <vector>
#include <imgui.h>
#include <imgui_memory_editor.h>
#include "common/dxmd/runtime.resource.hpp"
#include "common/dxmd/runtime.localization.hpp"
#include "common/dxmd/runtime.entity.hpp"
#include "common/dxmd/runtime.render.hpp"
#include "common/imgui_extra.hpp"
#include "editor/backend.hpp"

class TextureViewer {
    public:
    TextureViewer(ZTextureMap text);
    ~TextureViewer();
    void draw();

    private:
    ZTextureMap text;
    std::vector<uint32_t> gl_tex_ids = {};
};

class ResourceViewer : public ImGui::StandaloneWindow {
    public:
    struct ResLoadFailedException : public std::exception {
        EResourceStatus status;

        ResLoadFailedException(EResourceStatus status)
            : status(status) {}
    };

    ZRuntimeResourceID rrid;
    HeaderlibResourceLocator locator;
    std::optional<ZResourcePtr> resptr;
    std::optional<std::vector<char>> resdata;
    std::string name;
    MemoryEditor data_hex_view;
    MemoryEditor header_hex_view;

    std::optional<TextureViewer> texture_viewer;

    ResourceViewer(ResourceTreeNode &node);

    std::string get_title();
    void draw();

    private:
    template<typename T>
    TResourcePtr<T> load_and_install() {
        if(!this->resptr) {
            this->resptr = GetResourceManager()->LoadResource(this->rrid, false);
        }
        if(this->resptr->GetResourceStatus() != RESOURCE_STATUS_VALID) {
            throw ResLoadFailedException(this->resptr->GetResourceStatus());
        }
        return TResourcePtr<T>(*this->resptr);
    };
    std::vector<char> &load_raw_data();
    void draw_TELN(TResourcePtr<ZTextLineData> teln);
    void draw_TELI(TResourcePtr<ZTextListData> teli);
    void draw_XREF(TResourcePtr<ZExternReferenceMap> xref);
    void draw_TEXT_raw();
};