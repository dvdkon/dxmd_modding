// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "gui.hpp"
#include <assert.h>
#include <exception>
#include <utility>
#include <GL/glew.h>
#include <imgui.h>
#include <imgui_stdlib.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_glfw.h>
#include <pahil/logging.hpp>
#include <GLFW/glfw3.h>
#include "editor/gamedata.hpp"
#include "editor/gui/resourceviewer.hpp"

void HeaderlibBrowser::draw() {
    ImGui::Text("Name: %s",
        this->hlib_ptr.GetRuntimeResourceID().ToDebugString().ToCString());

    auto *hlib = reinterpret_cast<ZHeaderLibrary *>(this->hlib_ptr.GetRawPointer());
    ImGui::Text("Loaded library count: %llu", hlib->GetLoadedLibraryCount());
    ImGui::Text("Library info count: %llu", hlib->GetLibraryInfoCount());

    ImGui::Text("Resources:");
    ImGui::BeginChild("resources");

    for(size_t i = 0; i < hlib->GetLibraryInfoCount(); i++) {
        auto lib_info = hlib->GetLibraryInfo(i);
        ImGui::Text("Library info %zu", i);
        ImGui::Indent();
        ImGui::TextWrapped("Resource ID: %s",
            lib_info->GetResourceID().ToString().ToCString());
        ImGui::TextWrapped("Map resource ID: %s",
            lib_info->GetMapResourceID().ToDebugString().ToCString());
        ImGui::TextWrapped("Source resource ID: %s",
            lib_info->GetSourceRuntimeResourceID().ToDebugString().ToCString());

        ImGui::Text("Entries:");
        ImGui::Indent();
        for(size_t j = 0; j < lib_info->GetEntryCount(); j++) {
            auto rstub = lib_info->GetEntry(j).pStub;
            if(rstub == NULL) {
                ImGui::Text("- NULL");
                continue;
            }
            ImGui::TextWrapped("- [%s] %s",
                tag_to_string(rstub->GetResourceTag()).c_str(),
                rstub->GetRuntimeResourceID().ToDebugString().ToCString());
            ImGui::Text("  RRID: %s", rstub->GetRuntimeResourceID().ToHexDebugString().ToCString());;
            ImGui::Text("  Status: %d", rstub->GetResourceStatus());
        }
        ImGui::Unindent();

        ImGui::Unindent();
    }

    ImGui::EndChild();
}

void HeaderlibList::draw() {
    ImGui::SetNextWindowSizeConstraints({500, 300}, {FLT_MAX, FLT_MAX});
    ImGui::Begin(".headerlib list");

    if(ImGui::Button("Populate resource tree from all")) {
        editor_backend.index_resources_raw();
    }

    ImGui::Text("headerlibs:");
    ImGui::BeginChild("headerlibs");
    ImGui::Columns(3);
    ImGui::SetColumnWidth(0, 350);
    ImGui::SetColumnWidth(1, 70);
    ImGui::SetColumnWidth(2, 70);

    ImGui::clipper_vec<std::filesystem::path>(
            editor_backend.headerlibs, [](auto &hl) {
        ImGui::PushID(hl.string().c_str());
        ImGui::Text("%s", hl.filename().string().c_str());
        ImGui::NextColumn();
        if(editor_backend.loaded_headerlibs.find(hl.filename().string())
        != editor_backend.loaded_headerlibs.end()) {
            ImGui::Text("Loaded");
            ImGui::NextColumn();
            if(ImGui::Button("Open")) {
                gui.windows.add_window(
                    std::make_unique<HeaderlibBrowser>(
                        editor_backend.loaded_headerlibs[
                            hl.filename().string()]));
            }
        } else {
            if(ImGui::Button("Load")) {
                editor_backend.load_headerlib(hl);
                editor_backend.populate_resource_tree();
            }
            ImGui::NextColumn();
        }
        ImGui::NextColumn();
        ImGui::PopID();
    });

    ImGui::Columns(1);
    ImGui::EndChild();

    ImGui::End();
}

const char *type_name(STypeID *type) {
    if(!type->pTypeInfo) {
        return "UNNAMED";
    }
    return type->pTypeInfo->pszTypeName;
}

void TypeBrowser::draw() {
    ImGui::Begin("Type browser");

    if(this->types.empty() || ImGui::Button("Refresh")) {
        this->types.clear();
        auto &orig_type_map = GetTypeRegistry().m_typeNameMap;
        for(auto it = orig_type_map.Begin(); it != orig_type_map.End(); ++it) {
            this->types.push_back({it->m_key.m_chars.m_pointer, it->m_value});
        }
        std::sort(this->types.begin(), this->types.end(), [](auto &a, auto &b) {
            return a.first < b.first;
        });
    }

    ImGui::Columns(2);
    ImGui::BeginChild("Type list");

    ImGui::clipper_vec<std::pair<std::string, STypeID *>>(types, [&](auto &pair) {
        if(ImGui::Selectable(pair.first.c_str(),
                            pair.second == this->selected_type)) {
            this->selected_type = pair.second;
        }
    });

    ImGui::EndChild();
    ImGui::NextColumn();

    if(this->selected_type) {
        auto type_info = (*this->selected_type)->pTypeInfo;
        if(type_info == NULL) {
            ImGui::Text("Type does not have IType");
        } else {

            ImGui::Text("Name: %s", type_info->pszTypeName);
            ImGui::Text("Type size: %d", type_info->m_nTypeSize);
            auto iclass = type_info->QueryClassInterface();
            if(iclass) {
                ImGui::Text("Class:");
                ImGui::Indent();

                ImGui::Text("Base classes:");
                ImGui::Indent();
                for(size_t i = 0; i < iclass->m_nBaseClasses; i++) {
                    auto bc = iclass->m_pBaseClasses[i];
                    ImGui::Text("%llu: ", bc.m_nOffset);
                    ImGui::SameLine();
                    this->type_link(bc.m_Type);
                }
                ImGui::Unindent();

                ImGui::Text("Input pins:");
                ImGui::Indent();
                for(size_t i = 0; i < iclass->m_nInputPins; i++) {
                    auto ip = iclass->m_pInputPins[i];
                    ImGui::Text("- %s : ", ip.m_sPinName.ToCString());
                    ImGui::SameLine();
                    this->type_link(ip.m_Type);
                }
                ImGui::Unindent();

                ImGui::Text("Properties:");
                ImGui::Indent();
                for(size_t i = 0; i < iclass->m_nProperties; i++) {
                    auto p = iclass->m_pProperties[i];
                    ImGui::Text("- %s : ", p.m_pszPropertyName);
                    ImGui::SameLine();
                    this->type_link(p.m_propertyInfo.m_Type);
                }
                ImGui::Unindent();

                ImGui::Unindent();
            }
            auto ienum = type_info->QueryEnumType();
            if(ienum) {
                ImGui::Text("Enum cases:");
                ImGui::Indent();
                for(size_t i = 0; i < ienum->items.m_uiCount; i++) {
                    auto item =
                        reinterpret_cast<IEnumType::SEnumItem const *>(
                            ienum->items.m_pStart)[i];
                    ImGui::Text("%d: %s", item.nValue, item.szName);
                }
                ImGui::Unindent();
            }
            auto icontainer = type_info->QueryContainerType();
            if(icontainer) {
                ImGui::Text("Container");
            }
            auto iarray = type_info->QueryArrayType();
            if(iarray) {
                ImGui::Text("Array");
            }
        }
    }

    ImGui::Columns(1);

    ImGui::End();
}

void TypeBrowser::type_link(STypeID *t) {
    // TODO: Some kind of pretty color palette
    ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.5, .7, 1., 1.));
    if(ImGui::Selectable(type_name(t), false)) {
        this->selected_type = t;
    }
    ImGui::PopStyleColor(1);
}

void StartupWindow::draw() {
    ImGui::Begin("MDSuen Editor startup");

    ImGui::InputText("RUNTIME_PATH", &this->runtime_path);
    if(!editor_backend.initialised) {
        if(ImGui::Button("Initialise DXMD modules")) {
            editor_backend.initialise(this->runtime_path);
            gui.show_startup = false;
        }
    } else {
        ImGui::Text("DXMD modules initialised!");
    }

    ImGui::End();
}

static void glfw_error_callback(int errnum, const char *msg) {
    LOG << "GLFW error (" << errnum << "): " << msg;
}

static void APIENTRY opengl_debug_callback(
        GLenum source, GLenum type, unsigned int id, GLenum severity, GLsizei length,
        const char *message, const void *user_param) {
    LOG << "OpenGL Debug (" << source << ", " << type << ", " << id << ", " << severity << "): " << message;
}

void EditorGui::run() {
    glfwSetErrorCallback(glfw_error_callback);
    if(!glfwInit()) {
        throw std::runtime_error("glfwInit() failed!");
    }

    // TODO: Switchable
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);  

    auto win = glfwCreateWindow(1000, 600, "MDSuen Editor", NULL, NULL);
    if(win == NULL) {
        throw std::runtime_error("glfwCreateWindow() failed!");
    }
    glfwMakeContextCurrent(win);
    glfwSwapInterval(1); //VSync

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

    if(glewInit() != GLEW_OK) {
        throw std::runtime_error("glewInit() failed!");
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS); 
    glDebugMessageCallback(opengl_debug_callback, NULL);
    // Enable all
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    auto &io = ImGui::GetIO();
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    io.ConfigViewportsNoAutoMerge = true;

    // Go beyond Latin-2
    const ImWchar font_range[] = { 0x0020, 0xFFFF, 0 };
    auto fira_sans = io.Fonts->AddFontFromFileTTF(
        "resources/fonts/FiraSans-Regular.ttf", 14, NULL, font_range);
    io.FontDefault = fira_sans;

    ImGui_ImplGlfw_InitForOpenGL(win, true);
    ImGui_ImplOpenGL3_Init("#version 150");

    while(!glfwWindowShouldClose(win)) {
        glfwPollEvents();

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        ImGui::DockSpaceOverViewport();

        this->draw();

        ImGui::Render();
        int width, height;
        glfwGetFramebufferSize(win, &width, &height);
        glViewport(0, 0, width, height);
        glClearColor(0.f, 0.f, 0.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        GLFWwindow* backup_ctx = glfwGetCurrentContext();
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
        glfwMakeContextCurrent(backup_ctx);

        glfwSwapBuffers(win);
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(win);
    glfwTerminate();
}

void EditorGui::draw() {
    if(show_startup) this->startup.draw();
    if(editor_backend.initialised) {
        this->draw_menu();
        if(this->show_headerlib_list) this->headerlib_list.draw();
        if(this->show_resource_browser) this->resource_browser.draw();
        if(this->show_type_browser) this->type_browser.draw();
        this->windows.draw();
    }
}

void EditorGui::draw_menu() {
    if(ImGui::BeginMainMenuBar()) {
        if(ImGui::BeginMenu("Windows")) {
            ImGui::MenuItem(".headerlib list", "", &this->show_headerlib_list);
            ImGui::MenuItem("Resource browser", "", &this->show_resource_browser);
            ImGui::MenuItem("Type browser", "", &this->show_type_browser);

            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }
}

EditorGui gui;