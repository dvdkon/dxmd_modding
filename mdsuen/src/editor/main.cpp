// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík

#include <fstream>
#include <mutex>
#include <pahil/logging.hpp>
#include "editor/gui.hpp"
#include "editor/gamedata.hpp"
#include "common/dxmd_allocator.hpp"

int main(int argc, char *argv[]) {
    std::ofstream logfile("mdsuen_editor.log");
    std::mutex log_mutex;
    pahil::logger.msg_handler = [&](std::string msg) {
        log_mutex.lock();
        // Don't create ridiculously long logs
        if(logfile.tellp() < 100000000) {
            OutputDebugStringA((msg + "\n").c_str());
            logfile << msg << std::endl;
        }
        log_mutex.unlock();
    };

    LOG << "Starting mdsuen_editor...";

    gui.run();
    //return 0;

    /*auto runtime_path = "D:\\games\\DXMD\\runtime\\";
    init_dxmd_modules(runtime_path);

    //TEST
    std::shared_ptr<ZResourceID> test = g2_make_shared<ZResourceID>();
    auto test2 = test;

    auto rm = GetResourceManager();
    auto rlm = GetResourceLibraryManager();

    ZLocalResourceFileMediator lrfm(runtime_path);
    std::map<std::string, ZResourcePtr> loaded;
    load_headerlib_with_deps(
        lrfm,
        loaded,
        std::string(runtime_path) + "FFD0A3A9790387277B1A3F8963C518DC.pc_headerlib");

    {
        LOG << "Loading preinit.ini";
        ZResourceID rid("[[assembly:/common/preinit.ini].pc_resourcelibdef].pc_headerlib");
        auto rrid = ZRuntimeResourceID::QueryRuntimeResourceID(rid);
        rm->LoadResource(rrid, true);
    }
    {
        LOG << "Loading globalresources.ini";
        ZResourceID rid("[[assembly:/common/globalresources.ini].pc_resourcelibdef].pc_headerlib");
        auto rrid = ZRuntimeResourceID::QueryRuntimeResourceID(rid);
        rm->LoadResource(rrid, true);
    }
    {
        LOG << "Loading globalinclude.ini";
        ZResourceID rid("[[assembly:/common/globalinclude.ini].pc_resourcelibdef].pc_headerlib");
        auto rrid = ZRuntimeResourceID::QueryRuntimeResourceID(rid);
        rm->LoadResource(rrid, true);
    }*/

    LOG << "Exiting successfully!";
    return 0;
}