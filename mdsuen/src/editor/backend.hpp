// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík

#pragma once
#include <optional>
#include <istream>
#include <ostream>
#include "common/dxmd_allocator.hpp"
#include "editor/gamedata.hpp"

struct ResourceTreeNode {
    std::string name;
    std::vector<std::shared_ptr<ResourceTreeNode>> children;
    std::optional<ZRuntimeResourceID> rrid;
    std::optional<ZRuntimeResourceID> library_rrid;
    std::optional<std::string> hlib_name;

    ResourceTreeNode(std::string name) : name(name) {}
    ResourceTreeNode(
            std::string name,
            std::vector<std::shared_ptr<ResourceTreeNode>> &children,
            std::optional<ZRuntimeResourceID> rrid,
            std::optional<ZRuntimeResourceID> library_rrid,
            std::optional<std::string> hlib_name)
        : name(name), children(std::move(children)), rrid(rrid),
          library_rrid(library_rrid), hlib_name(hlib_name) {}
};

class EditorBackend {
    public:
    bool initialised = false;
    std::vector<std::filesystem::path> headerlibs;
    std::map<std::string, ZResourcePtr> loaded_headerlibs;
    ResourceTreeNode resource_tree_root {"/"};
    g2_unique_ptr<ZLocalResourceFileMediator> lrf_mediator;

    ~EditorBackend();

    void initialise(std::string runtime_path);
    void load_headerlib(std::string name);
    void load_headerlib(std::filesystem::path path);
    void add_to_resource_tree(
        ZResourceID &rid, ZRuntimeResourceID rrid,
        ZRuntimeResourceID library_rrid, const std::string &hlib_name);
    // Populate resource tree with all resources in loaded headerlibs
    void populate_resource_tree();
    // Populate resource tree by raw-loading all headerlibs
    void index_resources_raw();
    // A resource index is a file containing a list of all the resources found
    // in all headerlibs, so they don't all have to be loaded.
    // TODO: What about resources in standalone files?
    void write_resource_index(std::ostream &out);
    // This overload uses default "resource_index.txt" file
    void write_resource_index();
    // Loads the index into the resource tree
    void read_resource_index(std::istream &in);
    void read_resource_index();
};

std::shared_ptr<ResourceTreeNode> filter_resource_tree(
    std::string &search_query, ResourceTreeNode &root);

extern EditorBackend editor_backend;