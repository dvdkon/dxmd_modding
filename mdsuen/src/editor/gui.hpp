// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík

#pragma once

#include <string>
#include <map>
#include <optional>
#include "editor/backend.hpp"
#include "common/imgui_extra.hpp"
#include "common/dxmd/runtime.localization.hpp"
#include "common/dxmd/runtime.entity.hpp"
#include "common/dxmd/runtime.render.hpp"
#include "editor/gui/resourcebrowser.hpp"

class HeaderlibBrowser : public ImGui::StandaloneWindow {
    public:
    ZResourcePtr hlib_ptr;

    HeaderlibBrowser(ZResourcePtr hlib_ptr) : hlib_ptr(hlib_ptr) {}

    std::string get_title() { return ".headerlib browser"; }
    void draw();
};

class HeaderlibList {
    public:
    void draw();

    private:
};

class TypeBrowser {
    public:
    void draw();
    void type_link(STypeID *t);
    
    private:
    std::vector<std::pair<std::string, STypeID *>> types;
    std::optional<STypeID *> selected_type;
};

class StartupWindow {
    public:
    void draw();

    private:
    // Some code needs the trailing backslash!
    // TODO: Configurable default
    std::string runtime_path = "D:\\games\\DXMD\\runtime\\";
};

class EditorGui {
    public:
    ImGui::StandaloneWindowSet windows;
    StartupWindow startup;
    HeaderlibList headerlib_list;
    TypeBrowser type_browser;
    ResourceBrowser resource_browser;

    bool show_startup = true;
    bool show_headerlib_list = false;
    bool show_type_browser = false;
    bool show_resource_browser = false;

    void run();
    void draw();
    void draw_menu();
};

extern EditorGui gui;