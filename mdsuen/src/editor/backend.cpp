// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík

#include "backend.hpp"
#include "pahil/logging.hpp"
#include <assert.h>
#include <algorithm>
#include <fstream>
#include <stack>

EditorBackend::~EditorBackend() {
    if(this->initialised) {
        unload_dxmd_modules();
    } 
}

void EditorBackend::initialise(std::string runtime_path) {
    init_dxmd_modules(runtime_path);
    // the string pointed to by runtime_path is stored inside the object, so we
    // allocate a new persistent copy
    char *runtime_path_copy = new char[runtime_path.size()];
    strcpy(runtime_path_copy, runtime_path.c_str());
    this->lrf_mediator =
        g2_make_unique<ZLocalResourceFileMediator>(runtime_path_copy);
    this->headerlibs = find_all_headerlibs(runtime_path);
    //this->build_resource_tree();

    auto rm = GetResourceManager();
    {
        LOG << "Loading preinit.ini";
        ZResourceID rid("[[assembly:/common/preinit.ini].pc_resourcelibdef].pc_headerlib");
        auto rrid = ZRuntimeResourceID::QueryRuntimeResourceID(rid);
        rm->LoadResource(rrid, true);
    }
    {
        LOG << "Loading globalresources.ini";
        ZResourceID rid("[[assembly:/common/globalresources.ini].pc_resourcelibdef].pc_headerlib");
        auto rrid = ZRuntimeResourceID::QueryRuntimeResourceID(rid);
        rm->LoadResource(rrid, true);
    }

    std::filesystem::path rp(runtime_path);
    // Loading this also seems necessary. Why?
    // Creates duplicate stubs...
    //this->load_headerlib(rp / "3133BD95C91CCB418964E1B8BA3BB23D.pc_headerlib");
    this->load_headerlib(rp / "F75BA09D656F6CA5CDFADD3C1511D75A.pc_headerlib");

    try {
        this->read_resource_index();
    } catch(std::exception e) {
        // At least populate with loaded headerlibs
        this->populate_resource_tree();
    }

    this->initialised = true;
}

void EditorBackend::load_headerlib(std::string name) {
    this->load_headerlib(
        std::filesystem::path(
            static_cast<std::string>(this->lrf_mediator->m_RuntimePath) + name));
}

void EditorBackend::load_headerlib(std::filesystem::path path) {
    if(this->loaded_headerlibs.count(path.filename().string())) {
        return; // Already loaded
    }
    load_headerlib_with_deps(
        *this->lrf_mediator, this->loaded_headerlibs, path);
}

static void sort_resource_tree(ResourceTreeNode &node) {
    std::sort(
        node.children.begin(), node.children.end(),
        [](auto &n1, auto &n2) {
            return n1->name < n2->name;
        });
    for(auto &child : node.children) {
        sort_resource_tree(*child);
    }
}

void EditorBackend::add_to_resource_tree(
        ZResourceID &rid, ZRuntimeResourceID rrid,
        ZRuntimeResourceID library_rrid, const std::string &hlib_name) {
    // I'd prefer a reference here, but that results in a flood of very
    // helpful MSVC errors
    ResourceTreeNode *current_node = &this->resource_tree_root;
    auto path = split_rid_for_path(rid);
    for(size_t i = 0; i < path.size(); i++) {
        auto comp = path[i];
        bool found = false;
        for(auto& child : current_node->children) {
            if(child->name == comp) {
                current_node = child.get();
                found = true;
                break;
            }
        }
        if(!found) {
            auto new_child = std::make_shared<ResourceTreeNode>(comp);
            auto old_node = current_node;
            current_node = new_child.get();
            old_node->children.push_back(std::move(new_child));
        }
        if(i == path.size() - 1) {
            current_node->rrid = rrid;
            current_node->library_rrid = library_rrid;
            current_node->hlib_name = hlib_name;
        }
    }
}

void EditorBackend::populate_resource_tree() {
    this->resource_tree_root.children.clear();

    for(auto &[hlib_name, hlib_ptr] : this->loaded_headerlibs) {
        TResourcePtr<ZHeaderLibrary> hlib(hlib_ptr);
        for(size_t i = 0; i < hlib->GetLibraryInfoCount(); i++) {
            auto lib_info = hlib->GetLibraryInfo(i);
            for(size_t j = 0; j < lib_info->GetEntryCount(); j++) {
                auto entry = lib_info->GetEntry(j);
                if(entry.pStub == NULL) {
                    LOG << "Encountered resourcelib entry without resource stub while populating resource tree!"
                        << " LibraryInfo ID: " << lib_info->GetResourceID().ToString()
                        << " Index: " << j;
                    continue;
                }
                auto rrid = entry.pStub->GetRuntimeResourceID();
                auto rid = ZRuntimeResourceID::QueryResourceID(rrid, true);
                auto lib_rrid = entry.pStub->m_libraryID;
                this->add_to_resource_tree(rid, rrid, lib_rrid, hlib_name);
            }
        }
    }
}

void EditorBackend::index_resources_raw() {
    this->resource_tree_root.children.clear();
    for(auto &hlib_path : this->headerlibs) {
        LOG << "Populating resource tree with resources from " << hlib_path;
        auto hlib = load_headerlib_raw(hlib_path);
        auto idmap = parse_idmap_raw(hlib->idmap);
        for(auto &entry : idmap) {
            auto lib_rrid = get_lib_rrid(entry.container_rrid);
            this->add_to_resource_tree(
                entry.rid, entry.resource_rrid, lib_rrid, hlib_path.filename().string());
        }
    }
}

static void write_resource_index_inner(
        std::ostream &out, ResourceTreeNode &node, uint8_t indent_level) {
    for(auto &child : node.children) {
        for(size_t i = 0; i < indent_level; i++) {
            out << '\t';
        }
        out << child->name;
        if(child->rrid) {
            // hlib_file is assumed to be present as well
            out << '\t' << std::hex << child->rrid->GetID()
                << '\t' << child->library_rrid->GetID()
                << '\t' << *child->hlib_name;
        }
        out << '\n';
        write_resource_index_inner(out, *child, indent_level + 1);
    }
}

void EditorBackend::write_resource_index(std::ostream &out) {
    write_resource_index_inner(out, this->resource_tree_root, 0);
}

void EditorBackend::write_resource_index() {
    std::ofstream os("resource_index.txt");
    this->write_resource_index(os);
}

void EditorBackend::read_resource_index(std::istream &in) {
    this->resource_tree_root.children.clear();

    std::stack<ResourceTreeNode *> hist;
    hist.push(&this->resource_tree_root);
    int8_t prev_indent_level = -1;
    std::string line;
    while(std::getline(in, line)) {
        auto indent_level = line.find_first_not_of('\t');
        auto line_split = ZString(line).Trim().Split('\t');
        auto node = std::make_shared<ResourceTreeNode>(line_split[0]);
        if(line_split.Size() > 1) {
            auto rrid_num = strtoull(line_split[1].ToCString(), NULL, 16);
            auto rrid = ZRuntimeResourceID::Create(rrid_num);
            auto lib_rrid_num = strtoull(line_split[2].ToCString(), NULL, 16);
            auto lib_rrid = ZRuntimeResourceID::Create(lib_rrid_num);
            node->rrid = {rrid};
            node->library_rrid = lib_rrid;
            node->hlib_name = line_split[3];
        }

        if(indent_level > prev_indent_level) {
            assert(indent_level == prev_indent_level + 1);
        } else {
            for(int8_t i = indent_level; i <= prev_indent_level; i++) {
                hist.pop();
            }
        }
        auto node_ptr = &*node;
        auto t = hist.top();
        hist.top()->children.push_back(std::move(node));
        hist.push(node_ptr);

        prev_indent_level = indent_level;
    }
    sort_resource_tree(this->resource_tree_root);
}

void EditorBackend::read_resource_index() {
    std::ifstream is("resource_index.txt");
    this->read_resource_index(is);
}

static std::optional<std::shared_ptr<ResourceTreeNode>> filter_resource_tree_inner(
        std::string &search_query, std::shared_ptr<ResourceTreeNode> node) {
    if(node->name.find(search_query) != std::string::npos) {
        return node;
    }

    std::vector<std::shared_ptr<ResourceTreeNode>> new_children;
    for(auto &child : node->children) {
        auto new_child = filter_resource_tree_inner(search_query, child);
        if(new_child) {
            new_children.push_back(std::move(*new_child));
        }
    }
    if(new_children.size() > 0) {
        auto new_node = std::make_shared<ResourceTreeNode>(
            node->name, new_children, node->rrid, node->library_rrid, node->hlib_name);
        return std::move(new_node);
    } else {
        return {};
    }
}

std::shared_ptr<ResourceTreeNode> filter_resource_tree(
        std::string &search_query, ResourceTreeNode &root) {
    auto result = filter_resource_tree_inner(
        search_query, std::make_shared<ResourceTreeNode>(root));
    if(!result) {
        return std::make_unique<ResourceTreeNode>("/");
    } else {
        return std::move(*result);
    }
}

EditorBackend editor_backend;