// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2021 David Koňařík

#include "utils.hpp"
#include <codecvt>
#include "ShObjIdl.h"
#include "atlbase.h"

void catch_opengl_errors() {
    GLenum errnum = glGetError();
    if(errnum != GL_NO_ERROR) {
        throw OpenGLErrorException(errnum);
    }
}

void assert_hresult(HRESULT res) {
    if(!SUCCEEDED(res)) {
        throw WindowsErrorException(res);
    }
}

std::optional<std::filesystem::path> open_file_dialog(FileDialogType type) {
    HRESULT hr;

    CComPtr<IFileDialog> dialog;
    CLSID id;
    if(type == FileDialogType::OPEN_FILE) {
        id = CLSID_FileOpenDialog;
    } else if(type == FileDialogType::SAVE_FILE) {
        id = CLSID_FileSaveDialog;
    }
    assert_hresult(dialog.CoCreateInstance(id));

    hr = dialog->Show(NULL);
    if(hr == ERROR_CANCELLED) {
        return {};
    }
    assert_hresult(hr);

    CComPtr<IShellItem> item;
    assert_hresult(dialog->GetResult(&item));

    LPOLESTR wpath;
    assert_hresult(item->GetDisplayName(SIGDN_FILESYSPATH, &wpath));
    std::string path =
        std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(wpath);
    CoTaskMemFree(wpath);

    return path;
}