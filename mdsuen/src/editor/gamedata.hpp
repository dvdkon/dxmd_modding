// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík

#pragma once

#include <string>
#include <filesystem>
#include <functional>
#include "common/dxmd/runtime.resource.hpp"
#include "common/dxmd/system.render.hpp"

// --- Loading DXMD DLLs/modules

void init_dxmd_modules(std::string runtime_path);
void unload_dxmd_modules();

// --- Loading/installing resources via builtin methods

void load_headerlib_with_deps(
        ZLocalResourceFileMediator& lrf_mediator,
        std::map<std::string, ZResourcePtr>& loaded,
        std::filesystem::path path);

std::vector<std::filesystem::path> find_all_headerlibs(
        std::string runtime_path);

// --- Loading raw resource data

struct IDMapEntry {
    ZRuntimeResourceID container_rrid;
    ZRuntimeResourceID resource_rrid;
    ZResourceID rid;
    ZResourceID rid2;
};

// The per-resource information extracted from a headerlib
struct HeaderlibResourceLocator {
    ZResourceID resourcelib_rid;
    size_t offset;
    size_t length;
    std::vector<uint8_t> header_bytes;

    std::vector<char> load_data();
};

// WARN: The resultant pointer must be FREED without calling the destructor!
SHeaderLibrary *load_headerlib_raw(std::filesystem::path path);

std::vector<IDMapEntry> parse_idmap_raw(TArray<uint8_t> &data);

// Returns whether the resource was found
bool headerlib_raw_find_resource(
        SHeaderLibrary *headerlib, ZRuntimeResourceID rrid,
        HeaderlibResourceLocator *out_locator);

// --- Utility methods

std::string tag_to_string(uint32_t tag);

std::vector<ZResourceID> all_known_rids();

std::vector<std::string> split_rid_for_path(ZResourceID rid);

ZRuntimeResourceID get_lib_rrid(ZRuntimeResourceID rrid);

// --- Methods for specific resource types

std::vector<char> texture_as_dds(ZTextureMap &tmap);