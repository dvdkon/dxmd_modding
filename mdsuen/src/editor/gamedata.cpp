// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík

#include "gamedata.hpp"

#include "common/dxmd/basedll.hpp"
#include "common/dxmd/g2base.hpp"
#include "common/dxmd/nxapp.hpp"
#include "common/dxmd/runtime.activation.hpp"
#include "common/dxmd/runtime.animation.hpp"
#include "common/dxmd/runtime.core.hpp"
#include "common/dxmd/runtime.debug.hpp"
#include "common/dxmd/runtime.entity.hpp"
#include "common/dxmd/runtime.gfx.hpp"
#include "common/dxmd/runtime.localization.hpp"
#include "common/dxmd/runtime.physics.hpp"
#include "common/dxmd/runtime.physics.physx.hpp"
#include "common/dxmd/runtime.render.hpp"
#include "common/dxmd/runtime.resource.hpp"
#include "common/dxmd/runtime.sound.hpp"
#include "common/dxmd/runtime.streaming.hpp"
#include "common/dxmd/runtime.facefx.hpp"
#include "common/dxmd/runtime.animationgraph.hpp"
#include "common/dxmd/runtime.navigation.hpp"
#include "common/dxmd/runtime.purehair.hpp"
#include "common/dxmd/runtime.lodsystem.hpp"
#include "common/dxmd/runtime.video.hpp"
#include "common/dxmd/runtime.input.hpp"
#include "common/dxmd/runtime.windowsystem.hpp"
#include "common/dxmd/runtime.platformservices.hpp"
#include "common/dxmd/runtime.events.hpp"
#include "common/dxmd/system.io.hpp"
#include "common/dxmd/system.serializer.hpp"
#include "common/dxmd/resource.core.hpp"
#include "common/dxmd/game.core.hpp"
#include "common/dxmd/game.main.hpp"
#include "common/dxmd/game.sci.hpp"
#include "common/dxmd/game.cover.hpp"
#include "common/dxmd/game.camera.hpp"
#include "common/dxmd_allocator.hpp"
#include <stdexcept>
#include <fstream>
#include <optional>
#include <map>
#include <assert.h>
#include <DDS.h>
#include <pahil/logging.hpp>

using namespace std::literals;

class ConsoleTraceListener : public ITraceListener {
    public:
    // XXX: Don't forget this exists!
    ITraceListener::ETraceLevel level = ITraceListener::ETraceLevel::TRACE_WARNING;
    ITraceFilter *filter;

    virtual ~ConsoleTraceListener() {}
    virtual void Flush() { std::cerr << std::flush; }
    virtual void SetLevel(ITraceListener::ETraceLevel level) { this->level = level; }
    virtual ITraceListener::ETraceLevel GetLevel() { return this->level; }
    virtual void Write(uint32_t nBoardID, const ZString &pszFile, uint32_t nLine,
                       const char *pszCommand, ITraceListener::ETraceLevel eLevel,
                       const ZString &pszChannel, const ZString &pszString, bool bAppendNewline) {
        // TODO: Why is Set/GetLevel there if there's also the ITraceFilter?
        if(eLevel > this->level) {
            return;
        }
        std::string level_str;
        if (eLevel == TRACE_PRINT) {
            level_str = "P";
        } else if (eLevel == TRACE_ERROR) {
            level_str = "E";
        } else if (eLevel == TRACE_WARNING) {
            level_str = "E";
        } else if (eLevel == TRACE_MESSAGE) {
            level_str = "M";
        } else if (eLevel == TRACE_NONE) {
            level_str = "N";
        }
        std::ostringstream oss;
        oss << level_str << " " << pszFile << ":" << nLine << " " << pszChannel << "\t" << pszString
            << (bAppendNewline ? "\n" : "");
        LOG_BARE << oss.str();
    }
    virtual const char *GetStoredLine(uint64_t) {
        return NULL; // TODO
    }
    virtual void SetFilter(ITraceFilter *filter) { this->filter = filter; }
    virtual ITraceFilter *GetFilter() { return this->filter; }

    // TODO: Create some macros for a proper IComponentInterface impl
    virtual ZVariantRef GetVariantRef() { return ZVariantRef(); };
    virtual int64_t AddRef() { return 0; };
    virtual int64_t Release() { return 0; };
    virtual void *QueryInterface(STypeID *) { return this; };
};

class EditorApp : public ZApplicationBase {
    public:
    std::map<std::string, std::pair<ZString, ZString>> options;
    ZIniFile ini_file;

    EditorApp() {
        this->SetAssertHandlerFunction(
            [](const char *location_and_msg, const char *extra) {
                std::string line = location_and_msg;
                // Replace first \n with ":" and delete trailing '\n's
                line.replace(line.find("\n"), 1, ":");
                size_t last_newline_pos;
                while(last_newline_pos = line.rfind("\n"),
                      last_newline_pos = line.length() - 1) {
                    line.replace(last_newline_pos, 1, "");
                }
                if(std::string(extra) != "") {
                    line += " (" + std::string(extra) + ")";
                }
                LOG_BARE << "Assert hit in " << line;
                // Ignore all asserts, since there are some non-fatal ones in
                // this build of DXMD
                return EAssertResult_Ignore;
            });
    }

    virtual void OnBeforeInitialize() {}
    virtual void OnAfterInitialize() {}
    virtual void OnBeforeShutdown() {}
    virtual void OnAfterShutdown() {}
    virtual void OnBeforeModuleInitialize(IModule *) {}
    virtual void OnAfterModuleInitialize(IModule *) {}
    virtual void OnBeforeModuleShutdown(IModule *) {}
    virtual void OnAfterModuleShutdown(IModule *) {}
    virtual void OnModuleNotFound(const ZString &) {}

    virtual void SetOption(const ZString &sOption, const ZString &sValue,
                           const ZString &sValueSource) {
        this->options[sOption] = std::make_pair(sValue, sValueSource);
    }
    virtual const ZString &GetOption(const ZString &name) {
        static ZString empty = "";
        if (this->options.find(name) == this->options.end()) {
            return empty;
        }
        return this->options[name].first;
    }
    virtual bool GetOptionBool(const ZString &name) {
        auto value = this->GetOption(name);
        return value == "true" || value == "1";
    }
    virtual bool GetOptionOrDefaultBool(const ZString &name, bool default_val) {
        if (this->options.find(name) == this->options.end()) {
            return default_val;
        }
        return this->GetOptionBool(name);
    }

    virtual IIniFile *GetIniFile() { return &this->ini_file; }
    virtual void Exit() { exit(0); }
    virtual void StartBenchmark() {}
    virtual void InitVRDemo(bool) {}
    virtual ZString GetBenchmarkScene() { return ""; }
    virtual int32_t GetBenchmarkSpawnPoint() { return 0; }
    virtual ZString GetVRDemoScene() { return ""; }
    virtual int32_t GetVRDemoSpawnPoint() { return 0; }
};

class NxAllocatorIface : public nx::Allocator::GameInterface {
    virtual void *Allocate(uint64_t size) { return GetMemoryManager()->Allocate(size, 0); }
    virtual void Free(void *ptr) { GetMemoryManager()->Free(ptr); }
};

// TODO: Parity with engine.exe's G2Renderer
class G2Renderer : public nx::INxRenderer {
    public:
    virtual bool NxIsFirstRun() { return false; };
    virtual bool IsFeatureSupported(nx::NxRenderFeature f) {
        return f == nx::NxRenderFeature::DX12MultiGPU;
    }
    virtual void NxWindowMessage(HWND__ *, uint32_t, uint64_t, int64_t) {}
    virtual void NxWindowActivated(bool) {}
    virtual nx::INxRenderer::QuitResult NxGetQuitResult() {
        return nx::INxRenderer::QuitResult::Okay;
    }
    virtual void NxAskUserForQuitConfirmation() {}
    virtual void NxSettingsChanged(const nx::DisplaySettings &) {}
    virtual void NxInitialize(const D3DInitArgs &) {}
    virtual void NxSetRenderThreadParams(void *) {}
    virtual void NxDestroy() {}
};

void init_dxmd_modules(std::string runtime_path) {
    auto trace_listener = new ConsoleTraceListener;
    LockGlobalPointerTable();
    SetGlobalPointer("ioi_tracelistener", trace_listener);
    UnlockGlobalPointerTable();

    auto alloc_scope = new ZMemAllocatorScope(0x200000000); // 8GiB

    *g_ThreadIdMainThread = ZThread::GetCurrentThreadID();

    auto nx_alloc = new NxAllocatorIface();
    NxApp_Create(nx_alloc);

    // It seems that the main allocator can only allocate so many blocks, this
    // allocator somehow takes over the allocation of small blocks, so they
    // don't fill up the main allocator
    // Prefering allocator 4 is hardcoded into ZMemoryManager::Allocate
    auto small_alloc = G2NEW(ZSmallBlockAllocator)("main");
    GetMemoryManager()->InstallAllocator(4, small_alloc);

    nx::InitParams nx_params;
    nx_params.m_appInstance = GetModuleHandle(NULL);
    nx_params.m_registryPath = "Software\\Eidos Montreal\\Deus Ex: MD";
    dynamic_cast<nx::NxSteamImpl *>(NxApp_GetSteam())->m_bEnabled = false;
    NxApp_Initialize(&nx_params);

    nx::GameState::GameStateDefinition loading_state("Loading", 50);
    NxApp_GetGameState()->AddState(loading_state);

    auto cm = GetComponentManager();
    cm->Initialize();

    auto app = new EditorApp;
    cm->SetApplication(app);

    app->SetOption("SUSHI", "true", "code");
    app->SetOption("USE_RESOURCELIBS", "true", "code");
    app->SetOption("NO_RESOURCEDB", "true", "code");
    app->SetOption("USE_ARCHIVEFILE", "true", "code");
    app->SetOption("PROJECT_PATH", "", "code");
    app->SetOption("RUNTIME_PATH", runtime_path, "code");

    ZNetUtil::InitializeNetwork();

    ImportZSystemIOModule();
    ImportZSystemJobModule();
    ImportZSystemNetModule();
    ImportZSystemConnectionModule();
    ImportZSerializerModule();
    ImportZRuntimeCoreModule();
    ImportZRuntimeDebugModule();
    ImportZRuntimeResourceModule();
    ImportZRuntimeEntityModule();
    ImportZRuntimeLocalizationModule();

    auto nx_app = NxApp_GetApp();
    G2Renderer nx_renderer;
    nx_app->Init(nx_renderer);

    ImportZRuntimeRenderModule();
    ImportZRuntimeSoundModule();
    ImportZRuntimeGfxModule();
    ImportZRuntimeStreamingModule();
    ImportZRuntimeActivationModule();
    ImportZRuntimePhysicsModule();
    ImportZRuntimePhysicsPhysXModule();
    ImportZRuntimeAnimationModule();
    ImportZRuntimeAnimationGraphModule();
    ImportZRuntimeNavigationModule();
    ImportZRuntimeFaceFXModule();
    ImportZRuntimeLODSystemModule();
    ImportZRuntimePureHairModule();
    ImportZRuntimeVideoModule();
    ImportZRuntimeInputModule();
    ImportZRuntimeWindowSystemModule();
    ImportZRuntimePlatformServicesModule();
    ImportZRuntimeEventsModule();
    ImportZResourceCoreModule();
    ImportZGameCoreModule();
    ImportZGameMainModule();
    ImportZGameSCIModule();
    ImportZGameCoverModule();
    ImportZGameCameraModule();
}

void unload_dxmd_modules() {
    // Can't call this, because NxSteam destructor assumes it was initialised
    //NxApp_Destroy();
    // TODO: Fix all exceptions on exit
}

std::vector<char> read_binary_file(std::string path) {
    std::ifstream instream(path, std::ios::binary);
    instream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    instream.seekg(0, std::ios::end);
    size_t filesize = instream.tellg();
    instream.seekg(0, std::ios::beg);
    std::vector<char> filedata;
    filedata.resize(filesize);
    instream.read(filedata.data(), filesize);
    return filedata;
}

ZRuntimeResourceID get_dummy_rrid() {
    // The APIs require each loaded resource to have a RRID, but we can't know
    // the correct one from just the filename, so just make one up
    static uint32_t dummy_rrid_val = 0;
    dummy_rrid_val += 1;
    LOG << "Creating dummy RRID " << dummy_rrid_val;
    return ZRuntimeResourceID::DO_NOT_USE_ME_Create(dummy_rrid_val);
}

std::vector<ZRuntimeResourceID> get_resource_refs(char *filedata) {
    auto header_header = reinterpret_cast<SResourceHeaderHeader *>(filedata);
    if(header_header->m_nReferencesChunkSize == 0) {
        return {};
    }
    auto refs_chunk_ptr = reinterpret_cast<uint8_t *>(
        filedata + sizeof(SResourceHeaderHeader));
    auto header_reader = ZResourceHeaderReader(*header_header, refs_chunk_ptr);
    std::vector<ZRuntimeResourceID> rrids;
    for(size_t i = 0; i < header_reader.GetNumResourceIdentifiers(); i++) {
        rrids.push_back(header_reader.GetResourceIdentifier(i));
    }
    return rrids;
}

ZResourcePtr load_resource_file(std::vector<char> filedata) {
    auto header_header = reinterpret_cast<SResourceHeaderHeader *>(filedata.data());
    auto refs_chunk = filedata.data() + sizeof(SResourceHeaderHeader);
    auto states_chunk = refs_chunk + header_header->m_nReferencesChunkSize;
    auto hldata_inner = states_chunk + header_header->m_nStatesChunkSize;
    auto hldata_inner_len = filedata.size() - (hldata_inner - filedata.data());

    auto dummy_rrid = get_dummy_rrid();
    ZResourceStub *rstub = G2NEW(ZResourceStub)(
        dummy_rrid, EResourceStubType::NonLibrary, EResourceLoadPriority::High);
    rstub->SetResourceTag(header_header->m_type);
    auto rbuf = ZResourceDataBuffer::CreateFromBorrowedData(hldata_inner, hldata_inner_len);
    ZResourceReader rreader(rstub, rbuf, hldata_inner_len);
    auto rreader_ptr = TSharedPtr<ZResourceReader>::FromLocal(&rreader);
    ZResourcePtr rptr;
    rptr.m_pResourceStub = rstub;
    ZResourcePending rpending(rptr, rreader_ptr, 0);

    auto installer = GetResourceManager()->GetResourceInstaller(header_header->m_type);
    auto install_state = installer->Install(rpending);
    if(install_state != EResourceInstallerState::Done) {
        throw std::runtime_error(
            "Resource installation failed with state"s
            + std::to_string(install_state));
    }

    // m_nRefCount, an int64 is divided into 2 32-bit ints. The upper part is
    // "HEADER_REF" and the lower "DATA_REF"
    // XXX: Do we need this? Is this just covering up another bug?
    rptr.m_pResourceStub->AddRef();

    return rptr;
}

void load_headerlib_with_deps(
    ZLocalResourceFileMediator& lrf_mediator,
    std::map<std::string, ZResourcePtr>& loaded,
    std::filesystem::path path) {
    if(loaded.find(path.filename().string()) != loaded.end()) {
        return;
    }

    LOG << "Loading headerlib: " << path;

    auto filedata = read_binary_file(path.string());
    for(auto ref : get_resource_refs(filedata.data())) {
        auto path_str = lrf_mediator.GetResourceFileName(ref);
        LOG << "Loading dependency: " << path_str;
        std::filesystem::path ref_path(static_cast<std::string>(path_str));
        load_headerlib_with_deps(lrf_mediator, loaded, ref_path);
    }

    auto rptr = load_resource_file(filedata);
    loaded[path.filename().string()] = rptr;
}

std::vector<std::filesystem::path> find_all_headerlibs(
        std::string runtime_path) {
    std::vector<std::filesystem::path> headerlibs;
    for(auto file : std::filesystem::directory_iterator(runtime_path)) {
        auto path = file.path();
        if(path.extension() != ".pc_headerlib") {
            continue;
        }

        headerlibs.push_back(path);
    }
    return headerlibs;
}

std::vector<char> HeaderlibResourceLocator::load_data() {
    ZComponentCreateInfo stream_ci;
    auto rrid = ZRuntimeResourceID::QueryRuntimeResourceID(this->resourcelib_rid);
    stream_ci.AddArgument(GetTypeID<ZRuntimeResourceID>(), &rrid);
    auto type = ZResourceStreamer::EStreamType::DATA;
    stream_ci.AddArgument(GetTypeID<ZResourceStreamer::EStreamType>(), &type);
    ZBlockingResourceInputStream stream(stream_ci);
    assert(stream.IsValid());

    std::vector<char> data(this->length);
    // Seems like ORIGIN_BEGIN is broken, it sets the internal stream position
    // to some large number
    //stream.Seek(this->offset, IInputStream::ORIGIN_BEGIN);
    stream.Seek(this->offset, IInputStream::ORIGIN_CURRENT);
    stream.Read(data.data(), this->length);
    return data;
}

SHeaderLibrary *load_headerlib_raw(std::filesystem::path path) {
    IInputStream *stream = CreateFileInputStream(path.string(), false, false, true);

    SResourceHeaderHeader header_header;
    stream->Read(&header_header, sizeof(SResourceHeaderHeader));
    stream->Skip(header_header.m_nReferencesChunkSize + header_header.m_nStatesChunkSize);

    ZBinaryDeserializer *deserializer = CreateBinaryDeserializer();

    deserializer->SetInputStream(stream);
    size_t len = deserializer->BytesNeededForDeserialization();
    byte *out_buf = reinterpret_cast<byte *>(malloc(len));
    deserializer->DeserializeInto(out_buf);

    G2DELETE(deserializer);
    G2DELETE(stream);

    return reinterpret_cast<SHeaderLibrary *>(out_buf);
}

std::vector<IDMapEntry> parse_idmap_raw(TArray<uint8_t> &data) {
    uint32_t len = *reinterpret_cast<uint32_t *>(data.GetPointer());

    std::vector<IDMapEntry> entries;
    byte *base = data.GetPointer() + 4;
    for(size_t i = 0; i < len; i++) {
        auto container_rrid =
            ZRuntimeResourceID::Create(*reinterpret_cast<uint64_t *>(base));
        base += 8;
        auto resource_rrid =
            ZRuntimeResourceID::Create(*reinterpret_cast<uint64_t *>(base));
        base += 8;

        auto rid_len = *reinterpret_cast<uint32_t *>(base) + 1; // +1 for \0
        base += 4;
        ZResourceID rid(reinterpret_cast<char *>(base));
        base += rid_len;

        auto rid2_len = *reinterpret_cast<uint32_t *>(base) + 1; // +1 for \0
        base += 4;
        ZResourceID rid2(reinterpret_cast<char *>(base));
        base += rid2_len;

        entries.push_back({container_rrid, resource_rrid, rid, rid2});
    }

    return entries;
}

bool headerlib_raw_find_resource(
        SHeaderLibrary *headerlib, ZRuntimeResourceID rrid,
        HeaderlibResourceLocator *out_locator) {
    auto idmap = parse_idmap_raw(headerlib->idmap);

    ZRuntimeResourceID container_rrid;
    bool found = false;
    for(auto &entry : idmap) {
        auto entry_rrid = ZRuntimeResourceID::QueryRuntimeResourceID(entry.rid);
        if(entry_rrid == rrid) {
            container_rrid = get_lib_rrid(entry.container_rrid);
            found = true;
            break;
        }
    }
    if(!found) {
        return false;
    }

    SHeaderLibraryChunk *containing_chunk = NULL;
    for(auto &chunk : headerlib->chunks) {
        auto chunk_rrid = ZRuntimeResourceID::QueryRuntimeResourceID(chunk.sLibraryID);
        if(chunk_rrid == container_rrid) {
            containing_chunk = &chunk;
        }
    }
    assert(containing_chunk != NULL);
    out_locator->resourcelib_rid = containing_chunk->sLibraryID;

    size_t res_id_in_chunk = 0;
    // Header seems skipped by ZBlockingResourceInputStream
    out_locator->offset = containing_chunk->nOffset /*+ containing_chunk->chunkHeader.Size()*/;
    for(auto &entry : idmap) {
        if(entry.resource_rrid == rrid) {
            break;
        }
        if((entry.container_rrid.m_ID & 0xFFFFFFFF) == (container_rrid.m_ID & 0xFFFFFFFF)) {
            assert(res_id_in_chunk < containing_chunk->resourceHeaders.Size());
            auto res_header_bytes = containing_chunk->resourceHeaders[res_id_in_chunk];
            auto res_header = reinterpret_cast<SResourceHeaderHeader *>(res_header_bytes.GetPointer());
            out_locator->offset += res_header->m_nDataSize;
            if((containing_chunk->nFlags & (1 << 2)) > 0) {
                out_locator->offset += 24;
            }
            res_id_in_chunk++;
        }
    }

    assert(res_id_in_chunk < containing_chunk->resourceHeaders.Size());
    auto res_header_bytes = containing_chunk->resourceHeaders[res_id_in_chunk];
    out_locator->header_bytes = std::vector<uint8_t>(
        res_header_bytes.GetPointer(),
        res_header_bytes.GetPointer() + res_header_bytes.Size());
    auto res_header = reinterpret_cast<SResourceHeaderHeader *>(res_header_bytes.GetPointer());
    out_locator->length = res_header->m_nDataSize;
    if((containing_chunk->nFlags & (1 << 2)) > 0) {
        out_locator->length += 24;
    }

    return true;
}

std::string tag_to_string(uint32_t tag) {
    auto tag_arr = reinterpret_cast<char *>(&tag);
    return {tag_arr[3], tag_arr[2], tag_arr[1], tag_arr[0]};
}

std::vector<ZResourceID> all_known_rids() {
    std::vector<ZResourceID> rids;
    // TODO: Find a way to list all resources, even those with only RRIDs
    auto &res_map =
        (*_anonymous_namespace___g_LocalResolver)
        ->m_RuntimeResourceIDsToResourceIDs;
    for(auto it = res_map.Begin(); it != res_map.End(); ++it) {
        auto fst = it->first;
        auto snd = it->second;
        if(it->second.HasDifferentPrettyResourceID()) {
            LOG << "Different pretty RID? "
                << it->second.GetPrettyResourceID().ToString();
        }
        rids.push_back(it->second.GetResourceID());
    }
    return rids;
}

std::vector<std::string> split_rid_for_path_inner(
        ZResourceID rid,
        std::vector<std::string>& components) {
    if(rid.IsDerived()) {
        split_rid_for_path_inner(rid.GetSourceResourceID(), components);
        // TODO: Do these parameters only appear on derived RIDs?
        for(size_t i = 0; i < rid.GetParameterCount(); i++) {
            components.push_back(
                "("s + static_cast<std::string>(rid.GetParameter(i)) + ")"s);
        }
        components.push_back("."s + static_cast<std::string>(rid.GetExtension()));
    } else {
        components.push_back(static_cast<std::string>(rid.GetProtocolName()) + ":"s);
        auto path_split = rid.GetRootPath().Split('/');
        for(auto &comp : path_split) {
            if(!comp.IsEmpty()) {
                components.push_back(comp);
            }
        }
        std::string root_param = rid.GetRootParameters();
        if(root_param != "") {
            root_param[0] = '?'; // Replace "/" with "?"
            components.push_back(root_param);
        }
    }

    return components;
}

std::vector<std::string> split_rid_for_path(ZResourceID rid) {
    std::vector<std::string> components;
    split_rid_for_path_inner(rid, components);
    for(auto &c : components) {
        std::transform(c.begin(), c.end(), c.begin(),
                       [](auto c1) { return std::tolower(c1); });
    }
    return components;
}

ZRuntimeResourceID get_lib_rrid(ZRuntimeResourceID rrid) {
    if((rrid.m_ID | 0x8000000000000000) == 0) {
        throw std::invalid_argument("RRID is not container-index RRID");
    }
    return ZRuntimeResourceID::Create((rrid.m_ID & 0xFFFFFFFF) | 0x4000000000000000);
}

std::vector<char> texture_as_dds(ZTextureMap &tmap) {
    using namespace DirectX;

    std::vector<char> out;
    out.resize(sizeof(DDS_MAGIC) + sizeof(DDS_HEADER));
    *reinterpret_cast<uint32_t *>(out.data()) = DDS_MAGIC;

    DDS_HEADER header;
    header.size = sizeof(DDS_HEADER);
    header.flags = 1 | 2 | 4 | 8 | 0x1000 | 0x20000 | 0x80000 | 0x800000;
    header.height = tmap.GetHeight();
    header.width = tmap.GetWidth();
    header.pitchOrLinearSize = 0; // TODO
    header.depth = tmap.GetDepth();
    header.mipMapCount = tmap.GetNumMipLevels();
    header.caps = 8 | 0x1000 | 0x400000;
    header.caps2 = 0;
    if(tmap.GetDimensions() == ZTextureMap::DIMENSIONS_VOLUME) {
        header.caps2 |= DDS_FLAGS_VOLUME;
    }
    // TODO: Cubemaps in caps2

    DDS_HEADER_DXT10 dxt10;
    dxt10.miscFlag = 0;
    dxt10.miscFlags2 = 0;
    dxt10.arraySize = 1;
    if(tmap.GetDimensions() == ZTextureMap::DIMENSIONS_2D) {
        dxt10.resourceDimension = DDS_DIMENSION_TEXTURE2D;
    } else if(tmap.GetDimensions() == ZTextureMap::DIMENSIONS_CUBE) {
        dxt10.resourceDimension = DDS_DIMENSION_TEXTURE3D;
    } else if(tmap.GetDimensions() == ZTextureMap::DIMENSIONS_VOLUME) {
        dxt10.resourceDimension = DDS_DIMENSION_TEXTURE3D; // TODO
    }

    header.ddspf.size = sizeof(DDS_PIXELFORMAT);
    if(tmap.GetFormat() == RENDER_FORMAT_R8G8B8A8_UNORM) {
        header.ddspf.flags = DDS_RGBA;
        header.ddspf.RGBBitCount = 32;
        header.ddspf.RBitMask = 0xFF000000;
        header.ddspf.GBitMask = 0x00FF0000;
        header.ddspf.BBitMask = 0x0000FF00;
        header.ddspf.ABitMask = 0x000000FF;
    } else if(tmap.GetFormat() == RENDER_FORMAT_R16G16_FLOAT) {
        header.ddspf.flags = DDS_RGB;
        header.ddspf.RGBBitCount = 32;
        header.ddspf.RBitMask = 0xFFFF0000;
        header.ddspf.GBitMask = 0x0000FFFF;
        header.ddspf.BBitMask = 0x00000000;
        header.ddspf.ABitMask = 0x00000000;
    } else if(tmap.GetFormat() == RENDER_FORMAT_BC1_UNORM) {
        header.ddspf = DDSPF_DXT1;
    } else if(tmap.GetFormat() == RENDER_FORMAT_BC2_UNORM) {
        header.ddspf = DDSPF_DXT3;
    } else if(tmap.GetFormat() == RENDER_FORMAT_BC3_UNORM) {
        header.ddspf = DDSPF_DXT5;
    } else if(tmap.GetFormat() == RENDER_FORMAT_BC4_UNORM) {
        header.ddspf = DDSPF_DX10;
        dxt10.dxgiFormat = DXGI_FORMAT_BC4_UNORM;
    } else if(tmap.GetFormat() == RENDER_FORMAT_BC5_UNORM) {
        header.ddspf = DDSPF_DX10;
        dxt10.dxgiFormat = DXGI_FORMAT_BC5_UNORM;
    } else if(tmap.GetFormat() == RENDER_FORMAT_BC6H_UF16) {
        header.ddspf = DDSPF_DX10;
        dxt10.dxgiFormat = DXGI_FORMAT_BC6H_UF16;
    } else if(tmap.GetFormat() == RENDER_FORMAT_BC7_UNORM) {
        header.ddspf = DDSPF_DX10;
        dxt10.dxgiFormat = DXGI_FORMAT_BC7_UNORM;
    }

    *reinterpret_cast<DDS_HEADER *>(out.data() + 4) = header;

    if(header.ddspf.fourCC == MAKEFOURCC('D','X','1','0')) {
        size_t dxt10_offset = out.size();
        out.resize(out.size() + sizeof(DDS_HEADER_DXT10));
        *reinterpret_cast<DDS_HEADER_DXT10 *>(out.data() + dxt10_offset) = dxt10;
    }

    for(size_t i = 0; i < tmap.GetNumMipLevels(); i++) {
        ZTextureMap::SMipLevel mip;
        tmap.GetMipLevel(&mip, 0);

        size_t data_offset = out.size();
        out.resize(out.size() + mip.nSizeInBytes);
        memcpy(out.data() + data_offset, mip.pData, mip.nSizeInBytes);
    }

    return out;
}