// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "g2base.hpp"
#include "system.net.hpp"

class ZMessageBuffer {
    public:
    ZMessageBuffer(uint64_t initialCapacity);
    ~ZMessageBuffer();
    void Resize(uint64_t size);
    char *Get();
    uint64_t Size();
    IInputStream *GetInputStream();
    void Reserve(uint64_t capacity);
    char *m_pData;
    uint64_t m_nCapacity;
    uint64_t m_nSize;
};

class ZConnection : public IComponentInterface {
    public:
    ZRefCount m_refcount;
    virtual int64_t Release();
    virtual int64_t AddRef();
    int64_t GetRefCount();
    ZConnection(ZSocket *pSocket, ZThreadEvent *pDataReceivedEvent);
    ZConnection(const ZConnection &);
    ZConnection &operator=(const ZConnection &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZConnection();
    void Start();
    bool IsConnected();
    bool IsListening();
    bool Connect(const ZString &sAddr, uint16_t nPort);
    void Disconnect();
    bool StartListening(uint16_t nPort);
    void Post(const ZBuffer &message);
    void Post(const ZString &sMessage);
    uint16_t GetPort();
    uint32_t GetAddress();
    bool PopMessage();
    const ZMessageBuffer &GetLastPoppedMessage();
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetDisconnectedEvent();
    ZMutex m_connectionMutex;
    ZMutex m_postMutex;
    TComponentSharedPtr<ZSocket> m_pSocket;
    ZMessageBuffer m_messageBuffer;
    uint64_t m_nMessageSize;
    uint64_t m_nRead;
    int32_t m_nReadState;
    bool m_bStarted;
    bool PollSocket(char *pBuffer, uint64_t nTotalSize, bool aReadOnlyTotalSize);
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull>
        m_DisconnectedEvent;
    bool m_bDisconnectedEventSent;
    void SendDisconnectEventOnce();
};

class IRemoteCallManager : public IComponentInterface {
    public:
    virtual void RegisterIncomingRemoteSite(ZConnection *, const ZString &);
    virtual void RegisterOutgoingRemoteSite(ZConnection *, const ZString &);
    virtual void UnregisterRemoteSite(ZConnection *);
    virtual void UnregisterRemoteSite(const ZString &);
    virtual bool RemoteSiteExists(const ZString &);
    virtual void RegisterFunction(const ZString &, const ZDelegateBase &, bool);
    virtual void UnregisterFunction(const ZString &);
    virtual bool FunctionExists(const ZString &);
    virtual bool RemoteCall(const ZString &, const ZString &, ZVariant &, TArray<ZVariant> *);
    virtual void BeginRemoteCall(const ZString &, const ZString &,
                                 const ZDelegate<void __cdecl(ZVariant &, bool)> &,
                                 TArray<ZVariant> *, bool);
    virtual void ProcessQueuedThreadCalls();
    virtual void ProcessQueuedThreadCalls(const ZDelegate<bool __cdecl(void)> &);
    virtual ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetRemoteCallReceivedEvent();
    virtual ZEvent<unsigned __int64, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetProcessingRemoteCallsEvent();
    virtual ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetRemoteCallsProcessedEvent();
    void CallNoReturn(const ZString &, const ZString &);
    void BeginCall(const ZString &, const ZString &,
                   const ZDelegate<void __cdecl(ZVariant &, bool)> &);
};

class ZConnectionManager : public IComponentInterface {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZConnectionManager();
    ZConnectionManager(const ZConnectionManager &);
    ZConnectionManager &operator=(const ZConnectionManager &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZConnectionManager();
    ZConnection *CreateConnection(const ZString &sAddr, int32_t nPort);
    ZConnection *ListenForConnection(uint16_t nPort);
    static ZConnectionManager *Instance();
    void UseBlockingThread();
    bool IsConnectionManagerThread();
    bool ProcessConnections();
    ZThread m_workerThread;
    uint32_t UpdateThreadProc(void *pData);
    bool m_bRunning;
    ZMutex m_managerMutex;
    TArray<ZConnection *> m_connections;
    ZThreadEvent m_dataReceivedEvent;
};

void ImportZSystemConnectionModule();