// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "g2base.hpp"
#include "external.hpp"

class ZSocket: public IComponentInterface {
	public:
	enum EStatus {
		STATUS_IDLE,
		STATUS_CONNECTED,
		STATUS_LISTENING,
	};
	ZRefCount m_refcount;
	virtual int64_t Release(); // addr=0x7dc0
	virtual int64_t AddRef(); // addr=0x5690
	int64_t GetRefCount(); // addr=0x7200
	ZSocket(uint64_t socket, ZSocket::EStatus eStatus); // addr=0x45f0
	ZSocket(); // addr=0x4830
	ZSocket(const ZSocket &);
	ZSocket &operator=(const ZSocket &);
	virtual ZVariantRef GetVariantRef(); // addr=0x7220
	virtual void *QueryInterface(STypeID *iid); // addr=0x7ab0
	const SComponentMapEntry *GetComponentInterfacesInternal(); // addr=0x6ec0
	virtual ~ZSocket(); // addr=0x4d70
	bool Connect(const ZString &sAddr, uint16_t nPort); // addr=0x59f0
	bool Listen(uint16_t nPort); // addr=0x7890
	ZSocket *Accept(); // addr=0x5510
	bool Close(); // addr=0x57f0
	bool IsListening(); // addr=0x7850
	bool IsConnected(); // addr=0x77e0
	uint16_t GetPort(); // addr=0x7140
	uint32_t GetAddress(); // addr=0x6dc0
	void SetDataReceivedEvent(ZThreadEvent *pDataReceivedEvent); // addr=0x7f00
	bool Send(const void *pBuffer, uint64_t nSize); // addr=0x7e50
	uint64_t Receive(void *pBuffer, uint64_t nSize); // addr=0x7ba0
	uint64_t ReceiveAll(void *pBuffer, uint64_t nSize); // addr=0x7cc0
	uint32_t Peek(); // addr=0x7aa0
	void *GetUserData(); // addr=0x7210
	void SetUserData(void *pUserData); // addr=0x7f10
	uint32_t SocketThreadSend(void *__formal); // addr=0x85d0
	uint32_t SocketThreadReceive(void *__formal); // addr=0x7fc0
	void CloseFromThread(); // addr=0x59a0
	struct SocketSendMessage {
		char *mpBuff;
		uint32_t m_iLength;
	};
	boost::lockfree::queue<ZSocket::SocketSendMessage> m_SendQueue;
	ZThreadEvent m_SendEvent;
	uint8_t *m_RecvArray;
	uint32_t m_RecvArrayStart;
	uint32_t m_RecvArrayReceivedSize;
	uint32_t m_RecvArraySize;
	ZMutex m_RecvArrayMutex;
	ZThread m_ThreadSend;
	ZThread m_ThreadReceive;
	ZThreadEvent *m_pDataReceivedEvent;
	bool m_ThreadRunning;
	uint64_t m_socket;
	void *m_pUserData;
	ZSocket::EStatus m_eStatus;
};

void ImportZSystemNetModule();