// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "g2base.hpp"
#include <stdio.h>
#include <utility>

class ZFileInputStream : public IInputStream {
    public:
    ZRefCount m_refcount;
    virtual int64_t Release();
    virtual int64_t AddRef();
    int64_t GetRefCount();
    ZFileInputStream(ZComponentCreateInfo &Info);
    ZFileInputStream(const ZFileInputStream &);
    ZFileInputStream &operator=(const ZFileInputStream &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZFileInputStream();
    virtual bool IsValid();
    virtual bool IsSeekable();
    virtual uint64_t GetPosition();
    virtual uint64_t GetLength();
    virtual uint64_t Read(void *pBuffer, uint64_t nCount);
    virtual uint64_t Seek(int64_t offset, IBaseInputStream::ESeekOrigin origin);
    virtual uint64_t Skip(uint64_t nCount);
    virtual void Close();
    virtual ZString GetLastErrorMessage();
    _iobuf *m_pFile;
    uint64_t m_nLength;
    ZString m_sLastErrorMessage;
    ZString m_sPath;
};

class ZArchiveRoot {
    public:
    class ZArchivedFile {
        public:
        struct ArchiveChunk {
            uint64_t m_SourceBegin;
            uint64_t m_SourceEnd;
            uint64_t m_offset;
            uint64_t m_size;
        };
        ZArchivedFile(
            const std::pair<IInputStream *, ZArchiveRoot::ZArchivedFile::ArchiveChunk> *pChunks,
            uint32_t numChunks, uint64_t timeStamp, const ZString &name);
        virtual ~ZArchivedFile();
        const ZString &GetFileName() const;
        uint64_t GetTotalSize();
        uint64_t GetFileStamp();
        uint64_t Read(void *pDest, uint64_t count, uint64_t offset);
        ZString m_fileName;
        uint64_t m_timeStamp;
        TArray<std::pair<IInputStream *, ZArchiveRoot::ZArchivedFile::ArchiveChunk>> m_Chunks;
        uint64_t m_totalSize;
    };
    const TArray<TSharedPtr<ZArchiveRoot::ZArchivedFile>> &GetArchivedFiles();
    ZArchiveRoot(const ZString &path, IInputStream *pStream);
    TSharedPtr<ZArchiveRoot::ZArchivedFile> FindArchivedFile(const ZString &path);
    void ReadHeader(IInputStream *pStream);
    const ZString &GetFileName() const;
    IInputStream *GetFileStream();
    int32_t m_versionId;
    IInputStream *m_pStream;
    TArray<TSharedPtr<ZArchiveRoot::ZArchivedFile>> m_ArchivedFiles;
    TArray<TSharedPtr<ZArchiveRoot>> m_ArchiveLinks;
    ZMutex m_readLock;
    ZString m_FileName;
};

class ZArchiveFileManager : public IComponentInterface {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZArchiveFileManager();
    ZArchiveFileManager(const ZArchiveFileManager &);
    ZArchiveFileManager &operator=(const ZArchiveFileManager &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZArchiveFileManager();
    void Initialize();
    void Cleanup();
    void LoadArchives(const ZString &path);
    TSharedPtr<ZArchiveRoot> LoadArchive(const ZString &name);
    bool IsArchiveLoaded(const ZString &name);
    TSharedPtr<ZArchiveRoot::ZArchivedFile> GetArchiveFile(const ZString &path);
    TListIterator<TSharedPtr<ZArchiveRoot> const> FindArchivedFile(const ZString &name);
    ZMutex m_readMutex;
    ZString m_RuntimePath;
    TList<TSharedPtr<ZArchiveRoot>> m_archives;
};

class ITextWriter: public IComponentInterface {
	public:
	virtual IOutputStream *GetOutputStream();
	virtual bool WriteChars(char *, int32_t);
	virtual bool Write(const ZString &);
	virtual bool WriteLine(const ZString &);
	virtual bool WriteFormatted(const char *, ...);
};

void ImportZSystemIOModule();
ZArchiveFileManager *GetArchiveFileManager();
IInputStream *CreateFileInputStream(const ZString &sFilePath, bool bHintSequential,
                                    bool bHintHighThroughput, bool bUseResourceDeviceFileCache);