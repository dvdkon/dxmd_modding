// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "g2base.hpp"
#include "runtime.core.hpp"
#include "runtime.render.hpp"
#include "runtime.resource.hpp"

// Forward-declaration for codependent runtime.entity
class ZEntityImpl;
class ZSpatialEntity;

class ZDebugRender;

struct SDebugRenderViewport {
    uint32_t x;
    uint32_t y;
    uint32_t w;
    uint32_t h;
};

struct SDebugVertex {
    enum Offsets {
        OFFSET_POSITION,
        OFFSET_TEXCOORD0,
        OFFSET_COLOR,
        OFFSET_OBJECTID,
        OFFSET_NORMAL,
        OFFSET_TANGENT,
        OFFSET_BINORMAL,
    };
    enum Formats {
        FORMAT_POSITION,
        FORMAT_NORMAL,
        FORMAT_TANGENT,
        FORMAT_BINORMAL,
        FORMAT_TEXCOORD,
        FORMAT_COLOR,
    };
    float4 position;
    float4 textureCoord;
    uint32_t color;
    uint32_t nObjectID;
    uint32_t n;
    uint32_t T;
    uint32_t B;
    SDebugVertex(float, float, float, float, float, uint32_t);
    SDebugVertex(float, float, float, const float4, uint32_t);
    SDebugVertex(const float4, float, float, uint32_t);
    SDebugVertex(const float4, const float4, uint32_t);
    SDebugVertex();
};

struct SDebugTextBox {
    enum EAnchorTypeHorizontal {
        DEBUGTEXTANCHORH_LEFT,
        DEBUGTEXTANCHORH_CENTER,
        DEBUGTEXTANCHORH_RIGHT,
    };
    enum EAnchorTypeVertical {
        DEBUGTEXTANCHORV_TOP,
        DEBUGTEXTANCHORV_CENTER,
        DEBUGTEXTANCHORV_BOTTOM,
    };
    SMatrix m_mWorldTransform;
    float m_fRectHeight;
    SDebugTextBox::EAnchorTypeHorizontal m_eHorizontalAnchorType;
    SDebugTextBox::EAnchorTypeVertical m_eVerticalAnchorType;
    uint32_t m_nColor;
    float m_fFontSize;
};

class IDebugRenderPrimitive {
    public:
    IDebugRenderPrimitive();
    virtual ~IDebugRenderPrimitive();
    virtual bool Merge(const SDebugVertex *, const SMatrix44 *, uint64_t, uint32_t);
    virtual void Clear();
    virtual TArray<SDebugVertex> &GetVertices();
    virtual const TArray<SDebugVertex> &GetVertices() const;
    virtual void SetOwner(ZEntityImpl *);
    virtual ZEntityImpl *GetOwner();
};

class ZDebugRenderPrimitive {
    public:
    ZDebugRenderPrimitive(IDebugRenderPrimitive *pDebugRenderLineEntity,
                          IDebugRenderPrimitive *pDebugRenderTriangleEntity);
    void AddLines(const SDebugVertex *, const SMatrix44 *, uint64_t, uint32_t);
    void AddTriangles(const SDebugVertex *, const SMatrix44 *, uint64_t, uint32_t);
    void ClearLines();
    void ClearTriangles();
    void Clear();
    IDebugRenderPrimitive *GetLineEntity();
    IDebugRenderPrimitive *GetTriangleEntity();
    void SetOwner(ZEntityImpl *);
    IDebugRenderPrimitive *m_pDebugRenderLines;
    IDebugRenderPrimitive *m_pDebugRenderTriangles;
};

class IRenderDebugRender {
    public:
    enum {
        DEBUG_FONT_SIZE_X,
        DEBUG_FONT_SIZE_Y,
    };
    enum TEXTURETYPE {
        TEXTURE_NONE,
        TEXTURE_FONT,
        TEXTURE_MOUSE,
        TEXTURE_YELLOWWARNING,
        TEXTURE_REDWARNING,
        TEXTURE_CRYING,
        TEXTURE_LAST,
    };
    enum PRIMTYPE {
        PT_LINES,
        PT_TRIANGLES,
        MAX_NUM_PRIMTYPES,
    };
    enum SHADERMODE {
        SHADER_STANDARD,
        SHADER_DISABLE_VERTEXCOLOR,
    };
    enum {
        MAX_NUM_VERTICES,
    };
    virtual ~IRenderDebugRender();
    virtual void Begin(const SDebugRenderViewport &, const SMatrix43 &, const SMatrix &);
    virtual void End();
    virtual void Flush(IRenderDebugRender *);
    virtual void DrawLines(const SDebugVertex *, const SMatrix44 *, uint64_t, uint32_t, bool,
                           uint64_t);
    virtual void DrawTriangles(const SDebugVertex *, const SMatrix44 *, uint64_t, uint32_t, bool,
                               uint64_t);
    virtual void DrawPrim(ZDebugRender *, const ZSpatialEntity *);
    virtual void SetDrawMode(uint32_t);
    virtual void SetTexture(uint32_t);
    virtual void SetTexture(void *);
    virtual void SetShaderMode(IRenderDebugRender::SHADERMODE);
    virtual void AddDebugImage(float, float, float, float,
                               const TResourcePtr<IRenderTextureResource> &);
    virtual void DrawTextBox(const SDebugTextBox &, const char *, uint64_t);
    virtual bool IsInFrustum(const float4, float);
    virtual void Update();
    virtual void Clear();
    virtual void ToggleVisible();
    virtual IDebugRenderPrimitive *CreateDebugTrianglePrimitive(bool, uint64_t);
    virtual IDebugRenderPrimitive *CreateDebugLinePrimitive(bool, uint64_t);
    virtual ZDebugRenderPrimitive *CreateDebugPrimitive(bool, uint64_t);
    virtual void DestroyDebugTriangleEntity(IDebugRenderPrimitive *);
    virtual void DestroyDebugLineEntity(IDebugRenderPrimitive *);
    virtual void DestroyDebugPrimitive(ZDebugRenderPrimitive *);
};

struct SDebugPrimitiveDescriptor {
    SDebugPrimitiveDescriptor();
    ZGameTime m_tTimeToLive;
    bool m_bZBufferEnabled;
    uint64_t m_materialRRID;
};

struct SSpatialDebugPrimitive : public SDebugPrimitiveDescriptor {
    SSpatialDebugPrimitive();
    float4 m_vPosition;
    uint32_t m_nColor;
};

struct SScalableDebugPrimitive : public SSpatialDebugPrimitive {
    SScalableDebugPrimitive();
    float4 m_vScale;
    bool m_bSolid;
};

struct SComplexDebugPrimitive : public SScalableDebugPrimitive {
    SComplexDebugPrimitive();
    SMatrix m_mRotation;
};

struct SDebugCapsuleDescriptor : public SSpatialDebugPrimitive {
    SDebugCapsuleDescriptor(const float4);
    SDebugCapsuleDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugCapsuleDescriptor &SetZBufferEnabled(bool);
    SDebugCapsuleDescriptor &SetMaterial(uint64_t);
    SDebugCapsuleDescriptor &SetPosition(const float4);
    SDebugCapsuleDescriptor &SetColor(uint32_t);
    SDebugCapsuleDescriptor &SetSolid(bool);
    SDebugCapsuleDescriptor &SetRotation(const SMatrix &);
    SDebugCapsuleDescriptor &SetRadius(float);
    SDebugCapsuleDescriptor &SetHalfHeight(float);
    SMatrix m_mRotation;
    float m_fRadius;
    float m_fHalfHeight;
    bool m_bSolid;
};

struct SDebugText3DDescriptor : public SSpatialDebugPrimitive {
    SDebugText3DDescriptor(const float4, const float4, const ZString &);
    SDebugText3DDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugText3DDescriptor &SetZBufferEnabled(bool);
    SDebugText3DDescriptor &SetMaterial(uint64_t);
    SDebugText3DDescriptor &SetCenter(const float4);
    SDebugText3DDescriptor &SetColor(uint32_t);
    SDebugText3DDescriptor &SetText(const ZString &);
    SDebugText3DDescriptor &SetNormal(const float4);
    SDebugText3DDescriptor &SetFontSize(float);
    ZString m_sText;
    float4 m_vNormal;
    float m_fFontSize;
};

struct SDebugPyramidDescriptor : public SComplexDebugPrimitive {
    SDebugPyramidDescriptor(const float4);
    SDebugPyramidDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugPyramidDescriptor &SetZBufferEnabled(bool);
    SDebugPyramidDescriptor &SetMaterial(uint64_t);
    SDebugPyramidDescriptor &SetPosition(const float4);
    SDebugPyramidDescriptor &SetColor(uint32_t);
    SDebugPyramidDescriptor &SetSolid(bool);
    SDebugPyramidDescriptor &SetRotation(const SMatrix &);
    SDebugPyramidDescriptor &SetScale(const float4);
};

struct SDebugPointDescriptor : public SSpatialDebugPrimitive {
    SDebugPointDescriptor(const float4);
    SDebugPointDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugPointDescriptor &SetZBufferEnabled(bool);
    SDebugPointDescriptor &SetMaterial(uint64_t);
    SDebugPointDescriptor &SetPosition(const float4);
    SDebugPointDescriptor &SetColor(uint32_t);
};

struct SDebugSphereDescriptor : public SScalableDebugPrimitive {
    SDebugSphereDescriptor(const float4);
    SDebugSphereDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugSphereDescriptor &SetZBufferEnabled(bool);
    SDebugSphereDescriptor &SetMaterial(uint64_t);
    SDebugSphereDescriptor &SetPosition(const float4);
    SDebugSphereDescriptor &SetColor(uint32_t);
    SDebugSphereDescriptor &SetSolid(bool);
    SDebugSphereDescriptor &SetScale(const float4);
};

struct SDebugMatrixDescriptor : public SSpatialDebugPrimitive {
    SDebugMatrixDescriptor(const SMatrix &);
    SDebugMatrixDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugMatrixDescriptor &SetZBufferEnabled(bool);
    SDebugMatrixDescriptor &SetMaterial(uint64_t);
    SDebugMatrixDescriptor &SetPosition(const float4);
    SDebugMatrixDescriptor &SetColor(uint32_t);
    SDebugMatrixDescriptor &SetMatrix(const SMatrix &);
    SDebugMatrixDescriptor &SetScale(float);
    SDebugMatrixDescriptor &SetHighlightedAxis(uint32_t);
    SDebugMatrixDescriptor &SetBright(bool);
    SMatrix m_Matrix;
    float m_fScale;
    uint32_t m_nAxisMask;
    bool m_bBright;
};

struct SDebugCylinderDescriptor : public SSpatialDebugPrimitive {
    SDebugCylinderDescriptor(const float4);
    SDebugCylinderDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugCylinderDescriptor &SetZBufferEnabled(bool);
    SDebugCylinderDescriptor &SetMaterial(uint64_t);
    SDebugCylinderDescriptor &SetPosition(const float4);
    SDebugCylinderDescriptor &SetColor(uint32_t);
    SDebugCylinderDescriptor &SetHeight(float);
    SDebugCylinderDescriptor &SetRadius(float);
    SDebugCylinderDescriptor &SetSolid(bool);
    SDebugCylinderDescriptor &SetRotation(const SMatrix &);
    SMatrix m_mRotation;
    float m_fHeight;
    float m_fRadius;
    bool m_bSolid;
};

struct SDebugBoxDescriptor : public SComplexDebugPrimitive {
    SDebugBoxDescriptor(const float4);
    SDebugBoxDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugBoxDescriptor &SetZBufferEnabled(bool);
    SDebugBoxDescriptor &SetMaterial(uint64_t);
    SDebugBoxDescriptor &SetPosition(const float4);
    SDebugBoxDescriptor &SetColor(uint32_t);
    SDebugBoxDescriptor &SetSolid(bool);
    SDebugBoxDescriptor &SetRotation(const SMatrix &);
    SDebugBoxDescriptor &SetScale(const float4);
};

struct SDebugTriangleDescriptor : public SSpatialDebugPrimitive {
    SDebugTriangleDescriptor(const float4, const float4, const float4);
    SDebugTriangleDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugTriangleDescriptor &SetZBufferEnabled(bool);
    SDebugTriangleDescriptor &SetMaterial(uint64_t);
    SDebugTriangleDescriptor &SetVertices(const float4, const float4, const float4);
    SDebugTriangleDescriptor &SetColor(uint32_t);
    SDebugTriangleDescriptor &SetSolid(bool);
    float4 m_vPosition2;
    float4 m_vPosition3;
    bool m_bSolid;
};

struct SDebugArrowDescriptor : public SSpatialDebugPrimitive {
    SDebugArrowDescriptor(const float4, const float4, bool);
    SDebugArrowDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugArrowDescriptor &SetZBufferEnabled(bool);
    SDebugArrowDescriptor &SetMaterial(uint64_t);
    SDebugArrowDescriptor &SetStartPos(const float4);
    SDebugArrowDescriptor &SetEndPos(const float4);
    SDebugArrowDescriptor &SetDirection(const float4, const float4);
    SDebugArrowDescriptor &SetColor(uint32_t);
    SDebugArrowDescriptor &SetNormal(const float4);
    float4 m_vEndPosition;
    float4 m_vNormal;
    float m_fThickness;
    float m_fArrowHeadLength;
    bool m_bDoubleArrow;
};

struct SDebugConeDescriptor : public SComplexDebugPrimitive {
    SDebugConeDescriptor(const float4);
    SDebugConeDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugConeDescriptor &SetZBufferEnabled(bool);
    SDebugConeDescriptor &SetMaterial(uint64_t);
    SDebugConeDescriptor &SetCenter(const float4);
    SDebugConeDescriptor &SetColor(uint32_t);
    SDebugConeDescriptor &SetRotation(const SMatrix &);
    SDebugConeDescriptor &SetScale(const float4);
    SDebugConeDescriptor &SetSolid(bool);
};

struct SDebugCircleDescriptor : public SComplexDebugPrimitive {
    SDebugCircleDescriptor(const float4);
    SDebugCircleDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugCircleDescriptor &SetZBufferEnabled(bool);
    SDebugCircleDescriptor &SetMaterial(uint64_t);
    SDebugCircleDescriptor &SetCenter(const float4);
    SDebugCircleDescriptor &SetColor(uint32_t);
    SDebugCircleDescriptor &SetRotation(const SMatrix &);
    SDebugCircleDescriptor &SetScale(const float4);
    SDebugCircleDescriptor &SetSolid(bool);
};

struct SDebugSliceDescriptor : public SSpatialDebugPrimitive {
    SDebugSliceDescriptor(const float4, const float4, float);
    SDebugSliceDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugSliceDescriptor &SetZBufferEnabled(bool);
    SDebugSliceDescriptor &SetMaterial(uint64_t);
    SDebugSliceDescriptor &SetCenter(const float4);
    SDebugSliceDescriptor &SetColor(uint32_t);
    SDebugSliceDescriptor &SetRadius(float);
    SDebugSliceDescriptor &SetNormal(const float4);
    SDebugSliceDescriptor &SetStartAngle(float);
    SDebugSliceDescriptor &SetEndAngle(float);
    SDebugSliceDescriptor &SetAngles(float, float);
    SDebugSliceDescriptor &SetSolid(bool);
    float m_fRadiusLenght;
    float4 m_vNormal;
    float m_fStartAngle;
    float m_fEndAngle;
    bool m_bSolid;
};

struct SDebugLineDescriptor : public SSpatialDebugPrimitive {
    SDebugLineDescriptor(const float4, const float4);
    SDebugLineDescriptor &SetTimeToLive(const ZGameTime &);
    SDebugLineDescriptor &SetZBufferEnabled(bool);
    SDebugLineDescriptor &SetMaterial(uint64_t);
    SDebugLineDescriptor &SetStartPoint(const float4);
    SDebugLineDescriptor &SetEndPoint(const float4);
    SDebugLineDescriptor &SetColor(uint32_t);
    SDebugLineDescriptor &SetThickness(float, const float4);
    SDebugLineDescriptor &SetStartArrowHead(float);
    SDebugLineDescriptor &SetEndArrowHead(float);
    float4 m_vEndPoint;
    float m_fThickness;
    float4 m_vNormal;
    float m_fStartHeadLenght;
    float m_fEndHeadLenght;
};

struct SDebugRenderPrim;

class ZDebugRender {
    public:
    class ZRenderDebugDrawUpdates {
        public:
        ZRenderDebugDrawUpdates(ZEntityImpl *pEntity,
                                const ZDelegate<void __cdecl(ZDebugRender *)> &callback,
                                bool bSupportsPicking, bool bIsSpatialEntity);
        ZRenderDebugDrawUpdates(const ZDelegate<void __cdecl(ZDebugRender *)> &callback,
                                bool bSupportsPicking);
        ZRenderDebugDrawUpdates();
        bool operator==(const ZDebugRender::ZRenderDebugDrawUpdates &rhs);
        bool operator<(const ZDebugRender::ZRenderDebugDrawUpdates &rhs);
        bool IsSpatial();
        bool SupportsPicking();
        const ZDelegate<void __cdecl(ZDebugRender *)> &GetDelegate();
        const ZEntityImpl *GetEntity() const;
        ZEntityImpl *GetEntity();
        void Invoke(ZDebugRender *pDebugRender);
        ZDelegate<void __cdecl(ZDebugRender *)> m_Delegate;
        ZEntityImpl *m_pEntity;
        bool m_bSupportPicking;
        bool m_bIsSpatialEntity;
    };
    struct SRenderDebugCallbacks {
        SRenderDebugCallbacks(ZSpatialEntity *pEntity,
                              const ZDelegate<void __cdecl(ZDebugRender *)> &callback,
                              const ZDelegate<void __cdecl(ZDebugRender *)> &drawStart,
                              const ZDelegate<void __cdecl(ZDebugRender *)> &drawEnd,
                              bool bSupportsPicking);
        SRenderDebugCallbacks(ZEntityImpl *pEntity,
                              const ZDelegate<void __cdecl(ZDebugRender *)> &callback,
                              const ZDelegate<void __cdecl(ZDebugRender *)> &drawStart,
                              const ZDelegate<void __cdecl(ZDebugRender *)> &drawEnd,
                              bool bSupportsPicking);
        SRenderDebugCallbacks(const ZDelegate<void __cdecl(ZDebugRender *)> &callback,
                              const ZDelegate<void __cdecl(ZDebugRender *)> &drawStart,
                              const ZDelegate<void __cdecl(ZDebugRender *)> &drawEnd,
                              bool bSupportsPicking);
        SRenderDebugCallbacks();
        bool operator==(const ZDebugRender::SRenderDebugCallbacks &rhs);
        bool operator<(const ZDebugRender::SRenderDebugCallbacks &rhs);
        ZDebugRender::ZRenderDebugDrawUpdates m_DrawUpdate;
        ZDelegate<void __cdecl(ZDebugRender *)> m_DrawStart;
        ZDelegate<void __cdecl(ZDebugRender *)> m_DrawEnd;
    };
    struct SRenderDebugChannel {
        SRenderDebugChannel();
        TUnorderedLinearHashSet<ZSpatialEntity *, &Hash_DefaultHash<ZSpatialEntity *>, 31,
                                DefaultHashAllocator>
            m_Entities;
        TSet<ZDebugRender::SRenderDebugCallbacks> m_RenderDebugCallbacks;
        bool m_bActivated;
    };
    enum PRIMTYPE {
        PT_LINES,
        PT_TRIANGLES,
        PT_TEXTBOX,
    };
    enum TEXTALIGN {
        TEXTALIGN_CENTERX,
        TEXTALIGN_CENTERY,
        TEXTALIGN_CENTER,
        TEXTALIGN_RIGHT,
        TEXTALIGN_VIEW,
        TEXTALIGN_CENTERBLOCK,
    };
    enum EDebugChannel {
        DEBUGCHANNEL_NONE,
        DEBUGCHANNEL_GUIDES_DEFAULT,
        DEBUGCHANNEL_GUIDES_COVER,
        DEBUGCHANNEL_GUIDES_LADDERS,
        DEBUGCHANNEL_GUIDES_VENTILATORSHAFT,
        DEBUGCHANNEL_GUIDES_WINDOWS,
        DEBUGCHANNEL_GUIDES_LEDGES,
        DEBUGCHANNEL_GUIDES_JUMP,
        DEBUGCHANNEL_GUIDES_INTERACTION,
        DEBUGCHANNEL_GUIDES_NAVIGATION,
        DEBUGCHANNEL_GUIDES_END,
        DEBUGCHANNEL_DEFAULT,
        DEBUGCHANNEL_LIGHT,
        DEBUGCHANNEL_PARTICLES,
        DEBUGCHANNEL_PARTICLES_SH_PROBES,
        DEBUGCHANNEL_PARTITIONING,
        DEBUGCHANNEL_DECALS,
        DEBUGCHANNEL_CROWD,
        DEBUGCHANNEL_PHYSICS,
        DEBUGCHANNEL_TRIGGER_HIGHLIGHT,
        DEBUGCHANNEL_CHARACTER,
        DEBUGCHANNEL_PLAYER,
        DEBUGCHANNEL_DAMAGE,
        DEBUGCHANNEL_CARTOGRAPHY,
        DEBUGCHANNEL_AI,
        DEBUGCHANNEL_AI_GRID,
        DEBUGCHANNEL_AI_SITUATION,
        DEBUGCHANNEL_GAME,
        DEBUGCHANNEL_DYNAMIC_HAZARD,
        DEBUGCHANNEL_COLLISION_QUERY,
        DEBUGCHANNEL_ENGINE,
        DEBUGCHANNEL_SOUND,
        DEBUGCHANNEL_ANIMATION,
        DEBUGCHANNEL_ACTIONS,
        DEBUGCHANNEL_CLOTH,
        DEBUGCHANNEL_SOCIAL,
        DEBUGCHANNEL_ANCHORS,
        DEBUGCHANNEL_MEASURING_TAPE,
        DEBUGCHANNEL_STREAMING,
        DEBUGCHANNEL_STREAMING_INFORMATION,
        DEBUGCHANNEL_STREAMING_ROOTS,
        DEBUGCHANNEL_STREAMING_RADIAL,
        DEBUGCHANNEL_NOTES,
        DEBUGCHANNEL_ROLYPOLY,
        DEBUGCHANNEL_UMBRAVOLUME,
        DEBUGCHANNEL_HAIR,
        DEBUGCHANNEL_LOCATORS,
        DEBUGCHANNEL_END,
        DEBUGCHANNEL_SIMPLEDEBUG,
        DEBUGCHANNEL_MANIPULATORS,
        DEBUGCHANNEL_MAX,
    };
    ZDebugRender(ZDebugRender *pDebugPrimContainer);
    ~ZDebugRender();
    void SetTextOffset(SVector2 vTextOffset);
    void
    RegisterForDrawUpdate(const ZDelegate<void __cdecl(ZDebugRender *)> &callback);
    void RegisterForDrawUpdate(
        ZEntityImpl *pEntity, const ZDebugRender::EDebugChannel channel,
        const ZDebugRender::SRenderDebugCallbacks &RenderDebugCallbacks);
    void RegisterForDrawUpdate(void *pEntity, const ZDebugRender::EDebugChannel channel,
                               const ZDelegate<void __cdecl(ZDebugRender *)> &callback,
                               bool bSupportsPicking);
    void RegisterForDrawUpdate(ZSpatialEntity *pEntity, const ZDebugRender::EDebugChannel channel,
                               const ZDelegate<void __cdecl(ZDebugRender *)> &callback,
                               bool bSupportsPicking);
    void RegisterForDrawUpdate(ZEntityImpl *pEntity, const ZDebugRender::EDebugChannel channel,
                               const ZDelegate<void __cdecl(ZDebugRender *)> &callback,
                               bool bSupportsPicking);
    void
    RegisterForDrawUpdate(const ZDebugRender::EDebugChannel channel,
                          const ZDelegate<void __cdecl(ZDebugRender *)> &callback);
    void UnregisterForDrawUpdate(
        const ZDelegate<void __cdecl(ZDebugRender *)> &callback);
    void UnregisterForDrawUpdate(void *pEntity, const ZDebugRender::EDebugChannel channel,
                                 const ZDelegate<void __cdecl(ZDebugRender *)> &callback,
                                 bool bSupportsPicking);
    void UnregisterForDrawUpdate(
        ZEntityImpl *pEntity, const ZDebugRender::EDebugChannel channel,
        const ZDebugRender::SRenderDebugCallbacks &RenderDebugCallbacks);
    void UnregisterForDrawUpdate(
        const ZDebugRender::EDebugChannel channel,
        const ZDelegate<void __cdecl(ZDebugRender *)> &callback);
    void RegisterForDrawUpdateFullScreen(
        const ZDelegate<void __cdecl(ZDebugRender *)> &callback);
    void UnregisterForDrawUpdateFullScreen(
        const ZDelegate<void __cdecl(ZDebugRender *)> &callback);
    void RegisterForDrawUpdateConsole(
        const ZDelegate<void __cdecl(ZDebugRender *)> &callback);
    void UnregisterForDrawUpdateConsole(
        const ZDelegate<void __cdecl(ZDebugRender *)> &callback);
    void RegisterForDrawUpdateMonitor(
        const ZDelegate<void __cdecl(ZDebugRender *)> &callbackText,
        const ZDelegate<void __cdecl(ZDebugRender *)> &callback3D);
    void UnregisterForDrawUpdateMonitor(
        const ZDelegate<void __cdecl(ZDebugRender *)> &callbackText,
        const ZDelegate<void __cdecl(ZDebugRender *)> &callback3D);
    void Trim();
    void DoDrawUpdate(const ZDebugRender::EDebugChannel channel);
    void DoDrawUpdate();
    void DoDrawStart(const ZDebugRender::EDebugChannel channel);
    void DoDrawEnd(const ZDebugRender::EDebugChannel channel);
    void DoDrawUpdateFullScreen();
    void DoConsoleDrawUpdate();
    void DoMonitorDrawUpdate(bool text);
    void ClearDebugChannels(bool bClearCallbacks);
    void ResetDebugChannel(const ZDebugRender::EDebugChannel channel);
    void AddEntityToDebugChannel(const ZDebugRender::EDebugChannel channel,
                                 ZSpatialEntity *entity);
    void RemoveEntityFromDebugChannel(const ZDebugRender::EDebugChannel channel,
                                      ZSpatialEntity *entity);
    ZDebugRender *GetPickCollectorDebugRender();
    void Begin(IRenderDebugRender *pRenderDebugRender, const SDebugRenderViewport &viewport,
               const SMatrix43 &WorldMatrix, const SMatrix &ProjectionMatrix,
               const SMatrix &ClipPlaneProjectionMatrix);
    void End();
    const SDebugRenderViewport GetDebugTextViewport();
    void xDrawTextShadow(const SMatrix &m, const float4 s, const char *pszText,
                         uint64_t lTextLength, uint32_t lColor,
                         uint32_t lAlignFlags);
    void xDrawTextShadow(const float4 v, const char *pszText, uint32_t lColor,
                         uint32_t lAlignFlags);
    void xDrawText(const SMatrix &m, const float4 s, const char *pszText, uint64_t lTextLength,
                   uint32_t lColor, uint32_t lAlignFlags, bool bBox);
    void xDrawText(const float4 v, const char *pszText, uint32_t lColor, uint32_t lAlignFlags,
                   float fFontSizeScaling);
    void xDrawText3D(const float4 worldPosition, const float4 normal, const float4 up,
                     float fFontScale, const char *pszText, uint64_t nbCharInText,
                     uint32_t nColor);
    void xDrawText3D(const float4 worldPosition, const float4 normal, float fFontSize,
                     const char *pszText, uint64_t lLength, uint32_t nColor);
    void xDrawTriangle(const float4, const float4, const float4, bool);
    void xDrawPicture(const SMatrix &m, const float4 v, const float4 s, float tu, float tv,
                      uint32_t nColor, void *pTexture);
    void xDrawPicture(const SMatrix &m, const float4 v, const float4 s, float tu, float tv,
                      uint32_t nColor, IRenderDebugRender::TEXTURETYPE lTextureId);
    void Draw(const SDebugCapsuleDescriptor &desc);
    void Draw(const SDebugText3DDescriptor &desc);
    void Draw(const SDebugPyramidDescriptor &desc);
    void Draw(const SDebugPointDescriptor &desc);
    void Draw(const SDebugSphereDescriptor &desc);
    void Draw(const SDebugMatrixDescriptor &desc);
    void Draw(const SDebugCylinderDescriptor &desc);
    void Draw(const SDebugBoxDescriptor &desc);
    void Draw(const SDebugTriangleDescriptor &desc);
    void Draw(const SDebugArrowDescriptor &desc);
    void Draw(const SDebugConeDescriptor &desc);
    void Draw(const SDebugCircleDescriptor &desc);
    void Draw(const SDebugSliceDescriptor &desc);
    void Draw(const SDebugLineDescriptor &desc);
    void DrawLine(const float4 vStart, const float4 vEnd, uint32_t lColor);
    void DrawLineSolid(const float4 vStart, const float4 vEnd, uint32_t nColor, float fThickness,
                       const float4 vNormal);
    void DrawLineShaded(const float4 vStart, const float4 vEnd, const float4 vNormal,
                        uint32_t nColor, uint32_t nShadeColor);
    void DrawLineArrowHead(const float4 vStart, const float4 vEnd, uint32_t nColor,
                           float fThickness, float fArrowHeadLength,
                           const float4 vNormal);
    void DrawLineArrowHeadDouble(const float4 vStart, const float4 vEnd, uint32_t nColor,
                                 float fThickness, float fArrowHeadLength,
                                 const float4 vNormal);
    void DrawTriangle(const float4 v0, const float4 v1, const float4 v2,
                      uint32_t nColor);
    void DrawTriangleWire(const float4 v0, const float4 v1, const float4 v2,
                          uint32_t lColor);
    void DrawTriangleSolid(const float4 v0, const float4 v1, const float4 v2,
                           uint32_t lColor);
    void DrawBox(const SMatrix &m, const float4 v, const float4 s, uint32_t lColor);
    void DrawBoxColorFaces(const SMatrix &m, const float4 v, const float4 s,
                           uint32_t *lColor);
    void DrawBoxWire(const SMatrix &m, const float4 v, const float4 s, float borderWidth,
                     uint32_t nColor, bool bKeepDrawMode);
    void DrawBoxWire(const SMatrix &m, const float4 v, const float4 s, uint32_t nColor,
                     bool bKeepDrawMode);
    void DrawPlane(const SMatrix &m, const float4 v, const float4 s,
                   uint32_t lColor);
    void DrawPlaneWire(const SMatrix &m, const float4 v, const float4 s,
                       uint32_t lColor);
    void DrawPlaneSolid(const SMatrix &, const float4, const float4, uint32_t);
    void DrawPyramid(const SMatrix &m, const float4 v, const float4 s,
                     uint32_t lColor);
    void DrawPyramidSolid(const SMatrix &m, const float4 v, const float4 s,
                          uint32_t lColor);
    void DrawCappedPyramid(const SMatrix &m, const float4 v, float fAngleRad, float fLength,
                           float fAspectXByY, uint32_t lColor);
    void DrawCylinder(const SMatrix &mOrientation, const float4 vPosition, float fHeight,
                      float fRadius, uint32_t nColor);
    void DrawCylinderSolid(const SMatrix &mOrientation, const float4 vPosition, float fHeight,
                           float fRadius, uint32_t nColor);
    void DrawCone(const SMatrix &mRotation, const float4 vTipPosition, const float4 s,
                  uint32_t nColor);
    void DrawConeSolid(const SMatrix &mRotation, const float4 vTipPosition, const float4 s,
                       uint32_t nColor);
    void DrawCappedCone(const SMatrix &m, const float4 vTipPosition, float fAngleRad,
                        float fConeHeight, uint32_t lColor);
    void DrawCappedConeSolid(const SMatrix &m, const float4 v, float fAngleRad, float fLength,
                             uint32_t lColor);
    void DrawSlice(const SMatrix &m, const float4 v, const float4 s, uint32_t nColor,
                   float fStartAngle, float fEndAngle, uint32_t iStepsToACircle);
    void DrawSlice(const float4 vCenter, float fSize, const float4 vNormal, const float4 vUp,
                   uint32_t nColor, float fEndAngle, uint32_t iStepsToACircle);
    void DrawSliceWire(const SMatrix &m, const float4 v, const float4 s, uint32_t nColor,
                       float fStartAngle, float fEndAngle,
                       uint32_t iStepsToACircle);
    void DrawSliceWire(const float4 vCenter, float fSize, const float4 vNormal, const float4 vUp,
                       uint32_t nColor, float fStartAngle, float fEndAngle,
                       uint32_t iStepsToACircle);
    void DrawSliceSolid(const SMatrix &m, const float4 v, const float4 s, uint32_t nColor,
                        float fStartAngle, float fEndAngle,
                        uint32_t iStepsToACircle);
    void DrawSliceSolid(const float4 vCenter, float fSize, const float4 vNormal, const float4 vUp,
                        uint32_t nColor, float fStartAngle, float fEndAngle,
                        uint32_t iStepsToACircle);
    void DrawCircle(const SMatrix &m, const float4 v, const float4 s,
                    uint32_t lColor);
    void DrawCircleSolid(const SMatrix &m, const float4 v, const float4 s,
                         uint32_t lColor);
    void DrawSphere(const float4 vSphereCenter, const float4 vScale,
                    uint32_t lColor);
    void DrawSphereSolid(const float4 vSphereCenter, const float4 vScale,
                         uint32_t lColor);
    void DrawSphere3(const SMatrix &m, const float4 v, const float4 s,
                     uint32_t lColor);
    void DrawSphere3OutlinedShaded(const SMatrix &m, float fSize, uint32_t nColor);
    void DrawSphere3OutlinedShaded(const float4 vPosition, float fSize,
                                   uint32_t nColor);
    void DrawSemiSphere3OutlinedShaded(const float4 vCenter, const float4 vNormal,
                                       const float4 vXAxis, float fSize,
                                       uint32_t nColor);
    void DrawSphereArcs(const SMatrix &m, const float4 v, const float4 s,
                        uint32_t lColor);
    void DrawGrid(uint32_t lDivisionsX, uint32_t lDivisionsZ, uint32_t lColor);
    void DrawGrid3D(uint32_t lDivisionsX, uint32_t lDivisionsY, uint32_t lDivisionsZ, float fScale,
                    uint32_t nColor, const float4 vPosition);
    void DrawRuleOfThirdGrid(const uint32_t lColor);
    void DrawMatrix(const SMatrix &m, const float4 v, float fScale, uint32_t nAxisMask,
                    bool bBright);
    void DrawMatrix(const SMatrix &m, const float4 v, uint32_t nAxisMask,
                    bool bBright);
    void DrawMatrix(const SMatrix &m, const float4 v, float fScale);
    void DrawMatrix(const SMatrix &m, float fScale);
    void DrawFrustum(const SMatrix &mTransform, const SMatrix &mProjection, uint32_t nWireColor,
                     uint32_t nNearPlaneColor, uint32_t nFarPlaneColor);
    void DrawAxis(const float4 vStart, const float4 vDir, float fScale, uint32_t lColor,
                  uint32_t nAxis, bool bDrawArrowTip, bool bDrawAxisLabel);
    void DrawPoint(const float4 vPoint, uint32_t lColor);
    void DrawArrow(const SMatrix &m, const float4 v, const float4 s,
                   uint32_t lColor);
    void DrawArrowFlatSolid(const SMatrix &mOrientation, const float4 vPosition, const float4 vSize,
                            uint32_t lColor);
    void DrawArrowFlatSolidDouble(const SMatrix &mOrientation, const float4 vPosition,
                                  const float4 vSize, uint32_t lColor);
    void DrawCapsule(const SMatrix &m0, const float4 vCenter, float fRadius, float fHalfHeight,
                     uint32_t nColor, bool bDrawCenterCircle);
    void DrawSphericalCapSolid(const SMatrix &m, const float4 v, const float4 s, float fCapAngle,
                               uint32_t lColor);
    void DrawArc(const float4 vCenter, const float4 vNormal, const float4 vStartPoint,
                 const float4 vEndPoint, bool bInvertSweep, uint32_t nColor,
                 uint32_t iStepsToACircle);
    void DrawArc(const float4 vCenter, float fSize, const float4 vNormal, const float4 vStartDir,
                 uint32_t nColor, float fAngle, uint32_t iStepsToACircle);
    void DrawArcShaded(const float4 vCenter, const float4 vXDir, const float4 vYDir, float fSize,
                       float fStartAngle, float fSweep, uint32_t iStepsToACircle, uint32_t nColor,
                       uint32_t nShadeColor);
    void DrawArcShaded(const float4 vCenter, float fSize, const float4 vNormal,
                       const float4 vStartDir, uint32_t nColor, uint32_t nShadeColor, float fAngle,
                       uint32_t iStepsToACircle);
    void DrawArcShaded(const float4 vCenter, const float4 vNormal, const float4 vStartPoint,
                       const float4 vEndPoint, bool bInvertSweep, uint32_t nColor,
                       uint32_t nShadeColor, uint32_t iStepsToACircle);
    void DrawEllipse(const float4 vCenter, SVector2 vSize, const float4 vNormal,
                     const float4 vStartDir, uint32_t nColor, float fAngle,
                     uint32_t iStepsToACircle);
    void DrawEllipseSolid(const float4 vCenter, SVector2 vSize, const float4 vNormal,
                          const float4 vStartDir, uint32_t nColor, float fAngle,
                          uint32_t iStepsToACircle);
    void DrawParametricCurve(const ZParametricCurve &curve, float fGrain,
                             const uint32_t iColor);
    void DrawParametricCurveSolid(const ZParametricCurve &curve, float fThickness, float fGrain,
                                  const uint32_t iColor);
    void DrawSpline(const ZSpline &spline, uint32_t nSplineColor, float fSplineGranularity,
                    bool bDrawKnots, uint32_t nKnotsColor, bool bDrawTangents,
                    uint32_t nTangentColor, float fTangentScale);
    void DrawSplineSolid(const ZSpline &spline, float fThickness, uint32_t nSplineColor,
                         float fSplineGranularity, bool bDrawKnots,
                         uint32_t nKnotsColor);
    void DrawPrim(const ZSpatialEntity *pSpatialEntity);
    void xPrim(const SDebugVertex *pVertices, uint32_t nNumVerts);
    bool IsInsideFrustum(const SMatrix &mTransform, const float4 vCenter,
                         const float4 vHalfSize);
    static void BuildTransformMatrix(SMatrix44 &mTransform, const SMatrix &mRot, const float4 vPos,
                                     const float4 vScale);
    void TransformWorldToView(float4 &vOut, const float4 vIn);
    void TransformViewToWorld(float4 &vOut, const float4 vIn);
    void Transform(float4 &v, const SMatrix &m, const float4 p, const float4 s);
    void xBeginDraw(ZDebugRender::PRIMTYPE Type, const SMatrix44 &mTransform);
    void xBegin(ZDebugRender::PRIMTYPE Type);
    void xEndDraw();
    void xColor(const uint32_t color);
    void xVertex3(const float4 position);
    void xVertex3(const float x, const float y, const float z);
    void xVertex3v(const float *v);
    void xVertex2(const float x, const float y);
    void xVertex2v(const float *v);
    void xTexCoords2(const float4 t);
    void xTexCoords2(const float s, const float t);
    void xTexCoords2v(const float *v);
    void xEnd();
    uint32_t xSetDrawMode(uint32_t lDrawMode);
    void xReset();
    uint32_t xSetMaterialColor(uint32_t lMaterialColor);
    IRenderDebugRender::SHADERMODE
    xSetShaderMode(IRenderDebugRender::SHADERMODE lShaderMode);
    uint64_t xSetTexture(void *pTexture);
    uint64_t xSetTexture(int32_t lTextureId);
    const uint64_t xSetMaterial(uint64_t materialRRID);
    void xSetTrianglePrimitive(IDebugRenderPrimitive *pDebugTriangleEntity);
    void xSetLinePrimitive(IDebugRenderPrimitive *pDebugLineEntity);
    void xSetDebugPrimitive(ZDebugRenderPrimitive *pDebugEntityContainer);
    void SetTextMaterial(uint64_t materialRRID);
    uint32_t xSetObjectID(uint32_t nObjectID, uint8_t nPickingPriority);
    IDebugRenderPrimitive *CreateDebugTrianglePrimitive(bool bNoZTest,
                                                        uint64_t materialRRID);
    IDebugRenderPrimitive *CreateDebugLinePrimitive(bool bNoZTest,
                                                    uint64_t materialRRID);
    ZDebugRenderPrimitive *CreateDebugPrimitive(bool bNoZTest,
                                                uint64_t materialRRID);
    void DestroyDebugTriangleEntity(IDebugRenderPrimitive *pEntity);
    void DestroyDebugLineEntity(IDebugRenderPrimitive *pEntity);
    void DestroyDebugPrimitive(ZDebugRenderPrimitive *pEntity);
    const SDebugRenderViewport &GetViewport();
    float GetTextureFontWidth();
    float GetTextureFontHeight();
    float4 GetTextureFontSize();
    float GetTextureFontRatio();
    const SMatrix &GetWorldToView();
    const SMatrix &GetViewToWorld();
    const SMatrix &GetProjectionMatrix();
    bool IsOrtho();
    float OrthoScale();
    float PerpectiveScale(const float4 &position);
    float ViewScale(const float4 &position);
    ZDebugRender::SRenderDebugChannel &GetDebugChannel(int32_t index);
    enum AXIS {
        AXIS_X,
        AXIS_Y,
        AXIS_Z,
    };
    enum {
        MAX_NUM_VERTICES,
    };
    enum DEBUGPRIMS {
        DEBUGPRIM_SPHERE,
        DEBUGPRIM_BOX_WIRE,
        DEBUGPRIM_BOX,
        DEBUGPRIM_CONE,
        DEBUGPRIM_CAPSULE,
        DEBUGPRIM_MAXCOUNT,
    };
    bool IsRenderGraphNodeInDebugChannel(ZDebugRender::SRenderDebugChannel &, ZRenderGraphNode *);
    struct TCharTextureCoord {
        float4 topLeft;
        float4 bottomLeft;
        float4 bottomRight;
        float4 topRight;
    };
    enum SUPPORTED_CHARS {
        FIRST_CHAR,
        LAST_CHAR,
        NUMBER_OF_CHARS,
    };
    ZDebugRender::TCharTextureCoord m_aCharTextCoord[93];
    void InitCharTextureCoord();
    void xDrawSupportedChar(uint32_t ch, const float4 letterTopLeftPos,
                            const float4 letterTopRightPos, const float4 letterBottomRightPos,
                            const float4 letterBottomLeftPos);
    bool IsCharSupported(uint32_t ch);
    void RotateWorldToView(float4 &vOut, const float4 vIn);
    SMatrix RotationMatrixFromNormalVector(const float4 vNormal);
    SMatrix RotationMatrixFromNormalAndUpVectors(const float4 vNormal,
                                                 const float4 vUp);
    SMatrix RotationMatrixFromDirVector(const float4 tangent);
    void DrawCurrentPicture(const SMatrix &m, const float4 v, const float4 s, float tu, float tv,
                            uint32_t nColor);
    bool xCanFlush();
    void xFlush(bool bNeedVertexCopy);
    void AllocateDebugPrims();
    void FreeDebugPrims();
    void SetClipPlanes(const uint32_t nNumClipPlanes, float4 *pClipPlanes);
    void ConvertClipPlanesToSOA();
    void DrawCurveSectionSolid(const float4 vCurvePos, const float4 vCurveTangent,
                               const float4 vThickness, const uint32_t iColor, bool bIsFirst,
                               float4 *aVertices);
    void DrawSemiSphere3OutlinedShadedInternal(const float4 vCenter, const float4 vNormal,
                                               const float4 vXAxis, float fSize, uint32_t nColor,
                                               bool &bOutlineSectionDrawn,
                                               float4 &vOutlineSectionStart,
                                               float4 &vOutlineSectionEnd);
    void DrawLine_Internal(const float4 vStart, const float4 vEnd, uint32_t nColor,
                           float fThickness, const float4 vNormal, float fStartArrowHeadLength,
                           float fEndArrowHeadLength);
    void DrawLine_Internal(const float4 vStart, const float4 vEnd, uint32_t nColor,
                           float fStartArrowHeadLength, float fEndArrowHeadLength);
    void DrawSlice_Internal(const SMatrix &m, const float4 v, const float4 s, uint32_t nColor,
                            float fStartAngle, float fEndAngle, uint32_t iStepsToACircle,
                            bool bSolid, bool bDrawInternalBorders);
    void DrawSlice_Internal(const float4 vCenter, float fSize, const float4 vNormal,
                            const float4 vUp, uint32_t nColor, float fStartAngle, float fEndAngle,
                            uint32_t iStepsToACircle, bool bSolid,
                            bool bDrawInternalBorders);
    void DrawCircle_Internal(const SMatrix &m, const float4 v, const float4 s, uint32_t lColor,
                             bool bSolid);
    void DrawSphere_Internal(const float4 vSphereCenter, const float4 vScale, uint32_t lColor,
                             bool bSolid);
    void DrawCone_Internal(const float4 vBaseCenterPosition, const float4 vTipPosition,
                           const float4 sWorldScale, uint32_t lColor, bool bSolid, bool bCapped,
                           int32_t iNbOfEdges);
    void DrawCone_Internal(const SMatrix &mRotation, const float4 vTipPosition,
                           const float4 sWorldScale, uint32_t lColor, bool bSolid, bool bCapped,
                           int32_t iNbOfEdges);
    void DrawCylinder_Internal(const float4 vCap0CenterPos, const float4 vCap1CenterPos,
                               float fRadius, uint32_t nColor, bool bSolid,
                               bool bCapped);
    void DrawCylinder_Internal(const SMatrix &mOrientation, const float4 vCylinderCenter,
                               float fHeight, float fRadius, uint32_t nColor, bool bSolid,
                               bool bCapped);
    void DrawCirclePortion_Internal(float fStartAngle, float fEndAngle, const SMatrix &mOrientation,
                                    const float4 vCircleCenter, const float4 vScale,
                                    uint32_t lColor, bool bSolid, bool bDrawInternalBorders,
                                    bool bDrawIntermediateSlices);
    void InitCirclePoints();
    uint64_t GetIndexInCirclePoints(float fAngle);
    struct SDebugRenderState {
        ZDebugRender::PRIMTYPE m_Type;
        uint32_t m_DrawMode;
        uint32_t m_ObjectID;
        uint64_t m_TextureID;
        bool m_bIsTexturePtr;
        uint32_t m_MaterialColor;
        SMatrix44 m_Transform;
        uint64_t m_MaterialRRID;
        IRenderDebugRender::SHADERMODE m_ShaderMode;
        IDebugRenderPrimitive *m_pDebugTriangleEntity;
        IDebugRenderPrimitive *m_pDebugLineEntity;
        SDebugRenderState();
        bool operator==(const ZDebugRender::SDebugRenderState &op);
        bool operator!=(const ZDebugRender::SDebugRenderState &op);
    };
    ZMutex m_mutex;
    bool m_bChannelDisabled;
    SVector2 m_vTextOffset;
    SDebugRenderViewport m_Viewport;
    bool m_bInScene;
    bool m_bOrtho;
    float m_fOrthoScale;
    SMatrix m_WorldToView;
    SMatrix m_ViewToWorld;
    SMatrix m_Projection;
    enum {
        CIRCLE_SEGMENTS,
        CIRCLE_VERTICES,
    };
    bool m_bInside_xBegin;
    SDebugVertex m_CurrentVertex;
    ZDebugRender::SDebugRenderState m_CurrentState;
    ZDebugRender::SDebugRenderState m_WantedState;
    uint32_t m_lWantedColor;
    uint32_t m_lVertexCount;
    SDebugVertex m_Vertices[16384];
    float m_fTextureFontSize[2];
    TBag<ZDelegate<void __cdecl(ZDebugRender *)>, TArray<ZDelegate<void __cdecl(ZDebugRender *)>>>
        m_DrawUpdateEntries;
    TBag<ZDelegate<void __cdecl(ZDebugRender *)>, TArray<ZDelegate<void __cdecl(ZDebugRender *)>>>
        m_DrawUpdateFullScreenEntries;
    bool m_bDrawUpdateConsole;
    ZDelegate<void __cdecl(ZDebugRender *)> m_DrawUpdateConsole;
    TBag<ZDelegate<void __cdecl(ZDebugRender *)>, TArray<ZDelegate<void __cdecl(ZDebugRender *)>>>
        m_DrawUpdateMonitorTextEntries;
    TBag<ZDelegate<void __cdecl(ZDebugRender *)>, TArray<ZDelegate<void __cdecl(ZDebugRender *)>>>
        m_DrawUpdateMonitor3DEntries;
    IRenderDebugRender *m_pRenderDebugRender;
    SDebugRenderPrim *m_DebugPrims;
    ZDebugRender::SRenderDebugChannel m_DebugChannels[47];
    enum {
        MAX_DEBUG_RENDER_CLIPPLANES,
    };
    struct SClipPlanes4 {
        float4 x4;
        float4 y4;
        float4 z4;
        float4 w4;
    };
    TFixedArray<ZDebugRender::SClipPlanes4, 8> m_aClipPlanes4;
    uint32_t m_nNumClipPlanesSoA;
    float4 m_avClipPlanes[32];
    float4 *m_pCirclePoints;
    TArray<IDebugRenderPrimitive *> m_DebugRenderTrianglesToDestroy;
    TArray<IDebugRenderPrimitive *> m_DebugRenderLinesToDestroy;
    uint64_t m_pDebugTextMaterial;
};

struct SDebugRenderPrim {
    SDebugVertex *pVertices;
    uint32_t nNumVerts;
    ZDebugRender::PRIMTYPE nPrimType;
    int32_t nRef;
    SDebugRenderPrim();
};

void ImportZRuntimeDebugModule();