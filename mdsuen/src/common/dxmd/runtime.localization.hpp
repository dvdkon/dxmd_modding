// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "runtime.resource.hpp"

class ZTextListData {
    public:
    ZTextListData(ZResourcePending &resourcePending);
    ~ZTextListData();
    bool TryGetText(const ZString &sName, ZString &sText);
    bool TryGetText(int32_t nNameHash, ZString &sText);
    ZString GetText(const ZString &sName);
    ZString GetText(int32_t nNameHash);
    const THashMap<int, ZString, TDefaultHashMapPolicy<int, ZString>> &GetMap() const;
    ZString GetTextForNonlocalizedUse(const ZString &sName);
    static void SetLocDebugEnabled(bool enabled);
    static bool IsLocDebugEnabled();
    THashMap<int, ZString, TDefaultHashMapPolicy<int, ZString>> m_Map;
    ZRuntimeResourceID m_textListRID;
    static bool m_LocDebugEnabled;
};

class ZTextLineData {
    public:
    ZTextLineData(ZResourcePending &resourcePending);
    ~ZTextLineData();
    ZString GetText() const;
    int32_t GetTextLineNameHash();
    TResourcePtr<ZTextListData> m_pTextListResourcePtr;
    int32_t m_textLineNameHash;
    ZRuntimeResourceID m_textLineRID;
    static const int32_t INVALID_TEXT_LINE_NAME_HASH;
};

void ImportZRuntimeLocalizationModule();