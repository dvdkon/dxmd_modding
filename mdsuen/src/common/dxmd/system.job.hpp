// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "g2base.hpp"
#include <functional>
#include <map>

class ZJobThread;
class ZJobChainThread;

enum eJobPriority {
    eJobPriority_VeryHigh,
    eJobPriority_High,
    eJobPriority_Medium,
    eJobPriority_Low,
    eJobPriority_VeryLow,
    eJobPriority_LAST,
    eJobPriority_COUNT,
};

enum EFrameTime {
    EFrameTime_Invalid,
    EFrameTime_START_NOW,
    EFrameTime_END_WHENEVER,
    EFrameTime_NEXT_UPDATE,
    EFrameTime_FIRST,
    EFrameTime_PostSequence,
    EFrameTime_Physic_Begin,
    EFrameTime_Physic_DuringPhysic_HACK_RUN_MULTIPLE_TIMES,
    EFrameTime_Physic_End,
    EFrameTime_ApplyUpdateResult_Begin,
    EFrameTime_ApplyUpdateResult_BeforeFaceFX,
    EFrameTime_ApplyUpdateResult_End,
    EFrameTime_Final_Update_Begin,
    EFrameTime_Final_Update_End,
    EFrameTime_UI_Begin,
    EFrameTime_UI_End,
    EFrameTime_PostAsyncSceneQueries,
    EFrameTime_PostSceneContext,
    EFrameTime_LAST,
};

enum EDispatcher {
    EDispatcher_MainThread,
    EDispatcher_Threaded,
    EDispatcher_SequentiallyThreaded,
    EDispatcher_COUNT,
};

struct JobManagerJobQueue {
    ZSemaphore _semaphore;
    enum eJobSize {
        eJobSize_Small,
        eJobSize_Large,
        eJobSize_COUNT,
    };
    ZSpinlock _mutex[5][2];
    struct JobSize {
        std::vector<ZJobThread *, std::allocator<ZJobThread *>> m_smallJobs;
        std::vector<ZJobThread *, std::allocator<ZJobThread *>> m_largeJobs;
    };
    JobManagerJobQueue::JobSize _jobQueue[5];
    JobManagerJobQueue();
    ~JobManagerJobQueue();
    void LockMutex(uint32_t index, JobManagerJobQueue::eJobSize jobSize);
    void UnlockMutex(uint32_t index, JobManagerJobQueue::eJobSize jobSize);
    bool empty();
    bool push(ZJobThread **jobs, uint64_t njobs, eJobPriority priority);
    bool push(ZJobThread *job, eJobPriority priority);
    bool push_notify(ZJobThread **jobs, uint64_t njobs, eJobPriority priority);
    bool push_notify(ZJobThread *job, eJobPriority priority);
    void push_invalid();
    void notify(uint64_t njobs);
    ZJobThread *popWithAffinity(std::vector<ZJobThread *, std::allocator<ZJobThread *>> &jobQueue,
                                uint64_t myAffinity);
    ZJobThread *popWithJobChain(std::vector<ZJobThread *, std::allocator<ZJobThread *>> &jobQueue,
                                ZJobChainThread *jobChain);
    bool pop(ZJobThread *job);
    ZJobThread *pop(ZJobChainThread *jobChain);
    ZJobThread *pop(bool bAcceptLargeJobs, eJobPriority minPriority, eJobPriority maxPriority);
    ZJobThread *pop_wait(bool bAcceptLargeJobs, eJobPriority minPriority, eJobPriority maxPriority);
    bool HasQueuedJob(eJobPriority minimumPriority);
    void changeJobPriority(ZJobChainThread *jobChain, eJobPriority oldPrio, eJobPriority newPrio,
                           std::vector<ZJobThread *, std::allocator<ZJobThread *>> &oldQueue,
                           std::vector<ZJobThread *, std::allocator<ZJobThread *>> &newQueue,
                           JobManagerJobQueue::eJobSize jobSize);
    void changeJobPriority(ZJobChainThread *jobChain, eJobPriority oldPriority,
                           eJobPriority newPriority);
    bool push_internal(ZJobThread **jobs, uint64_t njobs, eJobPriority priority, bool doNotify);
};

class ZJobManagerThread : public IComponentInterface {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZJobManagerThread();
    ZJobManagerThread(const ZJobManagerThread &);
    ZJobManagerThread &operator=(const ZJobManagerThread &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZJobManagerThread();
    ZJobChainThread *CreateJobChain(eJobPriority aPriority);
    void DeleteJobChain(ZJobChainThread *pJobChain_);
    void FrameUpdate(EFrameTime time);
    void FrameUpdate();
    void DispatchAtTime(const char *staticNameDebug, const std::function<void(void)> &proc,
                        EDispatcher dispatcher, EFrameTime begin, EFrameTime end,
                        uint64_t affinityMask, eJobPriority aPriority, bool bLargeJob);
    EFrameTime GetLastFrameTimeExecuted();
    uint32_t GetWorkerThreadCount();
    bool HasQueuedJob(eJobPriority minimumPriority);
    void RescheduleJob(ZJobThread *pJob, eJobPriority aPriority);
    void AddOneJob(ZJobThread *pJob, eJobPriority aPriority);
    void AddJobs(ZJobThread **pJobs, uint32_t numJobs, eJobPriority aPriority);
    ZJobThread *GetJob(bool bAcceptLargeJob, eJobPriority minimumPriorityToHelp);
    bool ExecuteOneJob(bool bAcceptLargeJob, eJobPriority minimumPriorityToHelp);
    bool ExecuteOneJobFromChain(ZJobChainThread *aJobChain);
    bool TryToExecuteJob(ZJobThread *job);
    void ChangeJobsPriority(ZJobChainThread *jobChain, eJobPriority oldPriority,
                            eJobPriority newPriority);
    void SetWorkerCPU(uint32_t workerIndex, uint32_t CPUmask);
    void SetWaitDelegate(void (*func)());
    void ResetWaitDelegate();
    void WaitOnDelegate();
    TFuture<void> Run(const char *staticDebugName, const std::function<void(void)> &job,
                      EDispatcher dispatcher, EFrameTime begin, EFrameTime end,
                      uint64_t affinityMask, eJobPriority aPriority, bool bLargeJob);
    void SetOnAnyJobCompletedDelegate(void (*func)());
    struct FrameJobsPriority {
        FrameJobsPriority();
        TArray<ZJobThread *> m_jobs[3];
        ZJobChainThread *m_jobChain;
    };
    struct FrameJobs {
        EFrameTime m_start;
        EFrameTime m_end;
        ZJobManagerThread::FrameJobsPriority m_jobs[5];
        static void *operator new(size_t sz);
        static void *operator new[](size_t sz);
        static void operator delete(void *ptr);
        static void operator delete[](void *ptr);
        static void InitializePool();
        static void UninitializePool();
        static ZSpinlock m_mutexFrameJobs;
        static TSimpleBlockAllocator<ZJobManagerThread::FrameJobs> m_allocatorFrameJobs;
    };
    struct SThreadData {
        ZThread *m_pThread;
        uint32_t m_nCPU;
        ZThread::eThreadPriority m_ePriority;
        ZJobManagerThread *m_pJobManager;
        bool m_bTerminating;
    };
    static uint32_t WorkerThread(void *pParams);
    static void WorkerThread_Normal(ZJobManagerThread::SThreadData *pThreadData);
    static void WorkerThread_YieldOften(ZJobManagerThread::SThreadData *pThreadData,
                                        eJobPriority maxPriority);
    void AllocateWorkerThread(uint32_t nCPU);
    void TerminateWorkerThread(ZJobManagerThread::SThreadData *pThreadData);
    ZJobThread *GetJobFromWorker(bool bAcceptLargeJobs, eJobPriority minPriority,
                                 eJobPriority maxPriority);
    ZMutex m_mutex;
    JobManagerJobQueue m_JobQueue;
    TArray<ZJobManagerThread::SThreadData *> m_Threads;
    struct SDeleteChain {
        ZJobChainThread *m_pChain;
        uint32_t m_nCount;
    };
    TArray<ZJobManagerThread::SDeleteChain> m_DeleteJobChain;
    void (*m_WaitingDelegate)();
    void (*m_OnEveryJobCompletedDelegate)();
    void StartJobsSequentiallyOnMainThread(ZJobManagerThread::FrameJobsPriority *jobs);
    void StartJobsSequentiallyOnOtherThread(ZJobManagerThread::FrameJobsPriority *jobs);
    void StartJobsOnAnyThread(ZJobManagerThread::FrameJobsPriority *jobs);
    void DeleteEndingWheneverJobs();
    std::map<
        enum EFrameTime,
        std::vector<ZJobManagerThread::FrameJobs *, std::allocator<ZJobManagerThread::FrameJobs *>>,
        std::less<enum EFrameTime>,
        std::allocator<std::pair<enum EFrameTime const,
                                 std::vector<ZJobManagerThread::FrameJobs *,
                                             std::allocator<ZJobManagerThread::FrameJobs *>>>>>
        m_startingJobs;
    std::map<
        enum EFrameTime,
        std::vector<ZJobManagerThread::FrameJobs *, std::allocator<ZJobManagerThread::FrameJobs *>>,
        std::less<enum EFrameTime>,
        std::allocator<std::pair<enum EFrameTime const,
                                 std::vector<ZJobManagerThread::FrameJobs *,
                                             std::allocator<ZJobManagerThread::FrameJobs *>>>>>
        m_endingJobs;
    std::vector<ZJobManagerThread::FrameJobs *, std::allocator<ZJobManagerThread::FrameJobs *>>
        m_endingWheneverJobs;
    EFrameTime m_lastFrameTimeExecuted;
};

class ZJobChainThread {
    public:
    ZJobChainThread(ZJobManagerThread *pManager, eJobPriority aPriority);
    ZJobThread *CreateJob();
    void RescheduleJob(ZJobThread *pJob);
    uint64_t AddOneJob(ZJobThread *pJob);
    uint64_t AddJobs(ZJobThread **pJobs, uint32_t numJobs);
    uint64_t AddJobs(TArrayRef<ZJobThread *> pJobs);
    bool ExecuteOneJob();
    void WaitAllDoneActive(bool bAllowJobPriorityBoost, eJobPriority newPriotity);
    bool WaitAllDoneActiveOnce();
    void WaitAllDoneActiveAnyJobChain(eJobPriority minimumPriorityToHelp,
                                      bool bAllowJobPriorityBoost, eJobPriority newPriotity);
    ZJobManagerThread *GetManager();
    void JobPushed();
    void JobsPushed(uint32_t numJobs);
    void JobExecuted();
    bool IsFinished(bool wait);
    eJobPriority GetPriority();
    eJobPriority GetBoostedPriority();
    void SetThreadedCompletionCallback(ZDelegate<void(void)> callback);
    static void *operator new(size_t sz);
    static void *operator new[](size_t sz);
    static void operator delete(void *ptr);
    static void operator delete[](void *ptr);
    static void InitializePool();
    static void UninitializePool();
    static ZSpinlock m_mutexZJobChainThread;
    static TSimpleBlockAllocator<ZJobChainThread> m_allocatorZJobChainThread;
    ~ZJobChainThread();
    ZJobManagerThread *m_pManager;
    ZThreadEvent m_JobsDoneEvent;
    std::atomic<unsigned int> m_iActiveJobs;
    std::atomic<unsigned int> m_iJobs;
    eJobPriority m_ePriority;
    eJobPriority m_eBoostedPriority;
    ZDelegate<void(void)> m_completionCallback;
    static std::atomic<unsigned int> s_iJobs;
};

class ZJobProgramThread {
    public:
    ZJobProgramThread(void (*pJobFunc)(ZJobThread *));
    auto GetJobFunc() -> void (*)(ZJobThread *);
    void (*m_pJobFunc)(ZJobThread *);
};

class ZJobThread {
    public:
    ZJobThread(ZJobChainThread *pJobChain);
    ~ZJobThread();
    bool IsJobDone();
    void WaitJobDoneActive();
    void WaitJobDoneSpin();
    void SetUserData(const void *pData, uint64_t nSize);
    void SetJobProgram(ZJobProgramThread *pProgram, uint64_t nStackSize);
    void RestrictToAffinityMaskToWorkers(bool bIsASmallJob);
    void RestrictToAffinityMask(uint64_t affinityMask);
    void SetLargeJob(bool bLarge);
    void *GetUserData();
    void SetJobNotDone();
    void ExecuteJob();
    void ExecuteJobOutOfJobChain();
    ZJobChainThread *GetJobChain();
    static void *operator new(size_t sz);
    static void *operator new[](size_t sz);
    static void operator delete(void *ptr);
    static void operator delete[](void *ptr);
    static void InitializePool();
    static void UninitializePool();
    static ZSpinlock m_mutexZJobThread;
    static TSimpleBlockAllocator<ZJobThread> m_allocatorZJobThread;
    ZJobChainThread *m_pJobChain;
    uint8_t m_UserData[96];
    uint64_t m_affinity;
    void (*m_JobFunc)(ZJobThread *);
    bool m_bJobDone;
    bool m_bSelfDestroyOnceJobDone;
    bool m_bIsASmallJob;
};

void ImportZSystemJobModule();