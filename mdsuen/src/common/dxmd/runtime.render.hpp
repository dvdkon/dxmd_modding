// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "d3d11.h"
#include "d3d12.h"
#include "g2base.hpp"
#include "system.render.hpp"
#include "runtime.resource.hpp"

// Forward-declaration for codependent runtime.entity
class ZEntityRef;

class ZRenderGraphNode;
class IRenderGraphNodeReflector;
class ZRenderDeviceContext;
class ZRenderEffectPass;
class ZRenderEffect;
class ZRenderDrawCall;

// Classes I don't want to deal with right now
class ZRenderMaterialInstance;
class IRenderPrimitive;
class ZRenderPrimitive;

class IRenderRefCount {
    public:
    virtual ~IRenderRefCount();
    virtual void AddRef();
    virtual int32_t Release(bool);
};

enum ERenderResourceType {
    RENDER_RESOURCE_TYPE_TEXTURE2D,
    RENDER_RESOURCE_TYPE_TEXTURE3D,
    RENDER_RESOURCE_TYPE_BUFFER,
    RENDER_RESOURCE_TYPE_INVALID,
};

class IRenderResource : public IRenderRefCount {
    public:
    virtual ERenderResourceType GetResourceType();
};

class IRenderTextureResource : public IRenderRefCount {
    public:
    uint64_t GetSequenceLength();
    const IRenderResource *GetResource(uint32_t i);
    TArray<IRenderResource *> m_Resources;
};

enum EReflectableType {
    GEOM,
    DESTRUCTIBLE,
    CLOTH,
    LINKED,
    PARTICLES,
    CORONAS,
    LIGHTBEAM,
    SPATIAL,
    LIGHT,
    CAMERA,
    MATERIAL,
    VISIBILITY_PROXY,
    POSTFILTER,
    DESTINATION,
    VIDEO_PLAYER,
    VOLUMELIGHT,
    GIVOLUME,
    SCATTER,
    FLUID,
    GFX_MOVIE,
    CUBEMAPPROBE,
    HAIR,
    SHADOWCASTER,
    RAINACCESSIBILITY,
    TYPE_SIZE,
    INVALID,
    RENDERABLE_TYPE_MASK,
    RENDERABLE_TYPE_FIRST,
    RENDERABLE_TYPE_LAST,
};

struct SRenderReflectData {
    SRenderReflectData();
    float4 m_mObjectToWorld[3];
    SVector3 m_vHalfsize;
    uint32_t m_nFlags;
    uint8_t m_nVisibilityEventMask;
    SVector3 m_vLocalCenter;
    ZRenderGraphNode *m_pNode;
};

class ZReflectable : public IComponentInterface {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    struct __cta_17 {
        uint64_t bf;
    };
    ZReflectable();
    ZReflectable(const ZReflectable &);
    ZReflectable &operator=(const ZReflectable &);
    enum {
        COPYABLE,
        ASSIGNABLE,
    };
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    enum EChangeFlags {
        CHANGE_INIT,
        CHANGE_UNINIT,
        CHANGE_SPATIAL,
        CHANGE_OTHER,
        CHANGE_OTHER_SIMPLE,
        CHANGE_OTHER_MATERIALOVERRIDES,
        CHANGE_OTHER_MATERIALMODIFIERS,
        CHANGE_OTHER_PRIMITIVES_LIST,
        CHANGE_OTHER_PRIMITIVES_UPDATED,
        CHANGE_OTHER_DECALS,
        CHANGE_OTHER_RENDERGROUP,
        CHANGE_OTHER_UNIQUEID,
        CHANGE_OTHER_COMPOSITEDATA_LIST,
        CHANGE_OTHER_COMPOSITEDATA_CLOAK,
        CHANGE_OTHER_COMPOSITEDATA_TESSELATIONTITAN,
        CHANGE_OTHER_COMPOSITEDATA_FIRSTPERSONAUG,
        CHANGE_OTHER_COMPOSITEDATA_WATER,
        CHANGE_OTHER_SHADOW,
        CHANGE_OTHER_COMPOSITEDATA_ANY,
        CHANGE_LIFECYCLE_ANY,
        CHANGE_SPATIAL_ANY,
        CHANGE_OTHER_ANY,
        CHANGE_FULL,
    };
    virtual ~ZReflectable();
    void Init();
    void Uninit();
    void ReinitializeMembers();
    virtual ZEntityRef GetRenderableEntityRef();
    virtual IRenderGraphNodeReflector *GetReflector();
    virtual void InitializeType();
    ZRenderGraphNode *GetRenderGraphData();
    void SetRenderGraphData(ZRenderGraphNode *pData);
    void ReflectPre(SRenderReflectData &reflectData);
    void SetChangedFlag(uint32_t nChangedFlag);
    void ClearChangedFlag(uint32_t flagsToClear);
    uint32_t GetChangedFlag();
    EReflectableType GetType();
    void SetRenderSceneMask(uint64_t nRenderSceneMask);
    uint64_t GetRenderSceneMask();
    bool GetUseColorModifier();
    void SetColorModifier(uint32_t nColorModifier);
    uint32_t GetColorModifier();
    bool GetUseTimeAlive();
    void SetTimeAlive(float fTimeAlive);
    float GetTimeAlive();
    ZString GetRenderableEntityDebugName();
    ZString GetRenderableEntityDebugNameScenePath();
    virtual void OnChangeSpatialFlagSet();
    void SetAndClearLifeCycleFlag(uint32_t flagsToSet, uint32_t flagsToClear);
    void SetTypeInternal(EReflectableType reflectableType);
    uint64_t m_nRenderSceneMask;
    ZRenderGraphNode *m_pRenderGraphData;
    uint32_t m_nColorModifier;
    float m_fTimeAlive;
    int32_t m_nChangedFlag;
    uint32_t m_nType;
    bool m_bUseColorModifier;
    bool m_bUseTimeAlive;
};

class IRenderGraphNodeReflector : public IComponentInterface {
    public:
    static void RegisterType();
    enum EFlags {
        EFlags_SupportsReflect,
        EFlags_SupportsReflectPre,
        EFlags_None,
    };
    virtual void OnReflectableInitialized(ZReflectable *);
    virtual void OnReflectableChangedSpatial(ZReflectable *);
    virtual void OnReflectableChangedOther(ZReflectable *);
    virtual void OnReflectableUninitialized(ZReflectable *);
    virtual uint64_t GetGraphNodesInstanceCount();
    virtual EReflectableType GetReflectableType();
    virtual EReflectableType GetParentReflectableType();
    virtual void GetAllNodes(TArray<ZRenderGraphNode *> &);
    virtual void GetAllStaticNodes(TArray<ZRenderGraphNode *> &);
    virtual bool HasStaticNodes();
    virtual void GetAllDynamicNodes(TArray<ZRenderGraphNode *> &);
    virtual bool HasDynamicNodes(uint64_t);
    virtual void GetNodesNeedingReflect(TArray<ZRenderGraphNode *> &);
    virtual void BeginSyncWindow();
    virtual void EndSyncWindow();
    virtual void Clear();
    virtual void DeleteRemovedRGNs();
    virtual void Trim();
    virtual uint64_t PrepareReflectPre();
    virtual uint64_t PrepareReflectPreJobs(TArray<SRenderReflectData> &, uint64_t,
                                           ZJobChainThread *, TArray<ZJobThread *> &);
    virtual void ExecuteReflectPreJob(uint64_t, uint64_t, TArrayRef<SRenderReflectData> &);
    virtual void Reflect(ZJobChainThread *);
    virtual void ReflectBase();
};

class TRenderReferencedCountedImplBase {
    public:
    static void SwapDeletedResourceList(TArray<IRenderRefCount *> &destinationList);
    static void AppendDeletedResourceList(TArray<IRenderRefCount *> &destinationList);
    static void PushToDeletedResourceList(IRenderRefCount *resource);
    static ZMutex s_deletedResourcesMutex;
    static TArray<IRenderRefCount *> s_deletedResources;
};

template <typename T, size_t N>
class TRenderReferencedCountedImpl : public TRenderReferencedCountedImplBase,
                                     public IRenderResource {
    public:
    TRenderReferencedCountedImpl<T, N>(const TRenderReferencedCountedImpl<T, N> &);
    TRenderReferencedCountedImpl<T, N>();
    virtual void AddRef();
    virtual int32_t Release(bool delayed);
    virtual ~TRenderReferencedCountedImpl<T, N>();
    TRenderReferencedCountedImpl<T, N> &operator=(const TRenderReferencedCountedImpl<T, N> &);
    std::atomic<int> m_ReferenceCount;
};

class RenderReferencedCountedBaseStub : public IRenderRefCount {
    public:
    virtual ~RenderReferencedCountedBaseStub();
};

enum ERenderInputElement {
    RENDER_INPUT_ELEMENT_POSITION,
    RENDER_INPUT_ELEMENT_NORMAL,
    RENDER_INPUT_ELEMENT_TANGENT,
    RENDER_INPUT_ELEMENT_BINORMAL,
    RENDER_INPUT_ELEMENT_BLENDWEIGHT,
    RENDER_INPUT_ELEMENT_BLENDINDICES,
    RENDER_INPUT_ELEMENT_TEXCOORD,
    RENDER_INPUT_ELEMENT_COLOR,
    RENDER_INPUT_ELEMENT_SV_POSITION,
    RENDER_INPUT_ELEMENT_TESSNORMAL,
    RENDER_INPUT_ELEMENT_EDGENORMAL,
};

enum ERenderInputClassification {
    RENDER_INPUT_CLASSIFICATION_PER_VERTEX,
    RENDER_INPUT_CLASSIFICATION_PER_INSTANCE,
};

struct SRenderInputElementDesc {
    uint32_t nOffset;
    ERenderFormat eFormat;
    ERenderInputElement eElement;
    uint8_t nElementIndex;
    uint8_t nStreamIndex;
    ERenderInputClassification eClassification;
    uint32_t nInstanceDataStepRate;
};

class ZRenderInputLayout : public TRenderReferencedCountedImpl<RenderReferencedCountedBaseStub, 0> {
    public:
    ZRenderInputLayout(const SRenderInputElementDesc *pElements, uint64_t nNumElements);
    virtual ~ZRenderInputLayout();
    uint64_t GetElementCount();
    const SRenderInputElementDesc &GetElement(uint64_t index);
    TArray<SRenderInputElementDesc> m_Elements;
};

template <typename T, size_t N>
class TRenderResourceImpl : public TRenderReferencedCountedImpl<T, 0> {
    public:
    virtual ERenderResourceType GetResourceType();
};

struct SRenderDrawStats {
    uint32_t m_nNumDIPs;
    uint32_t m_nNumTriangles;
    uint32_t m_nNumClears;
};

class ZRenderCommandListBase {
    public:
    ZRenderCommandListBase();
    ~ZRenderCommandListBase();
    const SRenderDrawStats &GetStats();
    void AddToStats(uint32_t nClears, uint32_t nDIPs, uint32_t nTriangles);
    void ClearStats();
    SRenderDrawStats m_Stats;
};

class ZRenderCommandList : public ZRenderCommandListBase {
    public:
    static const uint32_t SANITY_REF;
    ZRenderCommandList();
    virtual ~ZRenderCommandList();
    virtual void Begin();
    virtual void End();
    bool IsFinishedByGpu();
    bool IsFlushed();
    uint32_t *m_pSanityValue;
};

enum ERenderResourceMipInterpolation {
    eRENDER_RESOURCE_MIP_INTERPOLATION_NONE,
    eRENDER_RESOURCE_MIP_INTERPOLATION_POINT,
    eRENDER_RESOURCE_MIP_INTERPOLATION_LINEAR,
    eRENDER_RESOURCE_MIP_INTERPOLATION_COUNT,
};

enum ERenderResourceUsage {
    RENDER_RESOURCE_USAGE_DEFAULT,
    RENDER_RESOURCE_USAGE_IMMUTABLE,
    RENDER_RESOURCE_USAGE_DYNAMIC,
    RENDER_RESOURCE_USAGE_STAGING,
};

struct SRenderTexture3DDesc {
    uint64_t nResourceMemoryScopeID;
    ZString sName;
    uint32_t nWidth;
    uint32_t nHeight;
    uint32_t nDepth;
    uint32_t nMipLevels;
    uint32_t nBindFlags;
    uint32_t nMiscFlags;
    uint32_t nCPUAccessFlags;
    ERenderFormat eFormat;
    ERenderResourceUsage eUsage;
    ERenderResourceMipInterpolation eMipInterpolation;
    SRenderTexture3DDesc();
};

struct SRenderMappedTexture3D {
    void *pData;
    uint32_t nRowPitch;
    uint32_t nSlicePitch;
};

enum ERenderResourceMapType {
    RENDER_RESOURCE_MAP_NONE,
    RENDER_RESOURCE_MAP_READ,
    RENDER_RESOURCE_MAP_WRITE,
    RENDER_RESOURCE_MAP_READ_WRITE,
    RENDER_RESOURCE_MAP_WRITE_DISCARD,
    RENDER_RESOURCE_MAP_WRITE_NO_OVERWRITE,
};

class ZRenderTexture3D : public TRenderResourceImpl<IRenderResource, 2> {
    public:
    ZRenderTexture3D(const SRenderTexture3DDesc *pDescription);
    virtual ~ZRenderTexture3D();
    static void GetMemoryRequirements(const SRenderTexture3DDesc &desc, uint32_t &neededSize,
                                      uint32_t &neededAlignment);
    const SRenderTexture3DDesc &GetDesc();
    bool IsDepthStencilTexture();
    bool IsRenderTargetTexture();
    virtual void Map(ZRenderDeviceContext *deviceContext, ERenderResourceMapType eMapType,
                     SRenderMappedTexture3D *pMappedTexture, uint32_t nSubResource);
    virtual void Unmap(ZRenderDeviceContext *deviceContext, uint32_t nSubResource);
    const SRenderTexture3DDesc m_Description;
};

struct SRenderTargetViewDesc {
    enum EViewDimension {
        VIEW_DIMENSION_TEXTURE2D,
        VIEW_DIMENSION_TEXTURE2DMS,
        VIEW_DIMENSION_TEXTURE2DARRAY,
        VIEW_DIMENSION_TEXTURE3D,
    };
    struct SViewTexture2D {
        uint32_t nMipSlice;
    };
    struct SViewTexture2DArray {
        uint32_t nMipSlice;
        uint32_t nFirstArraySlice;
        uint32_t nArraySize;
    };
    struct SViewTexture3D {
        uint32_t nMipSlice;
        uint32_t nFirstWSlice;
        uint32_t nWSize;
    };
    ERenderFormat eFormat;
    SRenderTargetViewDesc::EViewDimension eViewDimension;
    SRenderTargetViewDesc::SViewTexture2D viewTexture2D;
    SRenderTargetViewDesc::SViewTexture2DArray viewTexture2DArray;
    SRenderTargetViewDesc::SViewTexture3D viewTexture3D;
    SRenderTargetViewDesc();
};

enum ERenderViewType {
    RENDER_VIEW_TYPE_SHADER_RESOURCE_VIEW,
    RENDER_VIEW_TYPE_RENDER_TARGET_VIEW,
    RENDER_VIEW_TYPE_DEPTH_STENCIL_VIEW,
    RENDER_VIEW_TYPE_UNORDERED_ACCESS_VIEW,
};

class ZRenderTargetView3D
    : public TRenderReferencedCountedImpl<RenderReferencedCountedBaseStub, 0> {
    public:
    ZRenderTargetView3D(const SRenderTargetViewDesc *pDescription, ZRenderTexture3D *pTexture);
    virtual ~ZRenderTargetView3D();
    ERenderViewType GetViewType();
    ZRenderTexture3D *GetTexture();
    const SRenderTargetViewDesc &GetDesc();
    const SRenderTargetViewDesc m_Description;
    ZRenderTexture3D *m_pTexture;
};

struct SRenderSampleDesc {
    uint32_t nCount;
    uint32_t nQuality;
};

enum ERenderResourcePriority {
    eRENDER_RESOURCE_PRIORITY_NORMAL,
    eRENDER_RESOURCE_PRIORITY_CRUCIAL,
};

struct SRenderTexture2DDesc {
    uint64_t nResourceMemoryScopeID;
    ZString sName;
    uint32_t nWidth;
    uint32_t nHeight;
    uint32_t nMipLevels;
    uint32_t nArraySize;
    ERenderFormat eFormat;
    SRenderSampleDesc sampleDesc;
    uint32_t nBindFlags;
    uint32_t nMiscFlags;
    uint32_t nCPUAccessFlags;
    ERenderResourceUsage eUsage;
    ERenderResourcePriority ePriority;
    ERenderResourceMipInterpolation eMipInterpolation;
    SRenderTexture2DDesc();
};

enum ERenderPlaneUsage {
    RENDER_PLANE_USAGE_UNUSED,
    RENDER_PLANE_USAGE_DEFAULT,
    RENDER_PLANE_USAGE_STENCIL,
    RENDER_PLANE_USAGE_CMASK,
    RENDER_PLANE_USAGE_FMASK,
    RENDER_PLANE_USAGE_HTILE,
};

struct SRenderPlane {
    ERenderPlaneUsage m_eUsage;
    uint32_t m_nOffset;
    uint32_t m_nSize;
    bool IsTexture();
    bool IsBuffer();
};

struct SRenderMappedTexture2D {
    void *pData;
    uint32_t nRowPitch;
};

class ZRenderTexture2D : public TRenderResourceImpl<IRenderResource, 1> {
    public:
    ZRenderTexture2D(const SRenderTexture2DDesc *pDescription);
    virtual ~ZRenderTexture2D();
    static void GetMemoryRequirements(const SRenderTexture2DDesc &desc, uint32_t &neededSize,
                                      uint32_t &neededAlignment);
    const SRenderTexture2DDesc &GetDesc();
    bool IsSwapChainTexture();
    bool IsDepthStencilTexture();
    bool IsRenderTargetTexture();
    bool HasPlane(ERenderPlaneUsage usage);
    virtual const SRenderPlane *GetPlaneByUsage(ERenderPlaneUsage);
    virtual uint32_t GetPlaneSize(ERenderPlaneUsage);
    virtual void *GetGPUBuffer(uint32_t subresource);
    virtual void Map(ZRenderDeviceContext *deviceContext, ERenderResourceMapType eMapType,
                     SRenderMappedTexture2D *pMappedTexture, uint32_t nSubResource);
    virtual void Unmap(ZRenderDeviceContext *deviceContext, uint32_t nSubResource);
    uint32_t GetIADataSize();
    const uint8_t *GetIAData();
    void SetIAData(uint32_t nSize, const uint8_t *pData);
    const SRenderTexture2DDesc m_Description;
    const uint8_t *m_pIAData;
    uint32_t m_nIADataSize;
};

struct SRenderDepthStencilViewDesc {
    SRenderDepthStencilViewDesc();
    enum EViewDimension {
        VIEW_DIMENSION_TEXTURE2D,
        VIEW_DIMENSION_TEXTURE2DMS,
    };
    struct SViewTexture2D {
        uint32_t nMipSlice;
    };
    ERenderFormat eFormat;
    bool bReadOnly;
    SRenderDepthStencilViewDesc::EViewDimension eViewDimension;
    SRenderDepthStencilViewDesc::SViewTexture2D viewTexture2D;
};

// TODO: Is it really empty?
class ZRenderDevice {};

class ZRenderDepthStencilViewBase
    : public TRenderReferencedCountedImpl<RenderReferencedCountedBaseStub, 0> {
    public:
    ZRenderDepthStencilViewBase();
    ZRenderDepthStencilViewBase(const SRenderDepthStencilViewDesc *pDescription,
                                ZRenderTexture2D *pTexture, ZRenderDevice *pRenderDevice);
    virtual ~ZRenderDepthStencilViewBase();
    ERenderViewType GetViewType();
    ZRenderTexture2D *GetTexture();
    const SRenderDepthStencilViewDesc &GetDesc();
    SRenderDepthStencilViewDesc m_Description;
    ZRenderTexture2D *m_pTexture;
    ZRenderDevice *m_pRenderDevice;
};

class ZRenderTargetView : public TRenderReferencedCountedImpl<RenderReferencedCountedBaseStub, 0> {
    public:
    ZRenderTargetView(const SRenderTargetViewDesc *pDescription, ZRenderTexture2D *pTexture);
    virtual ~ZRenderTargetView();
    ERenderViewType GetViewType();
    ZRenderTexture2D *GetTexture();
    const SRenderTargetViewDesc &GetDesc();
    uint32_t GetViewWidth();
    uint32_t GetViewHeight();
    const SRenderTargetViewDesc m_Description;
    ZRenderTexture2D *m_pTexture;
};

class ZRenderDepthStencilView : public ZRenderDepthStencilViewBase {
    public:
    ZRenderDepthStencilView(const SRenderDepthStencilViewDesc *pDescription,
                            ZRenderTexture2D *pTexture, ZRenderDevice *pRenderDevice);
    virtual ~ZRenderDepthStencilView();
};

struct SRenderUnorderedAccessViewDesc {
    enum EViewDimension {
        UAV_DIMENSION_UNKNOWN,
        UAV_DIMENSION_BUFFER,
        UAV_DIMENSION_TEXTURE2D,
        UAV_DIMENSION_TEXTURE2DARRAY,
        UAV_DIMENSION_TEXTURE3D,
    };
    struct SUAVBuffer {
        enum EUAVFlag {
            BUFFER_UAV_FLAG_NONE,
            BUFFER_UAV_FLAG_RAW,
            BUFFER_UAV_FLAG_APPEND,
        };
        uint32_t nFirstElement;
        uint32_t nNumElements;
        SRenderUnorderedAccessViewDesc::SUAVBuffer::EUAVFlag eFlags;
    };
    struct SUAVTexture2D {
        uint32_t nMipSlice;
    };
    struct SUAVTexture2DArray {
        uint32_t nMipSlice;
        uint32_t nFirstArraySlice;
        uint32_t nArraySize;
    };
    struct SUAVTexture3D {
        uint32_t nMipSlice;
    };
    ERenderFormat eFormat;
    SRenderUnorderedAccessViewDesc::EViewDimension eViewDimension;
    ERenderPlaneUsage ePlane;
    SRenderUnorderedAccessViewDesc::SUAVBuffer UAVBuffer;
    SRenderUnorderedAccessViewDesc::SUAVTexture2D UAVTexture2D;
    SRenderUnorderedAccessViewDesc::SUAVTexture2DArray UAVTexture2DArray;
    SRenderUnorderedAccessViewDesc::SUAVTexture3D UAVTexture3D;
    SRenderUnorderedAccessViewDesc();
};

class ZRenderUnorderedAccessView
    : public TRenderReferencedCountedImpl<RenderReferencedCountedBaseStub, 0> {
    public:
    ZRenderUnorderedAccessView(const SRenderUnorderedAccessViewDesc *pDescription,
                               IRenderResource *pResource);
    virtual ~ZRenderUnorderedAccessView();
    ERenderViewType GetViewType();
    const SRenderUnorderedAccessViewDesc &GetDesc();
    IRenderResource *GetResource();
    virtual void Clear(ZRenderDeviceContext *, uint32_t, const uint32_t *, const float *);
    const SRenderUnorderedAccessViewDesc m_Description;
    IRenderResource *m_pResource;
};

struct SRenderBufferDescription {
    ZString sName;
    uint32_t nSize;
    uint32_t nBindingFlags;
    uint32_t nCPUAccessFlags;
    uint32_t nMiscFlags;
    uint32_t nStructureByteStride;
    ERenderResourceUsage eUsage;
    SRenderBufferDescription();
};

struct SRenderMappedBuffer {
    void *pData;
    uint32_t nRowPitch;
};

class ZRenderBuffer : public TRenderResourceImpl<IRenderResource, 3> {
    public:
    ZRenderBuffer(const SRenderBufferDescription *pDescription);
    virtual ~ZRenderBuffer();
    static void GetMemoryRequirements(const SRenderBufferDescription &desc, uint32_t &neededSize,
                                      uint32_t &neededAlignment);
    const SRenderBufferDescription &GetDesc();
    virtual bool IsValid();
    virtual void Map(ZRenderDeviceContext *, ERenderResourceMapType, SRenderMappedBuffer *,
                     uint32_t);
    virtual void Unmap(ZRenderDeviceContext *, uint32_t);
    virtual const void *GetCPUBuffer();
    const SRenderBufferDescription m_Description;
};

struct SRenderDeviceContextVertexBuffer {
    ZRenderBuffer *buffer;
    uint32_t stride;
    uint32_t offset;
    SRenderDeviceContextVertexBuffer(ZRenderBuffer *buffer_, uint32_t stride_, uint32_t offset_);
    SRenderDeviceContextVertexBuffer();
    bool operator==(const SRenderDeviceContextVertexBuffer &right);
    bool operator!=(const SRenderDeviceContextVertexBuffer &right);
};

struct SRenderDeviceContextIndexBuffer {
    ZRenderBuffer *buffer;
    ERenderFormat format;
    uint32_t offset;
    SRenderDeviceContextIndexBuffer(ZRenderBuffer *buffer_, ERenderFormat format_,
                                    uint32_t offset_);
    SRenderDeviceContextIndexBuffer();
    bool operator==(const SRenderDeviceContextIndexBuffer &right);
    bool operator!=(const SRenderDeviceContextIndexBuffer &right);
};

struct SStringID {
    uint32_t key;
    uint32_t regIdx;
    SStringID();
    operator unsigned int();
    static SStringID Build(const char *);
    static uint32_t Register(const char *, uint64_t, uint32_t);
    static void InitializeRegistry();
    static void CleanupRegistry();
};

struct SRenderConstDesc {
    SStringID stridName;
    uint16_t nSize;
    uint16_t nOffset;
    uint8_t viewDimension;
    SRenderConstDesc();
};

enum ERenderFilterPreset {
    MIN_MAG_MIP_POINT,
    MIN_MAG_POINT_MIP_LINEAR,
    MIN_POINT_MAG_LINEAR_MIP_POINT,
    MIN_POINT_MAG_MIP_LINEAR,
    MIN_LINEAR_MAG_MIP_POINT,
    MIN_LINEAR_MAG_POINT_MIP_LINEAR,
    MIN_MAG_LINEAR_MIP_POINT,
    MIN_MAG_MIP_LINEAR,
    ANISOTROPIC,
};

struct SRenderSamplerStateDesc {
    uint8_t minFilter;
    uint8_t magFilter;
    uint8_t mipFilter;
    uint8_t anisotropic;
    uint8_t _filter_pad;
    uint8_t anisotropyBias;
    uint8_t addressU;
    uint8_t addressV;
    uint8_t addressW;
    uint8_t mipClampRange;
    int8_t maxAnisotropy;
    uint8_t borderColorR;
    uint8_t borderColorG;
    uint8_t borderColorB;
    uint8_t borderColorA;
    float mipLODBias;
    SRenderSamplerStateDesc();
    ERenderFilterPreset GetFilterPreset();
    void SetFilterPreset(ERenderFilterPreset preset);
    void SetAnisotropyBias(float fBias);
};

class ZRenderSamplerStateBase {
    public:
    ZRenderSamplerStateBase(const SRenderSamplerStateDesc *pDescription);
    virtual ~ZRenderSamplerStateBase();
    const SRenderSamplerStateDesc &GetDesc();
    SRenderSamplerStateDesc m_Description;
};

class ZRenderSamplerState : public ZRenderSamplerStateBase {
    public:
    ZRenderSamplerState(const SRenderSamplerStateDesc *pDescription);
    virtual ~ZRenderSamplerState();
};

struct SRenderSamplerDesc : public SRenderConstDesc {
    SRenderSamplerDesc();
    ZRenderSamplerState *pSamplerState;
};

struct SRenderConstBufferDesc {
    SRenderConstBufferDesc();
    uint32_t nGuid;
    uint16_t nConstSize;
    uint16_t nWaste;
    uint16_t nNumConstants;
    uint16_t nNumSamplers;
    uint16_t nNumTextures;
    uint16_t nNumUAVs;
    static uint64_t GetAllocSize(uint16_t nNumConstants, uint16_t nNumSamplers,
                                 uint16_t nNumTextures, uint16_t nNumUAVs);
    uint8_t *GetDescriptorsPtr();
    uint64_t GetConstantDescriptorsOffset(uint64_t idx);
    uint64_t GetSamplerDescriptorsOffset(uint64_t idx);
    uint64_t GetTextureDescriptorsOffset(uint64_t idx);
    uint64_t GetUAVDescriptorsOffset(uint64_t idx);
    const SRenderConstDesc *GetConstantDesc(uint64_t idx);
    const SRenderSamplerDesc *GetSamplerDesc(uint64_t idx);
    const SRenderConstDesc *GetTextureDesc(uint64_t idx);
    const SRenderConstDesc *GetUAVDesc(uint64_t idx);
    void SetSamplerState(uint64_t, ZRenderSamplerState *);
};

union SFXFixupString {
    const char *pStr;
    uint64_t iStr;
};

struct SFXInputParams {
    SFXFixupString semanticName;
    uint8_t semanticIndex;
};

struct SFXConstantParams {
    SFXFixupString name;
    uint16_t offset;
    uint16_t size;
    uint8_t flags;
};

struct SFXConstantBufferParams {
    SFXFixupString name;
    uint16_t size;
    uint8_t bindPoint;
    uint8_t flags;
    uint16_t numConstants;
    uint16_t __pad16;
    uint32_t offConstants;
    const SFXConstantParams *GetConstantParams(uint64_t index);
};

struct SFXSamplerParams {
    SFXFixupString name;
    uint8_t bindPoint;
};

struct SFXTextureParams {
    SFXFixupString name;
    uint8_t bindPoint;
    uint8_t viewDimension;
};

struct SFXUAVParams {
    SFXFixupString name;
    uint8_t bindPoint;
    uint8_t viewDimension;
};

struct SFXReflectionTable {
    uint16_t instructionCount;
    uint8_t tesselatorDomain;
    uint8_t hsPartitioning;
    uint16_t numInputParams;
    uint16_t numConstantBufferParams;
    uint16_t numTextureParams;
    uint16_t numSamplerParams;
    uint16_t numUAVParams;
    uint16_t __pad16;
    uint32_t offInputParams;
    uint32_t offConstantBufferParams;
    uint32_t offTextureParams;
    uint32_t offSamplerParams;
    uint32_t offUAVParams;
    uint32_t __pad32;
    const SFXInputParams *GetInputParams(uint64_t);
    const SFXConstantBufferParams *GetConstantBufferParams(uint64_t index);
    const SFXSamplerParams *GetSamplerParams(uint64_t index);
    const SFXTextureParams *GetTextureParams(uint64_t index);
    const SFXUAVParams *GetUAVParams(uint64_t index);
};

struct SFXProgramHeader {
    uint32_t nNameOffset;
    uint32_t nCachedSize;
    uint32_t nCachedOffset;
    uint32_t nPhysicalSize;
    uint32_t nPhysicalOffset;
    uint32_t nIsPixelShader;
    uint32_t nConstantDescOffset;
    uint32_t nNumConstants;
    uint32_t nStartRegister;
    uint32_t nConstantsSize;
    uint32_t nSamplerDescOffset;
    uint32_t nNumSamplers;
    uint32_t nIASignature;
    uint32_t nRfxIndex;
};

enum ERenderShaderType {
    RENDER_SHADER_FRAGMENT,
    RENDER_SHADER_VERTEX,
    RENDER_SHADER_HULL,
    RENDER_SHADER_DOMAIN,
    RENDER_SHADER_GEOMETRY,
    RENDER_SHADER_COMPUTE,
    RENDER_SHADER_Count,
};

struct SShaderBinParams {
    const SFXReflectionTable *pReflectionTable;
    const uint8_t *pEffect;
    const SFXProgramHeader *pShaderHeader;
    const char *pDebugname;
    uint32_t nIASignatureOffset;
    ERenderShaderType ShaderType;
};

class ZRenderVideoRAMAllocator {
    public:
    enum EAllocatorPolicy {
        FirstFit,
        PreviousFit,
    };
    ZRenderVideoRAMAllocator(ZRenderVideoRAMAllocator::EAllocatorPolicy policy);
    ~ZRenderVideoRAMAllocator();
    void Initialize(void *memPool, uint32_t memPoolSize, uint32_t memBlockGranularity,
                    const ZString &sDebugName);
    void *Allocate(uint32_t nSize, uint32_t nAlignment, bool longLife, bool disableOOMMessage);
    void FreeAligned(void *nVirtualAddress);
    bool IsEmpty();
    void *GetBufferPtr();
    uint32_t GetBufferSize();
    bool IsInRange(void *nAddress);
    uint32_t GetAllocatedSize();
    void *AllocatePrevious(uint32_t nSize, uint32_t nAlignment, bool disableOOMMessage);
    void *AllocateFirst(uint32_t nSize, uint32_t nAlignment, bool disableOOMMessage);
    void *AllocateLast(uint32_t nSize, uint32_t nAlignment, bool disableOOMMessage);
    struct BlockRange {
        BlockRange(uint32_t beginBlock, uint32_t endBlock);
        uint32_t m_head;
        uint32_t m_tail;
    };
    ZRenderVideoRAMAllocator::EAllocatorPolicy m_policy;
    void *m_memPool;
    uint32_t m_memPoolSize;
    uint32_t m_memBlockGranularity;
    std::vector<ZRenderVideoRAMAllocator::BlockRange,
                std::allocator<ZRenderVideoRAMAllocator::BlockRange>>
        m_freeMemBlocks;
    std::vector<ZRenderVideoRAMAllocator::BlockRange,
                std::allocator<ZRenderVideoRAMAllocator::BlockRange>>
        m_allocatedBlocks;
    ZSpinlock m_lock;
    ZString m_sDebugName;
};

class ZRenderShaderDX12 {
    public:
    ZRenderShaderDX12(const uint8_t *byteCode, uint64_t byteCodeSize);
    ~ZRenderShaderDX12();
    static const D3D12_SHADER_BYTECODE InvalidShader;
    D3D12_SHADER_BYTECODE m_desc;
    uint32_t m_crc32;
};

struct SRenderShaderBinData {
    SRenderShaderBinData();
    SRenderShaderBinData(uint32_t CRC32, const SShaderBinParams &params,
                         ZRenderVideoRAMAllocator *pHeaderAllocator,
                         ZRenderVideoRAMAllocator *pBinaryAllocator);
    ~SRenderShaderBinData();
    int32_t IncRef();
    int32_t DecRef();
    void *pFetchShader;
    uint32_t nFetchShaderSize;
    uint32_t nCRC32;
    uint32_t nGPUSize;
    uint32_t nCPUSize;
    std::atomic<int> nRefCount;
    uint16_t nCBMask;
    uint16_t nInstructionCount;
    uint8_t nGlobalOffset;
    uint8_t nShaderType;
    void *pShaderAliased;
    ID3D11PixelShader *pPixelShader;
    ID3D11GeometryShader *pGeometryShader;
    ID3D11VertexShader *pVertexShader;
    ID3D11HullShader *pHullShader;
    ID3D11DomainShader *pDomainShader;
    ID3D11ComputeShader *pComputeShader;
    ZRenderShaderDX12 *pDX12Shader;
};

class ZRenderShader {
    public:
    ZRenderShader(ERenderShaderType shaderType);
    ~ZRenderShader();
    static SRenderConstBufferDesc NullConstBufferDesc;
    SRenderConstBufferDesc *pConstBufferDesc;
    const uint8_t nShaderType;
    uint32_t GetInstructionCount();
    ERenderShaderType GetShaderType();
    const SRenderConstBufferDesc &GetConstBufferDesc();
    uint32_t GetCRC32();
    uint32_t GetFetchShaderSize();
    uint16_t GetCBMask();
    uint32_t GetGlobalOffset();
    void SetShaderBinData(SRenderShaderBinData *pData);
    void *GetFetchShader();
    ID3D11VertexShader *GetVertexShader();
    ID3D11PixelShader *GetPixelShader();
    ID3D11ComputeShader *GetComputeShader();
    ID3D11HullShader *GetHullShader();
    ID3D11DomainShader *GetDomainShader();
    ID3D11GeometryShader *GetGeometryShader();
    ZRenderShaderDX12 *GetDX12Shader();
    void SetDX12Shader(ZRenderShaderDX12 *pDX12Shader);
    void SetFetchShader(void *pFetchShader);
    void SetFetchShaderSize(uint32_t nFetchShaderSize);
    void SetCBMask(uint16_t nCBMask);
    void SetCRC32(uint32_t nCRC32);
    void SetPixelShader(ID3D11PixelShader *pPixelShader);
    void SetVertexShader(ID3D11VertexShader *pVertexShader);
    void SetComputeShader(ID3D11ComputeShader *pComputeShader);
    SRenderShaderBinData *pShaderBinData;
    void *m_pFetchShaderCode;
    uint32_t m_FetchShaderCodeSize;
    uint32_t m_CRC32;
    ZRenderShaderDX12 *m_pShaderDX12;
    ID3D11PixelShader *m_pPixelShader;
    ID3D11VertexShader *m_pVertexShader;
    ID3D11ComputeShader *m_pComputeShader;
};

enum ERenderDestinationType {
    RENDER_DESTINATION_TYPE_SCREEN,
    RENDER_DESTINATION_TYPE_OFFSCREEN,
};

struct SRenderDestinationDesc {
    SRenderDestinationDesc();
    ZString m_sName;
    uint32_t m_nWidth;
    uint32_t m_nHeight;
    uint64_t m_nWindowHandle;
    uint32_t m_nAdapterID;
    uint32_t m_nMonitorID;
    uint32_t m_nRefreshRate;
    uint64_t m_nParentWindowHandle;
    bool m_bFullScreen;
    bool m_bExclusiveFullscreen;
    bool m_bMaximized;
    int32_t m_nWindowX;
    int32_t m_nWindowY;
    int32_t m_nWindowLeft;
    int32_t m_nWindowTop;
    uint32_t m_nWindowWidth;
    uint32_t m_nWindowHeight;
    uint32_t m_nFullScreenWidth;
    uint32_t m_nFullScreenHeight;
    float m_fAspectRatio;
    uint32_t m_nWindowShowCommand;
    uint32_t m_nMultiSampleCount;
    ERenderFormat m_eFormat;
    ERenderDestinationType m_eDestinationType;
    bool m_bHDRSupported;
    bool m_bHDREnabled;
    float m_minLuminance;
    float m_maxLuminance;
    bool NeedsReset(const SRenderDestinationDesc &rhs);
};

enum ESwapChainType {
    SWAPCHAIN_TYPE_SDR,
    SWAPCHAIN_TYPE_HDR,
    SWAPCHAIN_TYPE_COUNT,
};

class ZRenderBufferPoolD3D12;

struct SConstBufferImpl {
    static SConstBufferImpl *CreateConstBuffer(uint64_t nSize, uint64_t nConstBufferSlotSize);
    static void Release(SConstBufferImpl *pConstBufferImpl);
    struct NodeResourceDX12 {
        ID3D12Resource *m_pResource;
        ZRenderBufferPoolD3D12 *m_pPool;
        uint64_t m_nOffset;
        uint64_t m_nBufferLocation;
    };
    ID3D11Buffer *m_pBuffer;
    SConstBufferImpl::NodeResourceDX12 *m_pDx12NodeResources;
    uint64_t m_nTransientBufferLocation;
    uint32_t m_nSize;
    uint32_t m_nInternalSize;
    uint32_t m_CRC32;
    uint32_t m_bPermanent;
    uint32_t m_bMapped;
    uint32_t m_nFrameIndex;
    uint16_t m_nBytesWasted;
    uint16_t m_pad1;
    uint64_t m_pad2;
    void ValidateTempAlloc();
    char *Backbuffer();
    virtual void UpdateSubresource(ZRenderDeviceContext *deviceContext);
    SConstBufferImpl();
    virtual ~SConstBufferImpl();
};

struct ZRenderConstBufferRef {
    ZRenderConstBufferRef();
    static ZRenderConstBufferRef Null();
    void Release();
    void Clear();
    char *MapPermanent(uint64_t nSize, uint16_t nWaste);
    char *MapTemp(ZRenderShader *pShader);
    char *MapTemp(uint64_t nSize, uint16_t nWaste);
    char *MapTempPatch(ZRenderConstBufferRef cb);
    void Unmap(ZRenderDeviceContext *deviceContext);
    void ComputeCRC32();
    uint32_t GetCRC32();
    void *GetLastBuffer();
    uint64_t GetLastBufferSize();
    operator const bool();
    void DumpHex();
    void DumpFloats();
    SConstBufferImpl *m_pImpl;
};

enum ERenderCompareFunc {
    RENDER_COMPARISON_NEVER,
    RENDER_COMPARISON_LESS,
    RENDER_COMPARISON_EQUAL,
    RENDER_COMPARISON_LESS_EQUAL,
    RENDER_COMPARISON_GREATER,
    RENDER_COMPARISON_NOT_EQUAL,
    RENDER_COMPARISON_GREATER_EQUAL,
    RENDER_COMPARISON_ALWAYS,
};

enum ERenderPrimitiveTopology {
    RENDER_PRIMITIVE_TOPOLOGY_POINT_LIST,
    RENDER_PRIMITIVE_TOPOLOGY_LINE_LIST,
    RENDER_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
    RENDER_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
    RENDER_PRIMITIVE_TOPOLOGY_QUAD_LIST_EX,
    RENDER_PRIMITIVE_TOPOLOGY_RECT_LIST_EX,
    RENDER_PRIMITIVE_TOPOLOGY_PATCHLIST_OFFSET,
    RENDER_PRIMITIVE_TOPOLOGY_1_CONTROL_POINT_PATCHLIST,
    RENDER_PRIMITIVE_TOPOLOGY_2_CONTROL_POINT_PATCHLIST,
    RENDER_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST,
    RENDER_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST,
};

struct SRenderShaderResourceViewDesc {
    enum EViewDimension {
        VIEW_DIMENSION_UNKNOWN,
        VIEW_DIMENSION_BUFFER,
        VIEW_DIMENSION_TEXTURE1D,
        VIEW_DIMENSION_TEXTURE1DARRAY,
        VIEW_DIMENSION_TEXTURE2D,
        VIEW_DIMENSION_TEXTURE2DARRAY,
        VIEW_DIMENSION_TEXTURE2DMS,
        VIEW_DIMENSION_TEXTURE2DMSARRAY,
        VIEW_DIMENSION_TEXTURE3D,
        VIEW_DIMENSION_TEXTURECUBE,
        VIEW_DIMENSION_TEXTURECUBEARRAY,
    };
    struct SViewBuffer {
        enum ESRVFlag {
            BUFFER_SRV_FLAG_NONE,
            BUFFER_SRV_FLAG_RAW,
        };
        uint32_t nFirstElement;
        uint32_t nNumElements;
        SRenderShaderResourceViewDesc::SViewBuffer::ESRVFlag eFlags;
    };
    struct SViewTexture1D {
        uint32_t nMostDetailedMip;
        uint32_t nMipLevels;
    };
    struct SViewTexture1DArray {
        uint32_t nMostDetailedMip;
        uint32_t nMipLevels;
        uint32_t nFirstArraySlice;
        uint32_t nArraySize;
    };
    struct SViewTexture2D {
        uint32_t nMostDetailedMip;
        uint32_t nMipLevels;
    };
    struct SViewTexture2DArray {
        uint32_t nMostDetailedMip;
        uint32_t nMipLevels;
        uint32_t nFirstArraySlice;
        uint32_t nArraySize;
    };
    struct SViewTexture2DMS {
        uint32_t UnusedField_NothingToDefine;
    };
    struct SViewTexture2DMSArray {
        uint32_t nFirstArraySlice;
        uint32_t nArraySize;
    };
    struct SViewTexture3D {
        uint32_t nMostDetailedMip;
        uint32_t nMipLevels;
    };
    struct SViewTextureCube {
        uint32_t nMostDetailedMip;
        uint32_t nMipLevels;
    };
    struct SViewTextureCubeArray {
        uint32_t nMostDetailedMip;
        uint32_t nMipLevels;
        uint32_t nFirst2DArrayFace;
        uint32_t nNumCubes;
    };
    ERenderFormat eFormat;
    SRenderShaderResourceViewDesc::EViewDimension eViewDimension;
    ERenderResourceMipInterpolation eMipInterpolation;
    ERenderPlaneUsage ePlane;
    SRenderShaderResourceViewDesc::SViewBuffer viewBuffer;
    SRenderShaderResourceViewDesc::SViewTexture1D viewTexture1D;
    SRenderShaderResourceViewDesc::SViewTexture1DArray viewTexture1DArray;
    SRenderShaderResourceViewDesc::SViewTexture2D viewTexture2D;
    SRenderShaderResourceViewDesc::SViewTexture2DArray viewTexture2DArray;
    SRenderShaderResourceViewDesc::SViewTexture2DMS viewTexture2DMS;
    SRenderShaderResourceViewDesc::SViewTexture2DMSArray viewTexture2DMSArray;
    SRenderShaderResourceViewDesc::SViewTexture3D viewTexture3D;
    SRenderShaderResourceViewDesc::SViewTextureCube viewTextureCube;
    SRenderShaderResourceViewDesc::SViewTextureCubeArray viewTextureCubeArray;
    SRenderShaderResourceViewDesc();
};

class ZRenderShaderResourceView
    : public TRenderReferencedCountedImpl<RenderReferencedCountedBaseStub, 0> {
    public:
    ZRenderShaderResourceView(const SRenderShaderResourceViewDesc *pDescription,
                              IRenderResource *pResource);
    virtual ~ZRenderShaderResourceView();
    ERenderViewType GetViewType();
    IRenderResource *GetResource();
    const SRenderShaderResourceViewDesc &GetDesc();
    const SRenderShaderResourceViewDesc m_Description;
    IRenderResource *m_pResource;
};

struct SRenderDeviceContextUAV {
    ZRenderUnorderedAccessView *uav;
    uint32_t initialCount;
    SRenderDeviceContextUAV(ZRenderUnorderedAccessView *uav_, uint32_t initialCount_);
    SRenderDeviceContextUAV();
    bool operator==(const SRenderDeviceContextUAV &right);
    bool operator!=(const SRenderDeviceContextUAV &right);
};

enum ERenderBlendMode {
    RENDER_BLEND_ZERO,
    RENDER_BLEND_ONE,
    RENDER_BLEND_SRC_COLOR,
    RENDER_BLEND_INV_SRC_COLOR,
    RENDER_BLEND_SRC_ALPHA,
    RENDER_BLEND_INV_SRC_ALPHA,
    RENDER_BLEND_DEST_COLOR,
    RENDER_BLEND_INV_DEST_COLOR,
    RENDER_BLEND_DEST_ALPHA,
    RENDER_BLEND_INV_DEST_ALPHA,
    RENDER_BLEND_SRC_ALPHA_SAT,
    RENDER_BLEND_FACTOR,
    RENDER_BLEND_INV_FACTOR,
    RENDER_BLEND_SRC1_COLOR,
    RENDER_BLEND_INV_SRC1_COLOR,
    RENDER_BLEND_SRC1_ALPHA,
    RENDER_BLEND_INV_SRC1_ALPHA,
};

enum ERenderBlendOp {
    RENDER_BLEND_OP_ADD,
    RENDER_BLEND_OP_SUBTRACT,
    RENDER_BLEND_OP_REV_SUBTRACT,
    RENDER_BLEND_OP_MIN,
    RENDER_BLEND_OP_MAX,
};

struct SRenderBlendStateDesc {
    bool bAlphaToCoverageEnable;
    struct BlendStateDesc {
        bool bBlendEnable;
        ERenderBlendMode eSrcBlend;
        ERenderBlendMode eDestBlend;
        ERenderBlendOp eBlendOp;
        ERenderBlendMode eSrcBlendAlpha;
        ERenderBlendMode eDestBlendAlpha;
        ERenderBlendOp eBlendOpAlpha;
        uint8_t nRenderTargetWriteMask;
    };
    SRenderBlendStateDesc::BlendStateDesc mStates[8];
    SRenderBlendStateDesc::BlendStateDesc &operator[](uint64_t nIndex);
    const SRenderBlendStateDesc::BlendStateDesc &operator[](uint64_t nIndex) const;
    SRenderBlendStateDesc();
};

class ZRenderBlendStateBase {
    public:
    ZRenderBlendStateBase(const SRenderBlendStateDesc *pDescription);
    virtual ~ZRenderBlendStateBase();
    const SRenderBlendStateDesc &GetDesc();
    uint8_t GetCrc32Index();
    SRenderBlendStateDesc m_Description;
    uint8_t m_Crc32Index;
};

class ZRenderBlendState : public ZRenderBlendStateBase {
    public:
    ZRenderBlendState(const SRenderBlendStateDesc *pDescription);
    virtual ~ZRenderBlendState();
};

struct SRenderDeviceContextBlendState {
    ZRenderBlendState *blendState;
    float blendFactor;
    uint32_t nSampleMask;
    SRenderDeviceContextBlendState(ZRenderBlendState *blendState_, float blendFactor_,
                                   uint32_t nSampleMask_);
    SRenderDeviceContextBlendState();
    bool operator==(const SRenderDeviceContextBlendState &right);
    bool operator!=(const SRenderDeviceContextBlendState &right);
};

enum ERenderStencilOp {
    RENDER_STENCIL_OP_KEEP,
    RENDER_STENCIL_OP_ZERO,
    RENDER_STENCIL_OP_REPLACE,
    RENDER_STENCIL_OP_INCR_SAT,
    RENDER_STENCIL_OP_DECR_SAT,
    RENDER_STENCIL_OP_INVERT,
    RENDER_STENCIL_OP_INCR,
    RENDER_STENCIL_OP_DECR,
};

struct SRenderDepthStencilStateDesc {
    enum EDepthWriteMask {
        DEPTH_WRITE_MASK_NONE,
        DEPTH_WRITE_MASK_ALL,
    };
    struct SStencilOpDesc {
        ERenderStencilOp eStencilFailOp;
        ERenderStencilOp eStencilDepthFailOp;
        ERenderStencilOp eStencilPassOp;
        ERenderCompareFunc eStencilFunc;
    };
    bool bDepthEnable;
    SRenderDepthStencilStateDesc::EDepthWriteMask eDepthWriteMask;
    ERenderCompareFunc eDepthFunc;
    bool bStencilEnable;
    uint8_t nStencilReadMask;
    uint8_t nStencilWriteMask;
    SRenderDepthStencilStateDesc::SStencilOpDesc frontFace;
    SRenderDepthStencilStateDesc::SStencilOpDesc backFace;
    SRenderDepthStencilStateDesc();
    SRenderDepthStencilStateDesc &EnableDepth(ERenderCompareFunc eCompareFunc, bool bEnableWrite);
    SRenderDepthStencilStateDesc &EnableStencil(ERenderCompareFunc eCompareFunc, uint8_t eWriteMask,
                                                uint8_t eReadMask);
    SRenderDepthStencilStateDesc &SetStencilPassOp(ERenderStencilOp eFrontFaceOp,
                                                   ERenderStencilOp eBackFaceOp);
    SRenderDepthStencilStateDesc &SetStencilFailOp(ERenderStencilOp eFrontFaceOp,
                                                   ERenderStencilOp eBackFaceOp);
    SRenderDepthStencilStateDesc &SetStencilDepthFailOp(ERenderStencilOp eFrontFaceOp,
                                                        ERenderStencilOp eBackFaceOp);
};

class ZRenderDepthStencilState {
    public:
    ZRenderDepthStencilState(const SRenderDepthStencilStateDesc *pDescription);
    virtual ~ZRenderDepthStencilState();
    const SRenderDepthStencilStateDesc &GetDesc();
    const SRenderDepthStencilStateDesc m_Description;
};

struct SRenderDeviceContextDepthStencilState {
    ZRenderDepthStencilState *depthStencilState;
    uint8_t stencilRef;
    SRenderDeviceContextDepthStencilState(ZRenderDepthStencilState *depthStencilState_,
                                          uint8_t stencilRef_);
    SRenderDeviceContextDepthStencilState();
    bool operator==(const SRenderDeviceContextDepthStencilState &right);
    bool operator!=(const SRenderDeviceContextDepthStencilState &right);
};

enum ERenderFillMode {
    RENDER_FILL_MODE_WIRE,
    RENDER_FILL_MODE_SOLID,
};

enum ERenderCullMode {
    RENDER_CULL_NONE,
    RENDER_CULL_FRONT,
    RENDER_CULL_BACK,
    RENDER_CULL_COUNT,
};

struct SRenderRasterizerStateDesc {
    ERenderFillMode eFillMode;
    ERenderCullMode eCullMode;
    bool bFrontCounterClockWise;
    int32_t nDepthBias;
    float fDepthBiasClamp;
    float fSlopeScaledDepthBias;
    bool bDepthClipEnable;
    bool bScissorEnable;
    bool bMultiSampleEnable;
    bool bAntialiasedLineEnable;
    SRenderRasterizerStateDesc();
};

class ZRenderRasterizerStateBase {
    public:
    ZRenderRasterizerStateBase(const SRenderRasterizerStateDesc *pDescription);
    virtual ~ZRenderRasterizerStateBase();
    const SRenderRasterizerStateDesc &GetDesc();
    uint8_t GetCrc32Index();
    SRenderRasterizerStateDesc m_Description;
    uint8_t m_Crc32Index;
};

class ZRenderRasterizerState : public ZRenderRasterizerStateBase {
    public:
    ZRenderRasterizerState(const SRenderRasterizerStateDesc *pDescription);
    virtual ~ZRenderRasterizerState();
};

enum EAntialiasingMode {
    AA_DEFAULT,
    AA_CHECKERBOARD0,
    AA_CHECKERBOARD1,
};

struct SRenderViewport {
    bool IsNull();
    uint32_t x;
    uint32_t y;
    uint32_t w;
    uint32_t h;
};

struct SRenderRect {
    uint32_t x1;
    uint32_t y1;
    uint32_t x2;
    uint32_t y2;
};

class ZRenderPredication : public TRenderReferencedCountedImpl<IRenderRefCount, 0> {
    public:
    ZRenderPredication();
    virtual ~ZRenderPredication();
};

enum class ERenderSyncGpu {
    FULL_BARRIER,
    COLOR_BARRIER,
    GRAPHICS_2_GRAPHICS,
    DEPTH_BARRIER,
    PREPARE_HTILE_COPY,
    GRAPHICS_2_COMPUTE,
    PREPARE_COMPUTE,
    FINALIZE_COMPUTE,
    CHAIN_COMPUTE,
    WRITE_LABEL_FLUSH_CBDB,
    WRITE_LABEL_FLUSH_CBDB_L1_L2,
    PREPARE_UNCOMPRESS,
    ASYNC_GRAPHICS_COLOR_PRODUCER_COMPUTE_CONSUMER,
    ASYNC_COMPUTE_PRODUCE_L2,
    ASYNC_GRAPHICS_CONSUME_L2,
    ASYNC_COMPUTE_CHAIN_COMPUTE,
    ASYNC_WAIT_LABEL_NO_CACHE_FLUSH,
    DURANGO_WAIT_ON_FENCE,
};

enum class ERenderTopEvent {
    INV_L1,
    INV_L1_L2,
    INV_CB,
    INV_DB,
    INV_KCACHE,
    INV_ICACHE,
    COHERENCY_CB,
    COHERENCY_DB,
};

enum class ERenderEopEvent {
    FLUSH_INV_L1,
    FLUSH_INV_L1_L2,
    FLUSH_INV_CB,
    FLUSH_INV_CB_META,
    FLUSH_INV_DB,
    FLUSH_INV_DB_META,
};

enum class ERenderEosEvent {
    CS_DONE,
    PS_DONE,
};

enum class ERenderWriteValueEopEvent {
    NONE,
    INV_L1,
    FLUSH_INV_L1_L2,
};

enum class ERenderCondition {
    EQUAL,
};

enum class ERenderWaitOnAdressLocation {
    ME,
    PFP,
};

struct SRenderDeviceContextCreationParamsBase {
    bool immediateContext;
};

template <typename T, size_t N>
struct TStateCache {
    TStateCache<T, N>();
    void Reset();
    void SetState(uint64_t slot, const T &state);
    void SetState(const T &);
    void InheritStates(const TStateCache<T, N> &other);
    const T &GetState(uint64_t slot);
    const T *GetStatePtr(uint64_t slot);
    uint64_t IsStateInvalid(uint64_t slot);
    uint64_t IsStateSync(uint64_t slot);
    uint64_t IsStateBypassed(uint64_t slot);
    uint64_t IsStateModified(uint64_t);
    void InvalidateState(uint64_t);
    void InvalidateState();
    void SetStateModified(uint64_t slot);
    void SetStateModified();
    void SetAllStatesModified();
    void Bypass(uint64_t);
    void Bypass();
    void BypassAll();
    uint64_t m_bfInvalid;
    uint64_t m_bfModified;
    uint64_t m_bfSync;
    uint64_t m_bfOK;
    T m_states[N];
};

class ZRenderDeviceContextBase
    : public TRenderReferencedCountedImpl<RenderReferencedCountedBaseStub, 0> {
    public:
    bool IsImmediateContext();
    bool IsDeferredContext();
    ZRenderCommandList *GetCommandList();
    void BeginRecordingCmd(ZRenderCommandList *pCmd, ZRenderDeviceContextBase *deviceContext);
    void EndRecordingCmd();
    void Present();
    void SetRenderTargets(uint32_t nNumViews, ZRenderTargetView **ppRenderTargetViews,
                          ZRenderDepthStencilView *pDepthStencilView, uint32_t nClearFlags,
                          const float4 *pClearColors, float fDepthValue, uint32_t nStencilValue);
    void GetRenderTargets(uint32_t nNumViews, ZRenderTargetView **ppRenderTargetViews,
                          ZRenderDepthStencilView **ppDepthStencilView);
    void SetRenderTargetsAndUnorderedAccessViews(
        uint32_t nNumViews, ZRenderTargetView **ppRenderTargetViews,
        ZRenderDepthStencilView *pDepthStencilView, uint32_t nUAVStartSlot, uint32_t nNumUAVs,
        ZRenderUnorderedAccessView **ppUnorderedAccessView, const uint32_t *pUAVInitialCounts,
        uint32_t nClearFlags, const float4 *pClearColors, float fDepthValue, uint32_t nStencilValue,
        uint32_t *UAVClearValuesUint, float *UAVClearValuesFloat);
    void ClearRenderTargets(uint32_t nNumViews, ZRenderTargetView **ppRenderTargetViews,
                            ZRenderDepthStencilView *pDepthStencilView, uint32_t nClearFlags,
                            const float4 *pClearColors, float fDepthValue, uint32_t nStencilValue);
    void ClearUnorderedAccessViews(uint32_t nNumUAVs, ZRenderUnorderedAccessView **pUAVs,
                                   uint32_t nClearFlags, const uint32_t *UAVClearValuesUint,
                                   const float *UAVClearValuesFloat);
    void ClearHiStencil(ZRenderDepthStencilView *pDSV, uint32_t nFlags, uint32_t nStencilValue);
    void SetHiStencil(bool bEnable, ERenderCompareFunc eCompareFunc, uint8_t nMask, uint8_t nValue);
    void SetInputLayout(ZRenderInputLayout *inputLayout);
    void SetVertexBuffers(uint64_t startSlot, uint64_t numBuffers,
                          const SRenderDeviceContextVertexBuffer *vertexBuffers);
    void SetVertexBuffers(uint64_t startSlot, const SRenderDeviceContextVertexBuffer &vb);
    void SetIndexBuffer(const SRenderDeviceContextIndexBuffer &indexBuffer);
    void InvalidateIndexBuffer();
    void SetPrimitiveTopology(ERenderPrimitiveTopology ePrimitiveTopology);
    void SetSharedConstBuffer(uint32_t nSlot, ZRenderConstBufferRef pBuffer);
    void SetConstBuffersVS(uint64_t slot, SConstBufferImpl *cbImpl);
    void SetConstBuffersPS(uint64_t slot, SConstBufferImpl *cbImpl);
    void SetConstBuffersGS(uint64_t slot, SConstBufferImpl *cbImpl);
    void SetConstBuffersHS(uint64_t slot, SConstBufferImpl *cbImpl);
    void SetConstBuffersDS(uint64_t slot, SConstBufferImpl *cbImpl);
    void SetConstBuffersCS(uint64_t slot, SConstBufferImpl *cbImpl);
    void SetUserDataVS(uint64_t slot, uint32_t *uint32Ptr, uint64_t nUint32s);
    void SetSamplerStatesVS(uint64_t slot, ZRenderSamplerState *samplerState);
    void SetSamplerStatesPS(uint64_t slot, ZRenderSamplerState *samplerState);
    void SetSamplerStatesGS(uint64_t slot, ZRenderSamplerState *samplerState);
    void SetSamplerStatesHS(uint64_t slot, ZRenderSamplerState *samplerState);
    void SetSamplerStatesDS(uint64_t slot, ZRenderSamplerState *samplerState);
    void SetSamplerStatesCS(uint64_t slot, ZRenderSamplerState *samplerState);
    void SetShaderVS(ZRenderShader *shader);
    void SetShaderPS(ZRenderShader *shader);
    void SetShaderGS(ZRenderShader *shader);
    void SetShaderHS(ZRenderShader *shader);
    void SetShaderDS(ZRenderShader *shader);
    void SetShaderCS(ZRenderShader *shader);
    void SetShaderResourceViewsVS(uint64_t slot, ZRenderShaderResourceView *SRV);
    void SetShaderResourceViewsPS(uint64_t slot, ZRenderShaderResourceView *SRV);
    void SetShaderResourceViewsGS(uint64_t slot, ZRenderShaderResourceView *SRV);
    void SetShaderResourceViewsHS(uint64_t slot, ZRenderShaderResourceView *SRV);
    void SetShaderResourceViewsDS(uint64_t slot, ZRenderShaderResourceView *SRV);
    void SetShaderResourceViewsCS(uint64_t slot, ZRenderShaderResourceView *SRV);
    void SetUnorderedAccessViewsCS(uint64_t slot, const SRenderDeviceContextUAV &UAV);
    void SetBlendState(const SRenderDeviceContextBlendState &blendState);
    void SetDepthStencilState(const SRenderDeviceContextDepthStencilState &dsState);
    void SetRasterizerState(ZRenderRasterizerState *rs);
    void SetAntialiasingState(EAntialiasingMode mode);
    const SRenderViewport &GetViewport();
    void SetViewport(const SRenderViewport &vp);
    void SetScissorRect(const SRenderViewport &sr);
    void CopySubresourceRegion(IRenderResource *pDstResource, uint32_t dstSubresource,
                               uint32_t dstX, uint32_t dstY, uint32_t dstZ,
                               IRenderResource *pSrcResource, uint32_t srcSubresource,
                               const SRenderRect *pRect);
    void CopyResource(IRenderResource *pDstResource, IRenderResource *pSrcResource);
    void CopySubresource(IRenderResource *pDstResource, ERenderPlaneUsage eDstPlane,
                         IRenderResource *pSrcResource, ERenderPlaneUsage eSrcPlane);
    void CopyStructureCount(ZRenderBuffer *pDstBuffer, uint32_t nDstAlignedByteOffset,
                            ZRenderUnorderedAccessView *pSrcUAV);
    void CopyMem(ZRenderBuffer *pDstBuffer, void *pData, uint64_t nSize);
    void ResolveRTV(ZRenderTargetView *pDstRTV, ZRenderShaderResourceView *pSrcSRV);
    void UncompressColorBuffer(ZRenderTargetView *rtView);
    void UncompressDepthBuffer(ZRenderDepthStencilView *dsView);
    void ExecuteCommandList(ZRenderCommandList **cmdLists, uint64_t nCmdLists);
    void Map(ZRenderBuffer *buf, ERenderResourceMapType eMapType,
             SRenderMappedBuffer *pMappedBuffer, uint32_t nSubResource);
    void Map(ZRenderTexture3D *tex3D, ERenderResourceMapType eMapType,
             SRenderMappedTexture3D *pMappedTexture, uint32_t nSubResource);
    void Map(ZRenderTexture2D *tex2D, ERenderResourceMapType eMapType,
             SRenderMappedTexture2D *pMappedTexture, uint32_t nSubResource);
    void UnMap(ZRenderBuffer *buf, uint32_t nSubResource);
    void UnMap(ZRenderTexture3D *tex3D, uint32_t nSubResource);
    void UnMap(ZRenderTexture2D *tex2D, uint32_t nSubResource);
    void BeginPredicate(ZRenderPredication *pPredication);
    void EndPredicate(ZRenderPredication *pPredication);
    void ActivatePredication(ZRenderPredication *pPredication, bool bPredicateValue);
    void DeactivatePredication();
    virtual void SyncGpu(ERenderSyncGpu nGpuCacheFlags, uint64_t *label, uint64_t value);
    uint64_t *AllocLabel(uint64_t initialValue);
    void SendTopOfPipelineEvent(ERenderTopEvent e);
    void SendEndOfPipelineEvent(ERenderEopEvent e);
    void WriteAtEndOfShader(ERenderEosEvent evt, uint64_t *ptr, uint64_t value);
    void WriteAtEndOfPipeline(uint64_t *ptr, uint64_t value, ERenderWriteValueEopEvent evt);
    void WaitOnAddress(uint64_t *ptr, ERenderCondition cond, uint64_t value,
                       ERenderWaitOnAdressLocation location);
    uint64_t InsertFence();
    void DispatchCompute(ZRenderShader *pComputeShader, const ZRenderConstBufferRef pBuffer,
                         uint32_t nThreadX, uint32_t nThreadY, uint32_t nThreadZ);
    void DispatchComputeUnsafe(ZRenderShader *pComputeShader, const ZRenderConstBufferRef pBuffer,
                               uint32_t nThreadX, uint32_t nThreadY, uint32_t nThreadZ);
    void SetAutomaticViewportsEnabled(bool bEnabled);
    virtual void CopyData(void *, void *, uint64_t);
    virtual void DrawIndexed(uint32_t, uint32_t, int32_t, uint32_t, ZRenderShader *,
                             ZRenderShader *, ZRenderShader *, const ZRenderConstBufferRef,
                             const ZRenderConstBufferRef, const ZRenderConstBufferRef);
    virtual void DrawIndexed(uint32_t, uint32_t, int32_t, uint32_t, ZRenderShader *,
                             ZRenderShader *, const ZRenderConstBufferRef,
                             const ZRenderConstBufferRef);
    virtual void DrawIndexed(uint32_t, uint32_t, int32_t, uint32_t, ZRenderShader *const *,
                             const ZRenderConstBufferRef *);
    virtual void DrawInstancedIndirect(ZRenderBuffer *, uint32_t, ZRenderShader *const *,
                                       const ZRenderConstBufferRef *);
    virtual void DrawInstanced(uint32_t, uint32_t, int32_t, uint32_t, uint32_t,
                               ZRenderShader *const *, const ZRenderConstBufferRef *);
    virtual void DrawQuadNew(float, float, float, float, float, float, float, float, float, bool,
                             ZRenderShader *, ZRenderShader *, const ZRenderConstBufferRef,
                             const ZRenderConstBufferRef, ZRenderShader *,
                             const ZRenderConstBufferRef, ZRenderTexture2D *);
    virtual void BeginVertices(ERenderPrimitiveTopology, uint64_t, uint32_t, void **, const bool);
    virtual void DrawPrimitives(ERenderPrimitiveTopology, const void *, uint64_t, uint32_t,
                                ZRenderShader *, ZRenderShader *, const ZRenderConstBufferRef,
                                const ZRenderConstBufferRef, const bool);
    virtual void EndVertices(ZRenderShader *, ZRenderShader *, const ZRenderConstBufferRef,
                             const ZRenderConstBufferRef);
    virtual void FlushStatesGraphic();
    virtual void FlushStatesCompute();
    void PushStates();
    void PopStates();
    virtual void SyncStates(bool bInvalidateHWCache);
    virtual void BeginEvent(uint32_t, const char *);
    virtual void EndEvent();
    virtual void BeginTimer(uint64_t, const char *, uint64_t);
    virtual void EndTimer(uint64_t);
    static void SetGameFrameIndex(uint32_t index);
    void Reset();
    void InheritStates(ZRenderDeviceContextBase *deviceContext);
    ZRenderDeviceContextBase(const SRenderDeviceContextCreationParamsBase &creationParams);
    virtual ~ZRenderDeviceContextBase();
    void SetResourcesModifiedVS();
    void SetResourcesModifiedPS();
    void SetResourcesModifiedGS();
    void SetResourcesModifiedHS();
    void SetResourcesModifiedDS();
    void SetResourcesModifiedCS();
    void AddToStats(uint32_t nClears, uint32_t nDIPs, uint32_t nTriangles);
    bool ValidateDrawCall();
    bool ValidateDispatch();
    bool CheckPipelineStage(const char *stageName,
                            const TStateCache<ZRenderShader *, 1> &shaderCache,
                            const TStateCache<SConstBufferImpl *, 14> &cbCache,
                            const TStateCache<ZRenderShaderResourceView *, 63> &srvCache,
                            const TStateCache<SRenderDeviceContextUAV, 8> &uavCache,
                            const TStateCache<ZRenderSamplerState *, 16> &smpCache);
    bool CheckVSStage();
    virtual void DoReset();
    virtual void DoPresent();
    virtual void DoSyncStates(bool bInvalidateHWCache);
    virtual void DoInheritStates(ZRenderDeviceContextBase *deviceContext);
    virtual void DoPreSetRenderTargets();
    virtual void DoPreSetRenderTargetsAndUnorderedAccessViews();
    virtual uint64_t *DoAllocLabel(uint64_t initialValue);
    virtual void DoSendEndOfPipelineEvent(ERenderEopEvent e);
    virtual void DoSendTopOfPipelineEvent(ERenderTopEvent e);
    virtual void DoWriteAtEndOfShader(ERenderEosEvent evt, uint64_t *ptr, uint64_t value);
    virtual void DoPartialFlushEndOfShader(ERenderEosEvent evt);
    virtual void DoWriteAtEndOfPipeline(uint64_t *ptr, uint64_t value, ERenderWriteValueEopEvent e);
    virtual void DoWaitOnAddress(uint64_t *ptr, ERenderCondition cond, uint64_t value,
                                 ERenderWaitOnAdressLocation location);
    virtual void DoInsertWaitOnFence(uint64_t nFence);
    virtual void DoSetScissorRect();
    virtual void UnsetOutputResource(ZRenderShaderResourceView *SRV);
    virtual void DoSetRenderTargets();
    virtual void DoSetRenderTargetsAndUnorderedAccessViews();
    virtual void DoClearRenderTargetView(ZRenderTargetView *view, float4 color);
    virtual void DoClearDepthStencilView(ZRenderDepthStencilView *view, uint32_t clearFlags,
                                         float fDepthValue, uint8_t nStencilValue);
    virtual void DoClearUAVUint(ZRenderUnorderedAccessView *view, const uint32_t *value);
    virtual void DoClearUAVFloat(ZRenderUnorderedAccessView *view, const float *value);
    virtual void DoClearHiStencil(ZRenderDepthStencilView *pDSV, uint32_t nFlags,
                                  uint32_t nStencilValue);
    virtual void DoSetHiStencil(bool bEnable, ERenderCompareFunc eCompareFunc, uint8_t nMask,
                                uint8_t nValue);
    virtual void DoSetViewport();
    virtual void DoSetRasterizerState();
    virtual void DoSetDepthStencilState();
    virtual void DoSetBlendState();
    virtual void DoSetAntialiasingState();
    virtual void DoSetPrimitiveTopology();
    virtual void DoCopySubresourceRegion(IRenderResource *pDstResource, uint32_t dstSubresource,
                                         uint32_t dstX, uint32_t dstY, uint32_t dstZ,
                                         IRenderResource *pSrcResource, uint32_t srcSubresource,
                                         const SRenderRect *pRect);
    virtual void DoCopyResource(IRenderResource *pDstResource, IRenderResource *pSrcResource);
    virtual void DoCopySubresource(IRenderResource *pDstResource, ERenderPlaneUsage eDstPlane,
                                   IRenderResource *pSrcResource, ERenderPlaneUsage eSrcPlane);
    virtual void DoCopyStructureCount(ZRenderBuffer *pDstBuffer, uint32_t nDstAlignedByteOffset,
                                      ZRenderUnorderedAccessView *pSrcUAV);
    virtual void DoCopyMem(ZRenderBuffer *pDstBuffer, void *pData, uint64_t nSize);
    virtual void DoResolveRTV(ZRenderTargetView *pDstRTV, ZRenderShaderResourceView *pSrcSRV);
    virtual void DoUncompressColorBuffer(ZRenderTargetView *rtView);
    virtual void DoUncompressDepthBuffer(ZRenderDepthStencilView *dsView);
    virtual void DoFinalizeCommandList(ZRenderCommandList *cmdList);
    virtual void DoExecuteCommandList(ZRenderCommandList **cmdLists, uint64_t nCmdLists);
    virtual void DoBeginExecuteCommandList();
    virtual void DoEndExecuteCommandList();
    virtual void DoMap(ZRenderBuffer *buf, ERenderResourceMapType eMapType,
                       SRenderMappedBuffer *pMappedBuffer, uint32_t nSubResource);
    virtual void DoMap(ZRenderTexture3D *tex2D, ERenderResourceMapType eMapType,
                       SRenderMappedTexture3D *pMappedTexture, uint32_t nSubResource);
    virtual void DoMap(ZRenderTexture2D *tex2D, ERenderResourceMapType eMapType,
                       SRenderMappedTexture2D *pMappedTexture, uint32_t nSubResource);
    virtual void DoUnMap(ZRenderBuffer *buf, uint32_t nSubResource);
    virtual void DoUnMap(ZRenderTexture3D *tex2D, uint32_t nSubResource);
    virtual void DoUnMap(ZRenderTexture2D *tex2D, uint32_t nSubResource);
    virtual void DoBeginPredicate(ZRenderPredication *pPredication);
    virtual void DoEndPredicate(ZRenderPredication *pPredication);
    virtual void DoActivatePredication(ZRenderPredication *pPredication, bool bPredicateValue);
    virtual void DoDeactivatePredication();
    virtual uint64_t DoInsertFence();
    virtual void DoInvalidateIndexBuffer();
    virtual void DoDispatchComputeUnsafe(ZRenderShader *pComputeShader,
                                         const ZRenderConstBufferRef pBuffer, uint32_t nThreadX,
                                         uint32_t nThreadY, uint32_t nThreadZ);
    static uint32_t m_gameFrameIndex;
    const bool m_immediateContext;
    ZRenderCommandList *m_pCommandList;
    ZRenderConstBufferRef m_ActiveSharedConstBuffer[14];
    TStateCache<SRenderDeviceContextVertexBuffer, 8> m_vbCache;
    TStateCache<SRenderDeviceContextIndexBuffer, 1> m_ibCache;
    TStateCache<SConstBufferImpl *, 14> m_cbVSCache;
    TStateCache<SConstBufferImpl *, 14> m_cbPSCache;
    TStateCache<SConstBufferImpl *, 14> m_cbGSCache;
    TStateCache<SConstBufferImpl *, 14> m_cbHSCache;
    TStateCache<SConstBufferImpl *, 14> m_cbDSCache;
    TStateCache<SConstBufferImpl *, 14> m_cbCSCache;
    TStateCache<ZRenderSamplerState *, 16> m_ssVSCache;
    TStateCache<ZRenderSamplerState *, 16> m_ssPSCache;
    TStateCache<ZRenderSamplerState *, 16> m_ssGSCache;
    TStateCache<ZRenderSamplerState *, 16> m_ssHSCache;
    TStateCache<ZRenderSamplerState *, 16> m_ssDSCache;
    TStateCache<ZRenderSamplerState *, 16> m_ssCSCache;
    TStateCache<ZRenderShader *, 1> m_shVSCache;
    TStateCache<ZRenderShader *, 1> m_shPSCache;
    TStateCache<ZRenderShader *, 1> m_shGSCache;
    TStateCache<ZRenderShader *, 1> m_shHSCache;
    TStateCache<ZRenderShader *, 1> m_shDSCache;
    TStateCache<ZRenderShader *, 1> m_shCSCache;
    TStateCache<SRenderDeviceContextUAV, 8> m_uavVSCache;
    TStateCache<SRenderDeviceContextUAV, 8> m_uavPSCache;
    TStateCache<SRenderDeviceContextUAV, 8> m_uavGSCache;
    TStateCache<SRenderDeviceContextUAV, 8> m_uavHSCache;
    TStateCache<SRenderDeviceContextUAV, 8> m_uavDSCache;
    TStateCache<SRenderDeviceContextUAV, 8> m_uavCSCache;
    TStateCache<ZRenderShaderResourceView *, 63> m_srvVSCache;
    TStateCache<ZRenderShaderResourceView *, 63> m_srvPSCache;
    TStateCache<ZRenderShaderResourceView *, 63> m_srvGSCache;
    TStateCache<ZRenderShaderResourceView *, 63> m_srvHSCache;
    TStateCache<ZRenderShaderResourceView *, 63> m_srvDSCache;
    TStateCache<ZRenderShaderResourceView *, 63> m_srvCSCache;
    TStateCache<ZRenderInputLayout *, 1> m_ilCache;
    TStateCache<unsigned int, 2> m_udVSCache;
    SRenderViewport m_viewport;
    SRenderViewport m_scissorRect;
    ZRenderTargetView *m_pRenderTargetViews[8];
    ZRenderDepthStencilView *m_pDepthStencilView;
    ZRenderUnorderedAccessView *m_pRenderUAVs[8];
    uint32_t m_pUAVInitialCounts[8];
    uint32_t m_UAVStartSlot;
    uint32_t m_UAVNumSlots;
    SRenderDeviceContextBlendState m_blendState;
    SRenderDeviceContextDepthStencilState m_depthStencilState;
    ZRenderRasterizerState *m_rasterizerState;
    ERenderPrimitiveTopology m_primTopology;
    EAntialiasingMode m_antialiasingState;
    struct StatesBundle {
        SRenderDeviceContextBlendState blendState;
        ZRenderRasterizerState *rasterizerState;
        SRenderDeviceContextDepthStencilState depthStencilState;
    };
    static const uint32_t MaxStateBundles;
    uint32_t m_nPushedStateBundlesCnt;
    ZRenderDeviceContextBase::StatesBundle m_PushedStates[3];
    struct MappedInfo {
        void *rsc;
        uint64_t mapType;
    };
    static const uint32_t MaxMappedInfos;
    ZRenderDeviceContextBase::MappedInfo m_mappedInfos[4];
    uint32_t m_mappedInfosStackSize;
    bool m_bAutomaticViewports;
};

enum EMultiGPUHint {
    RENDER_MGPU_SYNC_FORCE_ENABLE,
    RENDER_MGPU_SYNC_AUTOMATIC,
    RENDER_MGPU_SYNC_MANUAL,
    RENDER_MGPU_RENDER_BEGIN,
    RENDER_MGPU_RENDER_BEGIN_NOSYNC,
    RENDER_MGPU_RENDER_END,
};

class ZRenderDeviceContext : public ZRenderDeviceContextBase {
    public:
    ZRenderDeviceContext(const SRenderDeviceContextCreationParamsBase &creationParams);
    virtual ~ZRenderDeviceContext();
    virtual void DrawPrimitives(ERenderPrimitiveTopology ePrimitiveTopology, const void *pVertices,
                                uint64_t nNumPrimitives, uint32_t nVertexSize,
                                ZRenderShader *pFragmentShader_, ZRenderShader *pVertexShader_,
                                const ZRenderConstBufferRef ConstBufferF,
                                const ZRenderConstBufferRef ConstBufferV, const bool bAddToStats);
    virtual void Draw(uint32_t, uint32_t);
    virtual void StoreRightEyeResult();
    virtual void SendMultiGPUHint(EMultiGPUHint, IRenderResource *, uint64_t, uint64_t);
    virtual void Barrier(ZRenderTexture2D *, uint64_t, uint32_t, bool);
    virtual void UpdateSubresource(ZRenderBuffer *, uint32_t, void *, uint64_t);
    virtual void UpdateSubresource(ZRenderTexture2D *, uint32_t, void *, uint64_t);
    bool IsStereoscopic3DLeftEyeRendering();
    void SetStereoscopic3DLeftEyeRendering(bool bSet);
    void SetScaleformStereoscopicProjectionMatrices(const SMatrix &mCenter,
                                                    const SMatrix &mCorrected);
    float CalculateScaleformStereoscopic3DOffset(float fDepth);
    void BindShaders(ZRenderShader *pFragmentShader, ZRenderShader *pGeometryShader,
                     ZRenderShader *pVertexShader, ZRenderShader *pHullShader,
                     ZRenderShader *pDomainShader, const ZRenderConstBufferRef ConstBufferF,
                     const ZRenderConstBufferRef ConstBufferG,
                     const ZRenderConstBufferRef ConstBufferV,
                     const ZRenderConstBufferRef ConstBufferH,
                     const ZRenderConstBufferRef ConstBufferD);
    virtual void UnsetOutputResource(ZRenderShaderResourceView *SRV);
    virtual void UnsetOutputResource(IRenderResource *pResource, uint32_t subResourceMask);
    virtual void UnsetInputResource(IRenderResource *pResource, uint32_t subResourceMask);
    bool m_bStereoscopic3DLeftEyeRendering;
    SMatrix m_mScaleformStereosopicProjectionCenter;
    SMatrix m_mScaleformStereosopicProjectionCorrected;
};

enum ERenderPrimitiveType {
    PRIM_TYPE_MESH_STANDARD,
    PRIM_TYPE_MESH_STANDARD_TC1,
    PRIM_TYPE_MESH_STANDARD_HIRES,
    PRIM_TYPE_MESH_STANDARD_TC1_HIRES,
    PRIM_TYPE_MESH_STANDARD_TC2,
    PRIM_TYPE_MESH_STANDARD_TC3,
    PRIM_TYPE_MESH_STANDARD_TC2_HIRES,
    PRIM_TYPE_MESH_STANDARD_TC3_HIRES,
    PRIM_TYPE_MESH_LINKED,
    PRIM_TYPE_MESH_LINKED_HIRES,
    PRIM_TYPE_MESH_LINKED_TC1,
    PRIM_TYPE_MESH_LINKED_TC1_HIRES,
    PRIM_TYPE_MESH_WEIGHTED,
    PRIM_TYPE_MESH_WEIGHTED_HIRES,
    PRIM_TYPE_MESH_WEIGHTED_TC1,
    PRIM_TYPE_MESH_WEIGHTED_TC1_HIRES,
    PRIM_TYPE_DESTRUCTIBLE,
    PRIM_TYPE_DESTRUCTIBLE_TC1,
    PRIM_TYPE_CLOTHING,
    PRIM_TYPE_CLOTHING_TC1,
    PRIM_TYPE_CLOTHING_WEIGHTED,
    PRIM_TYPE_CLOTHING_WEIGHTED_TC1,
    PRIM_TYPE_SIMPLE,
    PRIM_TYPE_SIMPLE_TEX,
    PRIM_TYPE_SIMPLE_TEX_16BIT,
    PRIM_TYPE_RAIN,
    PRIM_TYPE_GENERIC,
    PRIM_TYPE_DECAL_STANDARD,
    PRIM_TYPE_DECAL_LINKED,
    PRIM_TYPE_DECAL_WEIGHTED,
    PRIM_TYPE_DECAL_DESTRUCTIBLE,
    PRIM_TYPE_SPRITES_SA,
    PRIM_TYPE_SPRITES_AO,
    PRIM_TYPE_DEBUG,
    NUM_PRIM_TYPES,
    PRIM_TYPE_INVALID,
};

struct SFXPassHeader {
    uint32_t nNameOffset;
    uint32_t nVertexShaderIndex;
    uint32_t nPixelShaderIndex;
    uint32_t nGeometryShaderIndex;
    uint32_t nHullShaderIndex;
    uint32_t nDomainShaderIndex;
    uint32_t nComputeShaderIndex;
};

struct SFXReflectionRegistry {
    uint16_t magic;
    uint16_t version;
    uint16_t numReflexionTables;
    uint16_t rtflags;
    uint32_t offStrings;
    uint32_t __pad32;
    const SFXReflectionTable *GetReflectionTable(uint64_t index);
};

class ZRenderSamplerStateCache {
    public:
    void Reserve(uint64_t capacity);
    void PushBack(const ZHashedString &sName, ZRenderSamplerState *pSamplerState);
    ZRenderSamplerState *GetSamplerStateByName(const ZHashedString &sName);
    TArray<ZHashedString> m_SamplerStateNames;
    TArray<ZRenderSamplerState *> m_SamplerStates;
};

struct SFXTechniqueHeader {
    uint32_t nNameOffset;
    uint32_t nPasses;
    uint32_t nPassStartOffset;
};

class ZRenderEffectTechnique {
    public:
    ZRenderEffectTechnique(const ZString &sName, ZRenderEffect *pEffect, const uint8_t *pEffectData,
                           uint32_t nEffectSize, const SFXTechniqueHeader *pTechniqueHeader,
                           const SFXReflectionRegistry *pReflectionReg,
                           const ZRenderSamplerStateCache &samplerStateCache,
                           uint32_t nShadersOffset, uint32_t nIASignatureOffset);
    ~ZRenderEffectTechnique();
    uint64_t GetNumPasses();
    ZRenderEffectPass *GetPassByIndex(uint64_t nIndex);
    ZRenderEffect *GetEffect();
    const ZString &GetName();
    uint32_t GetMaxInstructionsDebug();
    uint32_t GetNbSampledTexturesDebug();
    const ZString &GetDebugName();
    ZString m_sName;
    ZRenderEffect *m_pEffect;
    TArray<ZRenderEffectPass *> m_Passes;
};

class ZRenderEffectPass {
    public:
    ZRenderEffectPass(ZRenderEffectTechnique *pTechnique, const uint8_t *pEffect,
                      uint32_t nEffectSize, const SFXPassHeader *pPassHeader,
                      const SFXReflectionRegistry *pReflectionReg,
                      const ZRenderSamplerStateCache &samplerStateCache, uint32_t nShadersOffset,
                      uint32_t nIASignatureOffset);
    ~ZRenderEffectPass();
    ZRenderShader *GetVertexShader();
    ZRenderShader *GetGeometryShader();
    ZRenderShader *GetFragmentShader();
    ZRenderShader *GetHullShader();
    ZRenderShader *GetDomainShader();
    ZRenderShader *GetComputeShader();
    ZRenderEffectTechnique *GetTechnique();
    void PatchSamplerStatesForHighRes();
    void InitShader(ZRenderShader *pShader, const uint8_t *pEffect, uint32_t nEffectSize,
                    const ZString &ShaderName, bool isVertexShader,
                    const SFXReflectionTable *reflectionTable,
                    const ZRenderSamplerStateCache &samplerStateCache);
    ZRenderSamplerState **m_ppUnpatchedSamplerState;
    ZRenderSamplerState *PatchSamplerStateForHighRes(ZRenderSamplerState *pSamplerState);
    void RestoreSamplerStates();
    void ReleaseUnpatchedSamplerStates();
    ZRenderEffectTechnique *m_pTechnique;
    ZRenderShader m_VertexShader;
    ZRenderShader m_GeometryShader;
    ZRenderShader m_PixelShader;
    ZRenderShader m_HullShader;
    ZRenderShader m_DomainShader;
    ZRenderShader m_ComputeShader;
};

enum EShaderStage {
    SHADER_STAGE_VERTEX,
    SHADER_STAGE_PIXEL,
    SHADER_STAGE_GEOMETRY,
    SHADER_STAGE_HULL,
    SHADER_STAGE_DOMAIN,
    SHADER_STAGE_COMPUTE,
    SHADER_STAGE_COUNT,
};

class ZRenderEffect : public TRenderReferencedCountedImpl<RenderReferencedCountedBaseStub, 0> {
    public:
    ZRenderEffect(const ZString &sName, const uint8_t *pEffect, uint32_t nDataSize,
                  ZRenderDevice *pRenderDevice);
    virtual ~ZRenderEffect();
    ZRenderEffectTechnique *GetTechniqueByName(const ZString &sName);
    ZRenderDevice *GetRenderDevice();
    ZRenderShader *GetVertexShader(const ZString &name, uint32_t nPass);
    ZRenderShader *GetGeometryShader(const ZString &name, uint32_t nPass);
    ZRenderShader *GetFragmentShader(const ZString &name, uint32_t nPass);
    ZRenderShader *GetHullShader(const ZString &name, uint32_t nPass);
    ZRenderShader *GetDomainShader(const ZString &name, uint32_t nPass);
    ZRenderShader *GetComputeShader(const ZString &name, uint32_t nPass);
    void GetAllShaders(EShaderStage stage, TArray<ZRenderShaderDX12 *> &shaders);
    const ZString &GetName();
    bool GetAllPipelinesDoneLoading();
    void SetAllPipelinesDoneLoading(bool bSet);
    ZString m_sName;
    ZRenderDevice *m_pRenderDevice;
    TArray<ZRenderEffectTechnique *> m_Techniques;
    bool m_bAllPipelinesDoneLoading;
};

class ZRenderMaterialEffectData {
    public:
    ZRenderMaterialEffectData(ZRenderEffect *pRenderEffect);
    virtual ~ZRenderMaterialEffectData();
    ZRenderEffect *GetRenderEffect();
    ZRenderEffect *m_pRenderEffect;
};

struct SRMaterialProperties {
    enum MATERIAL_FLAGS {
        MF_REFLECTION2D,
        MF_REFRACTION2D,
        MF_FROSTINESS,
        MF_EMISSIVE,
        MF_DISCARD,
        MF_LM_SKIN,
        MF_PRIMCLASS_STANDARD,
        MF_PRIMCLASS_LINKED,
        MF_PRIMCLASS_WEIGHTED,
        MF_DOFOVERRIDE,
        MF_USES_DEFAULT_VS,
        MF_USES_SPRITE_SA_VS,
        MF_USES_SPRITE_AO_VS,
        MF_ALPHA,
        MF_USES_SIMPLE_SHADER,
        MF_UNLIT,
        MF_USES_COLOR_MODIFIER,
        MF_LM_EYE,
        MF_VERTEXCS,
        MF_LM_HAIR,
        MF_CLOAKING,
        MF_GHOST,
        MF_DEBUG,
        MF_TESSELLATIONTITAN,
        MF_TITANUNDERLYINGMATERIAL,
        MF_PPLL,
        MF_WRINKLES,
        MF_VERTEX_SHLIGHTING,
        MF_OPAQUE_SOFT_DEPTH_INTERSECT,
        MF_ETHEREAL,
        MF_USES_LIGHTCLIPPING,
    };
    uint32_t lMaterialClassType;
    uint32_t lMaterialClassIndex;
    uint32_t lMaterialClassFlags;
    uint32_t lTransparencyFlags;
    uint32_t lMaterialDescriptor;
    uint32_t lImpactMaterial;
    uint32_t lEffectResource;
    bool IsOccluder();
    bool IsOpaqueShadowCaster();
    bool IsTransparent();
    bool IsArbitraryOriented();
};

enum ERenderLayer {
    RENDER_LAYER_ZPASS,
    RENDER_LAYER_ZPASS_DISCARD,
    RENDER_LAYER_EMISSIVE,
    RENDER_LAYER_EMISSIVE_DITHER,
    RENDER_LAYER_SHADOWMAP,
    RENDER_LAYER_SHADOWMAP_DITHER,
    RENDER_LAYER_TRANSPARENT,
    RENDER_LAYER_DEFERRED_2,
    RENDER_LAYER_DEFERRED_2_DITHER,
    RENDER_LAYER_DEFERRED_SIMPLE_2,
    RENDER_LAYER_DEFERRED_SIMPLE_2_DITHER,
    RENDER_LAYER_WIREFRAME,
    RENDER_LAYER_SIMPLE,
    RENDER_LAYER_SIMPLE_SHADE,
    RENDER_LAYER_RAINMAP,
    RENDER_LAYER_DECAL,
    RENDER_LAYER_DECAL_DITHER,
    RENDER_LAYER_CHARACTERRAIN,
    RENDER_LAYER_VERTEX_PREPROCESS,
    RENDER_LAYER_XRAY,
    RENDER_LAYER_TRANSLUCENCE,
    RENDER_LAYER_CLOAKMASK,
    RENDER_LAYER_CLOAK,
    RENDER_LAYER_VELOCITY,
    RENDER_LAYER_REFLECTION,
    RENDER_LAYER_REFLECTION_DITHER,
    RENDER_LAYER_REFRACTION_MASK,
    MAX_NUM_LAYERS,
    RENDER_LAYER_ZPREPASS,
    RENDER_LAYER_MASK_DITHER,
    RENDER_LAYER_FRAGMENT_OVERRIDE_MASK,
    RENDER_LAYER_SHADOW_MASK,
    RENDER_LAYER_DEFERRED_MASK,
};

enum ERenderQueryType {
    RENDER_QUERY_TYPE_OCCLUSION,
    RENDER_QUERY_TYPE_TIMESTAMP,
    RENDER_QUERY_TYPE_TIMESTAMPFREQ,
};

struct SRenderQueryDesc {
    ERenderQueryType eQueryType;
    uint32_t nMiscFlags;
};

class ZRenderQuery : public TRenderReferencedCountedImpl<IRenderRefCount, 0> {
    public:
    ZRenderQuery(const SRenderQueryDesc *pDescription);
    virtual ~ZRenderQuery();
    const SRenderQueryDesc &GetDesc();
    virtual void Begin();
    virtual void End();
    virtual bool GetData(void *, uint32_t, uint32_t);
    void GatherData(uint32_t nDataSize, uint32_t nFlags);
    bool IsValid();
    void GetLatestData(void *dest, uint32_t size);
    const SRenderQueryDesc m_Description;
    char *m_pLastData;
    uint32_t m_lastDataSize;
    bool m_isValid;
};

class ZRenderListAllocator {
    public:
    ZRenderListAllocator(const ZRenderListAllocator &);
    ZRenderListAllocator();
    ~ZRenderListAllocator();
    void BeginFrame();
    void EndFrame();
    uint8_t *Alloc(uint64_t numDrawCalls);
    uint64_t GetNbMaxDrawCalls();
    ZRenderListAllocator &operator=(const ZRenderListAllocator &);
    bool CanAdjustSizeAutomatically();
    uint64_t m_nNbMaxDrawCalls;
    uint64_t m_nMaxRequiredDrawCalls;
};

struct SRenderListDescriptor {
    ZRenderListAllocator *renderListAllocator;
    uint64_t numDrawCalls;
    ERenderLayer renderLayer;
    uint32_t batchingEnable;
    uint32_t setInstanceDataVelocity;
    uint32_t setInstanceDataVelocityVertexCompute;
};

class SInstanceBuffer {
    public:
    SInstanceBuffer(uint64_t numInstances, uint64_t nDataSize);
    ~SInstanceBuffer();
    uint64_t GetCapacity();
    uint64_t GetInstanceSize();
    ZRenderShaderResourceView *GetSRV();
    void Map(ZRenderDeviceContext *deviceContext, SRenderMappedBuffer &mappedBuffer);
    void UnMap(ZRenderDeviceContext *deviceContext);
    const uint64_t m_Capacity;
    const uint64_t m_InstanceSize;
    ZRenderShaderResourceView *m_SRV;
    ZRenderBuffer *m_GPUBuffer;
};

class ZInstanceBufferManager {
    public:
    enum TYPE {
        TYPE_16BPerInstance,
        TYPE_64BPerInstance,
        TYPE_COUNT,
    };
    class InstanceBufferContainer {
        public:
        InstanceBufferContainer(const uint64_t instanceCount);
        ~InstanceBufferContainer();
        SInstanceBuffer *Allocate(ZInstanceBufferManager::TYPE type);
        void Flip();
        const uint64_t m_InstanceCount;
        uint32_t m_CurrentFrame;
        struct SInstanceBufferPool {
            TArray<SInstanceBuffer *> m_FreeInstanceBuffers;
            TArray<SInstanceBuffer *> m_InUseInstanceBuffers[2];
        };
        ZInstanceBufferManager::InstanceBufferContainer::SInstanceBufferPool
            m_instanceBufferPools[2];
    };
    class ZInstanceBufferAllocator {
        public:
        ZInstanceBufferAllocator();
        ~ZInstanceBufferAllocator();
        SInstanceBuffer *GetFreeInstanceBuffer(uint64_t numInstances,
                                               ZInstanceBufferManager::TYPE type);
        void Flip();
        ZInstanceBufferManager::InstanceBufferContainer *
        GetInstanceBufferContainer(uint64_t slotIndex);
        TArray<ZInstanceBufferManager::InstanceBufferContainer *> m_containers;
    };
    ZInstanceBufferManager();
    ~ZInstanceBufferManager();
    static uint64_t GetInstanceSizeFromType(ZInstanceBufferManager::TYPE type);
    SInstanceBuffer *GetFreeInstanceBuffer(uint64_t numInstances,
                                           ZInstanceBufferManager::TYPE type);
    void Flip();
    ZInstanceBufferManager::ZInstanceBufferAllocator m_InstanceBufferAllocators;
};

class ZRenderList {
    public:
    static const uint64_t s_InvalidKey;
    struct PairKeyItem {
        uint64_t key;
        ZRenderDrawCall *item;
        bool operator<(const ZRenderList::PairKeyItem &other);
    };
    static void Reset();
    static ZRenderList *CreateRenderList(const SRenderListDescriptor &desc);
    uint64_t SortAndTrim();
    void PrepareDraw(ZRenderDeviceContext *dc, ZInstanceBufferManager *instancebufferManager,
                     uint64_t firstDC, uint64_t dcCount);
    void Draw(ZRenderDeviceContext *dc, ZRenderConstBufferRef *objectIdCBs, uint64_t firstDC,
              uint64_t dcCount, ZRenderShader **shadersIn, ZRenderConstBufferRef *cbuffersIn);
    uint64_t GetDrawCallCount();
    ZRenderList(const ZRenderList &);
    ZRenderList(const SRenderListDescriptor &desc, uint8_t *dataPtr,
                ZRenderList::PairKeyItem *keyItemsPtr);
    ZRenderDrawCall *AddDrawCall(uint64_t index);
    void SubmitInvalidDrawCall(uint64_t index);
    bool AreInstanciable(const ZRenderDrawCall &drawCall1, const ZRenderDrawCall &drawCall2);
    ZRenderList &operator=(const ZRenderList &);
    static uint64_t m_nextFrameID;
    const uint64_t m_magic;
    uint64_t m_frameID;
    const SRenderListDescriptor m_Descriptor;
    uint8_t *const m_DataPtr;
    ZRenderList::PairKeyItem *const m_KeyItemsPtr;
};

class ZDispatchCall {
    public:
    enum {
        eDispatchMaxSRV,
        eDispatchMaxUAV,
        eDispatchMaxConstBuffers,
    };
    ZDispatchCall(const ZDispatchCall &);
    ZDispatchCall();
    void SetShaderResourceView(uint32_t nSlot, ZRenderShaderResourceView *pResource);
    void SetUnorderedAccessView(uint32_t nSlot, ZRenderUnorderedAccessView *pUAV);
    void SetDispatch(ZRenderShader *pComputeShader, const ZRenderConstBufferRef pBuffer,
                     uint32_t nThreadX, uint32_t nThreadY, uint32_t nThreadZ);
    void SetConstBuffer(uint32_t nSlot, ZRenderConstBufferRef pResource);
    void SetName(const char *sName);
    void Reset();
    ZDispatchCall &operator=(const ZDispatchCall &);
    struct SRV {
        ZRenderShaderResourceView *m_pSRV;
        uint8_t m_Slot;
    };
    struct UAV {
        ZRenderUnorderedAccessView *m_pUAV;
        uint8_t m_Slot;
    };
    struct CB {
        ZRenderConstBufferRef m_constbuffer;
        uint8_t m_Slot;
    };
    uint32_t m_nbSRVs;
    uint32_t m_nbUAVs;
    uint32_t m_nbCbs;
    ZRenderShader *m_pShader;
    uint32_t nThreadGroupCountX;
    uint32_t nThreadGroupCountY;
    uint32_t nThreadGroupCountZ;
    ZDispatchCall::SRV m_SRVs[20];
    ZDispatchCall::UAV m_UAVs[8];
    ZDispatchCall::CB m_CBs[8];
    ZRenderConstBufferRef m_ConstBuffer;
    const char *m_sName;
};

struct SRenderBufferAccess {
    SRenderBufferAccess();
    void AddRef();
    void Release();
    ZRenderShaderResourceView *m_pSRV;
    ZRenderUnorderedAccessView *m_pUAV;
    ZRenderBuffer *m_pBuffer;
};

struct SCmdBase {
    uint8_t Tag;
    uint8_t Cmd;
    uint16_t Pad;
    uint32_t Size;
};

struct SCmdBufferPage {
    SCmdBufferPage(uint8_t *pMem, uint32_t size);
    uint8_t *m_pStart;
    uint32_t m_size;
    uint32_t m_usedSize;
    void Release();
};

enum EComputeJobRing {
    EComputeJobGraphics,
    EComputeJobAsync0,
};

enum EDataBufferSlot {
    EDATA_BUFFER_SLOT_START,
    EDATA_BUFFER_SLOT_APEX_BONE_MATRIX_PREV,
    EDATA_BUFFER_SLOT_VELOCITY_ALL_INSTANCES,
    EDATA_BUFFER_SLOT_SH_PROBES,
    EDATA_BUFFER_SLOT_VELOCITYVERTEXCOMPUTE_ALL_INSTANCES,
    EDATA_BUFFER_SLOT_APEX_BONE_MATRIX,
    EDATA_BUFFER_SLOT_ALL_INSTANCES,
    EDATA_BUFFER_SLOT_PROBES_BUFFER,
    EDATA_BUFFER_SLOT_PROBES_INDICES,
    EDATA_BUFFER_SLOT_PROBES_NODES,
    EDATA_BUFFER_SLOT_MISC_VELOCITY_DATA,
    EDATA_BUFFER_SLOT_MAX,
};

class ZCommandRecorder {
    public:
    enum {
        CMD_TYPE_BLENDSTATE,
        CMD_TYPE_RASTERIZERSTATE,
        CMD_TYPE_DEPTHSTENCILSTATE,
        CMD_TYPE_ANTIALIASINGSTATE,
        CMD_TYPE_INPUTLAYOUT,
        CMD_TYPE_SHAREDCONSTBUFFER,
        CMD_TYPE_SHADERRESOURCEVIEW,
        CMD_TYPE_VERTEXBUFFERS,
        CMD_TYPE_INDEXBUFFER,
        CMD_TYPE_CONSTBUFFERVF,
        CMD_TYPE_CONSTBUFFERV,
        CMD_TYPE_CONSTBUFFERF,
        CMD_TYPE_CONSTBUFFERH,
        CMD_TYPE_CONSTBUFFERD,
        CMD_TYPE_CONSTBUFFERG,
        CMD_TYPE_CONSTBUFFERC,
        CMD_TYPE_SHADERVF,
        CMD_TYPE_VERTEXSHADER,
        CMD_TYPE_FRAGMENTSHADER,
        CMD_TYPE_HULLSHADER,
        CMD_TYPE_DOMAINSHADER,
        CMD_TYPE_GEOMETRYSHADER,
        CMD_TYPE_COMPUTESHADER,
        CMD_TYPE_DRAWINDEXED,
        CMD_TYPE_DRAWINSTANCED,
        CMD_TYPE_DRAWINSTANCEDINDIRECT,
        CMD_TYPE_SET_RENDERTARGETS,
        CMD_TYPE_SET_VIEWPORT,
        CMD_TYPE_SET_SCISSOR_RECT,
        CMD_TYPE_CLEAR_HISTENCIL,
        CMD_TYPE_SET_HISTENCIL,
        CMD_TYPE_UNCOMPRESS_COLORBUFFER,
        CMD_TYPE_UNCOMPRESS_DEPTHBUFFER,
        CMD_TYPE_CLEAR_CURRENT_RENDERTARGETS,
        CMD_TYPE_CLEAR_RENDERTARGETS,
        CMD_TYPE_CLEAR_UAV,
        CMD_TYPE_COPY,
        CMD_TYPE_COPY_SUBRESOURCE,
        CMD_TYPE_COPY_SUBRESOURCE_REGION,
        CMD_TYPE_COPY_DATA,
        CMD_TYPE_RESOLVERTV,
        CMD_TYPE_RENDER_MARKER,
        CMD_TYPE_BEGIN_TIMER,
        CMD_TYPE_END_TIMER,
        CMD_TYPE_END_DETAIL_TIMER,
        CMD_TYPE_BEGIN_EVENT,
        CMD_TYPE_END_EVENT,
        CMD_TYPE_PUSH_STATES,
        CMD_TYPE_POP_STATES,
        CMD_TYPE_UNMAP_CONST_BUFFER,
        CMD_TYPE_UNMAP_RENDER_PRIMITIVE,
        CMD_TYPE_DRAW_QUAD,
        CMD_TYPE_COMPUTE_TEXTURE,
        CMD_TYPE_COMPUTE_UAV,
        CMD_TYPE_DISPATCH_COMPUTE,
        CMD_TYPE_DISPATCH_COMPUTEJOB,
        CMD_TYPE_CLEAR_RENDERTARGET_ASYNC,
        CMD_TYPE_WAIT_CLEAR_RENDERTARGET_ASYNC,
        CMD_TYPE_FLUSHSTATES_COMPUTE,
        CMD_TYPE_COPY_STRUCTURECOUNT,
        CMD_TYPE_COPY_MEM,
        CMD_TYPE_SET_DATA_BUFFER,
        CMD_TYPE_BEGIN_OCCLUSIONPRED,
        CMD_TYPE_END_OCCLUSIONPRED,
        CMD_TYPE_BEGIN_QUERY,
        CMD_TYPE_END_QUERY,
        CMD_TYPE_GET_DATA_QUERY,
        CMD_TYPE_ACTIVATE_OCCLUSIONPRED,
        CMD_TYPE_DEACTIVATE_OCCLUSIONPRED,
        CMD_TYPE_CALLBACK,
        CMD_TYPE_CALLBACK_WITH_PARAM,
        CMD_TYPE_END_RECCORD,
        CMD_TYPE_JUMP,
        CMD_TYPE_BEGIN_RECORDING_CMD_LIST,
        CMD_TYPE_END_RECORDING_CMD_LIST,
        CMD_TYPE_DRAW_RENDERLIST,
        CMD_TYPE_SET_RENDERTARGETSANDUNORDEREDACCESSVIEWS,
        CMD_TYPE_FLUSH_GPU_CACHES,
        CMD_TYPE_STORE_RIGHT_EYE_RESULT,
        CMD_TYPE_SEND_MULTIGPU_HINT,
        CMD_TYPE_SET_SCALEFORM_STEREOSCOPIC_PROJECTION_MATRICES,
        CMD_TYPE_BARRIER,
        CMD_TYPE_BEGIN_CAPTURE,
        CMD_TYPE_END_CAPTURE,
        CMD_TYPE_SET_USER_DATA,
        NUM_CMD_TYPES,
    };
    ZCommandRecorder(const ZCommandRecorder &);
    ZCommandRecorder();
    ~ZCommandRecorder();
    void Begin(bool isMainRecorder);
    bool IsMainRecorder();
    void Release();
    void ReleaseAllPages();
    void BeginRecordingCmd(ZRenderCommandList *cmdList);
    void EndRecordingCmd();
    void SetRenderTargets(uint32_t nNumViews, ZRenderTargetView **ppRenderTargetViews,
                          ZRenderDepthStencilView *pDepthStencilView, uint32_t nClearFlags,
                          const float4 *pClearColors, float fDepthValue, uint32_t nStencilValue);
    void SetRenderTargetsAndUnorderedAccessViews(
        uint32_t nNumViews, ZRenderTargetView **ppRenderTargetViews,
        ZRenderDepthStencilView *pDepthStencilView, uint32_t nUAVStartSlot, uint32_t nNumUAVs,
        ZRenderUnorderedAccessView **ppUnorderedAccessView, const uint32_t *pUAVInitialCounts,
        uint32_t nClearFlags, const float4 *pClearColors, float fDepthValue, uint32_t nStencilValue,
        uint32_t *pUAVClearValuesUint, float *pUAVClearValuesFloat);
    void ClearHiStencil(ZRenderDepthStencilView *pDSV, uint32_t nFlags, uint32_t nStencilValue);
    void SetHiStencil(bool bEnable, ERenderCompareFunc eCompareFunc, uint8_t nMask, uint8_t nValue);
    void ClearRenderTargets(uint32_t nNumViews, ZRenderTargetView **ppRenderTargetViews,
                            ZRenderDepthStencilView *pDepthStencilView, uint32_t nClearFlags,
                            const float4 *pClearColors, float fDepthValue, uint32_t nStencilValue);
    void ClearUnorderedAccessView(ZRenderUnorderedAccessView *pUAV, uint32_t nClearFlags,
                                  const uint32_t *UAVClearValuesUint,
                                  const float *UAVClearValuesFloat);
    void UncompressColorBuffer(ZRenderTargetView *pRTV);
    void UncompressDepthBuffer(ZRenderDepthStencilView *pDSV);
    void SetViewport(const SRenderViewport &viewport);
    void SetScissorRect(const SRenderViewport &scissorRect);
    void BeginEvent(uint32_t nColor, const char *pszName);
    void EndEvent();
    void AddRenderMarker(uint64_t id, uint64_t data);
    void BeginTimer(uint64_t id, const char *pszName, uint64_t data);
    void EndTimer(uint64_t id);
    void BeginQuery(ZRenderQuery *pQuery);
    void EndQuery(ZRenderQuery *pQuery);
    bool GetData(ZRenderQuery *pQuery, void *pData, uint32_t nDataSize, uint32_t nFlags);
    void DrawRenderList(ZRenderList *renderList);
    void ExecuteCallBack(ZDelegate<void __cdecl(ZRenderDeviceContext *, void *, int)> cb,
                         void *data, uint32_t size);
    void ExecuteCallBack(ZDelegate<void __cdecl(ZRenderDeviceContext *)> cb);
    void Copy(IRenderResource *dest, IRenderResource *src);
    void CopySubresource(IRenderResource *dest, ERenderPlaneUsage eDstPlane, IRenderResource *src,
                         ERenderPlaneUsage eSrcPlane);
    void CopySubresourceRegion(IRenderResource *pDstResource, uint32_t dstSubresource,
                               uint32_t dstX, uint32_t dstY, uint32_t dstZ,
                               IRenderResource *pSrcResource, uint32_t srcSubresource,
                               const SRenderRect *pRect);
    void CopyData(void *pDst, void *pSrc, uint64_t nSise);
    void ResolveRTV(ZRenderTargetView *pDstRTV, ZRenderShaderResourceView *pSrcSRV);
    void SyncGpu(ERenderSyncGpu nGpuCacheFlags, uint64_t *pLabel, uint64_t value);
    void SyncGpu(ERenderSyncGpu nGpuCacheFlags);
    void DrawQuadNew(float x, float y, float w, float h, const float4 &vColor, float fZValue,
                     float tx, float ty, float wtx, float wty, bool bUseDefaultVS,
                     ZRenderShader *pFragmentShader_, ZRenderShader *pVertexShader_,
                     const ZRenderConstBufferRef ConstBufferF,
                     const ZRenderConstBufferRef ConstBufferV, ZRenderShader *pGeometryShader_,
                     const ZRenderConstBufferRef ConstBufferG);
    void PushStates();
    void PopStates();
    void EndReccord();
    void UnmapConstBuffer(ZRenderConstBufferRef ref);
    void SetComputeTexture(uint32_t nTexUnit, ZRenderShaderResourceView *pResource);
    void SetUnorderedAccessViewsCompute(uint32_t nUnit, ZRenderUnorderedAccessView *pUAV,
                                        uint32_t nInitialCount);
    void DispatchCompute(ZRenderShader *pComputeShader, const ZRenderConstBufferRef pBuffer,
                         uint32_t nThreadX, uint32_t nThreadY, uint32_t nThreadZ);
    void DispatchComputeUnsafe(ZRenderShader *pComputeShader, const ZRenderConstBufferRef pBuffer,
                               uint32_t nThreadX, uint32_t nThreadY, uint32_t nThreadZ);
    void DispatchComputeJob(EComputeJobRing ring, ZDispatchCall *pDispatchCallArray,
                            uint64_t arraySize);
    void DispatchComputeJobUnsafe(EComputeJobRing ring, ZDispatchCall *pDispatchCallArray,
                                  uint64_t arraySize);
    void FlushStatesCompute();
    void CopyStructureCount(ZRenderBuffer *pDstBuffer, uint32_t nDstAlignedByteOffset,
                            ZRenderUnorderedAccessView *pSrcUAV);
    void CopyMem(ZRenderBuffer *pDstBuffer, void *pData, uint64_t nSize);
    void SetDataBuffer(ERenderShaderType eShaderStage, EDataBufferSlot nSlotId,
                       const SRenderBufferAccess &pRenderBuffer);
    void ClearRenderTargetAsync(ZRenderTargetView *pRenderTargetView, const float4 clearColor,
                                uint64_t *pLabelSyncWithGfx, uint64_t *pLabelFinish,
                                uint64_t nRefValue);
    void WaitClearRenderTargetAsync(uint64_t *pLabelFinish, uint64_t nRefValue);
    void BeginCapture();
    void EndCapture();
    int32_t GetNumPendingCommands();
    int32_t ConsumeCommand();
    void WriteCommandHeader(SCmdBase *pCmd, uint8_t nCmdType, uint32_t commandSize);
    void SetBlendState(ZRenderBlendState *pBlendState, uint32_t nSampleMask);
    void SetRasterizerState(ZRenderRasterizerState *pRasterizerState);
    void SetDepthStencilState(ZRenderDepthStencilState *pDepthStencilState, uint8_t nStencilRef);
    void SetAntialiasingState(EAntialiasingMode mode);
    void SetInputLayout(ZRenderInputLayout *pInputLayout);
    void SetSharedConstBuffer(uint32_t nCBuffer, uint32_t nCBufferSlot,
                              ZRenderConstBufferRef SharedConstBuffer);
    void SetUserData(ERenderShaderType eShaderStage, uint32_t slot, uint32_t *uint32Ptr,
                     uint64_t nUint32s);
    void SetShaderResourceView(ERenderShaderType eShaderStage, uint32_t nTexUnit,
                               ZRenderShaderResourceView *pShaderResourceView);
    void SetVertexBuffers(uint32_t nStartSlot, uint32_t nNumBuffers,
                          ZRenderBuffer *const *pVertexBuffers, const uint8_t *pStrides);
    void SetIndexBuffer(ZRenderBuffer *pIndexBuffer, ERenderFormat eFormat, uint32_t nOffset);
    void SetConstBuffers(ZRenderConstBufferRef ConstBufferV, ZRenderConstBufferRef ConstBufferF);
    void SetConstBufferV(ZRenderConstBufferRef ConstBufferV);
    void SetConstBufferF(ZRenderConstBufferRef ConstBufferF);
    void SetConstBufferH(ZRenderConstBufferRef ConstBufferH);
    void SetConstBufferD(ZRenderConstBufferRef ConstBufferD);
    void SetConstBufferG(ZRenderConstBufferRef ConstBufferG);
    void SetConstBufferC(ZRenderConstBufferRef ConstBufferC);
    void SetShaders(ZRenderShader *pShaderV, ZRenderShader *pShaderF);
    void SetVertexShader(ZRenderShader *pShader);
    void SetFragmentShader(ZRenderShader *pShader);
    void SetHullShader(ZRenderShader *pShader);
    void SetDomainShader(ZRenderShader *pShader);
    void SetGeometryShader(ZRenderShader *pShader);
    void SetComputeShader(ZRenderShader *pShader);
    void SetDrawIndexed(uint32_t nStartIndex, uint32_t nNumIndices, int32_t nBaseVertex,
                        uint32_t nNumVertices, ERenderPrimitiveTopology eMeshTopology);
    void SetDrawInstanced(uint32_t nStartIndex, uint32_t nNumIndices, int32_t nBaseVertex,
                          uint32_t nNumVertices, ERenderPrimitiveTopology eMeshTopology,
                          uint32_t instanceCount);
    void SetDrawInstancedIndirect(ZRenderBuffer *pBuffer, ERenderPrimitiveTopology eMeshTopology);
    void BeginOcclusionPredication(ZRenderPredication *pPredication);
    void EndOcclusionPredication(ZRenderPredication *pPredication);
    void ActivateOcclusionPredication(ZRenderPredication *pPredication, bool bPredicateValue);
    void DeactivateOcclusionPredication();
    void StoreRightEyeResult();
    void SendMultiGPUHint(EMultiGPUHint hint, IRenderResource *pResource, uint64_t offset,
                          uint64_t size);
    void SetScaleformProjectionMatrices(const SMatrix &mCenter, const SMatrix &mCorrected);
    void Barrier(ZRenderTexture2D *pBuf, uint16_t state, uint32_t subres, uint8_t bStencil);
    const uint8_t *const GetStart();
    const uint8_t *const GetPut();
    SCmdBufferPage *GetFirstCmdPage();
    int32_t IncrementConsumedCmdCount();
    int32_t GetConsumedCmdCount();
    void JumpToStart(ZCommandRecorder *jumpTo);
    void JumpBack(ZCommandRecorder *jumpTo);
    struct SBookmark {
        ZCommandRecorder *m_CommandRecorder;
        SCmdBufferPage *m_NextPage;
        uint64_t m_PageMemOffset;
    };
    void JumpToBookmark(const ZCommandRecorder::SBookmark &bookmark);
    ZCommandRecorder::SBookmark GetBookmark();
    void DispatchComputeInternal(ZRenderShader *pComputeShader, const ZRenderConstBufferRef pBuffer,
                                 uint32_t nThreadX, uint32_t nThreadY, uint32_t nThreadZ,
                                 bool unsafe);
    void DispatchComputeJobInternal(EComputeJobRing ring, ZDispatchCall *pDispatchCallArray,
                                    uint64_t arraySize, bool unsafe);
    TArray<SCmdBufferPage *> m_memoryPages;
    uint32_t m_pageSize;
    SCmdBufferPage *m_currentPage;
    uint32_t m_currentPageIndex;
    bool m_isMainRecorder;
    uint8_t *CondExpand(uint32_t nCmdSize);
    void GetMoreSpace(uint32_t nNewSize);
    ZCommandRecorder &operator=(const ZCommandRecorder &);
    void IncrementCmdCount(int32_t nCommands);
    void IncrementCmdCount();
    ZRenderConstBufferRef m_ConstantBuffers[6];
    ZRenderShader *m_Shaders[6];
    uint64_t m_nNumIndices;
    uint32_t m_nNumDrawCalls;
    std::atomic<int> m_numCmds;
    std::atomic<int> m_numConsumedCmd;
    ZThreadEvent m_event;
    uint8_t *m_pStart;
    uint8_t *m_pEnd;
    uint8_t *m_pPut;
};

class IRenderPrimitiveBuffer {
    public:
    virtual ~IRenderPrimitiveBuffer();
    virtual void Map(ERenderResourceMapType, uint32_t, uint64_t, void **);
    virtual void Unmap(ZJobChainThread *);
    virtual void Update(uint32_t, uint32_t, const void *);
    virtual const uint8_t *GetCPUBuffer();
};

struct SRenderPrimitiveMeshDesc {
    ERenderPrimitiveType ePrimitiveType;
    uint32_t nNumVertices;
    uint32_t nNumIndices;
    ERenderPrimitiveTopology eRenderPrimitiveTopology;
    uint32_t nColorOffset;
    uint8_t nNumStreams;
    uint8_t anStreamStride[4];
    bool bHasTessellationStream;
    enum {
        DESC_MESH_UNCOMPRESSED,
        DESC_MESH_NO_COPY_FOR_CPU_READ,
        DESC_MESH_NO_MULTIGPU_SYNC,
    };
    uint32_t nFlags;
    SRenderPrimitiveMeshDesc();
};

class IRenderResourceAsyncUpdate : public IRenderRefCount {
    public:
    virtual void Apply(ZRenderDeviceContext *);
};

class ZRenderAsyncUpdateRenderPrimitiveProperties
    : public TRenderReferencedCountedImpl<IRenderResourceAsyncUpdate, 0> {
    public:
    ZRenderAsyncUpdateRenderPrimitiveProperties(IRenderPrimitive *pPrimitiveInstance);
    virtual ~ZRenderAsyncUpdateRenderPrimitiveProperties();
    virtual void Apply(ZRenderDeviceContext *deviceContext);
    void SetBounds(const float4 vMin, const float4 vMax);
    void SetNumIndicesToDraw(uint32_t nNumIndicesToDraw);
    void SetLODMask(uint8_t);
    void SetVertexBuffers(uint32_t, ZRenderBuffer **);
    uint32_t m_nNumIndicesToDraw;
    uint32_t m_nNumBuffers;
    IRenderPrimitive *m_pPrim;
    uint8_t m_nLODMask;
    bool m_bChangedBounds;
    bool m_bChangedNumIndicesToDraw;
    bool m_bChangedLODMask;
    bool m_bChangedBuffers;
    ZRenderBuffer *m_pVertexBuffers[4];
};

class IRenderMaterialInstanceData {
    public:
    virtual const TResourcePtr<ZEntityRef> &GetMaterialDescriptor();
    virtual bool IsOccluder();
    virtual bool IsOpaqueShadowCaster();
    virtual bool IsTransparent();
    virtual uint32_t IsTwoSided();
    virtual bool IsArbitraryOriented();
    virtual ZRenderMaterialInstance *GetMaterialInstance();
};

class ZRenderMaterialInstanceData : public IRenderMaterialInstanceData {
    public:
    ZRenderMaterialInstanceData(const TSharedPtr<ZResourceReader> &pResourceReader,
                                ZRenderMaterialInstance *pMaterialInstance);
    virtual ~ZRenderMaterialInstanceData();
    virtual const TResourcePtr<ZEntityRef> &GetMaterialDescriptor();
    virtual bool IsOccluder();
    virtual bool IsOpaqueShadowCaster();
    virtual bool IsTransparent();
    virtual uint32_t IsTwoSided();
    virtual bool IsArbitraryOriented();
    virtual ZRenderMaterialInstance *GetMaterialInstance();
    TSharedPtr<ZResourceReader> m_pResourceReader;
    ZRenderMaterialInstance *m_pMaterialInstance;
    TResourcePtr<ZEntityRef> m_MaterialDescriptor;
};

template <typename T, typename C>
class TBag {
    public:
    TBag<T, C>(TBag<T, C> *);
    TBag<T, C>(const TBag<T, C> &);
    TBag<T, C>();
    void Reserve(uint64_t, bool);
    void Trim();
    TBag<T, C> &operator=(TBag<T, C> *);
    TBag<T, C> &operator=(const TBag<T, C> &);
    bool operator==(const TBag<T, C> &);
    const C &Container() const;
    C &Container();
    TArrayIterator<T const> Begin() const;
    TArrayIterator<T> Begin();
    TArrayIterator<T const> End() const;
    TArrayIterator<T> End();
    TArrayEnumerator<T const> GetEnumerator() const;
    TArrayEnumerator<T> GetEnumerator();
    bool IsEmpty();
    uint64_t Size();
    uint64_t Capacity();
    TArrayIterator<T const> Find(T const &) const;
    TArrayIterator<T> Find(T const &);
    void Assign(const TBag<T, C> &);
    void Clear();
    void Swap(TBag<T, C> &);
    TArrayIterator<T> Insert(T const &);
    TArrayIterator<T> Erase(TArrayIterator<T>);
    void Prefetch();
    C m_container;
};

struct SColiBoxHeader {
    uint16_t nNumChunks;
    uint16_t nTriPerChunk;
};

class ZRenderSharedResources;

class ZRenderContext;

struct SRenderDrawContext {
    const SMatrix &GetWorldToViewMatrix();
    const ZRenderContext *GetRenderContext();
    void SetRenderContext(ZRenderContext *);
    float4 GetPerObjectSimpleColor();
    void SetPerObjectSimpleColor(const float4);
    bool GetMaterialHasVertexShader();
    void SetMaterialHasVertexShader(bool bVal);
    bool GetIgnoreMaterialVertexShader();
    void SetIgnoreMaterialVertexShader(bool);
    bool GetIgnoreMaterialPixelShader();
    void SetIgnoreMaterialPixelShader(bool);
    bool GetIgnoreVertexBuffers();
    void SetIgnoreVertexBuffers(bool);
    bool GetMaterialHasTessellationShader();
    void SetMaterialHasTessellationShader(bool bVal);
    bool GetMaterialUsesClip();
    void SetMaterialUsesClip(bool bVal);
    float GetMaterialTessellationMaxRange();
    void SetMaterialTessellationMaxRange(float fVal);
    uint32_t GetMaterialTessellationComponents();
    void SetMaterialTessellationComponents(uint32_t uComponents);
    ERenderLayer GetRenderLayer();
    ZRenderShaderResourceView *GetWhiteSRV();
    bool GetDisableTextures();
    bool GetDisplayTexelDensity();
    bool GetWireFrameOverlay();
    bool GetOcclusionOverlay();
    bool GetDisplayShaderPerformance();
    bool GetSetupMiscConstBuffer();
    void SetBlendStateOverrided(bool bOverrided);
    bool IsBlendStateOverrided();
    void SetRasterizerStateOverrided(bool bOverrided);
    bool IsRasterizerStateOverrided();
    bool GetBatchingEnable();
    bool ApplyMaterials();
    bool IsTransparentPass();
    bool IsEmissiveAdditive();
    bool IsFPSDrawMode();
    bool HasBackLight();
    bool HasDecalModulate();
    ZRenderRasterizerState *GetRasterizerState(ERenderCullMode eCullMode);
    ZRenderRasterizerState *GetDecalRasterizerState(ERenderCullMode eCullMode);
    float4 m_vPerObjectSimpleColor;
    ERenderLayer m_eRenderLayer;
    ZRenderShaderResourceView *m_pWhiteSRV;
    const ZRenderContext *m_pRenderContext;
    ZRenderRasterizerState *m_pRasterizerStates[3];
    ZRenderRasterizerState *m_pDecalRasterizerStates[3];
    bool m_bMaterialHasVertexShader;
    bool m_bIgnoreMaterialVertexShader;
    bool m_bIgnoreMaterialPixelShader;
    bool m_bIgnoreVertexBuffers;
    bool m_bMaterialHasTessellation;
    bool m_bMaterialUsesClip;
    bool m_bDisableTexture;
    bool m_bDisplayTexelDensity;
    bool m_bWireFrameOverlay;
    bool m_bOcclusionOverlay;
    bool m_bDisplayShaderPerformance;
    bool m_bSetupMiscConstBuffer;
    bool m_bBlendStateOverrided;
    bool m_bRasterizerStateOverrided;
    bool m_bBatchingEnable;
    bool m_bApplyMaterials;
    bool m_bTransparentPass;
    bool m_bEmissiveAdditive;
    bool m_bFPSDrawMode;
    bool m_bHasBackLight;
    bool m_bHasDecalModulate;
    float m_fMaterialTessellationMaxRange;
    uint32_t m_uMaterialTessellationComponents;
};

class ZRenderDrawCall {
    public:
    enum {
        kMaxVertexBufferCount,
        kMaxSharedBufferCount,
        kMaxSRVCount,
        kMaxShaderStageCount,
    };
    enum StateFlag {
        kBlendStateFlag,
        kRasterizerStateFlag,
        kLast,
        kMax,
    };
    ZRenderDrawCall(const ZRenderDrawCall &);
    ZRenderDrawCall();
    void SetNormalizedDepth(float z);
    void SetWorldDepth(float z);
    void SetZBias(uint8_t bias);
    void SetInputLayout(ZRenderInputLayout *inputLayout);
    void SetVertexBuffers(uint32_t nStartSlot, uint32_t nNumBuffers,
                          ZRenderBuffer *const *pVertexBuffers, const uint8_t *pStrides,
                          const uint32_t *pOffsets);
    void SetIndexBuffer(ZRenderBuffer *pIndexBuffer, uint32_t nOffset);
    void AddSharedConstBuffer(uint64_t slot, const ZRenderConstBufferRef &constBuffer,
                              bool doUnmap);
    void AddConstBuffer(ERenderShaderType eShaderStage, const ZRenderConstBufferRef &constBuffer,
                        bool doUnmap);
    void AddSRV(ERenderShaderType eShaderStage, uint64_t slot, ZRenderShaderResourceView *srv);
    void SetShader(ERenderShaderType eShaderStage, ZRenderShader *shader);
    void SetBlendState(ZRenderBlendState *pBlendState);
    void SetRasterizerState(ZRenderRasterizerState *pRasterizerState);
    void SetDrawIndexed(uint32_t nStartIndex, uint32_t nNumIndices, int32_t nBaseVertex,
                        uint32_t nNumVertices, ERenderPrimitiveTopology eMeshTopology);
    void SetUseFullVS(bool useFullVS);
    void SetIgnoreVertexShader(bool ignoreVertexShader);
    void SetIgnorePixelShader(bool ignorePixelShader);
    bool GetIgnorePixelShader();
    void SetIgnoreVertexBuffers(bool ignoreVertexBuffers);
    void SetUseOrder(bool useOrder);
    void SetLayerOffset(uint8_t layerOffset);
    void SetOrder(uint8_t order);
    void SetPrimitiveVertexType(ERenderPrimitiveType primitiveVertexType);
    // void SetPrimitiveType(IRenderPrimitive::EPrimitiveType primitiveType);
    void SetObjectToWorld(const SMatrix &objectToWorld);
    void SetPreviousObjectToWorld(const SMatrix &previousObjectToWorld);
    void SetVertexBufferStartIndices(uint32_t previousStartIndex, uint32_t currentStartIndex);
    void SetTessellationComponents(uint32_t components);
    uint32_t GetTessellationComponents();
    uint8_t GetMeshTopology();
    ZRenderShader *GetShader(ERenderShaderType);
    ZRenderDrawCall &operator=(const ZRenderDrawCall &);
    struct VertexBuffer_SOA {
        ZRenderBuffer *buffer[4];
        uint32_t offset[4];
        uint8_t slot[4];
        uint8_t stride[4];
    };
    struct SharedConstBuffer_SOA {
        ZRenderConstBufferRef cbufferRef[12];
        uint8_t slot[12];
        uint16_t doUnmapBF;
    };
    struct ConstBuffer_SOA {
        ZRenderConstBufferRef cbufferRef[5];
        uint8_t stage[5];
        uint8_t doUnmapBF;
    };
    struct SRV_SOA {
        ZRenderShaderResourceView *srv[32];
        uint8_t stage[32];
        uint8_t slot[32];
    };
    struct Shader_SOA {
        ZRenderShader *shader[5];
        uint8_t stage[5];
    };
    ZRenderInputLayout *m_InputLayout;
    ZRenderBuffer *m_IndexBuffer;
    uint32_t m_IndexBufferOffset;
    uint32_t m_DrawStartIndex;
    uint32_t m_DrawNumIndices;
    uint32_t m_DrawBaseVertex;
    uint32_t m_DrawNumVertices;
    uint8_t m_PrimitiveType;
    uint8_t m_VertexBufferCount;
    uint8_t m_SharedConstBufferCount;
    uint8_t m_ConstBufferCount;
    uint8_t m_SRVCount;
    uint8_t m_ShaderCount;
    uint8_t m_MeshTopology;
    uint8_t m_PrimitiveVertexType;
    uint8_t m_StateFlags;
    uint8_t m_UseFullVS;
    uint8_t m_LayerOffset;
    uint8_t m_bUseOrder;
    uint8_t m_IgnoreVertexShader;
    uint8_t m_IgnorePixelShader;
    uint8_t m_IgnoreVertexBuffers;
    uint8_t m_hdr[48];
    uint8_t m_ZBias;
    float m_NormalizedDepth;
    float m_WorldDepth;
    uint32_t m_TessellationComponents;
    SMatrix m_ObjectToWorld;
    SMatrix m_PreviousObjectToWorld;
    uint32_t m_VertexBufferStartIndices[2];
    ZRenderDrawCall::VertexBuffer_SOA m_VertexBuffers;
    ZRenderDrawCall::SharedConstBuffer_SOA m_SharedConstBuffers;
    ZRenderDrawCall::ConstBuffer_SOA m_ConstBuffers;
    ZRenderDrawCall::Shader_SOA m_Shaders;
    ZRenderBlendState *m_BlendState;
    ZRenderRasterizerState *m_RasterizerState;
    ZRenderDrawCall::SRV_SOA m_SRVs;
};

enum EGraphTraversalType {
    GRAPHTRAVERSAL_MAIN,
    GRAPHTRAVERSAL_SHADOWCASTER,
    GRAPHTRAVERSAL_RAIN,
    GRAPHTRAVERSAL_REFLECTIVEWATER,
    GRAPHTRAVERSAL_XRAY,
    GRAPHTRAVERSAL_Count,
    GRAPHTRAVERSAL_INVALID,
};

class ZRenderPrimitiveInstance;

class ZRenderGraphNode : public ZVTablePaddingRemover {
    public:
    virtual ~ZRenderGraphNode();
    enum FLAGS {
        RESERVED_FLAG_0,
        RESERVED_FLAG_1,
        RESERVED_FLAG_2,
        RESERVED_FLAG_3,
        RESERVED_FLAG_4,
        RESERVED_FLAG_5,
        RESERVED_FLAG_6,
        RESERVED_FLAG_7,
        RESERVED_FLAG_8,
        DECAL_MASK,
        IS_OCCLUDER_ONLY,
        PERFORM_VISIBILITY_TEST,
        IS_RENDERABLE,
        CAST_SHADOWS,
        IS_PLAYER,
        DONT_DRAW,
        IS_OCCLUDER,
        USE_TIMEALIVE,
        IS_STATIC_GEOM,
        IS_SCALE_INVERTED,
        HAS_CLOAK,
        HAS_CLOAK_IN_TRANSITION,
        SUPPORT_VELOCITY,
        HAS_COLOR_MODIFIER,
        HAS_TESSELLATION_TITAN_EFFECT,
        VISIBILITY_ALWAYSPASS,
        HAS_BACKLIGHT,
        IS_HALFRES,
        HAS_VERTEX_COMPUTE,
        IS_BACKGROUND,
        IS_FOREGROUND,
        TRANSPARENT_EXCLUDE_FROM_DOF,
    };
    ZRenderGraphNode();
    void Reflect(ZReflectable &reflectable, ZJobChainThread *pJobChain);
    void ReflectBase(const ZReflectable &reflectable, const SRenderReflectData &reflectData);
    virtual void CleanUp();
    virtual bool IsSource();
    virtual void PostVisibleChangedEvent(ZReflectable &reflectable, bool bVisible);
    virtual bool SetAdditionalSharedConstBufferAndTextures_NEW(
        ZRenderDrawCall &drawCall, ZRenderPrimitiveInstance *pRenderPrimitiveInstance,
        const ZRenderContext *const pRenderContext, ERenderLayer eLayer);
    const SMatrix GetObjectToWorldTransform();
    const float4 GetObjectToWorldTrans();
    void SetObjectToWorldTransform(const SVector4 *pInputMatrix3x4);
    float4 GetLocalHalfSize();
    float4 GetLocalCenter();
    void SetLocalHalfSize(const SVector3 &vHalfSize);
    void SetLocalCenter(const SVector3 &vCenter);
    void SetLocal(const float4 vHalfSize, const float4 vCenter);
    void SetMinWorld(const SVector3 &minWorld);
    void SetMaxWorld(const SVector3 &maxWorld);
    void GetTotalExtentsInternal(const ZReflectable &reflectable, SVector3 &vMin, SVector3 &vMax);
    void ReflectBaseCommon(const ZReflectable &reflectable, const SRenderReflectData &reflectData);
    void SetType(EReflectableType type);
    void GetTotalExtents(SVector3 &vMin, SVector3 &vMax);
    const SVector3 &GetMinExtent();
    const SVector3 &GetMaxExtent();
    void SetPerformVisibilityTest(bool bPerformVisibilityTest);
    void SetCastShadows(bool bCastShadows);
    void SetColorModifier(uint32_t nColorModifier);
    uint32_t GetColorModifier();
    void SetTimeAlive(float fTimeAlive);
    float GetTimeAlive();
    void SetRenderSceneMask(uint64_t nRenderSceneMask);
    uint64_t GetRenderSceneMask();
    EReflectableType GetType();
    void SetReflectable(ZReflectable *reflectable);
    ZReflectable *GetReflectable();
    void SetDistanceSortValue(int32_t);
    int32_t GetDistanceSortValue();
    bool IsStatic();
    bool IsOccluder();
    bool IsBackground();
    bool IsForeground();
    bool IsOccluderOnly();
    bool HasVertexCompute();
    bool IsRenderable();
    virtual const ZString &GetDebugName();
    void SetReflectInProgress(bool reflectInProgress);
    virtual void FirstTimeReflectInit(ZReflectable &reflectable);
    uint8_t GetVisibilityGridType();
    void SetVisibilityGridType(uint8_t nVisibilityGridType);
    int32_t GetVisibilityId();
    void SetVisibilityId(int32_t id);
    uint64_t GetVelocityId();
    void SetVelocityId(uint64_t id);
    bool SupportVelocity();
    uint8_t GetVisibilityEventMask();
    bool NeedsToBeVisibleThisFrame(uint32_t renderFrameCounter);
    enum {
        EDefaultCharacterLightParam,
    };
    virtual uint32_t GetCharacterLightParam();
    virtual void SetCharacterLightParam(uint32_t nParam);
    bool HasVisibilityEvent(EGraphTraversalType graphTraversalType);
    uint8_t m_nType;
    uint8_t m_nVisibilityGridType;
    uint8_t m_nVisibilityEventMask;
    uint32_t m_nFlags;
    uint64_t m_nRenderSceneMask;
    std::atomic<ZReflectable *> m_pReflectable;
    uint32_t m_nRenderFrameCounter;
    SVector4 m_mObjectToWorld_[3];
    SVector3 m_vCenter_;
    SVector3 m_vHalfSize_;
    SVector3 m_vMinWorld;
    SVector3 m_vMaxWorld;
    uint32_t m_nColorModifier;
    float m_fTimeAlive;
    uint64_t m_velocityId;
    int32_t m_visibilityId;
    bool m_reflectInProgress;
    ZString m_sDebugName;
};

class ZRenderTextureResource : public TRenderReferencedCountedImpl<IRenderTextureResource, 0> {
    public:
    ZRenderTextureResource(const TArray<IRenderResource *> &resources,
                           const TArray<ZRenderShaderResourceView *> &SRVs,
                           ZTextureMap::EInterpretAs eInterpretAs,
                           ZTextureMap::EDimensions eDimensions);
    ZRenderTextureResource(IRenderResource *pResource, ZRenderShaderResourceView *pSRV,
                           ZTextureMap::EInterpretAs eInterpretAs,
                           ZTextureMap::EDimensions eDimensions);
    virtual ~ZRenderTextureResource();
    ZRenderTexture2D *GetTexture2D(uint64_t nIndex);
    ZRenderShaderResourceView *GetSRV(uint64_t nIndex);
    ZTextureMap::EInterpretAs GetInterpretAs();
    ZTextureMap::EDimensions GetDimensions() const;
    void SetReady(bool bReady);
    TArray<ZRenderShaderResourceView *> m_SRVs;
    ZTextureMap::EInterpretAs m_eInterpretAs;
    ZTextureMap::EDimensions m_eDimensions;
    bool m_bReady;
};

void ImportZRuntimeRenderModule();