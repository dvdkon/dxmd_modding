// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "g2base.hpp"
#include "system.io.hpp"
#include "runtime.debug.hpp"

namespace SaveUtils {
enum EEntityIgnoredWhenSavingResult {
    Ignored,
    NotIgnored,
    Disabled,
};
};

// Forward-declarations
class ZEntityType;
class ZEntityImpl;
class ISceneContext;
class ZCustomSaveDataSerializer;
class ZCustomSaveDataDeserializer;
class IEntityBlueprintFactory;
class ZSaveGameDataSerializer;
class ZSaveGameDataDeserializer;
class ZSaveDataValueStorage;
class ZLocalIDPathToEntityRefStorage;
class IEntityFactory;
struct SEntityInstantiationSaveData;

class ZSubEntityPath {
    public:
    static void RegisterType();
    ZSubEntityPath(const TArrayRef<unsigned int> &subEntityIds);
    ZSubEntityPath();
    bool IsEmpty();
    bool operator<(const ZSubEntityPath &other);
    bool operator==(const ZSubEntityPath &other);
    uint64_t GetHash();
    uint32_t GetPathLength();
    uint32_t GetPathDepth();
    ZString ToString();
    static const ZSubEntityPath RootPath;
    uint64_t GetFullValue();
    void StoreHash(uint64_t hashValue, uint32_t hashLength);
    uint32_t m_LowIdHashes;
    uint32_t m_HighIdHashes;
};

class ZEntityRef {
    public:
    class ZNULL {};

    static void RegisterType();
    ZEntityRef(ZEntityRef::ZNULL *__formal);
    ZEntityRef(const ZEntityRef &rhs);
    ZEntityRef();
    ~ZEntityRef();
    void *QueryInterfacePtr(STypeID *interfaceID);
    ZString ToString();
    uint64_t ToCString(char *pBuffer, uint64_t nSize, const ZString &sFormat);
    bool IsNull();
    bool IsValid();
    ISceneContext *GetContext();
    ZEntityImpl *GetEntityImpl();
    void FillSubsetArray(uint32_t nSubsetID, TMinCapacityArray<ZEntityRef, 512> &entities,
                         bool bChildSet);
    ZString GetDebugName();
    ZString GetDebugNameScenePath();
    bool
    IsRegisteredForDeletionListening(const ZDelegate<void __cdecl(ZEntityRef const &)> &fpCallback);
    bool
    RegisterForDeletionListening(const ZDelegate<void __cdecl(ZEntityRef const &)> &fpCallback);
    void
    UnregisterForDeletionListening(const ZDelegate<void __cdecl(ZEntityRef const &)> &fpCallback);
    void IgnoreEntityWhenSaving();
    ZEntityRef &operator=(ZEntityRef::ZNULL *__formal);
    ZEntityRef &operator=(const ZEntityRef &rhs);
    bool operator==(ZEntityRef::ZNULL *__formal);
    bool operator==(const ZEntityRef &rhs);
    bool operator!=(ZEntityRef::ZNULL *__formal);
    bool operator!=(const ZEntityRef &rhs);
    bool operator<(const ZEntityRef &rhs);
    bool operator>(const ZEntityRef &rhs);
    ZEntityType **GetEntityTypePtrPtr();
    static ZEntityRef FromEntityTypePtrPtr(ZEntityType **pEntityTypePtrPtr);
    static ZEntityRef FromUnresolvedEntityTypePtrPtr(ZEntityType **pEntityTypePtrPtr);
    static ZEntityRef FromTEntityRef(ZVariantRef entityRef);
    uint32_t GetHashCode();
    ZEntityType **m_pEntityTypePtrPtr;
};

template <typename T>
class TEntityRef {
    public:
    class ZNULL {};

    static void RegisterType();
    static STemplatedTypeName1 s_typeName;
    static const ZConstructorInfo s_constructors[0];
    static IClassType s_typeInfo;
    TEntityRef<T>(TEntityRef<T>::ZNULL *__formal);
    TEntityRef<T>(const ZEntityRef &rhs);
    TEntityRef<T>(const TEntityRef<T> &);
    TEntityRef<T>();
    TEntityRef<T> &operator=(TEntityRef<T>::ZNULL *);
    TEntityRef<T> &operator=(const ZEntityRef &);
    TEntityRef<T> &operator=(const TEntityRef<T> &);
    operator const class ZEntityRef &();
    T *operator->();
    T *GetRawPointer();
    bool IsNull();
    bool IsValid();
    void FillSubsetArray(uint32_t, TMinCapacityArray<ZEntityRef, 512> &, bool);
    ZEntityImpl *GetEntityImpl();
    ISceneContext *GetContext();
    ZString GetDebugName();
    ZString GetDebugNameScenePath();
    ZString ToString();
    uint64_t ToCString(char *, uint64_t, const ZString &);
    bool IsRegisteredForDeletionListening(const ZDelegate<void __cdecl(ZEntityRef const &)> &);
    bool RegisterForDeletionListening(const ZDelegate<void __cdecl(ZEntityRef const &)> &);
    void UnregisterForDeletionListening(const ZDelegate<void __cdecl(ZEntityRef const &)> &);
    uint32_t GetHashCode();
    ZEntityType **GetEntityTypePtrPtr();
    bool operator==(TEntityRef<T>::ZNULL *);
    bool operator==(const TEntityRef<T> &);
    bool operator!=(TEntityRef<T>::ZNULL *);
    bool operator!=(const TEntityRef<T> &);
    bool operator<(const TEntityRef<T> &);
    bool operator>(const TEntityRef<T> &);
    static TEntityRef<T> FromEntityTypePtrPtr(ZEntityType **, T *);
    ZEntityRef m_entityRef;
    T *m_pInterfaceRef;
};

class ZBaseSafeEntityRef {
    public:
    static void RegisterType();
    ZBaseSafeEntityRef();
};

class ZBaseDeletionListener {
    public:
    static void RegisterType();
    virtual ~ZBaseDeletionListener();
    static ZSpinlock &GetSpinLock();
    ZBaseDeletionListener();
    bool Register(const ZEntityRef &entity);
    void Unregister(const ZEntityRef &entity);
    virtual bool EntityDeleted(const ZEntityRef &);
    void OnEntityDeleted(const ZEntityRef &entity);
    bool m_bIsRegistered;
};

template <typename T>
class TGenericRef {
    public:
    TGenericRef<T>(const T &);
    TGenericRef<T>();
    bool IsNull();
    bool IsValid();
    const ZEntityRef &ImplicitCast();
    const ZEntityRef &GetEntity();
    T GetValue();
    const T &GetValueConstRef();
    T m_ref;
};

class IEntity : public IComponentInterface {
    public:
    static void RegisterType();
    static void EntityReferenceableClassFunction();
    virtual ZEntityRef GetID();
};

enum EEntityLifeCycleStates {
    eConstructed,
    eCreationStates,
    eInitializing,
    eInitialized,
    ePostInitialized,
    ePostInitializationStates,
    eRestoringSaveData,
    eSavedPropertiesRestored,
    eSavedCustomDataRestored,
    eEnteringStaging,
    eStagingStates,
    eStaged,
    eExitingStaging,
    eEnteringEditMode,
    eInEditMode,
    eExitingEditMode,
    eStarting,
    eRunningStates,
    eStarted,
    eRemoving,
    eDestructionStates,
    eRemoved,
    eUninitializing,
    eUninitialized,
    eDestructing,
    eDestructed,
    eLifeCycleStateCount,
};

class ZEntityImpl : public IEntity {
    public:
    static const SComponentMapEntry s_componentMap[0];
    virtual ~ZEntityImpl();
    virtual void InitializeEntity();
    virtual void PostInitializeEntityThreaded();
    virtual void UninitializeEntity();
    virtual void OnEntityRemovedFromScene();
    static void RegisterType();
    virtual STypeID *GetEntityTypeID();
    virtual int64_t AddRef();
    virtual int64_t Release();
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    static void EntityReferenceableClassFunction();
    enum EFunctionFilterPersistence {
        eCallFunction,
        eDoNotCallFunction,
        eReevaluateFunctionCall,
    };
    enum SaveDataCompatibility {
        eDataNotBackwardsCompatible,
        eDataBackwardsCompatible,
    };
    ZEntityImpl(ZComponentCreateInfo &Info);
    virtual void OnEnterStaging();
    virtual void OnExitStaging();
    virtual void Start();
    virtual void FirstStart();
    virtual void OnEnterEdit();
    virtual void OnExitEdit();
    ZEntityImpl::EFunctionFilterPersistence ShouldCallFunctionForEntity(uint32_t functionIndex);
    virtual ZEntityImpl::EFunctionFilterPersistence
    ShouldCallFunctionForEntity(uint32_t functionIndex, bool bAllowReEvaluation);
    bool IsInEdit();
    virtual ZEntityRef GetID();
    ZEntityType *GetEntityType();
    bool SetProperty(uint32_t nPropertyID, const ZVariantRef &value);
    bool SetProperty(const ZString &sName, const ZVariantRef &value);
    void SignalInputPin(uint32_t nPinID, const ZVariantRef &data);
    void SignalInputPin(const ZString &sPin, const ZVariantRef &data);
    void SignalOutputPin(uint32_t nPinID, const ZVariantRef &data, bool bAllowDelayed);
    void SignalOutputPin(const ZString &sPin, const ZVariantRef &data, bool bAllowDelayed);
    bool HasOutputPin(const ZString &sPin);
    bool HasOutputPin(uint32_t sPin);
    uint32_t GetOutputPinCount(uint32_t sPin);
    bool IsInputPinArgumentValid(uint32_t nPinID, const ZVariantRef &data);
    bool IsInputPinArgumentValid(const ZString &sPin, const ZVariantRef &data);
    bool
    IsRegisteredForDeletionListening(const ZDelegate<void __cdecl(ZEntityRef const &)> &fpCallback);
    bool
    RegisterForDeletionListening(const ZDelegate<void __cdecl(ZEntityRef const &)> &fpCallback);
    void
    UnregisterForDeletionListening(const ZDelegate<void __cdecl(ZEntityRef const &)> &fpCallback);
    ZEntityImpl *GetExposedEntity(uint32_t nNameID);
    ZEntityImpl *GetExposedEntity(const ZString &sName);
    ZString GetDebugName();
    ZString GetDebugNameScenePath();
    ZString GetDebugNameSceneInstance();
    static void NotifyAllDeletionListenersUnderEntity(const ZEntityRef &rootEntity);
    static uint64_t GetLocalOffsetOfEntityTypeMember();
    static ZEntityImpl *GetEntityImpl(ZEntityType **pEntityType, int64_t entityOffset);
    void OnInputToOutputHandlerInternal(const ZVariantRef &data, uint32_t nExtraData);
    ZEntityType **GetEntityTypePtrPtr();
    ZEntityType **GetRealEntityTypePtrPtr();
    static uint32_t GetActivatePinID();
    static uint32_t GetDeactivatePinID();
    ZEntityType *EnsureUniqueType(uint32_t nUniqueMapMask);
    ISceneContext *GetContext();
    ISceneContext *GetMainContextForSpawning();
    ISceneContext *GetGameplayContextForSpawning();
    virtual ZEntityImpl *GetEditorParentEntity();
    virtual bool IsEditorParent();
    void IgnoreEntityWhenSaving();
    SaveUtils::EEntityIgnoredWhenSavingResult IsIgnoredWhenSaving();
    virtual bool IsManuallySavable();
    virtual void SetIsManuallySavable(bool bIsManuallySavable);
    virtual bool IsRegisteredForManualSave();
    virtual void NotifyRegisteredForManualSaveChange(bool bIsRegisteredForManualSave);
    bool IsDynamicEntity();
    bool IsStaticEntity();
    uint64_t GetDynamicFlag();
    EEntityLifeCycleStates GetLifeCycleState();
    bool HasDeletionListener();
    bool IsInConstructedState();
    bool IsInInitializingState();
    bool IsInInitializedState();
    bool IsInPostInitializedState();
    bool IsInRestoringSaveDataState();
    bool IsInSavedPropertiesRestoredState();
    bool IsInSavedCustomDataRestoredState();
    bool IsInEditModeState();
    bool IsInStagedState();
    bool IsInStartingState();
    bool IsInStartedState();
    bool IsInRemovingState();
    bool IsInRemovedState();
    bool IsInUninitializingState();
    bool IsInUninitializedState();
    bool IsInDestructingState();
    bool IsInDestructedState();
    bool IsInSaveGameLoadingState();
    bool IsInAPostInitializedState();
    bool IsInARemovedState();
    bool IsInAInitializedState();
    bool IsInAUninitializedState();
    bool IsReferenceable();
    bool CanFirePins();
    bool CanBeRegisteredForManualSave();
    bool IsFiringPinsTooEarly();
    const char *GetLifeCycleStateDescription();
    static bool IsValidEntityStateTransition(const ZEntityImpl *pEntityImpl,
                                             EEntityLifeCycleStates currentState,
                                             EEntityLifeCycleStates newLifeCycleState);
    bool IsValidTransitionState(EEntityLifeCycleStates newLifeCycleState);
    bool IsValidNextState(EEntityLifeCycleStates newLifeCycleState,
                          EEntityLifeCycleStates normalLifeCycleState,
                          EEntityLifeCycleStates skippedLifeCycleState,
                          uint32_t functionCheckIndex);
    static uint64_t s_nEntityDynamicFlagBitMask;
    static uint64_t s_nEntityStaticFlagBitMask;
    void VerifyNewLifeCycleStateIsValid(EEntityLifeCycleStates newLifeCycleState);
    void SetLifeCycleState(EEntityLifeCycleStates newLifeCycleState);
    void SetDynamicFlag(uint64_t uiDynamicFlag);
    void FinalSetLifeCycleState(EEntityLifeCycleStates newLifeCycleState);
    virtual void UncheckedSetLifeCycleState(EEntityLifeCycleStates newLifeCycleState);
    void FillSubsetArray(uint32_t nSubsetID, TMinCapacityArray<ZEntityRef, 512> &entities,
                         bool bChildSet);
    virtual uint64_t GetMemUsage();
    void SendDeletionListenerCallback();
    static int32_t HashSet_ZEntityRef(const ZEntityRef &key);
    void ClearDeletionListenerFlag();
    ZEntityType *m_pEntityType;
    ISceneContext *m_pContext;
    static uint64_t s_nEntityFlagsBitMask;
    static uint64_t s_nEntityStateBitMask;
    static uint64_t s_nEntityDeletionListenerFlagBitMask;
    static uint64_t s_nSceneContextPointerBitMask;
    static uint32_t s_nActivatePinID;
    static uint32_t s_nDeactivatePinID;
    static TUnorderedLinearHashMap<
        ISceneContext *,
        TUnorderedLinearHashMap<
            ZEntityRef,
            TUnorderedLinearHashSet<ZDelegate<void __cdecl(ZEntityRef const &)>,
                                    &HashSet_ZDeletionDelegate, 31, DefaultHashAllocator>,
            &ZEntityImpl::HashSet_ZEntityRef, 31, DefaultHashAllocator>,
        &Hash_DefaultHash<ISceneContext *>, 31, DefaultHashAllocator>
        s_registeredDeletionListeners;
    ZEntityImpl *GetOwnerEntity();
    const ZEntityImpl *GetEntitySceneInstance();
};

class ZSpatialEntity : public ZEntityImpl {
    public:
    static const SComponentMapEntry s_componentMap[0];
    virtual ~ZSpatialEntity();
    virtual void InitializeEntity();
    virtual void PostInitializeEntityThreaded();
    virtual void UninitializeEntity();
    virtual void OnEntityRemovedFromScene();
    static void RegisterType();
    virtual STypeID *GetEntityTypeID();
    virtual int64_t AddRef();
    virtual int64_t Release();
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    static void EntityReferenceableClassFunction();
    enum EIncludeFlags {
        INCLUDE_NONE,
        INCLUDE_CHILDREN,
    };
    enum EExcludeFlags {
        EXCLUDE_NONE,
        EXCLUDE_BONES,
        EXCLUDE_BOUNDS,
        EXCLUDE_ANCHORS,
    };
    enum EGenericDataChangeType {
        GENERICDATACHANGE_CHILDATTACHED,
        GENERICDATACHANGE_CHILDDETACHING,
        GENERICDATACHANGE_PARENTCHANGED,
    };
    struct SHelperPrimitiveResource {
        SHelperPrimitiveResource();
        bool bAllocated;
        bool bVisible;
        bool bIsFacingCam;
        bool bAnchor;
        bool bOverrideColor;
        uint32_t nWantedColor;
        SMatrix43 mTransform;
        ZResourcePtr pHelperPrimitiveResource;
    };
    static uint32_t GetTransformParentPropertyID();
    static uint32_t GetLocalToParentTransformPropertyID();
    ZSpatialEntity(const ZSpatialEntity &);
    ZSpatialEntity(ZComponentCreateInfo &Info);
    bool GetVisible();
    bool HasAnInvisibleParent();
    virtual void SetVisible(bool bVisible);
    void OnSetVisible();
    void OnSetInvisible();
    void SetExtraVisibilityFlag(bool bVisible);
    virtual void SetPrivate(bool bPrivate);
    virtual bool IsPrivate();
    virtual void SetFPSDrawMode(bool bFPSDrawMode);
    virtual void IgnoreParentFPSDrawMode(bool bFPSDrawMode);
    bool IsFPSDrawMode();
    virtual void SetEditorSelected(bool bSelected);
    virtual bool GetEditorSelected();
    bool IsAnyParentSelected();
    virtual void SetEditorSelectable(bool bSelectable);
    virtual bool GetEditorSelectable();
    virtual void SetEditorVisible(bool bVisible);
    virtual bool GetEditorVisible();
    void SetEditorFilteredOut(bool bFilteredOut);
    virtual bool IsPickable();
    virtual void OnManipulateFinished();
    virtual void SetTransformParent(TEntityRef<ZSpatialEntity> parent);
    void InitializeTransformParent(TEntityRef<ZSpatialEntity> parent);
    ZSpatialEntity *GetTransformParent();
    virtual TEntityRef<ZSpatialEntity> GetTransformParentEntityPtr();
    bool IsDescendantOf(const ZSpatialEntity *const pSpatial);
    bool IsDescendantOf(const TEntityRef<ZSpatialEntity> &pSpatial);
    uint32_t GetUniqueID();
    bool TryGetUniqueID(uint32_t &uniqueID);
    static uint32_t GetInvalidUniqueID();
    static void StaticCustomSaveData(const ZSpatialEntity &obj, ZVariant &outData,
                                     int32_t &outVersion);
    static void StaticCustomLoadData(ZSpatialEntity &obj, const ZVariantRef &inData,
                                     int32_t inVersion);
    void CustomSaveData_ZSpatialEntity(ZVariant &outData, int32_t currentVersion);
    void CustomLoadData_ZSpatialEntity(const ZVariantRef &inData, int32_t currentVersion);
    void Init();
    void Uninit();
    virtual ZSpatialEntity *GetScene();
    void AttachChild(ZSpatialEntity *pChild);
    void DetachChild(TArrayIterator<ZSpatialEntity *> pChild, bool bUpdateBounds);
    void DetachChild(ZSpatialEntity *pChild);
    void AttachTo(ZSpatialEntity *pParent);
    void Detach();
    void DetachChilds(bool updateBounds);
    void DetachChilds();
    void DebugUniqueID();
    virtual void AttachVisibilityProxy(TEntityRef<ZSpatialEntity> pProxy);
    virtual void DetachVisibilityProxy(TEntityRef<ZSpatialEntity> pProxy);
    void GetObjectToWorldMatrixTo3x4Mem(float4 *pOutputMatrix3x4Input);
    void GetObjectToWorldMatrixTo4x3Mem(SMatrix43 &mOutput);
    SMatrix GetObjectToWorldMatrix();
    SMatrix GetObjectToWorldMatrixWithoutScale();
    SMatrix GetObjectToWorldMatrixWithoutScaleFacing(SMatrix mToFacingReference, bool ortho);
    void SetObjectToParentMatrix(const SMatrix &mObjectToParent);
    void SetObjectToParentMatrixWithoutChangingScale(const SMatrix &mObjectToParent);
    void SetObjectToParentMatrixWithoutChangingScaleVerify(const SMatrix &mObjectToParent);
    void SetObjectToParentUnscaledMatrix(const SMatrix &mObjectToParent);
    void SetObjectToParentUnscaledMatrixVerify(const SMatrix &mObjectToParent, float epsilon);
    void SetObjectToParentMatrixVerify(const SMatrix &mObjectToParent);
    void SetObjectToParentUnscaledMatrixWithScaleVerify(const SMatrix &mUnscaledObjectToParent,
                                                        const float4 vScale,
                                                        bool bRegisterManuallySavedEntity);
    void SetObjectToWorldMatrix(const SMatrix &mObjectToWorld);
    void SetObjectToWorldMatrixWithoutScale(const SMatrix &mObjectToWorld);
    void SetObjectToWorldMatrixWithoutScaleVerify(const SMatrix &mObjectToWorld);
    void SetObjectToWorldUnscaledMatrixVerify(const SMatrix &mObjectToWorld, float epsilon);
    void SetWorldPosition(const float4 vWorldPosition);
    void SetWorldPositionVerify(const float4 vWorldPosition, float epsilon);
    void SetLocalPosition(const float4 vLocalPosition);
    virtual void CalculateBounds(float4 &vMin_, float4 &vMax_, const uint32_t nIncludeFlags,
                                 const uint32_t nExcludeFlags);
    float4 GetScaling();
    void SetScaling(const float4 scale, bool bCallScaleListeners);
    void EnableScaling();
    void DisableScaling();
    bool IsScaleEnabled();
    virtual void OnScaleChangeResetToIdentity(const ZEntityRef &entity, const float4 mWorldScale);
    float4 GetObjectToWorldScaling();
    bool IsObjectToParentIdentity();
    const float4 GetLocalPosition();
    float4 GetLocalPosition(const float4 vWorldPosition);
    float4 GetWorldPosition();
    float4 GetWorldPosition(const float4 vLocalPosition);
    float4 GetLocalDirection(const float4 vWorldDirection);
    float4 GetWorldDirection(const float4 vLocalDirection);
    float4 GetWorldForward();
    float4 GetWorldLeft();
    float4 GetWorldUp();
    SMatrix TransformObjectToWorld(const SMatrix &mMatrix);
    SMatrix TransformWorldToObject(const SMatrix &mMatrix);
    void RegisterObjectToWorldChangeCallBack(
        const ZDelegate<void __cdecl(ZSpatialEntity const *)> &CallBack);
    void UnregisterObjectToWorldChangeCallBack(
        const ZDelegate<void __cdecl(ZSpatialEntity const *)> &CallBack);
    bool IsRegisteredObjectToWorldChangeCallBack(
        const ZDelegate<void __cdecl(ZSpatialEntity const *)> &Callback);
    void RegisterScaleChangeCallBack(
        const ZDelegate<void __cdecl(ZEntityRef const &, float4)> &CallBack);
    void UnregisterScaleChangeCallBack(
        const ZDelegate<void __cdecl(ZEntityRef const &, float4)> &CallBack);
    bool IsRegisteredForScaleChangeCallBack(
        const ZDelegate<void __cdecl(ZEntityRef const &, float4)> &Callback);
    void
    RegisterForSpatialChangedCallBack(const ZDelegate<void __cdecl(ZEntityRef const &)> &CallBack);
    void UnregisterForSpatialChangedCallBack(
        const ZDelegate<void __cdecl(ZEntityRef const &)> &CallBack);
    bool IsRegisteredForSpatialChangedCallBack(
        const ZDelegate<void __cdecl(ZEntityRef const &)> &Callback);
    void RegisterForGenericDataChangedCallBack(
        const ZDelegate<void __cdecl(
            ZEntityRef const &, enum ZSpatialEntity::EGenericDataChangeType, void *)> &CallBack);
    void UnregisterForGenericDataChangedCallBack(
        const ZDelegate<void __cdecl(
            ZEntityRef const &, enum ZSpatialEntity::EGenericDataChangeType, void *)> &CallBack);
    bool IsRegisteredForGenericDataChangedCallBack(
        const ZDelegate<void __cdecl(
            ZEntityRef const &, enum ZSpatialEntity::EGenericDataChangeType, void *)> &Callback);
    virtual ZEntityImpl *GetEditorParentEntity();
    virtual bool IsManuallySavable();
    virtual void SetIsManuallySavable(bool bIsManuallySavable);
    virtual bool IsRegisteredForManualSave();
    virtual void NotifyRegisteredForManualSaveChange(bool bIsRegisteredForManualSave);
    virtual bool GetDisplayBounds();
    virtual bool GetDisplayPivot();
    virtual bool GetDisplayHelpers();
    virtual bool GetDisplayHelperPrimitive();
    virtual const ZDebugRender::EDebugChannel GetDebugChannel();
    virtual bool IsDrawDebugVisible(ZDebugRender *pDebugRender);
    virtual void DrawDebugObjects(ZDebugRender *pDebugRender);
    virtual void DrawDebugObjectsStart(ZDebugRender *pDebugRender);
    virtual void DrawDebugObjectsEnd(ZDebugRender *pDebugRender);
    virtual bool IsDebugDrawDirty();
    virtual void SetDebugDrawDirty(bool bDebugDataDirty);
    virtual ZDebugRenderPrimitive *GetDebugEntity();
    void CreateDebugPrimitive(ZDebugRender *pDebugRender);
    void DestroyDebugEntity(ZDebugRender *pDebugRender);
    virtual void SetDisplayBounds(bool bDisplayBounds);
    virtual void SetDisplayPivot(bool bDisplayPivot);
    virtual void SetDisplayHelpers(bool bDisplayHelpers);
    void SetDisplayHelperPrimitive(bool bDisplayHelperPrimitive);
    void SetDebugChannel(const ZDebugRender::EDebugChannel channel);
    void SetHelperPrimitive(const ZResourcePtr &pHelperPrimitive, bool isFacingCam, bool isAnchor);
    uint64_t AddHelperPrimitive(const ZResourcePtr &pHelperPrimitive, bool isFacingCam,
                                bool isAnchor);
    void RemoveHelperPrimitive(const uint64_t nIndex);
    void RemoveHelperPrimitive(const ZResourcePtr &pHelperPrimitive);
    void RemoveHelperPrimitives();
    const ZResourcePtr GetHelperPrimitive(const uint64_t nIndex);
    bool GetHelperPrimitiveVisible(const uint64_t nIndex);
    void SetHelperPrimitiveVisible(const uint64_t nIndex, const bool bVisible);
    bool GetHelperPrimitiveFacingCam(const uint64_t nIndex);
    void SetHelperPrimitiveFacingCam(const uint64_t nIndex, const bool bFacing);
    bool GetHelperPrimitiveOverrideColor(const uint64_t nIndex);
    void SetHelperPrimitiveOverrideColor(const uint64_t nIndex, bool bOverride);
    uint32_t GetHelperPrimitiveWantedColor(const uint64_t nIndex);
    void SetHelperPrimitiveWantedColor(const uint64_t nIndex, uint32_t nColor);
    bool GetHelperPrimitiveAllocated(const uint64_t nIndex);
    void SetHelperPrimitiveTransform(const uint64_t nIndex, const SMatrix43 &mTransform);
    const SMatrix43 &GetHelperPrimitiveTransform(const uint64_t nIndex);
    bool GetHelperPrimitiveIsAnchor(const uint64_t nIndex);
    uint64_t GetNumberOfHelperPrimitives();
    void UpdateBounds();
    bool HasChildren();
    const SMatrix GetObjectToParentMatrix();
    const SMatrix GetObjectToParentMatrixWithoutScale();
    float4 GetLocalCenter();
    const SVector3 &GetLocalCenterInMem();
    float4 GetLocalHalfSize();
    const SVector3 &GetLocalHalfSizeInMem();
    void SetPreciseMatrixChange(bool bPrecise);
    void InternalAttachChild(ZSpatialEntity *pChild, bool notifySaveSystemEntityChanged);
    void InternalSetScaling(const float4 scale, bool bCallScaleListeners,
                            bool notifySaveSystemEntityChanged);
    void InternalSetObjectToParentMatrix(const SMatrix &mObjectToParent,
                                         bool notifySaveSystemEntityChanged);
    void InternalOnObjectToParentChanged(bool notifySaveSystemEntityChanged);
    void InternalSetTransformParent(TEntityRef<ZSpatialEntity> parent,
                                    bool notifySaveSystemEntityChanged);
    void InternalSetObjectToParentMatrixVerify(const SMatrix &mObjectToParent,
                                               bool notifySaveSystemEntityChanged);
    void InternalSetObjectToWorldMatrix(const SMatrix &mObjectToWorld,
                                        bool notifySaveSystemEntityChanged);
    virtual void OnObjectScaleChanged(bool bCallScaleListeners);
    virtual void OnObjectToParentChanged();
    virtual void OnTransformParentIDChanged();
    virtual void OnVisibleChanged(const bool &bWasVisible);
    void OnIsPrivateChanged();
    void OnFPSDrawModeChanged();
    void NotifyChange();
    void NotifyChangeChildrenRec();
    virtual void OnSpatialChange();
    virtual void OnEnterStaging();
    virtual void OnExitStaging();
    void RaiseSpatialChange();
    void RaiseChildAttached(ZSpatialEntity *childAttached);
    void RaiseChildDetaching(ZSpatialEntity *childDetaching);
    void RaiseParentChanged(ZSpatialEntity *newParent);
    virtual void OnHelperPrimitiveChanged(const ZRuntimeResourceID &args);
    ZSpatialEntity &operator=(const ZSpatialEntity &);
    const TArray<ZSpatialEntity *> &GetTransformChildren();
    void OnObjectToWorldChanged();
    void SetParentHiddenRec(bool bParentHidden);
    void SetParentFPSModeRec(bool bParentFPSMode);
    void SetIsSceneChildRec(bool bIsSceneChild);
    SMatrix GetObjectToWorldMatrixInternal();
    void OnScaleVecChanged();
    void SetWorldMatrixDirty();
    void EvaluateIfLocalTransformIsIdentity();
    void SetPreciseMatrixChangeInherited(bool bParentPrecise);
    bool IsListeningToHelperPrimitiveChange(const ZResourcePtr &pHelperPrimitive);
    void SetHelperPrimitiveResource(ZSpatialEntity::SHelperPrimitiveResource &pPrimitive,
                                    const ZResourcePtr &pHelperPrimitive);
    bool m_bIsWorldMatrixDirty;
    bool m_bIsLocalTransformIdentity;
    bool m_bPreciseMatrixChange;
    bool m_bPreciseMatrixChangeInherited;
    bool m_bDisplayBounds;
    bool m_bDisplayPivot;
    bool m_bDisplayHelpers;
    bool m_bDisplayHelperPrimitive;
    bool m_bEditorSelectable;
    bool m_bEditorSelected;
    bool m_bEditorVisible;
    bool m_bFilteredOut;
    bool m_bParentHidden;
    bool m_bParentFPSMode;
    bool m_bSceneChild;
    bool m_bFPSDrawMode;
    bool m_bOverrideParentFPSDrawMode;
    bool m_bIncludeInParentsBounds;
    bool m_bDebugDataDirty;
    bool m_bNotifyChange;
    bool m_bIsManuallySavable;
    bool m_bIsStaged;
    bool m_bExtraVisibilityFlag;
    bool m_bVisible;
    bool m_bIsPrivate;
    bool m_bScalingEnabled;
    bool m_bRegisteredForManualSave;
    SMatrix43 m_mObjectToParent;
    SMatrix43 m_mWorldMatrix;
    ZSpatialEntity *m_pTransformParent;
    TArray<ZSpatialEntity *> m_transformChildren;
    TEntityRef<ZSpatialEntity> m_transformParentID;
    ZEvent<ZSpatialEntity const *, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull>
        *m_pTransformChangeCallBackEvent;
    ZEvent<ZEntityRef const &, float4 const, ZEventNull, ZEventNull, ZEventNull, ZEventNull>
        *m_pScaleChangeCallBackEvent;
    ZEvent<ZEntityRef const &, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull>
        *m_pSpatialChangedCallBackEvent;
    ZEvent<ZEntityRef const &, enum ZSpatialEntity::EGenericDataChangeType, void *, ZEventNull,
           ZEventNull, ZEventNull> *m_pGenericDataChangedCallBackEvent;
    uint32_t m_nEntityID;
    SVector3 m_vCenter;
    SVector3 m_vHalfSize;
    SVector3 m_vScaleVec;
    ZDebugRender::EDebugChannel m_eDebugChannel;
    TArray<ZSpatialEntity::SHelperPrimitiveResource> m_aHelperPrimitives;
    TArray<TEntityRef<ZSpatialEntity>> m_apVisibilityProxies;
    ZDebugRenderPrimitive *m_pDebugPrimitive;
    ZTraceMessageSlot m_ErrorTraceMessageSlot;
    ZAssertlock m_hierarchyLock;
    ZAssertlock m_transformChangeLock;
};

template <typename T>
class TDeletionListener : public ZBaseDeletionListener {
    public:
    static void RegisterType();
    TDeletionListener<T>(const TDeletionListener<T> &);
    TDeletionListener<T>(const T &);
    TDeletionListener<T>();
    virtual ~TDeletionListener<T>();
    TDeletionListener<T> &operator=(const TDeletionListener<T> &);
    TDeletionListener<T> &operator=(const T &);
    bool operator==(const TDeletionListener<T> &);
    bool operator!=(const TDeletionListener<T> &);
    bool operator<(const TDeletionListener<T> &);
    operator const class ZEntityRef &();
    ZSpatialEntity *operator->();
    const T operator*();
    const T Get();
    const TGenericRef<T> &GetGenericRef();
    bool IsNull();
    bool IsValid();
    void Swap(TDeletionListener<T> &);
    void OnRefChanged(const T &);
    void RegisterForDeletionListening(const TGenericRef<T> &);
    void UnregisterForDeletionListening(const TGenericRef<T> &);
    virtual bool EntityDeleted(const ZEntityRef &);
    TGenericRef<T> m_ref;
};

template <typename T>
class TSafeEntityRef : public ZBaseSafeEntityRef {
    public:
    static void RegisterType();
    TSafeEntityRef<T>(const ZEntityRef &);
    TSafeEntityRef<T>(const TEntityRef<T> &);
    TSafeEntityRef<T>();
    T *operator->();
    T *GetRawPointer();
    bool IsNull();
    bool IsValid();
    TEntityRef<T> Get();
    TSafeEntityRef<T> &operator=(void *);
    TSafeEntityRef<T> &operator=(const ZEntityRef &);
    TSafeEntityRef<T> &operator=(const TEntityRef<T> &);
    TSafeEntityRef<T> &operator=(const TSafeEntityRef<T> &);
    bool operator==(const TEntityRef<T> &);
    bool operator==(const TSafeEntityRef<T> &);
    bool operator!=(const TEntityRef<T> &);
    bool operator!=(const TSafeEntityRef<T> &);
    bool operator<(const TSafeEntityRef<T> &);
    operator const class TEntityRef<T>();
    bool Save(ZCustomSaveDataSerializer *);
    bool Load(ZCustomSaveDataDeserializer *);
    TDeletionListener<TEntityRef<T>> m_entityRef;
};

class ZAccessorRef {
    public:
    static void RegisterType();
    class ZNULL {
        public:
    };
    ZAccessorRef(ZAccessorRef::ZNULL *__formal);
    ZAccessorRef(const ZAccessorRef &rhs);
    ZAccessorRef(const ZEntityRef &entityRef, uint32_t propertyID);
    ZAccessorRef(const ZEntityRef &entityRef);
    ZAccessorRef();
    ~ZAccessorRef();
    ZAccessorRef &operator=(ZAccessorRef::ZNULL *__formal);
    ZAccessorRef &operator=(const ZAccessorRef &rhs);
    const ZEntityRef &GetEntityRef();
    uint32_t GetPropertyID();
    bool IsValid();
    bool operator!=(ZAccessorRef::ZNULL *__formal);
    bool operator==(const ZAccessorRef &rhs);
    bool operator==(ZAccessorRef::ZNULL *__formal);
    bool operator<(const ZAccessorRef &rhs);
    ZEntityRef m_entityRef;
    uint32_t m_propertyID;
};

class ZArrayElementRef {
    public:
    static void RegisterType();
    class ZNULL {
        public:
    };
    ZArrayElementRef(ZArrayElementRef::ZNULL *__formal);
    ZArrayElementRef(ZArrayElementRef *rhs);
    ZArrayElementRef(const ZArrayElementRef &rhs);
    ZArrayElementRef(const ZEntityRef &entityRef, uint32_t propertyID, uint64_t index);
    ZArrayElementRef(const ZEntityRef &entityRef);
    ZArrayElementRef();
    ~ZArrayElementRef();
    ZArrayElementRef &operator=(ZArrayElementRef::ZNULL *__formal);
    ZArrayElementRef &operator=(ZArrayElementRef *rhs);
    ZArrayElementRef &operator=(const ZArrayElementRef &rhs);
    bool operator==(ZArrayElementRef::ZNULL *__formal);
    bool operator==(const ZArrayElementRef &rhs);
    const ZAccessorRef &GetAccessorRef();
    uint64_t GetIndex();
    bool IsValid();
    bool operator!=(ZArrayElementRef::ZNULL *__formal);
    bool operator<(const ZArrayElementRef &rhs);
    ZAccessorRef m_accessorRef;
    uint64_t m_index;
};

class ZSaveGameDumpInfoWriter {
    public:
    ZSaveGameDumpInfoWriter(const ZString &baseDumpFileName, const ZString dumpFileNameAffix);
    ~ZSaveGameDumpInfoWriter();
    void WriteLine(uint64_t lineDetailLevel, const ZString &line);
    void CreateSubWriter(const ZString subFileAffix,
                         TUniquePtr<ZSaveGameDumpInfoWriter> &pNewSubWriter);
    void CreateNextDumpFile();
    void CloseCurrentDumpFile();
    static const uint64_t MAX_SINGLE_DUMP_FILE_SIZE;
    ITextWriter *m_pCurrentTextWriter;
    IOutputStream *m_pCurrentOutputStream;
    uint64_t m_nCurrentFileIndex;
    uint64_t m_nBytesWrittenToCurrentFile;
    ZString m_baseDumpFileName;
    ZString m_platformBaseDumpFileName;
    ZString m_baseDumpFileExtension;
};

class IGameDataSerializer {
    public:
    IGameDataSerializer();
    virtual ~IGameDataSerializer();
    virtual bool SerializeData(IOutputStream *);
    virtual uint64_t GetApproximateSaveDataSize();
};

class IGameDataDeserializer {
    public:
    IGameDataDeserializer();
    virtual ~IGameDataDeserializer();
    virtual bool DeserializeData(IInputStream *);
};

class ICustomSaveGameDataSerializer {
    public:
    virtual ~ICustomSaveGameDataSerializer();
    virtual bool CustomSerialize(const void *, ZSaveGameDataSerializer *, IOutputStream *);
    virtual bool CustomDeserialize(void *, ZSaveGameDataDeserializer *, IInputStream *, uint32_t);
    virtual uint32_t GetSerializerVersion();
};

class ISaveDataTarget {
    public:
    virtual ~ISaveDataTarget();
    virtual ZVariantRef GetTargetOfDeserialize(uint64_t);
    virtual void SubDataDeserialized(uint64_t, bool);
    virtual bool ShouldDeserializeSubData(uint64_t);
    virtual bool ShouldCustomDeserializeSubData(uint64_t);
    virtual bool CustomDeserializeSubData(uint64_t, ZSaveGameDataDeserializer *, bool, uint64_t);
    virtual const ICustomSaveGameDataSerializer *GetCustomDeserializerForType(STypeID *);
};

class ZDataTypeDirectlyWritableStatusCache {
    public:
    bool IsTypeDirectlyWritable(const IType *pType);
    static bool InternalIsTypeDirectlyWritable(const IType *pType);
    TMap<IType const *, bool> m_typeDirectlyWritableStatusCache;
};

class ZSaveGameDataDeserializer : public IGameDataDeserializer {
    public:
    struct SStoredBaseClassInfo {
        SStoredBaseClassInfo();
        const IType *m_pBaseClassType;
        int32_t m_currentBaseClassIndex;
    };
    struct SStoredPropertyInfo {
        SStoredPropertyInfo();
        const IType *m_pPropertyType;
        uint32_t m_propertyId;
        int32_t m_currentPropertyIndex;
    };
    struct SStoredTypeInfo {
        SStoredTypeInfo();
        const IType *m_pStoredType;
        TArray<TPair<ZSaveGameDataDeserializer::SStoredBaseClassInfo, bool>> m_baseClasses;
        TArray<TPair<ZSaveGameDataDeserializer::SStoredPropertyInfo, bool>> m_topLevelProperties;
        bool m_bIsFullMatchWithCurrentType;
    };
    struct SStoredTypesTable {
        TArray<ZSaveGameDataDeserializer::SStoredTypeInfo> m_typesStorage;
        TMap<STypeID *, ZSaveGameDataDeserializer::SStoredTypeInfo *> m_typeToStoredTypeInfo;
    };
    ZSaveGameDataDeserializer(ISaveDataTarget &saveDataTarget);
    virtual ~ZSaveGameDataDeserializer();
    virtual bool DeserializeData(IInputStream *inputStream);
    bool DeserializeObjectFromStream(IInputStream *pStream, ZVariantRef &dataToDeserializeTo,
                                     bool bCompressionEnabled);
    bool StartDeserializeObjectFromStream(IInputStream *pStream, STypeID *typeId);
    bool EndDeserializeObjectFromStream(IInputStream *pStream);
    static bool
    RecurseIntoMemory(ZSaveGameDataDeserializer *pThis, IInputStream *pStream, const IType *type,
                      void *address, const ZSaveGameDataDeserializer::SStoredTypesTable &typesTable,
                      ZDataTypeDirectlyWritableStatusCache &typeDirectlyWritableStatusCache);
    bool RecurseIntoMemory(IInputStream *pStream, const IType *type, void *address);
    bool RecurseIntoMemory(IInputStream *pStream, ZVariantRef &dataToDeserializeTo);
    IInputStream *GetStreamForDeserializingObject(IInputStream *pInputStream,
                                                  bool bCompressionEnabled);
    void FinalizeStreamForDeserializingObject(IInputStream *pInputStream, bool bCompressionEnabled);
    IInputStream *GetRawInputStream();
    static bool ReadFromStream(IInputStream *pStream, void *pData, uint64_t length);
    const IType *ReadTypeFromStream(IInputStream *pStream);
    static bool ReadTypesTable(IInputStream *pStream,
                               ZSaveGameDataDeserializer::SStoredTypesTable &typesTable);
    static bool ReadTypeNameFromStream(IInputStream *pStream, const IType *&pReadType);
    static bool ReadString(IInputStream *pStream, ZString &str);
    static const IType *ReadType(IInputStream *pStream,
                                 const ZSaveGameDataDeserializer::SStoredTypesTable &typesTable);
    IInputStream *m_pInputStream;
    ISaveDataTarget &m_saveDataTarget;
    ZSaveGameDataDeserializer::SStoredTypesTable m_typesTable;
    ZDataTypeDirectlyWritableStatusCache m_typeDirectlyWritableStatusCache;
    static char sDeserializerVersion[4];
};

class ISaveDataSource {
    public:
    virtual ~ISaveDataSource();
    virtual uint64_t GetNumberOfSubDatasToSerialize();
    virtual ZVariantRef GetSubDataToSerialize(uint64_t);
    virtual uint64_t GetSubDataKey(uint64_t);
    virtual void SubDataSerialized(uint64_t, bool);
    virtual bool ShouldCustomSerializeSubData(uint64_t);
    virtual bool CustomSerializeSubData(uint64_t, ZSaveGameDataSerializer *);
    virtual const ICustomSaveGameDataSerializer *GetCustomSerializerForType(STypeID *);
};

struct SubSaveDataSectionInfo {
    SubSaveDataSectionInfo(uint64_t, uint64_t, uint64_t);
    SubSaveDataSectionInfo();
    uint64_t m_sectionKey;
    uint64_t m_sectionOffset;
    uint64_t m_sectionSize;
};

class ZSaveGameDataSerializer : public IGameDataSerializer {
    public:
    ZSaveGameDataSerializer(const ISaveDataSource &saveDataSource, bool enableCompression);
    virtual ~ZSaveGameDataSerializer();
    virtual bool SerializeData(IOutputStream *pOutputStream);
    virtual uint64_t GetApproximateSaveDataSize();
    IOutputStream *GetStreamForSerializingObject(IOutputStream *pSourceStream);
    void FinalizeStreamForSerializingObject(IOutputStream *pSerializingStream);
    bool StartSerializeObjectToStream(IOutputStream *pStream, STypeID *typeId);
    bool EndSerializeObjectToStream(IOutputStream *pStream);
    bool SerializeObjectToStream(IOutputStream *pStream, const ZVariantRef &root);
    bool RecurseObjectToStream(IOutputStream *pStream, const IType *type, const void *address);
    bool RecurseObjectToStream(IOutputStream *pStream, const ZVariantRef &root);
    IOutputStream *GetRawOutputStream();
    static bool WriteToStream(IOutputStream *pStream, const void *pData, uint64_t length);
    bool WriteTypeToStream(IOutputStream *pStream, STypeID *typeId);
    bool WriteHeaderToStream(IOutputStream *pStream);
    bool WriteSubDataTableToStream(IOutputStream *pStream);
    bool WriteTablesToStream(IOutputStream *pStream,
                             const TArray<SubSaveDataSectionInfo> &subDataSections,
                             uint64_t startOfDataOffset);
    static bool WriteTypeNameToStream(IOutputStream *pStream, STypeID *typeID);
    static bool WriteStringToStream(IOutputStream *pStream, const ZString &str);
    void ClearInternalDataStructures();
    uint16_t TypeIdToIndex(STypeID *typeID);
    IOutputStream *m_pOutputStream;
    bool m_enableCompression;
    ZDataTypeDirectlyWritableStatusCache m_typeDirectlyWritableStatusCache;
    TArray<STypeID *> m_typeIDsTable;
    const ISaveDataSource &m_saveDataSource;
    static char sSerializerVersion[4];
};

class ZEntityIDPath {
    public:
    static void RegisterType();
    ZEntityIDPath(uint32_t entityAllocationId, const ZSubEntityPath &aSubEntityIds);
    ZEntityIDPath(const ZEntityIDPath &other);
    ZEntityIDPath();
    ~ZEntityIDPath();
    ZEntityIDPath &operator=(const ZEntityIDPath &other);
    bool operator<(const ZEntityIDPath &other);
    bool IsEmpty();
    bool operator==(const ZEntityIDPath &other);
    ZEntityIDPath GetBasePath();
    ZString ToString();
    ZSubEntityPath m_subEntityPath;
    uint32_t m_entityAllocationId;
};

class ZEntityIDPathStorage;

struct SEntityResourceIdentifier {
    static void RegisterType();
    SEntityResourceIdentifier(ZRuntimeResourceID entityFactoryRRID,
                              ZRuntimeResourceID entityResourceRRID);
    SEntityResourceIdentifier(const SEntityResourceIdentifier &other);
    SEntityResourceIdentifier();
    SEntityResourceIdentifier &operator=(const SEntityResourceIdentifier &other);
    bool IsEmpty();
    bool operator==(const SEntityResourceIdentifier &other);
    bool operator<(const SEntityResourceIdentifier &other);
    bool IsValid();
    ZString ToString();
    ZRuntimeResourceID m_entityFactoryRRID;
    ZRuntimeResourceID m_entityResourceRRID;
};

class ZLocalEntityRefToIDPathStorage {
    public:
    ZLocalEntityRefToIDPathStorage(const ZLocalEntityRefToIDPathStorage &otherLocalStorage);
    ZLocalEntityRefToIDPathStorage(ZEntityIDPathStorage &sourceStorage);
    ~ZLocalEntityRefToIDPathStorage();
    bool GetEntityIDPathForEntityToBeSaved(const ZEntityRef &allocatedEntity,
                                           ZEntityIDPath &outPath,
                                           SEntityResourceIdentifier &singletonResource);
    void AddAllocationIDFromWhichToAllowReferences(uint64_t allocationId);
    void AddContextFromWhichToAllowReferences(const ISceneContext *allowedSceneContext);
    void RegisterEntitiesToIgnoreWhenSaving(const ZEntityRef &entityRef);
    TUnorderedLinearHashSet<ZEntityRef, &ZEntityImpl::HashSet_ZEntityRef, 31, DefaultHashAllocator>
        &GetModifiableEntitiesToIgnoreWhenSavingSetForContext(uint64_t contextKey);
    TUnorderedLinearHashSet<ZEntityRef, &ZEntityImpl::HashSet_ZEntityRef, 31, DefaultHashAllocator>
        &GetModifiableAllEntitiesToIgnoreWhenSavingSet();
    const TSet<unsigned __int64> &GetReferencedSceneContexts();
    void CollapseEntitiesToIgnoreWhenSaving();
    const TSet<unsigned int> &GetAllReferencedAllocationIDs();
    void AddSceneContextSavedInStorage(uint64_t contextKey);
    ZMutex m_entitiesToIgnoreMutex;
    TMap<unsigned __int64, TUnorderedLinearHashSet<ZEntityRef, &ZEntityImpl::HashSet_ZEntityRef, 31,
                                                   DefaultHashAllocator>>
        m_entitiesToIgnoreWhenSavingPerContext;
    TUnorderedLinearHashSet<ZEntityRef, &ZEntityImpl::HashSet_ZEntityRef, 31, DefaultHashAllocator>
        m_allEntitiesToIgnoreWhenSaving;
    TSet<ISceneContext const *> m_allowedContextReferences;
    TSet<unsigned __int64> m_allowedAllocationIds;
    TSet<unsigned __int64> m_referencedSceneContexts;
    ZEntityIDPathStorage &m_sourceStorage;
    ZMutex m_storageMutex;
    TSet<unsigned int> m_referencedAllocations;
    TSet<unsigned __int64> m_sceneContextsSavedInStorage;
};

struct SEntityOffsetToIDPath {
    SEntityOffsetToIDPath(uint64_t entityOffset, const ZSubEntityPath &entityIDPath);
    SEntityOffsetToIDPath();
    ZEntityRef GetEntityRefFromRootEntity(const ZEntityRef &rootEntityRef);
    uint8_t GetOffsetFromActualEntity();
    uint64_t m_entityOffset;
    ZSubEntityPath m_entityIDPath;
};

struct SSubEntitySaveInfo {
    SSubEntitySaveInfo(uint64_t entityOffset, uint32_t numberOfPropertiesSaveOperations,
                       uint32_t numberOfCustomSaveOperations, const ZSubEntityPath &entityIDPath,
                       const IEntityBlueprintFactory *pBlueprintFactory);
    SSubEntitySaveInfo();
    ZEntityRef GetEntityRefFromRootEntity(const ZEntityRef &rootEntityRef);
    uint8_t GetOffsetFromActualEntity();
    uint32_t GetNumberOfIndicesNeedForAllOperations();
    bool HasPropertiesToSave();
    bool HasCustomSaveFunction();
    bool operator==(const SSubEntitySaveInfo &other);
    SEntityOffsetToIDPath m_entityOffsetForIDPath;
    const IEntityBlueprintFactory *m_pBlueprintFactory;
    uint32_t m_nPropertySaveOperations;
    uint32_t m_nCustomSaveOperations;
};

class ZEntityIDPathStorage : public IComponentInterface {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZEntityIDPathStorage();
    ZEntityIDPathStorage(const ZEntityIDPathStorage &);
    ZEntityIDPathStorage &operator=(const ZEntityIDPathStorage &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    static const uint32_t INVALID_ENTITY_ALLOCATION_ID;
    struct SMemoryRange {
        SMemoryRange(uint64_t startAddress, uint64_t endAddress);
        SMemoryRange();
        bool operator<(const ZEntityIDPathStorage::SMemoryRange &other);
        uint64_t m_startAddress;
        uint64_t m_endAddress;
    };
    virtual ~ZEntityIDPathStorage();
    void RegisterEntityAllocation(const ZEntityRef &allocatedEntity,
                                  IEntityBlueprintFactory *pAllocatingFactory,
                                  uint32_t savedEntityAllocationId);
    void UnRegisterEntityAllocation(const ZEntityRef &allocatedEntity);
    void RemapEntityAllocation(const ZEntityRef &allocatedEntity, uint32_t newAllocationId);
    uint32_t GetEntityAllocationIdToBeSaved(const ZEntityRef &allocatedEntity);
    ZEntityRef GetEntityFromAllocationId(uint32_t allocationId);
    void FillEntityIDPathToEntityRefMapping(TMap<ZEntityIDPath, ZEntityRef> &);
    ZLocalEntityRefToIDPathStorage CreateLocalIDPathStorage();
    void CreateLocalEntityRefStorage(TUniquePtr<ZLocalIDPathToEntityRefStorage> &pEntityRefStorage);
    void AssignIDsToEntities();
    void ClearEntityIDAssignment();
    bool GetSaveInfoForBlueprintSubEntity(uint64_t entityAddressToSearch, ZEntityRef &outRootEntity,
                                          SSubEntitySaveInfo &outEntitySaveInfo);
    bool GetRootEntityOwningSubEntity(uint64_t entityAddressToSearch, ZEntityRef &outRootEntity,
                                      const IEntityBlueprintFactory *&pOutBlueprintFactory,
                                      ZEntityIDPathStorage::SMemoryRange &rootEntityAddressRange);
    bool GetRootEntityOwningSubEntity(uint64_t entityAddressToSearch, ZEntityRef &outRootEntity,
                                      ZEntityIDPathStorage::SMemoryRange &rootEntityAddressRange);
    bool GetRootEntityOwningSubEntity(uint64_t entityAddressToSearch, ZEntityRef &outRootEntity,
                                      const IEntityBlueprintFactory *&pOutBlueprintFactory);
    bool GetRootEntityOwningSubEntity(uint64_t entityAddressToSearch, ZEntityRef &outRootEntity);
    bool GetIDPathForEntityToBeSaved(const ZEntityRef &entityRef, ZEntityRef &outRootEntity,
                                     ZEntityIDPath &foundIdPath, bool *pbEntityExists);
    bool HasPendingPrefetchMappingsToComplete();
    void SetLargestAssignedEntityId(uint32_t largestEntityId);
    uint32_t GetLargestAssignedEntityId();
    void
    PreFetchEntityOffsetToPathMappingForBlueprint(const IEntityBlueprintFactory *pBlueprintFactory,
                                                  bool bAddReference);
    void ImmediatelyPreFetchEntityOffsetToPathMappingForBlueprint(
        const IEntityBlueprintFactory *pBlueprintFactory, bool bAddReference);
    void ReleasePreFetchOfEntityOffsetToPathMappingForBlueprint(
        const IEntityBlueprintFactory *pBlueprintFactory);
    void ReleaseAllUnreferencedEntityOffsetToPathMappings();
    void HoldReleaseOfEntityOffsetToPathMappings();
    void AllowReleaseOfEntityOffsetToPathMappings();
    void UpdateLargestAssignedEntityId(uint32_t largestEntityId);
    void InternalReleasePreFetchOfEntityOffsetToPathMappingForBlueprint(
        const IEntityBlueprintFactory *pBlueprintFactory);
    void InternalCreateEntityOffsetToPathMappingForBlueprint(
        const IEntityBlueprintFactory *pBlueprintFactory,
        TArray<SEntityOffsetToIDPath> &entityOffsetToIDPathMapping, bool bAddReference,
        bool bSortMappings);
    void InternalReleaseAllUnreferencedEntityOffsetToPathMappings();
    const TArray<SEntityOffsetToIDPath> &
    GetEntityOffsetToPathMappingForBlueprint(const IEntityBlueprintFactory *pBlueprintFactory,
                                             bool bAddReference, bool &bCacheHit);
    bool IsSaveSystemEnabled();
    void WaitForUnregisterTrackingToComplete();
    void TrackEntityUnregisters();
    void StopTrackingEntityUnregisters();
    bool AreUnregistersTracked();
    struct SBlueprintInstantiationInfo {
        SBlueprintInstantiationInfo(
            const ZEntityIDPathStorage::SBlueprintInstantiationInfo &otherInfo);
        SBlueprintInstantiationInfo(const ZEntityRef &rootEntity,
                                    const IEntityBlueprintFactory *pBlueprintFactory);
        SBlueprintInstantiationInfo();
        ZEntityRef m_rootEntity;
        const IEntityBlueprintFactory *m_pBlueprintFactory;
    };
    struct SUnregisteredEntityInstantiation {
        SUnregisteredEntityInstantiation(
            uint32_t allocationId,
            const ZEntityIDPathStorage::SBlueprintInstantiationInfo &otherInfo);
        SUnregisteredEntityInstantiation();
        uint32_t m_allocationId;
        TResourcePtr<IEntityBlueprintFactory> m_pRemovedEntityBluePrintFactory;
        ZEntityRef m_rootEntity;
    };
    TUnorderedLinearHashMap<ZEntityRef, unsigned int, &ZEntityImpl::HashSet_ZEntityRef, 31,
                            DefaultHashAllocator>
        m_assignedEntityIds;
    TMap<ZEntityIDPathStorage::SMemoryRange, ZEntityIDPathStorage::SBlueprintInstantiationInfo>
        m_memoryRangeToBlueprintInstantiation;
    TSet<ZEntityRef> m_unassignedEntities;
    TMap<ZEntityIDPathStorage::SMemoryRange, ZEntityIDPathStorage::SUnregisteredEntityInstantiation>
        m_unregisteredBlueprintInstantiations;
    struct SCachedEntityOffsetToIDPath {
        SCachedEntityOffsetToIDPath();
        uint32_t m_refCount;
        TArray<SEntityOffsetToIDPath> m_entityOffsetToIDPathMapping;
    };
    uint32_t m_pathMappingsCanBeReleased;
    TMap<IEntityBlueprintFactory const *, ZEntityIDPathStorage::SCachedEntityOffsetToIDPath>
        m_cachedEntityPathMappingForBlueprints;
    TSet<IEntityBlueprintFactory const *> m_pathMappingsPendingFetch;
    uint32_t m_largestAssginedId;
    ZSharedMutex m_mutex;
    ZSharedMutex m_prefetchMutex;
    ZSharedMutex m_unregisteredMutex;
    uint32_t m_unregistersTracked;
    TMap<unsigned int, ZEntityRef> m_assignedEntityIdToEntityRef;
};

class ZLocalIDPathToEntityRefStorage {
    public:
    ZLocalIDPathToEntityRefStorage(const ZLocalIDPathToEntityRefStorage &otherLocalStorage);
    ZLocalIDPathToEntityRefStorage(ZEntityIDPathStorage &sourceStorage);
    ~ZLocalIDPathToEntityRefStorage();
    bool GetEntityRefForStoredEntityPath(const ZEntityIDPath &pathToSearch,
                                         ZEntityRef &outFoundEntity);
    bool GetEntityRefForStoredEntityPathFromGivenRootEntity(const ZEntityRef &rootEntity,
                                                            const ZSubEntityPath &pathToSearch,
                                                            ZEntityRef &outFoundEntity);
    void GatherAssignedEntityInfo();
    struct SAssignedIdToEntityRefMapping {
        SAssignedIdToEntityRefMapping(uint32_t entityId, const ZEntityRef &rootEntity);
        SAssignedIdToEntityRefMapping();
        uint32_t m_entityId;
        ZEntityRef m_rootEntity;
    };
    TArray<ZLocalIDPathToEntityRefStorage::SAssignedIdToEntityRefMapping> m_assignedIdsToEntityRef;
    TMap<ZEntityIDPath, ZEntityRef> m_cachedPathToRefMapping;
    ZEntityIDPathStorage &m_sourceStorage;
    ZSharedMutex m_storageMutex;
    uint64_t m_numberOfIDPathsRegistered;
};

struct SResourcePtrsToLoad {
    void WaitForResourcePtrsToBeFinished();
    void ReleaseHeldResources();
    ZResourcePtr AddResourcePtrToLoad(ZRuntimeResourceID resRRID);
    bool AreResourcePtrsFinished();
    bool IsResourcePtrFinishedProcessing(const ZResourcePtr &resPtr);
    TArray<ZResourcePtr> m_resourcePtrsToLoad;
    TArray<ZResourcePtr> m_resourcePtrsLoaded;
    ZMutex m_resourcePtrsMutex;
};

class ZCustomSaveDataEntityReferenceStorage {
    public:
    ZCustomSaveDataEntityReferenceStorage(ZCustomSaveDataEntityReferenceStorage *rhs);
    ZCustomSaveDataEntityReferenceStorage(const ZCustomSaveDataEntityReferenceStorage &rhs);
    ZCustomSaveDataEntityReferenceStorage();
    ZCustomSaveDataEntityReferenceStorage &operator=(ZCustomSaveDataEntityReferenceStorage *rhs);
    ZCustomSaveDataEntityReferenceStorage &
    operator=(const ZCustomSaveDataEntityReferenceStorage &rhs);
    bool GetEntityReferenceStoredAtIndex(uint16_t index, ZEntityRef &ref);
    bool GetAccessorReferenceStoredAtIndex(uint16_t index, ZAccessorRef &ref);
    bool GetArrayElementReferenceStoredAtIndex(uint16_t index, ZArrayElementRef &ref);
    uint16_t GetOrCreateEntityReferenceIndex(const ZEntityRef &ref);
    uint16_t GetOrCreateAccessorReferenceIndex(const ZAccessorRef &ref);
    uint16_t GetOrCreateArrayElementReferenceIndex(const ZArrayElementRef &ref);
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
    bool CheckValidityOfReferences();
    bool SerializeTo(ZSaveGameDataSerializer *pSerializer, IOutputStream *pStreamToSerializeWith);
    bool DeserializeFrom(ZSaveGameDataDeserializer *pDeserializer,
                         IInputStream *pStreamToDeserializeWith, uint32_t dataVersion);
    void ReadReferencesFromDisk(ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
                                ZSaveDataValueStorage &saveDataValueStorage,
                                SResourcePtrsToLoad &resPtrsToLoad);
    void StoreReferencesToDisk(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                               ZSaveDataValueStorage &saveDataValueStorage);
    uint32_t m_staticReferencesStored;
    TFixedArray<ZEntityRef, 4> m_staticSavedEntityReferences;
    TArray<ZEntityRef> m_dynamicSavedEntityReferences;
    TArray<ZAccessorRef> m_savedAccessorReferences;
    TArray<ZArrayElementRef> m_savedArrayElementReferences;
    TArray<unsigned int> m_staticEntityReferenceIndices;
    TArray<unsigned int> m_dynamicEntityReferenceIndices;
    TArray<unsigned int> m_accessorReferenceIndices;
    TArray<unsigned int> m_arrayElementReferenceIndices;
};

class ZCustomSaveDataTypeIDStorage {
    public:
    ZCustomSaveDataTypeIDStorage(ZCustomSaveDataTypeIDStorage *rhs);
    ZCustomSaveDataTypeIDStorage(const ZCustomSaveDataTypeIDStorage &rhs);
    ZCustomSaveDataTypeIDStorage();
    ZCustomSaveDataTypeIDStorage &operator=(ZCustomSaveDataTypeIDStorage *rhs);
    ZCustomSaveDataTypeIDStorage &operator=(const ZCustomSaveDataTypeIDStorage &rhs);
    bool GetTypeIDStoredAtIndex(uint16_t index, STypeID *&ref);
    uint16_t GetOrCreateTypeIDIndex(STypeID *typeID);
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
    bool SerializeTo(ZSaveGameDataSerializer *pSerializer, IOutputStream *pStreamToSerializeWith);
    bool DeserializeFrom(ZSaveGameDataDeserializer *pDeserializer,
                         IInputStream *pStreamToDeserializeWith, uint32_t dataVersion);
    TArray<STypeID *> m_savedEntityTypeIDs;
};

class ZCustomSaveDataRuntimeResourceIDStorage {
    public:
    ZCustomSaveDataRuntimeResourceIDStorage(ZCustomSaveDataRuntimeResourceIDStorage *rhs);
    ZCustomSaveDataRuntimeResourceIDStorage(const ZCustomSaveDataRuntimeResourceIDStorage &rhs);
    ZCustomSaveDataRuntimeResourceIDStorage();
    ZCustomSaveDataRuntimeResourceIDStorage &
    operator=(ZCustomSaveDataRuntimeResourceIDStorage *rhs);
    ZCustomSaveDataRuntimeResourceIDStorage &
    operator=(const ZCustomSaveDataRuntimeResourceIDStorage &rhs);
    bool GetResourcePtrStoredAtIndex(uint16_t index, ZResourcePtr &resourcePtr);
    uint16_t GetOrCreateResourcePtrIndex(const ZResourcePtr &resourcePtr);
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
    bool SerializeTo(ZSaveGameDataSerializer *pSerializer, IOutputStream *pStreamToSerializeWith);
    bool DeserializeFrom(ZSaveGameDataDeserializer *pDeserializer,
                         IInputStream *pStreamToDeserializeWith, uint32_t dataVersion);
    void ReadResourcePtrsFromDisk(ZSaveDataValueStorage &saveDataValueStorage,
                                  SResourcePtrsToLoad &resPtrsToLoad);
    void StoreResourcePtrsToDisk(ZSaveDataValueStorage &saveDataValueStorage);
    TArray<ZResourcePtr> m_savedResourcePtrs;
    TArray<unsigned int> m_savedResourcePtrIndices;
};

class ZCustomSaveDataStorage {
    public:
    static void RegisterType();
    ZCustomSaveDataStorage(ZCustomSaveDataStorage *rhs);
    ZCustomSaveDataStorage(const ZCustomSaveDataStorage &rhs);
    ZCustomSaveDataStorage(int32_t dataVersion);
    ZCustomSaveDataStorage();
    ~ZCustomSaveDataStorage();
    ZCustomSaveDataStorage &operator=(ZCustomSaveDataStorage *rhs);
    ZCustomSaveDataStorage &operator=(const ZCustomSaveDataStorage &rhs);
    void ReserveDataSize(uint64_t reserveSize);
    uint64_t GetFullDataSize();
    int32_t GetDataVersion();
    int32_t GetWriterVersion();
    uint64_t GetInitialReserve();
    uint64_t &GetInitialReserveReference();
    void WriteToFile(IOutputStream *pStream);
    static const uint32_t CUSTOM_SERIALIZER_VERSION;
    bool SerializeTo(ZSaveGameDataSerializer *pSerializer, IOutputStream *pStreamToSerializeWith);
    bool DeserializeFrom(ZSaveGameDataDeserializer *pDeserializer,
                         IInputStream *pStreamToDeserializeWith, uint32_t dataVersion);
    void ReadResourcePtrsFromDisk(ZSaveDataValueStorage &saveDataValueStorage,
                                  SResourcePtrsToLoad &resPtrsToLoad);
    void StoreResourcePtrsToDisk(ZSaveDataValueStorage &saveDataValueStorage);
    void ReadReferencesFromDisk(ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
                                ZSaveDataValueStorage &saveDataValueStorage,
                                SResourcePtrsToLoad &resPtrsToLoad);
    void StoreReferencesToDisk(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                               ZSaveDataValueStorage &saveDataValueStorage);
    bool CheckValidityOfStoredReferences();
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
    struct SCoreCustomSaveDataStorageData {
        SCoreCustomSaveDataStorageData(
            const ZCustomSaveDataStorage::SCoreCustomSaveDataStorageData *rhs);
        SCoreCustomSaveDataStorageData(
            const ZCustomSaveDataStorage::SCoreCustomSaveDataStorageData &rhs);
        SCoreCustomSaveDataStorageData(int32_t dataVersion);
        SCoreCustomSaveDataStorageData();
        ZCustomSaveDataStorage::SCoreCustomSaveDataStorageData &
        operator=(ZCustomSaveDataStorage::SCoreCustomSaveDataStorageData *rhs);
        ZCustomSaveDataStorage::SCoreCustomSaveDataStorageData &
        operator=(const ZCustomSaveDataStorage::SCoreCustomSaveDataStorageData &rhs);
        int32_t m_writerVersion;
        int32_t m_dataVersion;
        TFixedArray<unsigned char, 64> m_staticDataBuffer;
        uint64_t m_fullDataSize;
    };
    TFixedArray<unsigned char, 64> &GetStaticDataBuffer();
    const TFixedArray<unsigned char, 64> &GetStaticDataBuffer() const;
    TArray<unsigned char> &GetDynamicDataBuffer();
    const TArray<unsigned char> &GetDynamicDataBuffer() const;
    ZCustomSaveDataEntityReferenceStorage *GetReferenceStorage();
    const ZCustomSaveDataEntityReferenceStorage *GetReferenceStorage() const;
    ZCustomSaveDataTypeIDStorage *GetTypeIDStorage();
    const ZCustomSaveDataTypeIDStorage *GetTypeIDStorage() const;
    ZCustomSaveDataRuntimeResourceIDStorage *GetRRIDStorage();
    const ZCustomSaveDataRuntimeResourceIDStorage *GetRRIDStorage() const;
    uint64_t &GetFullDataSizeReference();
    ZCustomSaveDataStorage::SCoreCustomSaveDataStorageData m_coreData;
    TArray<unsigned char> m_dynamicDataBuffer;
    uint64_t m_initialReserve;
    ZCustomSaveDataEntityReferenceStorage *m_pReferenceStorage;
    ZCustomSaveDataTypeIDStorage *m_pTypeIDStorage;
    ZCustomSaveDataRuntimeResourceIDStorage *m_pRRIDStorage;
};

class ZCustomSaveDataSerializer {
    public:
    static const int32_t IMPLEMENTED_CUSTOM_SAVE_DATA_VERSION;
    static const uint8_t START_OBJECT_FLAG;
    static const uint8_t END_OBJECT_FLAG;
    ZCustomSaveDataSerializer(const ZCustomSaveDataSerializer &other);
    ZCustomSaveDataSerializer(TFixedArray<unsigned char, 64> &staticDataBuffer,
                              TArray<unsigned char> &dynamicDataBuffer, uint64_t &fullSizeOfData,
                              uint64_t &initialBufferReserve,
                              ZCustomSaveDataEntityReferenceStorage *pReferenceStorage,
                              ZCustomSaveDataTypeIDStorage *pTypeIDStorage,
                              ZCustomSaveDataRuntimeResourceIDStorage *pRRIDStorage,
                              ZCustomSaveDataSerializer *pParentSerializer);
    ZCustomSaveDataSerializer(ZCustomSaveDataStorage &sourceStorage,
                              ZCustomSaveDataSerializer *pParentSerializer);
    virtual ~ZCustomSaveDataSerializer();
    bool StartSerializerForNextObject(uint32_t typeId, uint8_t version);
    bool SaveBinaryData(const void *inData, uint32_t dataSize);
    bool SaveRuntimeResourceID(const ZRuntimeResourceID &runtimeResourceID);
    bool SaveResourcePtr(const ZResourcePtr &resourcePtr);
    bool SaveEntityReference(const ZArrayElementRef &ref);
    bool SaveEntityReference(const ZAccessorRef &ref);
    bool SaveEntityReference(const ZEntityRef &ref);
    bool BeginSaveObject(uint32_t typeId, uint8_t version);
    bool EndSaveObject();
    bool InternalSaveVariant(const ZVariant &data);
    bool InternalSaveResourcePtr(const ZResourcePtr &data);
    bool InternalSaveTypeID(STypeID *const &typeID);
    bool AssertIfChildSerializerNotEmpty();
    TFixedArray<unsigned char, 64> &m_staticDataBuffer;
    TArray<unsigned char> &m_dynamicDataBuffer;
    uint64_t &m_fullSizeOfData;
    uint64_t &m_initialDataReserveSize;
    ZCustomSaveDataEntityReferenceStorage *m_pReferenceStorage;
    ZCustomSaveDataTypeIDStorage *m_pTypeIDStorage;
    ZCustomSaveDataRuntimeResourceIDStorage *m_pRRIDStorage;
    void PushChildSerializer(ZCustomSaveDataSerializer *pSerializer);
    void PopChildSerializer(ZCustomSaveDataSerializer *pSerializer);
    ZCustomSaveDataSerializer *m_pChildSerializer;
    ZCustomSaveDataSerializer *m_pParentSerializer;
    uint64_t m_startedObjectOffset;
};

class ZCustomSaveDataDeserializer {
    public:
    static const int32_t SUPPORTED_CUSTOM_SAVE_DATA_VERSION;
    ZCustomSaveDataDeserializer(const ZCustomSaveDataDeserializer &other);
    ZCustomSaveDataDeserializer(const ZCustomSaveDataStorage &sourceStorage,
                                uint64_t bufferStartPosition, uint64_t bufferSize);
    ZCustomSaveDataDeserializer(const TFixedArray<unsigned char, 64> &staticDataBuffer,
                                const TArray<unsigned char> &dynamicDataBuffer,
                                const ZCustomSaveDataEntityReferenceStorage *pReferenceStorage,
                                const ZCustomSaveDataTypeIDStorage *pTypeIDStorage,
                                const ZCustomSaveDataRuntimeResourceIDStorage *pRRIDStorage,
                                uint64_t bufferStartPosition, uint64_t bufferSize);
    virtual ~ZCustomSaveDataDeserializer();
    bool LoadBinaryData(void *outData, uint32_t dataSize);
    bool LoadRuntimeResourceID(ZRuntimeResourceID &runtimeResourceID);
    bool LoadResourcePtr(ZResourcePtr &resourcePtr);
    bool LoadEntityReference(ZArrayElementRef &ref);
    bool LoadEntityReference(ZAccessorRef &ref);
    bool LoadEntityReference(ZEntityRef &ref);
    bool SkipRestOfCurrentObjectData();
    bool SkipToEndOfCurrentObject();
    bool SkipToEndOfNextObject(uint32_t expectedTypeId);
    bool BeginLoadObject(uint32_t expectedTypeId, uint8_t &version, uint32_t &objectDataSize);
    bool CanEndLoadObject();
    bool EndLoadObject();
    bool LoadNextObjectInfo(uint32_t expectedTypeId, uint8_t &outVersion, uint32_t &outDataSaize,
                            uint64_t &outBufferStartPosition);
    bool IsBufferFinished();
    uint64_t GetDataSize();
    uint64_t GetDataPosition();
    uint8_t GetLastLoadedObjectVersion();
    virtual uint64_t &GetFullDataPositionReference();
    bool InternalLoadResourcePtr(ZResourcePtr &data);
    bool InternalLoadVariant(ZVariant &data);
    bool InternalLoadTypeID(STypeID *&data);
    const TFixedArray<unsigned char, 64> &m_staticDataBuffer;
    const TArray<unsigned char> &m_dynamicDataBuffer;
    const ZCustomSaveDataEntityReferenceStorage *m_pReferenceStorage;
    const ZCustomSaveDataTypeIDStorage *m_pTypeIDStorage;
    const ZCustomSaveDataRuntimeResourceIDStorage *m_pRRIDStorage;
    uint64_t m_bufferSize;
    uint8_t m_lastLoadedObjectVersion;
    uint64_t m_bufferStartPosition;
    uint64_t m_endOfObjectOffset;
    uint64_t m_bytesRead;
};

class ZSpatialEntityRef {
    public:
    static void RegisterType();
    ZSpatialEntityRef(const SMatrix &, TFuture<TSafeEntityRef<ZSpatialEntity>>);
    ZSpatialEntityRef(const TEntityRef<ZSpatialEntity> &);
    ZSpatialEntityRef();
    bool IsValid();
    float4 GetWorldPosition();
    SMatrix GetWorldMatrix();
    const TEntityRef<ZSpatialEntity> &GetSpatialEntity();
    void Reset();
    void Save(ZCustomSaveDataSerializer *, int32_t);
    void Load(ZCustomSaveDataDeserializer *, int32_t);
    SMatrix m_ImmediateMatrix;
    TFuture<TSafeEntityRef<ZSpatialEntity>> m_futureSpatialEntity;
    TSafeEntityRef<ZSpatialEntity> m_ImmediateSpatialEntity;
};

enum ESceneContextType {
    SceneContextType_Invalid,
    SceneContextType_GlobalResource,
    SceneContextType_GlobalInclude,
    SceneContextType_Scene,
    SceneContextType_Editor,
    SceneContextType_Gameplay,
    SceneContextType_MainScene,
    SceneContextType_EntitySingleton,
    SceneContextType_All,
};

class ZSceneData {
    public:
    ZSceneData(const ZSceneData &src);
    ZSceneData(const ZString &sceneResource);
    ZSceneData(const ZResourceID &sceneID);
    ZSceneData();
    ~ZSceneData();
    bool HasSceneData();
    ZString GetSceneName();
    const ZString &GetSceneSourceID();
    ZResourceID GetSceneSourceResourceID();
    const ZRuntimeResourceID &GetEntityTemplateRID();
    const ZRuntimeResourceID &GetEntityBlueprintRID();
    void SetHeaderLib(const ZResourceID &headerlib);
    const ZRuntimeResourceID &GetHeaderLibRID();
    const ZRuntimeResourceID &GetLayoutDefRID();
    void InitRIDs(const ZResourceID &sceneID);
    ZString m_sceneFile;
    ZRuntimeResourceID m_entitytemplateRID;
    ZRuntimeResourceID m_entityblueprintRID;
    ZRuntimeResourceID m_headerlibRID;
    ZRuntimeResourceID m_layoutdefRID;
};

enum ESceneContextOwnership {
    eOwnershipInvalid,
    eOwnershipToEditor,
    eOwnershipToUser,
    eOwnershipToSystem,
    eOwnershipToDLC,
};

struct SSceneInitParameters {
    SSceneInitParameters(const SSceneInitParameters &src);
    SSceneInitParameters(ESceneContextType contextType, const ZSceneData &sceneData,
                         const TEntityRef<ZSpatialEntity> &pParent);
    SSceneInitParameters(ESceneContextType, const ZSceneData &);
    SSceneInitParameters(ESceneContextType, const ZResourceID &,
                         const TEntityRef<ZSpatialEntity> &);
    SSceneInitParameters(ESceneContextType contextType, const ZResourceID &sceneID);
    SSceneInitParameters(ESceneContextType contextType, const ZString &sceneID);
    SSceneInitParameters(ESceneContextType, uint64_t, const ZEntityRef &);
    SSceneInitParameters(ESceneContextType contextType, uint64_t contextKey);
    void SetSceneResource(const ZSceneData &sceneData);
    void SetSceneResource(const ZString &sceneResource);
    void SetSceneResource(const ZResourceID &sceneID);
    const ESceneContextType &GetContextType();
    static uint64_t GetContextKey(const ZString &name);
    static uint64_t GetContextKey(const ZRuntimeResourceID &sceneID);
    static uint64_t GetContextKey(const ZResourceID &sceneID);
    const uint64_t &GetContextKey();
    ZString GetDebugName();
    ZString GetSceneName();
    ESceneContextOwnership GetOwnership();
    void SetOwnership(ESceneContextOwnership ownership);
    ESceneContextType m_contextType;
    uint64_t m_contextKey;
    TScopedPtr<ZSceneData> m_pSceneData;
    TEntityRef<ZSpatialEntity> m_pParent;
    ESceneContextOwnership m_ownership;
};

class ZEntityFunctionCaller {
    public:
    enum ECallOperation {
        eCallBlueprintInit,
        eCallFactoryInit,
        eCallCppInit,
        eCallSetPostInitProperties,
        eCallRemove,
        eCallOperationCount,
    };
    static void RegisterOperationHandlers(
        ZEntityFunctionCaller::ECallOperation operation,
        const std::function<bool __cdecl(ZEntityRef const &, void *, unsigned int &, __int64)>
            &handler,
        const std::function<bool __cdecl(ZEntityRef const &, void *, unsigned int)> &reEvaluator);
    static const std::function<bool __cdecl(ZEntityRef const &, void *, unsigned int &, __int64)> &
    GetHandlerForOperation(ZEntityFunctionCaller::ECallOperation operation);
    static const std::function<bool __cdecl(ZEntityRef const &, void *, unsigned int)> &
    GetReEvaluatorForOperation(ZEntityFunctionCaller::ECallOperation operation);
    bool ExecuteFunctionCall(int64_t endBudgetTick);
    bool EvaluateCall();
    const ZEntityRef &GetEntity();
    ZEntityRef m_entity;
    const std::function<bool(ZEntityRef const &, void *, unsigned int &, __int64)> &m_handler;
    void *m_pFactory;
    ZEntityFunctionCaller::ECallOperation m_operation;
    uint32_t m_extraData;
    uint32_t m_groupID;
    bool m_bForceCompleteGroup;
    bool m_bNeedToReevaluate;
    ZEntityFunctionCaller(const ZEntityRef &entity, void *pFactory,
                          ZEntityFunctionCaller::ECallOperation operation, bool bNeedsToReevaluate,
                          uint32_t groupID, bool bForceCompleteGroup);
};

struct SEntityFunctionCallCollector {
    SEntityFunctionCallCollector();
    class ZScopedDisableRecursiveCollection {
        public:
        ZScopedDisableRecursiveCollection(SEntityFunctionCallCollector &collector);
        ~ZScopedDisableRecursiveCollection();
        SEntityFunctionCallCollector &m_collector;
    };
    class ZScopedForceUseForcedGroupID {
        public:
        ZScopedForceUseForcedGroupID(SEntityFunctionCallCollector &collector,
                                     bool bForceUseForcedGroupID);
        ~ZScopedForceUseForcedGroupID();
        SEntityFunctionCallCollector &m_collector;
        bool m_bPreviousUseForcedGroupID;
        uint32_t m_previousForcedGroupID;
    };
    class ZScopedCallGroup {
        public:
        ZScopedCallGroup(SEntityFunctionCallCollector &collector, bool bStartScope,
                         bool bMustFinish);
        ~ZScopedCallGroup();
        SEntityFunctionCallCollector &m_collector;
        bool m_bScopeStarted;
    };
    bool CallFunctions(bool bReverse, int64_t endTick);
    void AddFunctionCaller(const ZEntityRef &entity, void *pFactory,
                           ZEntityFunctionCaller::ECallOperation operation,
                           bool bNeedsToReevaluate);
    bool ShouldCollectRecursively();
    TMap<unsigned int, TPair<ZString, unsigned __int64>> m_typeCounts;
    void StartUseForcedGroupID(bool bForceUseForcedGroupID, bool &bPreviousUseValue,
                               uint32_t &previousForcedGroupID);
    void FinishUseForcedGroupID(bool bForceUseForcedGroupID);
    uint64_t StartCallGroup(bool bMustFinish);
    void FinishCallGroup();
    TArray<ZEntityFunctionCaller> m_entitiesToCall;
    TArray<ZEntityFunctionCaller> m_iteratingEntities;
    TArray<bool> m_groupingStack;
    uint64_t m_iteratingIndex;
    uint32_t m_currentForceGroupID;
    uint32_t m_disallowCollectRecursively;
    bool m_bUseForcedGroupID;
};

struct SLoadEntityInfo {
    SLoadEntityInfo(const SEntityInstantiationSaveData &rootEntitySaveData,
                    const ZSaveDataValueStorage &saveDataValueStorage);
    const SEntityInstantiationSaveData &m_rootEntitySaveData;
    const ZSaveDataValueStorage &m_saveDataValueStorage;
};

class ZSubEntityStatsAccumulator {
    public:
    ZSubEntityStatsAccumulator();
    uint64_t GetTotalEntityCount();
    uint64_t GetTotalPathMappingCount();
    const TFixedArray<std::atomic<unsigned int>, 2> &GetFunctionImplementationCounts();
    void IncrementEntityPathMappingCount();
    void AddEntityFunctionImplementationCounts(const ZBitArray &implementedFunctions);
    std::atomic<unsigned __int64> m_totalEntityCount;
    std::atomic<unsigned __int64> m_totalPathMappingCount;
    TFixedArray<std::atomic<unsigned int>, 2> m_functionImplementationCounts;
};

struct SEntityCreationInfo {
    SEntityCreationInfo(const SEntityCreationInfo &);
    SEntityCreationInfo(uint64_t uiDynamicFlag);
    uint64_t m_uiDynamicFlag;
    TSharedPtr<ZSubEntityStatsAccumulator> m_pStatsAccumulator;
};

enum class EStreamingPriority {
    Immediate,
    High,
    Default,
    Low,
    COUNT,
    Invalid,
};

class IEntityPersistenceHelper {
    public:
    IEntityPersistenceHelper();
    virtual ~IEntityPersistenceHelper();
    virtual void ProcessEntity(const ZEntityRef &, bool);
    virtual void RegisterEntityIgnoredWhenSaving(const ZEntityRef &);
    virtual void Finalize();
};

struct SInterfaceData {
    STypeID *m_Type;
    int64_t m_nInterfaceOffset;
    bool operator==(const SInterfaceData &other);
    bool operator<(STypeID *const &otherType);
    bool operator<(const SInterfaceData &other);
};

struct SExposedEntityData {
    uint32_t m_nExposedEntityNameID;
    int64_t m_nEntityOffset;
    uint32_t m_propertyID;
};

struct SSubsetData {
    ZHashedString m_subsetName;
    uint32_t m_nSubsetFlags;
    STypeID *m_subsetType;
    TArray<__int64> m_aEntityOffsets;
    TArray<TPair<__int64, SSubsetData *>> m_aEntitySubsets;
};

struct SPropertyData {
    SPropertyData();
    bool IsRuntimeEditableOrConstAfterStart();
    bool IsStreamable();
    bool IsMediaStreamable();
    bool IsSavable();
    bool IgnoreSpawnerSavableCheck();
    bool ShouldCallSetter(bool bPostInit);
    uint32_t m_nPropertyID;
    uint64_t m_nEntityOffset;
    int64_t m_nPropertyOffset;
    const SPropertyInfo *m_pInfo;
};

struct SPinData {
    ZPinFunctor m_functor;
    int64_t m_nOffsetToThisPtr;
    uint64_t m_nExtraData;
};

class ZEntityOperationValueIndexContainer {
    public:
    static void RegisterType();
    ZEntityOperationValueIndexContainer(const ZEntityOperationValueIndexContainer &other);
    ZEntityOperationValueIndexContainer(ZEntityOperationValueIndexContainer *other);
    ZEntityOperationValueIndexContainer();
    ZEntityOperationValueIndexContainer &
    operator=(const ZEntityOperationValueIndexContainer &other);
    uint64_t GetContainerSize();
    void StoreIndexAtPosition(uint32_t index, uint32_t position);
    const uint32_t &GetIndexAtPosition(uint32_t position);
    void ResizeContainer(uint32_t newSize);
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
    TArray<unsigned int> m_indexContainer;
};

struct SInstantiationSubEntitySaveInfo {
    static const uint32_t SKIPPED_SAVE_VALUE_INDEX;
    SInstantiationSubEntitySaveInfo(const ZSubEntityPath &saveDataPath);
    SInstantiationSubEntitySaveInfo();
    bool TryGetPropertyValueToSet(const ZSaveDataValueStorage &saveDataValueStorage,
                                  const ZEntityOperationValueIndexContainer *pIndexContainer,
                                  uint32_t relativeIndexOfPropertySave, uint32_t classID,
                                  uint32_t propertyId, ZVariantRef &outValue,
                                  STypeID *propertyTypeId);
    void AddSavedPropertyValue(const ZString &typeName, bool isRenderMaterial,
                               ZSaveDataValueStorage &saveDataValueStorage,
                               ZEntityOperationValueIndexContainer *pIndexContainer,
                               uint32_t &indexPosition, const SPropertyData *pPropertyData,
                               const ZEntityRef &propertyEntitySource, bool isStreamableProperty);
    void AddSavedPropertyValue(const ZString &typeName, bool isRenderMaterial,
                               ZSaveDataValueStorage &saveDataValueStorage,
                               ZEntityOperationValueIndexContainer *pIndexContainer,
                               uint32_t &indexPosition, const SPropertyData *pPropertyData,
                               const ZEntityRef &propertyEntitySource, bool isStreamableProperty,
                               bool valueCanBeStoredInIndex);
    void AddSavedPropertyValueUsingTemporaryBuffer(
        const ZString &typeName, bool isRenderMaterial, ZSaveDataValueStorage &saveDataValueStorage,
        ZEntityOperationValueIndexContainer *pIndexContainer, uint32_t &indexPosition,
        void *pPropertyAddress, const IType *pPropertyType);
    void AddSavedPropertyValueUsingTemporaryBuffer(
        const ZString &typeName, bool isRenderMaterial, ZSaveDataValueStorage &saveDataValueStorage,
        ZEntityOperationValueIndexContainer *pIndexContainer, uint32_t &indexPosition,
        void *pPropertyAddress, const IType *pPropertyType, bool bCanPropertyBeStoredInIndex);
    void AddSkippedPropertyValue(ZEntityOperationValueIndexContainer *pIndexContainer,
                                 uint32_t &indexPosition);
    bool TryGetSubEntityCustomSaveData(const ZSaveDataValueStorage &saveDataValueStorage,
                                       const ZEntityOperationValueIndexContainer *pIndexContainer,
                                       uint32_t customSaveOperationIndex, uint32_t classID,
                                       int32_t &outCustomSaveDataVersion, ZVariantRef &outValue,
                                       uint32_t startIndexOfCustomSaveOperations);
    void AddCustomSaveDataValue(const ZString &typeName,
                                ZSaveDataValueStorage &saveDataValueStorage,
                                ZEntityOperationValueIndexContainer *pIndexContainer,
                                uint32_t &indexPosition, ZVariant &customSaveData,
                                uint32_t customSaveDataVersion);
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
    ZSubEntityPath m_subEntitySaveDataPath;
    uint32_t m_startIndexOfClassSaveDataIndices;
};

struct SEntityInstantiationSaveData {
    SEntityInstantiationSaveData(const SEntityInstantiationSaveData &other);
    SEntityInstantiationSaveData(SEntityInstantiationSaveData *other);
    SEntityInstantiationSaveData();
    SEntityInstantiationSaveData &operator=(const SEntityInstantiationSaveData &other);
    bool IsEmpty();
    void Clear();
    void StoreAllocationFactoryResource(ZRuntimeResourceID allocationFactoryResource,
                                        ZSaveDataValueStorage &saveDataValueStorage);
    ZRuntimeResourceID
    GetStoredAllocationFactoryResource(const ZSaveDataValueStorage &saveDataValueStorage);
    SInstantiationSubEntitySaveInfo &
    GetLastOrCreateSubEntitySaveInfo(const ZSubEntityPath &subEntityPath);
    SInstantiationSubEntitySaveInfo &CreateSubEntitySaveInfo(const ZSubEntityPath &subEntityPath);
    const SInstantiationSubEntitySaveInfo *
    GetExistingSaveSubEntitySaveInfo(const ZSubEntityPath &subEntityPath);
    bool TryUpdateLastFetchedIndex(const ZSubEntityPath &subEntityPath);
    void BaseDumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                              uint64_t &dataSizeAccumulator, bool isEntityDynamic);
    ZRuntimeResourceID m_allocationFactoryRRID;
    uint32_t m_originalEntityAllocationId;
    uint32_t m_indexOfLastSaveInfoFetched;
    uint32_t m_currentIndexContainerPosition;
    ZString m_savedEntityName;
    TArray<SInstantiationSubEntitySaveInfo> m_subEntitySaveInfos;
    ZEntityOperationValueIndexContainer m_indexContainer;
};

struct SDynamicEntityInstantiationSaveData : public SEntityInstantiationSaveData {
    SDynamicEntityInstantiationSaveData(const SDynamicEntityInstantiationSaveData &other);
    SDynamicEntityInstantiationSaveData(SDynamicEntityInstantiationSaveData *other);
    SDynamicEntityInstantiationSaveData();
    SDynamicEntityInstantiationSaveData &
    operator=(const SDynamicEntityInstantiationSaveData &other);
    bool IsEntityDynamic();
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
};

struct SStaticEntityInstantiationSaveData : public SEntityInstantiationSaveData {
    SStaticEntityInstantiationSaveData(const SStaticEntityInstantiationSaveData &other);
    SStaticEntityInstantiationSaveData(SStaticEntityInstantiationSaveData *other);
    SStaticEntityInstantiationSaveData();
    SStaticEntityInstantiationSaveData &operator=(const SStaticEntityInstantiationSaveData &other);
    bool IsEntityDynamic();
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
};

class ISceneRestoreHelper {
    public:
    ISceneRestoreHelper();
    virtual ~ISceneRestoreHelper();
    virtual ZEntityRef CreateEntity(ISceneContext *, const SDynamicEntityInstantiationSaveData &,
                                    const ZSaveDataValueStorage *, const ZEntityRef &);
    virtual ZEntityRef CreateEntity(ISceneContext *, const SStaticEntityInstantiationSaveData &,
                                    const ZSaveDataValueStorage *, const ZEntityRef &);
    virtual void RestoreAllCreatedEntityProperties();
    virtual void RestoreAllCreatedEntityCustomSaveData();
    virtual bool IsContextNewlyLoaded(uint64_t);
    virtual const ZSaveDataValueStorage &GetSaveDataValueStorageAtIndex(uint32_t);
    virtual void ReserveSpaceForExpectedEntitiesToRestore(uint64_t);
};

struct SSingletonSaveData {
    SSingletonSaveData(uint32_t singletonId, uint32_t singletonRestoreTiming);
    SSingletonSaveData();
    ~SSingletonSaveData();
    bool IsEmpty();
    void Clear();
    uint32_t m_singletonId;
    uint32_t m_singletonRestoreTiming;
    uint32_t m_saveDataVersion;
    uint32_t m_saveDataIndex;
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
};

enum ESingletonRestoreTiming {
    eBeforeSceneContextStateRestore,
    eBeforeLoadOfNeededSceneContexts,
    eBeforeEntityCreation,
    eBeforeEntityPropertyRestore,
    eBeforeEntityCustomRestore,
    eAfterEverythingIsRestored,
    eRestoreTimingCount,
};

class ZSceneContextSaveData {
    public:
    static void RegisterType();
    enum ESceneSaveDataType {
        eGlobalResourceSaveData,
        eGloablIncludeSaveData,
        eSceneSaveData,
    };
    ZSceneContextSaveData(const ZSceneContextSaveData &other);
    ZSceneContextSaveData(ZSceneContextSaveData *other);
    ZSceneContextSaveData();
    ZSceneContextSaveData &operator=(const ZSceneContextSaveData &other);
    ~ZSceneContextSaveData();
    bool IsEmpty();
    void Trim();
    void Clear();
    SSingletonSaveData &
    CreateSingletonSaveDataWithIdAndTiming(uint32_t singletonId,
                                           ESingletonRestoreTiming restoreTiming);
    const SSingletonSaveData *
    GetSingletonSaveDataWithIdAndTiming(uint32_t singletonId,
                                        ESingletonRestoreTiming restoreTiming);
    bool SerializeTo(ZSaveGameDataSerializer *pSerializer, IOutputStream *pStreamToSerializeWith);
    bool DeserializeFrom(ZSaveGameDataDeserializer *pDeserializer,
                         IInputStream *pStreamToDeserializeWith);
    void AcquireInstantiationResourceReferences(SResourcePtrsToLoad &resPtrsToLoad);
    uint64_t m_sceneContextKey;
    uint64_t m_parentSceneContextKey;
    ESceneContextType m_sceneContextType;
    ZSceneContextSaveData::ESceneSaveDataType m_saveDataType;
    ZString m_sceneID;
    uint64_t m_ownership;
    TArray<SStaticEntityInstantiationSaveData> m_sceneStaticEntityInstantiations;
    TArray<SDynamicEntityInstantiationSaveData> m_sceneDynamicEntityInstantiations;
    TArray<SSingletonSaveData> m_sceneSingletonSaveDatas;
    uint32_t m_saveDataValueStorageIndex;
    bool m_bSceneSaveDataReady;
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
};

class ZThreadedFactoryTraversalJobInfo {
    public:
    ZThreadedFactoryTraversalJobInfo(const ZEntityRef &rootEntity, uint32_t priority);
    ZThreadedFactoryTraversalJobInfo(const ZEntityRef &rootEntity);
    virtual ~ZThreadedFactoryTraversalJobInfo();
    void Reference();
    void Dereference();
    virtual bool IsTraversalCompleted();
    bool IsStarted();
    uint32_t GetRefCount();
    void WaitUntilTraversalIsFinished();
    TFuture<ZEntityRef> GetCompletionFuture();
    TFuture<ZEntityRef> GetPostCompletionFuture();
    void StartTraversalJobs();
    void ReserveTraversalJobs(uint64_t nJobs);
    ZJobThread *CreateTraversalJob();
    ZJobThread *CreateAndSetupTraversalJob(ZJobProgramThread *pProgram, void *pUserData,
                                           uint64_t userDataSize);
    ZEntityRef m_rootEntity;
    ZJobThread *CreateTraversalJobOnly();
    virtual void StartWaitingTraversalJobs();
    virtual void SignalCompletion();
    TPromise<ZEntityRef> m_CompletionPromise;
    TPromise<ZEntityRef> m_PostCompletionPromise;
    ZJobChainThread *m_pJobChain;
    TArray<ZJobThread *> m_jobsToRun;
    TArray<ZJobThread *> m_jobsStarted;
    ZAssertlock m_assertLocker;
    ZMutex m_jobLocker;
    bool m_bStarted;
};

struct SEntityFactoryTimeslicingParameter {
    SEntityFactoryTimeslicingParameter(bool bDisabled);
    bool disabled;
    TArray<unsigned __int64> processingStack;
    int64_t endTick;
    uint32_t currentDepth;
    uint32_t numIterationsBeforeTestingTime;
    uint32_t currentIteration;
    static SEntityFactoryTimeslicingParameter Disabled;
};

class ISceneContext : public IComponentInterface {
    public:
    enum ESceneContextFlag {
        eLoading,
        eLoaded,
        ePreloading,
        ePreloaded,
        eInstantiating,
        eInstantiated,
        ePostIniting,
        ePostInited,
        eUnloading,
        eReinitializing,
        eStreamedOut,
        eUninitialized,
        eDestructed,
        eReadOnly,
        eInEditMode,
        eInStartMode,
    };
    enum EFreeType {
        RemoveFromContext,
        FreeMemory,
        Full,
    };
    enum EClearSceneFlags {
        CLEAR_NOTHING,
        CLEAR_FOR_EDITOR,
        UNINITIALIZE_FOR_EDITOR,
        FULLY_UNLOAD_SCENE,
        UNLOAD_AND_KEEP_SCENE_ENTITY,
    };
    virtual const uint64_t &GetContextKey();
    virtual uint64_t GetMainParentKey();
    virtual void SetParentContext(ISceneContext *);
    virtual ISceneContext *GetParentContext();
    virtual void SetParentEntity(const TEntityRef<ZSpatialEntity> &);
    virtual void AddFlag(ISceneContext::ESceneContextFlag);
    virtual void ClearFlag(ISceneContext::ESceneContextFlag);
    virtual uint64_t GetSceneSavableEntityCount();
    virtual uint64_t GetSceneDynamicSavableEntityCount();
    virtual uint64_t GetSceneStaticSavableEntityCount();
    virtual uint64_t GetEstimatedSceneSaveEntityCount();
    virtual const ESceneContextType &GetSceneContextType();
    virtual void SetParameters(TUniquePtr<SSceneInitParameters> &);
    virtual SSceneInitParameters *GetParameters();
    virtual const SSceneInitParameters *GetParameters() const;
    virtual bool IsLoading();
    virtual bool IsLoaded();
    virtual bool IsPreloading();
    virtual bool IsPreloaded();
    virtual bool IsInstantiating();
    virtual bool IsInstantiated();
    virtual bool IsPostIniting();
    virtual bool IsPostInited();
    virtual bool IsUnloading();
    virtual bool IsReinitializing();
    virtual bool IsStreamedOut();
    virtual bool IsUninitialized();
    virtual bool IsDestructed();
    virtual bool IsInEditMode();
    virtual bool IsInStartMode();
    virtual bool IsReadOnly();
    virtual bool IsValidForAddEntity();
    virtual bool IsEmpty();
    virtual bool IsOrphanContextLoadedBySystem();
    virtual bool AreAllResourcesReady();
    virtual bool NewSceneEntity(const TResourcePtr<IEntityFactory> &, const ZString &);
    virtual bool NewSceneEntity(const ZString &);
    struct SNewEntityParams {
        SNewEntityParams();
        SNewEntityParams(const ZString &debugName,
                         const TResourcePtr<IEntityFactory> &entityFactory);
        SNewEntityParams(const ZString &, const ZRuntimeResourceID &);
        ISceneContext::SNewEntityParams &InitProperties(const TMap<unsigned int, ZVariant> &);
        ISceneContext::SNewEntityParams &TransformParent(const ZEntityRef &transformParent);
        ISceneContext::SNewEntityParams &PostInitState(EEntityLifeCycleStates);
        ISceneContext::SNewEntityParams &EntitySavable(bool entitySavable);
        ISceneContext::SNewEntityParams &StageEntityOnly(bool);
        ISceneContext::SNewEntityParams &InstallStreamedDependencies(bool);
        ISceneContext::SNewEntityParams &IgnoreNonExistantInitProperty(bool);
        ZString m_DebugName;
        ZRuntimeResourceID m_ResourceID;
        TMap<unsigned int, ZVariant> m_InitProperties;
        ZEntityRef m_TransformParent;
        EEntityLifeCycleStates m_PostInitState;
        bool m_EntitySavable;
        bool m_StageEntityOnly;
        bool m_InstallStreamedDependencies;
        bool m_bIgnoreNonExistantInitProperty;
    };
    virtual ZEntityRef NewEntity(const ISceneContext::SNewEntityParams &);
    struct SNewEntityAsyncParams {
        SNewEntityAsyncParams();
        SNewEntityAsyncParams(const ZString &debugName,
                              const TResourcePtr<IEntityFactory> &entityFactory);
        SNewEntityAsyncParams(const ZString &debugName, const ZRuntimeResourceID &resourceID);
        ISceneContext::SNewEntityAsyncParams &
        InitProperties(const TMap<unsigned int, ZVariant> &initProperties);
        ISceneContext::SNewEntityAsyncParams &TransformParent(const ZEntityRef &transformParent);
        ISceneContext::SNewEntityAsyncParams &EntityAllocationID(uint32_t entityAllocationID);
        ISceneContext::SNewEntityAsyncParams &PostInitState(EEntityLifeCycleStates);
        ISceneContext::SNewEntityAsyncParams &Priority(EStreamingPriority priority);
        ISceneContext::SNewEntityAsyncParams &EntitySavable(bool entitySavable);
        ISceneContext::SNewEntityAsyncParams &Uninitialized(bool uninitialized);
        ISceneContext::SNewEntityAsyncParams &StageEntityOnly(bool);
        ISceneContext::SNewEntityAsyncParams &
        InstallStreamedDependencies(bool installStreamedDependencies);
        ZString m_DebugName;
        ZRuntimeResourceID m_ResourceID;
        TMap<unsigned int, ZVariant> m_InitProperties;
        ZEntityRef m_TransformParent;
        uint32_t m_EntityAllocationID;
        EEntityLifeCycleStates m_PostInitState;
        EStreamingPriority m_Priority;
        bool m_EntitySavable;
        bool m_Uninitialized;
        bool m_StageEntityOnly;
        bool m_InstallStreamedDependencies;
    };
    virtual TFuture<ZEntityRef> NewEntityAsync(const ISceneContext::SNewEntityAsyncParams &);
    virtual TFuture<ZEntityRef>
    InitializeEntityAsync(const ZEntityRef &, const TResourcePtr<IEntityFactory> &,
                          const TMap<unsigned int, ZVariant> &, const ZEntityRef &, uint32_t, bool,
                          EEntityLifeCycleStates, EStreamingPriority, bool);
    virtual void StageEntity(const ZEntityRef &, EStreamingPriority);
    virtual void UnstageEntity(const ZEntityRef &, const TMap<unsigned int, ZVariant> &);
    virtual ZEntityRef CreateEntityForRestore(const ZString &, const TResourcePtr<IEntityFactory> &,
                                              uint32_t, const ZEntityRef &);
    virtual ZEntityRef CreateOwnedEntityForRestore(const ZString &,
                                                   const TResourcePtr<IEntityFactory> &, uint32_t,
                                                   const ZEntityRef &);
    virtual void OnEntityPreInit(const ZEntityRef &, uint32_t, bool);
    virtual void OnEntityAdded(const ZEntityRef &, EStreamingPriority, bool);
    virtual void RegisterEntityFactory(const ZEntityRef &, const TResourcePtr<IEntityFactory> &);
    virtual void BeginStatePreloading();
    virtual void EndStatePreloading();
    virtual void CancelStatePreloading();
    virtual void BeginStateLoading();
    virtual void EndStateLoading();
    virtual void CancelLoading();
    virtual void EndStateInstantiating();
    virtual void CancelStateInstantiating();
    virtual void BeginStatePostIniting();
    virtual void EndStatePostIniting();
    virtual void CancelStatePostIniting();
    virtual void BeginStateUnloading(bool);
    virtual void BeginStateReloading();
    virtual void ClearScene(bool);
    virtual void ReleaseSceneResources();
    virtual void OnClearSceneDone();
    virtual ZThreadedFactoryTraversalJobInfo *CreateScene_PreInit(bool);
    virtual void CollectSceneEntityInitList(SEntityFunctionCallCollector &);
    virtual void CreateScene_TimeslicedInit(SEntityFactoryTimeslicingParameter &);
    virtual ZThreadedFactoryTraversalJobInfo *CreateScene_ThreadedInit(bool);
    virtual ZThreadedFactoryTraversalJobInfo *CreateScene_PostInit(EEntityLifeCycleStates, bool);
    virtual ZThreadedFactoryTraversalJobInfo *CreateSceneWithoutInit(bool);
    virtual void EnableKillOnly(const ZEntityRef &);
    virtual void DisableKillOnly(const ZEntityRef &);
    virtual const ZRuntimeResourceID &GetSceneFactoryID();
    virtual const TResourcePtr<IEntityFactory> &GetSceneFactoryResource();
    virtual const TResourcePtr<IEntityBlueprintFactory> &GetSceneBlueprintFactoryResource();
    virtual const TResourcePtr<ZHeaderLibrary> &GetSceneHeaderLibrary();
    virtual void SetSceneHeaderLib(const TResourcePtr<ZHeaderLibrary> &);
    virtual void SetSceneFactoryResource(const TResourcePtr<IEntityFactory> &);
    virtual void AllocateSceneResources();
    virtual const ZRuntimeResourceID &GetFactoryRuntimeResourceID(const ZEntityRef &);
    virtual const ZRuntimeResourceID &GetOriginFactoryRuntimeResourceID(const ZEntityRef &);
    virtual const TResourcePtr<IEntityFactory> &GetResourcePtr(const ZEntityRef &);
    virtual bool EnsureAllEntitiesDeleted(bool);
    virtual void DropSceneFactory();
    virtual ZEvent<ISceneContext *, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetSceneLoadedEvent();
    virtual ZEvent<ISceneContext *, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetScenePreUnloadingEvent();
    virtual ZEvent<ISceneContext *, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetSceneUnloadingEvent();
    virtual const TEntityRef<ZSpatialEntity> &GetScene();
    virtual void EnterEditEntities();
    virtual void ExitEditEntities();
    virtual void StartEntities();
    virtual void StopEntities();
    virtual ZEntityRef CreateUninitializedEntityWithFactory(const ZString &,
                                                            const TResourcePtr<IEntityFactory> &,
                                                            uint32_t);
    virtual bool IsPointerFromSceneBlock(uint8_t *);
    virtual bool IsEntityOwnedByTheScene(const ZEntityRef &);
    virtual bool DoesEntityExistInOwnedByTheScene(const ZEntityRef &);
    virtual bool DoesEntityExist(const ZEntityRef &);
    virtual bool CanDeleteEntity(const ZEntityRef &);
    virtual void IgnoreEntityWhenSaving(const ZEntityRef &);
    virtual void StopIgnoringEntityWhenSaving(const ZEntityRef &);
    virtual SaveUtils::EEntityIgnoredWhenSavingResult IsEntityIgnoredWhenSaving(const ZEntityRef &);
    virtual void AssertEntityIgnoredWhenSaving(const ZEntityRef &);
    virtual void AssertEntityNotIgnoredWhenSaving(const ZEntityRef &);
    virtual void PersistAllEntities(IEntityPersistenceHelper &);
    virtual void RecreateAllEntities(ISceneRestoreHelper &, const ZSceneContextSaveData &);
    virtual void ReInitializeScene(EEntityLifeCycleStates);
    virtual void ConstructAllEntities();
    virtual void ConfigureAllEntities();
    virtual void DeleteAllDynamicEntities();
    virtual void RemoveAllEntities();
    virtual void UninitAllEntities();
    virtual void UninitializeEntityIteration(const ZEntityRef &,
                                             SEntityFactoryTimeslicingParameter &);
    virtual void DestructAllEntities();
    virtual void ReInitializeEntity(const ZEntityRef &, EEntityLifeCycleStates);
    virtual ZEntityType **GetSubEntityById(uint32_t);
    virtual const TArray<ISceneContext *> &GetChildSceneContexts();
    virtual void GetCurrentAndChildSceneContextKeysRecursive(TArray<unsigned __int64> &);
    virtual const TArray<ZEntityRef> &GetEntitiesOwnedByTheScene();
    virtual const TArray<ZEntityRef> &GetAllEntities();
    virtual void AddChildSceneContext(ISceneContext *);
    virtual void RemoveChildSceneContext(ISceneContext *);
    virtual void UninitEntity(const ZEntityRef &);
    virtual void FreeEntity(const ZEntityRef &, ISceneContext::EFreeType);
    virtual void OnEntityRemoving(const ZEntityRef &, bool, SEntityFunctionCallCollector *);
    virtual void ChangeKey(const uint64_t &);
    virtual void ResyncParentEntityFromEditor(const TEntityRef<ZSpatialEntity> &);
    virtual ZEvent<ZEntityRef const &, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetEntityAddedEvent();
    virtual ZEvent<ZEntityRef const &, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetEntityRemovedEvent();
    virtual void DumpSceneInfo(ITextWriter *);
    virtual ZString GetSceneContextState();
    virtual ZString GetSceneContextStatus();
};

struct SEntitySaveOperation {
    SEntitySaveOperation(uint32_t classId, uint32_t propertyId);
    SEntitySaveOperation(uint32_t classId);
    SEntitySaveOperation();
    bool operator==(const SEntitySaveOperation &rhs);
    uint32_t m_classId;
    bool m_bIsCustomSaveOperation;
    char m_padding1;
    char m_padding2;
    char m_padding3;
    uint32_t m_savedPropertyId;
};

struct SOrderedClassSaveOperations {
    SOrderedClassSaveOperations(const SOrderedClassSaveOperations &other);
    SOrderedClassSaveOperations(SOrderedClassSaveOperations *other);
    SOrderedClassSaveOperations();
    SOrderedClassSaveOperations &operator=(const SOrderedClassSaveOperations &other);
    bool IsEmpty();
    void AddPropertySaveOperation(uint32_t classId, uint32_t propertyId);
    void AddCustomSaveOperation(uint32_t classId);
    bool operator==(const SOrderedClassSaveOperations &rhs);
    bool TryGetIndexOfPropertySaveOperation(uint32_t classId, uint32_t propertyId,
                                            uint32_t &lastFetchedIndex);
    bool TryGetIndexOfCustomSaveOperation(uint32_t classId, uint32_t &lastFetchedIndex);
    uint32_t GetIndexOfFirstCustomSaveOperation();
    const TArray<SEntitySaveOperation> &GetAllOperations();
    uint32_t GetNumberOfIndicesNeededForOperations();
    uint32_t m_nReferenceCount;
    uint32_t m_propertySaveCount;
    uint32_t m_customSaveDataCount;
    TArray<SEntitySaveOperation> m_orderedOperations;
};

class ZSaveGameJobManager {
    public:
    ZSaveGameJobManager();
    ~ZSaveGameJobManager();
    bool CanAcceptJobs();
    static uint64_t GetAppropriateNonCriticalAffinity();
    TSharedPtr<ZJobThread> CreateNewJob(uint64_t affinityMask);
    void AddNewJob(const TSharedPtr<ZJobThread> &pJob);
    void AddNewJobs(const TArray<TSharedPtr<ZJobThread>> &pJobs);
    bool IsIdle();
    void ClearFinishedJobs();
    bool HasJobsToRun();
    uint32_t GetMaxNumberOfConcurrentJobs();
    void WaitForAllJobDone(bool bActiveWait, bool bBoostPriority);
    void QueueNewJob(const TSharedPtr<ZJobThread> &pJob);
    void AddJobsToChain(const TArray<TSharedPtr<ZJobThread>> &pJobsToAdd);
    bool InternalClearFinishedJobs();
    ZMutex m_jobManagerMutex;
    ZJobChainThread *m_pJobChain;
    TArray<TSharedPtr<ZJobThread>> m_pCurrentJobs;
    TArray<TSharedPtr<ZJobThread>> m_pQueuedJobs;
};

class ZSaveDataValueStorageStatsStorage {
    public:
    ZSaveDataValueStorageStatsStorage(const ZSaveDataValueStorageStatsStorage &other);
    ZSaveDataValueStorageStatsStorage(ZSaveDataValueStorageStatsStorage *other);
    ZSaveDataValueStorageStatsStorage();
    ZSaveDataValueStorageStatsStorage &operator=(const ZSaveDataValueStorageStatsStorage &other);
    struct SEntityTypeSaveDataStats {
        SEntityTypeSaveDataStats();
        void
        AggregateStats(const ZSaveDataValueStorageStatsStorage::SEntityTypeSaveDataStats &other);
        uint64_t m_timesSaved;
        uint64_t m_classesSaved;
        uint64_t m_timesSkipped;
        uint64_t m_propertiesSaved;
        uint64_t m_sizeOfProperties;
        uint64_t m_timesPropertiesUnique;
        uint64_t m_timesPropertiesReused;
        uint64_t m_timesPropertyStoredDirectly;
        uint64_t m_timesPropertyStoredWithoutVariant;
        uint64_t m_timesCustomSaveDataStored;
        uint64_t m_timesCustomSaveDataStoredDirectly;
        uint64_t m_timesCustomSaveDataReused;
        uint64_t m_sizeOfCustomSaveData;
        uint64_t m_storeTimeSpent;
        uint64_t m_propertyTimeSpent;
        uint64_t m_customTimeSpent;
        uint64_t m_timeSpent;
        uint32_t m_maxPathDepth;
    };
    struct SDataTypeSaveDataStats {
        SDataTypeSaveDataStats();
        void AggregateStats(const ZSaveDataValueStorageStatsStorage::SDataTypeSaveDataStats &other);
        uint64_t m_timesSaved;
        uint64_t m_uniqueValues;
        uint64_t m_timesReused;
        uint64_t m_totalUsedSpace;
        uint64_t m_totalSpaceSaved;
    };
    struct SSingletonSaveDataStats {
        SSingletonSaveDataStats();
        void
        AggregateStats(const ZSaveDataValueStorageStatsStorage::SSingletonSaveDataStats &other);
        uint64_t m_sizeOfSavedData;
        uint64_t m_timeSpent;
    };
    TMap<ZString, ZSaveDataValueStorageStatsStorage::SSingletonSaveDataStats>
        m_gameSingletonSaveStatsMap;
    TMap<ZString,
         TMap<unsigned __int64, ZSaveDataValueStorageStatsStorage::SSingletonSaveDataStats>>
        m_sceneContextSingletonSaveStatsMap;
    TMap<ZString, ZSaveDataValueStorageStatsStorage::SEntityTypeSaveDataStats>
        m_TypeSaveDataStatsMap;
    TMap<ZString, ZSaveDataValueStorageStatsStorage::SEntityTypeSaveDataStats>
        m_RenderMaterialTypeSaveDataStatsMap;
    TMap<STypeID *, ZSaveDataValueStorageStatsStorage::SDataTypeSaveDataStats>
        m_DataTypeSaveStatsMap;
    struct SSceneContextSaveDataStats {
        SSceneContextSaveDataStats();
        uint64_t m_sceneSaveTime;
        uint64_t m_sceneEntityCount;
        TMap<ZString, TPair<unsigned __int64, unsigned __int64>> m_typeSaveCounts;
        void
        AggregateStats(const ZSaveDataValueStorageStatsStorage::SSceneContextSaveDataStats &other);
    };
    TMap<unsigned __int64, ZSaveDataValueStorageStatsStorage::SSceneContextSaveDataStats>
        m_perSceneEntityTypeStats;
    uint64_t m_totalTimeForSave;
    void DumpStats(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth);
    bool ShouldTrackStats();
    void UpdateDataTypeStats(STypeID *valueTypeId, uint64_t &actualSizeStored, bool bValueReused);
    void Clear();
    void AggregateStats(const ZSaveDataValueStorageStatsStorage &otherStats);
    bool m_shouldTrackStats;
};

class ZBaseFragmentedSaveDataStorage {
    public:
    static void RegisterType();
    ZBaseFragmentedSaveDataStorage(const ZBaseFragmentedSaveDataStorage &other);
    ZBaseFragmentedSaveDataStorage(ZBaseFragmentedSaveDataStorage *other);
    ZBaseFragmentedSaveDataStorage(bool reuseExistingValues, const ZString &storageName);
    ZBaseFragmentedSaveDataStorage(bool reuseExistingValues);
    ZBaseFragmentedSaveDataStorage();
    ZBaseFragmentedSaveDataStorage &operator=(const ZBaseFragmentedSaveDataStorage &other);
    virtual ~ZBaseFragmentedSaveDataStorage();
    bool CanBeQueriedBeforePreConversionToMemoryValues();
    bool CanBeQueriedBeforeConversionToMemoryValues();
    uint32_t GetTotalSizeOfStorage();
    virtual uint32_t GetSizeOfReuseCache();
    const ZString &GetStorageName();
    static void CheckValidityOfEntityReference(const ZEntityRef &entityRef);
    virtual void FinalizeSaveDataStorage(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                         ZSaveDataValueStorage &saveDataValueStorage,
                                         ZSaveGameJobManager *pSaveGameJobManager);
    void PrePrepareSaveDataStorageToBeRestored(ZSaveDataValueStorage &saveDataValueStorage,
                                               SResourcePtrsToLoad &resPtrsToLoad,
                                               ZSaveGameJobManager *pSaveGameJobManager);
    void
    PrepareSaveDataStorageToBeRestored(ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
                                       ZSaveDataValueStorage &saveDataValueStorage,
                                       SResourcePtrsToLoad &resPtrsToLoad,
                                       ZSaveGameJobManager *pSaveGameJobManager);
    virtual STypeID *GetStoredType();
    virtual bool TryGetStoredVariantData(uint32_t index, ZVariantRef &outValue,
                                         STypeID *propertyType);
    virtual uint32_t StoreRawData(void *pData, STypeID *typeId);
    virtual uint32_t StoreVariantData(ZVariant *saveData);
    virtual void Clear();
    virtual void Trim();
    uint32_t GetNumberOfStoredValues();
    bool IsEmpty();
    virtual void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                  uint64_t &dataSizeAccumulator);
    virtual uint64_t GetPerElementSaveDataStorageOverhead();
    virtual void PreRemapStoredValueToMemoryValueInPartOfFragment(
        uint32_t fragmentIndex, uint32_t startValueIndexToRemap,
        uint32_t maxNumberOfValueIndicesToRemap, ZSaveDataValueStorage &saveDataValueStorage,
        SResourcePtrsToLoad &resPtrsToLoad);
    virtual void RemapStoredValueToMemoryValueInPartOfFragment(
        uint32_t fragmentIndex, uint32_t startValueIndexToRemap,
        uint32_t maxNumberOfValueIndicesToRemap,
        ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void RemapMemoryValueToStoredValueInPartOfFragment(
        uint32_t fragmentIndex, uint32_t startValueIndexToRemap,
        uint32_t maxNumberOfValueIndicesToRemap,
        const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
        ZSaveDataValueStorage &saveDataValueStorage);
    bool ReuseValues();
    virtual bool IsValueReuseSupported();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedValueIndex(const ZVariant &saveData);
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual bool NeedsToPreRemapStoredValues();
    virtual void PreRemapStoredValueToInMemoryValue(void *storedValue,
                                                    ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad);
    virtual bool NeedsToRemapStoredValues();
    virtual void
    RemapInMemoryValueToStoredValue(void *storedValue,
                                    const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage);
    virtual void RemapStoredValueToInMemoryValue(
        void *storedValue, ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual uint32_t GetMaxValuesToRemapDuringRemapJobs();
    virtual void
    RemapMemoryValuesToStoredValues(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage,
                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual void PreRemapStoredValuesToMemoryValues(ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad,
                                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual void
    RemapStoredValuesToMemoryValues(ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
                                    ZSaveDataValueStorage &saveDataValueStorage,
                                    SResourcePtrsToLoad &resPtrsToLoad,
                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual void InternalClear();
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    virtual void AssignStoredValueFromRawData(void *storedValue, void *pData, STypeID *typeId);
    virtual STypeID *GetTypeOfStoredValue(const void *storedValue);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
    void DumpSimpleStoredValueSaveGameInfo(STypeID *typeId, const void *storedValue,
                                           ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                           uint64_t &dataSizeAccumulator);
    static uint64_t GetFragmentsToPreReservePerStorage();
    uint32_t m_totalStorageSize;
    bool m_reuseExistingValues;
    uint32_t m_timesValuesReused;
    ZString m_storageName;
};

template <typename T>
class TBaseFragmentedSaveDataStorage : public ZBaseFragmentedSaveDataStorage {
    public:
    static void RegisterBaseFragmentedSaveDataStorageType();
    TBaseFragmentedSaveDataStorage<T>(const TBaseFragmentedSaveDataStorage<T> &other);
    TBaseFragmentedSaveDataStorage<T>(TBaseFragmentedSaveDataStorage<T> *other);
    TBaseFragmentedSaveDataStorage<T>(bool reuseExistingValues, const ZString &storageName);
    TBaseFragmentedSaveDataStorage<T>(bool);
    TBaseFragmentedSaveDataStorage<T>();
    TBaseFragmentedSaveDataStorage<T> &operator=(const TBaseFragmentedSaveDataStorage<T> &other);
    virtual ~TBaseFragmentedSaveDataStorage<T>();
    virtual bool TryGetStoredVariantData(uint32_t index, ZVariantRef &outValue,
                                         STypeID *propertyType);
    virtual uint32_t StoreRawData(void *pData, STypeID *typeId);
    virtual uint32_t StoreVariantData(ZVariant *saveData);
    virtual STypeID *GetStoredType();
    virtual void Clear();
    virtual void Trim();
    virtual void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                  uint64_t &dataSizeAccumulator);
    virtual uint64_t GetPerElementSaveDataStorageOverhead();
    virtual void PreRemapStoredValueToMemoryValueInPartOfFragment(
        uint32_t fragmentIndex, uint32_t startValueIndexToRemap,
        uint32_t maxNumberOfValueIndicesToRemap, ZSaveDataValueStorage &saveDataValueStorage,
        SResourcePtrsToLoad &resPtrsToLoad);
    virtual void RemapStoredValueToMemoryValueInPartOfFragment(
        uint32_t fragmentIndex, uint32_t startValueIndexToRemap,
        uint32_t maxNumberOfValueIndicesToRemap,
        ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void RemapMemoryValueToStoredValueInPartOfFragment(
        uint32_t fragmentIndex, uint32_t startValueIndexToRemap,
        uint32_t maxNumberOfValueIndicesToRemap,
        const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
        ZSaveDataValueStorage &saveDataValueStorage);
    virtual void PreRemapStoredValuesToMemoryValues(ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad,
                                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual void
    RemapMemoryValuesToStoredValues(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage,
                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual void
    RemapStoredValuesToMemoryValues(ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
                                    ZSaveDataValueStorage &saveDataValueStorage,
                                    SResourcePtrsToLoad &resPtrsToLoad,
                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual const TArray<TArray<T>> &GetStorageFragments() const;
    virtual TArray<TArray<T>> &GetStorageFragments();
    virtual STypeID *GetTypeOfStoredValue(const void *storedValue);
    const T *ConvertToActualType(const void *);
    T *ConvertToActualType(void *storedValue);
};

template <typename T>
class TBaseFragmentedPODSaveDataStorage : public TBaseFragmentedSaveDataStorage<T> {
    public:
    static void RegisterBaseFragmentedPODDataStorageType();
    TBaseFragmentedPODSaveDataStorage<T>(const TBaseFragmentedPODSaveDataStorage<T> &other);
    TBaseFragmentedPODSaveDataStorage<T>(TBaseFragmentedPODSaveDataStorage<T> *other);
    TBaseFragmentedPODSaveDataStorage<T>(bool reuseExistingValues, const ZString &storageName);
    TBaseFragmentedPODSaveDataStorage<T>(bool reuseExistingValues);
    TBaseFragmentedPODSaveDataStorage<T>();
    virtual ~TBaseFragmentedPODSaveDataStorage<T>();
    TBaseFragmentedPODSaveDataStorage<T> &
    operator=(const TBaseFragmentedPODSaveDataStorage<T> &other);
    virtual uint64_t GetPerElementSaveDataStorageOverhead();
    virtual bool IsValueReuseSupported();
    virtual uint32_t FindOrCreateReusedValueIndex(const ZVariant &saveData);
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    virtual void AssignStoredValueFromRawData(void *storedValue, void *pData, STypeID *typeId);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
};

template <typename T>
int32_t HashFunctionUsingObjectMemory(const T &key);

class ZColorRGBFragmentedSaveDataStorage : public TBaseFragmentedPODSaveDataStorage<SColorRGB> {
    public:
    static void RegisterType();
    ZColorRGBFragmentedSaveDataStorage(const ZColorRGBFragmentedSaveDataStorage &other);
    ZColorRGBFragmentedSaveDataStorage(ZColorRGBFragmentedSaveDataStorage *other);
    ZColorRGBFragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                       const ZString &storageName);
    ZColorRGBFragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZColorRGBFragmentedSaveDataStorage();
    ZColorRGBFragmentedSaveDataStorage &operator=(const ZColorRGBFragmentedSaveDataStorage &other);
    virtual ~ZColorRGBFragmentedSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<SColorRGB>> &GetStorageFragments() const;
    virtual TArray<TArray<SColorRGB>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    TArray<TArray<SColorRGB>> m_variantStorageFragments;
    TUnorderedLinearHashMap<SColorRGB, unsigned int, &HashFunctionUsingObjectMemory<SColorRGB>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

class ZFloat4FragmentedSaveDataStorage : public TBaseFragmentedPODSaveDataStorage<float4> {
    public:
    static void RegisterType();
    ZFloat4FragmentedSaveDataStorage(const ZFloat4FragmentedSaveDataStorage &other);
    ZFloat4FragmentedSaveDataStorage(ZFloat4FragmentedSaveDataStorage *other);
    ZFloat4FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                     const ZString &storageName);
    ZFloat4FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZFloat4FragmentedSaveDataStorage();
    ZFloat4FragmentedSaveDataStorage &operator=(const ZFloat4FragmentedSaveDataStorage &other);
    virtual ~ZFloat4FragmentedSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<float4>> &GetStorageFragments() const;
    virtual TArray<TArray<float4>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    TArray<TArray<float4>> m_variantStorageFragments;
    TUnorderedLinearHashMap<float4, unsigned int, &HashFunctionUsingObjectMemory<float4>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

class ZVector2FragmentedSaveDataStorage : public TBaseFragmentedPODSaveDataStorage<SVector2> {
    public:
    static void RegisterType();
    ZVector2FragmentedSaveDataStorage(const ZVector2FragmentedSaveDataStorage &other);
    ZVector2FragmentedSaveDataStorage(ZVector2FragmentedSaveDataStorage *other);
    ZVector2FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                      const ZString &storageName);
    ZVector2FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZVector2FragmentedSaveDataStorage();
    ZVector2FragmentedSaveDataStorage &operator=(const ZVector2FragmentedSaveDataStorage &other);
    virtual ~ZVector2FragmentedSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<SVector2>> &GetStorageFragments() const;
    virtual TArray<TArray<SVector2>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    TArray<TArray<SVector2>> m_variantStorageFragments;
    TUnorderedLinearHashMap<SVector2, unsigned int, &HashFunctionUsingObjectMemory<SVector2>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

class ZVector3FragmentedSaveDataStorage : public TBaseFragmentedPODSaveDataStorage<SVector3> {
    public:
    static void RegisterType();
    ZVector3FragmentedSaveDataStorage(const ZVector3FragmentedSaveDataStorage &other);
    ZVector3FragmentedSaveDataStorage(ZVector3FragmentedSaveDataStorage *other);
    ZVector3FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                      const ZString &storageName);
    ZVector3FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZVector3FragmentedSaveDataStorage();
    ZVector3FragmentedSaveDataStorage &operator=(const ZVector3FragmentedSaveDataStorage &other);
    virtual ~ZVector3FragmentedSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<SVector3>> &GetStorageFragments() const;
    virtual TArray<TArray<SVector3>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    TArray<TArray<SVector3>> m_variantStorageFragments;
    TUnorderedLinearHashMap<SVector3, unsigned int, &HashFunctionUsingObjectMemory<SVector3>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

class ZVector4FragmentedSaveDataStorage : public TBaseFragmentedPODSaveDataStorage<SVector4> {
    public:
    static void RegisterType();
    ZVector4FragmentedSaveDataStorage(const ZVector4FragmentedSaveDataStorage &other);
    ZVector4FragmentedSaveDataStorage(ZVector4FragmentedSaveDataStorage *other);
    ZVector4FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                      const ZString &storageName);
    ZVector4FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZVector4FragmentedSaveDataStorage();
    ZVector4FragmentedSaveDataStorage &operator=(const ZVector4FragmentedSaveDataStorage &other);
    virtual ~ZVector4FragmentedSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<SVector4>> &GetStorageFragments() const;
    virtual TArray<TArray<SVector4>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    TArray<TArray<SVector4>> m_variantStorageFragments;
    TUnorderedLinearHashMap<SVector4, unsigned int, &HashFunctionUsingObjectMemory<SVector4>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

class ZMatrixFragmentedSaveDataStorage : public TBaseFragmentedPODSaveDataStorage<SMatrix> {
    public:
    static void RegisterType();
    ZMatrixFragmentedSaveDataStorage(const ZMatrixFragmentedSaveDataStorage &other);
    ZMatrixFragmentedSaveDataStorage(ZMatrixFragmentedSaveDataStorage *other);
    ZMatrixFragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                     const ZString &storageName);
    ZMatrixFragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZMatrixFragmentedSaveDataStorage();
    ZMatrixFragmentedSaveDataStorage &operator=(const ZMatrixFragmentedSaveDataStorage &other);
    virtual ~ZMatrixFragmentedSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<SMatrix>> &GetStorageFragments() const;
    virtual TArray<TArray<SMatrix>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    TArray<TArray<SMatrix>> m_variantStorageFragments;
    TUnorderedLinearHashMap<SMatrix, unsigned int, &HashFunctionUsingObjectMemory<SMatrix>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

class ZMatrix43FragmentedSaveDataStorage : public TBaseFragmentedPODSaveDataStorage<SMatrix43> {
    public:
    static void RegisterType();
    ZMatrix43FragmentedSaveDataStorage(const ZMatrix43FragmentedSaveDataStorage &other);
    ZMatrix43FragmentedSaveDataStorage(ZMatrix43FragmentedSaveDataStorage *other);
    ZMatrix43FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                       const ZString &storageName);
    ZMatrix43FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZMatrix43FragmentedSaveDataStorage();
    ZMatrix43FragmentedSaveDataStorage &operator=(const ZMatrix43FragmentedSaveDataStorage &other);
    virtual ~ZMatrix43FragmentedSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<SMatrix43>> &GetStorageFragments() const;
    virtual TArray<TArray<SMatrix43>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    TArray<TArray<SMatrix43>> m_variantStorageFragments;
    TUnorderedLinearHashMap<SMatrix43, unsigned int, &HashFunctionUsingObjectMemory<SMatrix43>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

class ZUInt64FragmentedSaveDataStorage
    : public TBaseFragmentedPODSaveDataStorage<unsigned __int64> {
    public:
    static void RegisterType();
    ZUInt64FragmentedSaveDataStorage(const ZUInt64FragmentedSaveDataStorage &other);
    ZUInt64FragmentedSaveDataStorage(ZUInt64FragmentedSaveDataStorage *other);
    ZUInt64FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                     const ZString &storageName);
    ZUInt64FragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZUInt64FragmentedSaveDataStorage();
    ZUInt64FragmentedSaveDataStorage &operator=(const ZUInt64FragmentedSaveDataStorage &other);
    virtual ~ZUInt64FragmentedSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<unsigned __int64>> &GetStorageFragments() const;
    virtual TArray<TArray<unsigned __int64>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    TArray<TArray<unsigned __int64>> m_variantStorageFragments;
    TUnorderedLinearHashMap<unsigned __int64, unsigned int,
                            &HashFunctionUsingObjectMemory<unsigned __int64>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

template <typename T>
int32_t HashFunctionUsingGetHashCode(const T &key);

class ZRuntimeResourceIDSaveDataStorage
    : public TBaseFragmentedPODSaveDataStorage<ZRuntimeResourceID> {
    public:
    static void RegisterType();
    ZRuntimeResourceIDSaveDataStorage(const ZRuntimeResourceIDSaveDataStorage &other);
    ZRuntimeResourceIDSaveDataStorage(ZRuntimeResourceIDSaveDataStorage *other);
    ZRuntimeResourceIDSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                      const ZString &storageName);
    ZRuntimeResourceIDSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZRuntimeResourceIDSaveDataStorage();
    ZRuntimeResourceIDSaveDataStorage &operator=(const ZRuntimeResourceIDSaveDataStorage &other);
    virtual ~ZRuntimeResourceIDSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<ZRuntimeResourceID>> &GetStorageFragments() const;
    virtual TArray<TArray<ZRuntimeResourceID>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    TArray<TArray<ZRuntimeResourceID>> m_variantStorageFragments;
    TUnorderedLinearHashMap<ZRuntimeResourceID, unsigned int,
                            &HashFunctionUsingGetHashCode<ZRuntimeResourceID>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

class ZStringFragmentedSaveDataStorage : public TBaseFragmentedPODSaveDataStorage<ZString> {
    public:
    static void RegisterType();
    ZStringFragmentedSaveDataStorage(const ZStringFragmentedSaveDataStorage &other);
    ZStringFragmentedSaveDataStorage(ZStringFragmentedSaveDataStorage *other);
    ZStringFragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                     const ZString &storageName);
    ZStringFragmentedSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZStringFragmentedSaveDataStorage();
    ZStringFragmentedSaveDataStorage &operator=(const ZStringFragmentedSaveDataStorage &other);
    virtual ~ZStringFragmentedSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<ZString>> &GetStorageFragments() const;
    virtual TArray<TArray<ZString>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    TArray<TArray<ZString>> m_variantStorageFragments;
    TUnorderedLinearHashMap<ZString, unsigned int, &HashFunctionUsingGetHashCode<ZString>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

template <typename T>
class TBaseFragmentedVariantDataStorage : public TBaseFragmentedSaveDataStorage<T> {
    public:
    static void RegisterBaseFragmentedVariantDataStorageType();
    TBaseFragmentedVariantDataStorage<T>(bool, const ZString &, uint32_t);
    TBaseFragmentedVariantDataStorage<T>(bool, uint32_t);
    TBaseFragmentedVariantDataStorage<T>(const TBaseFragmentedVariantDataStorage<T> &other);
    TBaseFragmentedVariantDataStorage<T>(TBaseFragmentedVariantDataStorage<T> *other);
    TBaseFragmentedVariantDataStorage<T>(const ZString &storageName,
                                         uint32_t maxValuesToRemapDuringRemapJobs);
    TBaseFragmentedVariantDataStorage<T>();
    TBaseFragmentedVariantDataStorage<T> &
    operator=(const TBaseFragmentedVariantDataStorage<T> &other);
    virtual ~TBaseFragmentedVariantDataStorage<T>();
    virtual bool NeedsToRemapStoredValues();
    virtual void
    RemapInMemoryValueToStoredValue(void *storedValue,
                                    const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage);
    virtual void RemapStoredValueToInMemoryValue(
        void *storedValue, ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void PreRemapStoredValueToInMemoryValue(void *storedValue,
                                                    ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad);
    virtual uint32_t GetMaxValuesToRemapDuringRemapJobs();
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    virtual STypeID *GetTypeOfStoredValue(const void *storedValue);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
    virtual uint64_t GetPerElementSaveDataStorageOverhead();
    uint32_t m_maxValuesToRemapDuringRemapJobs;
};

class ZSimpleSavedVariantData {
    public:
    static void RegisterType();
    ZSimpleSavedVariantData(ZSimpleSavedVariantData *rhs);
    ZSimpleSavedVariantData(const ZSimpleSavedVariantData &rhs);
    ZSimpleSavedVariantData(ZVariant *value);
    ZSimpleSavedVariantData(const ZVariant &value);
    ZSimpleSavedVariantData();
    virtual ~ZSimpleSavedVariantData();
    ZSimpleSavedVariantData &operator=(ZSimpleSavedVariantData *);
    ZSimpleSavedVariantData &operator=(const ZSimpleSavedVariantData &rhs);
    virtual void
    RemapStoredValueToInMemoryType(ZLocalIDPathToEntityRefStorage *pEntityPathToEntityRefMapping,
                                   ZSaveDataValueStorage &saveDataValueStorage,
                                   SResourcePtrsToLoad &resPtrsToLoad);
    virtual bool TryReferenceValueOfDesiredType(ZVariantRef &propertyValue,
                                                STypeID *const &desiredType);
    virtual void GatherResourceReferences(SResourcePtrsToLoad &resPtrsToLoad);
    virtual bool
    RemapMemoryValueToStoredValue(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                  ZSaveDataValueStorage &saveDataValueStorage);
    virtual bool IsInMemoryValueValid();
    bool IsEmpty();
    void Clear();
    ZVariant m_value;
    virtual void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                  uint64_t &dataSizeAccumulator);
    virtual bool CheckValidityOfStoredReferences();
};

class ZSimpleFragmentedVariantDataStorage
    : public TBaseFragmentedVariantDataStorage<ZSimpleSavedVariantData> {
    public:
    static void RegisterType();
    ZSimpleFragmentedVariantDataStorage(const ZSimpleFragmentedVariantDataStorage &other);
    ZSimpleFragmentedVariantDataStorage(ZSimpleFragmentedVariantDataStorage *other);
    ZSimpleFragmentedVariantDataStorage(const ZString &storageName,
                                        uint32_t maxValuesToRemapDuringRemapJobs);
    ZSimpleFragmentedVariantDataStorage();
    ZSimpleFragmentedVariantDataStorage &
    operator=(const ZSimpleFragmentedVariantDataStorage &other);
    virtual ~ZSimpleFragmentedVariantDataStorage();
    virtual bool NeedsToRemapStoredValues();
    virtual const TArray<TArray<ZSimpleSavedVariantData>> &GetStorageFragments() const;
    virtual TArray<TArray<ZSimpleSavedVariantData>> &GetStorageFragments();
    TArray<TArray<ZSimpleSavedVariantData>> m_variantStorageFragments;
};

class ZResourcePtrSaveDataStorage : public TBaseFragmentedPODSaveDataStorage<ZRuntimeResourceID> {
    public:
    static void RegisterType();
    ZResourcePtrSaveDataStorage(const ZResourcePtrSaveDataStorage &other);
    ZResourcePtrSaveDataStorage(ZResourcePtrSaveDataStorage *other);
    ZResourcePtrSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                const ZString &storageName);
    ZResourcePtrSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues);
    ZResourcePtrSaveDataStorage();
    ZResourcePtrSaveDataStorage &operator=(const ZResourcePtrSaveDataStorage &other);
    virtual ~ZResourcePtrSaveDataStorage();
    virtual void Trim();
    virtual void InternalClear();
    virtual const TArray<TArray<ZRuntimeResourceID>> &GetStorageFragments() const;
    virtual TArray<TArray<ZRuntimeResourceID>> &GetStorageFragments();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual uint32_t GetSizeOfReuseCache();
    virtual bool NeedsToPreRemapStoredValues();
    virtual void
    RemapInMemoryValueToStoredValue(void *storedValue,
                                    const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage);
    virtual void PreRemapStoredValueToInMemoryValue(void *storedValue,
                                                    ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad);
    virtual uint32_t FindOrCreateReusedValueIndex(const ZVariant &saveData);
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    TArray<TArray<ZRuntimeResourceID>> m_variantStorageFragments;
    TUnorderedLinearHashMap<ZRuntimeResourceID, unsigned int,
                            &HashFunctionUsingGetHashCode<ZRuntimeResourceID>, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

int32_t HashFunctionForZEntityRef(const ZEntityRef &key);

class ZEntityResourceEntityIDPath {
    public:
    static void RegisterType();
    ZEntityResourceEntityIDPath(const SEntityResourceIdentifier &resourceIdentifier,
                                const ZSubEntityPath &subEntityPath);
    ZEntityResourceEntityIDPath(const ZEntityResourceEntityIDPath &other);
    ZEntityResourceEntityIDPath();
    ~ZEntityResourceEntityIDPath();
    ZEntityResourceEntityIDPath &operator=(const ZEntityResourceEntityIDPath &other);
    bool operator<(const ZEntityResourceEntityIDPath &other);
    bool IsEmpty();
    bool operator==(const ZEntityResourceEntityIDPath &other);
    ZString ToString();
    ZSubEntityPath m_subEntityPath;
    SEntityResourceIdentifier m_resourceIdentifier;
};

class ZEntityRefSaveDataStorage : public ZBaseFragmentedSaveDataStorage {
    public:
    static void RegisterType();
    ZEntityRefSaveDataStorage(const ZEntityRefSaveDataStorage &other);
    ZEntityRefSaveDataStorage(ZEntityRefSaveDataStorage *other);
    ZEntityRefSaveDataStorage(bool bLargeReserverForReuseMap, const ZString &storageName);
    ZEntityRefSaveDataStorage(bool bLargeReserverForReuseMap);
    ZEntityRefSaveDataStorage();
    ZEntityRefSaveDataStorage &operator=(const ZEntityRefSaveDataStorage &other);
    virtual ~ZEntityRefSaveDataStorage();
    virtual uint32_t StoreRawData(void *pData, STypeID *typeId);
    virtual uint32_t StoreVariantData(ZVariant *saveData);
    virtual void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                  uint64_t &dataSizeAccumulator);
    virtual uint64_t GetPerElementSaveDataStorageOverhead();
    virtual uint32_t GetSizeOfReuseCache();
    virtual STypeID *GetStoredType();
    virtual STypeID *GetTypeOfStoredValue(const void *storedValue);
    virtual bool TryGetStoredVariantData(uint32_t index, ZVariantRef &outValue,
                                         STypeID *propertyType);
    virtual void Clear();
    virtual void Trim();
    virtual void RemapStoredValueToMemoryValueInPartOfFragment(
        uint32_t fragmentIndex, uint32_t startValueIndexToRemap,
        uint32_t maxNumberOfValueIndicesToRemap,
        ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void RemapMemoryValueToStoredValueInPartOfFragment(
        uint32_t fragmentIndex, uint32_t startValueIndexToRemap,
        uint32_t maxNumberOfValueIndicesToRemap,
        const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
        ZSaveDataValueStorage &saveDataValueStorage);
    virtual bool IsValueReuseSupported();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedValueIndex(const ZVariant &saveData);
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual void AssignStoredValueFromRawData(void *storedValue, void *pData, STypeID *typeId);
    virtual void PreRemapStoredValuesToMemoryValues(ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad,
                                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual void
    RemapMemoryValuesToStoredValues(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage,
                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual void
    RemapStoredValuesToMemoryValues(ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
                                    ZSaveDataValueStorage &saveDataValueStorage,
                                    SResourcePtrsToLoad &resPtrsToLoad,
                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual bool NeedsToRemapStoredValues();
    virtual bool NeedsToPreRemapStoredValues();
    virtual uint32_t GetMaxValuesToRemapDuringRemapJobs();
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
    TUnorderedLinearHashMap<ZEntityRef, unsigned int, &HashFunctionForZEntityRef, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
    TArray<TArray<TPair<unsigned int, ZEntityIDPath>>> m_entityIDStorageFragments;
    TArray<TArray<TPair<unsigned __int64, ZEntityResourceEntityIDPath>>>
        m_entityResourceEntityIDStorageFragments;
    TArray<TArray<ZEntityRef>> m_remappedValues;
    uint32_t m_maxValuesToRemapDuringRemapJobs;
    static const ZEntityRef InvalidEntityRef;
};

class ZRemappedSavedVariantData : public ZSimpleSavedVariantData {
    public:
    static void RegisterType();
    ZRemappedSavedVariantData(ZRemappedSavedVariantData *rhs);
    ZRemappedSavedVariantData(const ZRemappedSavedVariantData &rhs);
    ZRemappedSavedVariantData(ZVariant *value);
    ZRemappedSavedVariantData(const ZVariant &value);
    ZRemappedSavedVariantData();
    virtual ~ZRemappedSavedVariantData();
    ZRemappedSavedVariantData &operator=(ZRemappedSavedVariantData *);
    ZRemappedSavedVariantData &operator=(const ZRemappedSavedVariantData &rhs);
    virtual void
    RemapStoredValueToInMemoryType(ZLocalIDPathToEntityRefStorage *pEntityPathToEntityRefMapping,
                                   ZSaveDataValueStorage &saveDataValueStorage,
                                   SResourcePtrsToLoad &resPtrsToLoad);
    virtual bool TryReferenceValueOfDesiredType(ZVariantRef &propertyValue,
                                                STypeID *const &desiredType);
    virtual bool
    RemapMemoryValueToStoredValue(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                  ZSaveDataValueStorage &saveDataValueStorage);
    virtual void GatherResourceReferences(SResourcePtrsToLoad &resPtrsToLoad);
    virtual bool IsInMemoryValueValid();
    virtual bool CheckValidityOfStoredReferences();
    static bool TryExtractEntityReferencePropertyFromEntityIDPath(
        ZEntityRef &outEntityRef, const ZEntityIDPath &refValue,
        ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping);
    static bool TryExtractEntityResourceEntityReferencePropertyFromEntityResourceEntityIDPath(
        ZEntityRef &outEntityRef, const ZEntityResourceEntityIDPath &refValue,
        ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping);
    static bool MakeBaseSubEntityPathFromBaseEntityReference(
        uint32_t &allocationId, ZSubEntityPath &outPath,
        SEntityResourceIdentifier &singletonResource, ZEntityRef refValue,
        const ZLocalEntityRefToIDPathStorage &entityIDPathStorage);
    static void
    GatherResourceReferencesFromEntityResourceIDPath(const ZEntityResourceEntityIDPath &refValue,
                                                     SResourcePtrsToLoad &resPtrsToLoad);
    static bool CheckValidityOfStoredEntityReference(const ZEntityRef &refValue);
};

class ZRemappedFragmentedVariantDataStorage
    : public TBaseFragmentedVariantDataStorage<ZRemappedSavedVariantData> {
    public:
    static void RegisterType();
    ZRemappedFragmentedVariantDataStorage(const ZRemappedFragmentedVariantDataStorage &other);
    ZRemappedFragmentedVariantDataStorage(ZRemappedFragmentedVariantDataStorage *other);
    ZRemappedFragmentedVariantDataStorage(bool reuseExistingValues, const ZString &storageName,
                                          uint32_t maxValuesToRemapDuringRemapJobs);
    ZRemappedFragmentedVariantDataStorage(bool reuseExistingValues,
                                          uint32_t maxValuesToRemapDuringRemapJobs);
    ZRemappedFragmentedVariantDataStorage(const ZString &storageName,
                                          uint32_t maxValuesToRemapDuringRemapJobs);
    ZRemappedFragmentedVariantDataStorage();
    ZRemappedFragmentedVariantDataStorage &
    operator=(const ZRemappedFragmentedVariantDataStorage &other);
    virtual ~ZRemappedFragmentedVariantDataStorage();
    virtual const TArray<TArray<ZRemappedSavedVariantData>> &GetStorageFragments() const;
    virtual TArray<TArray<ZRemappedSavedVariantData>> &GetStorageFragments();
    virtual uint32_t FindOrCreateReusedValueIndex(const ZVariant &saveData);
    TArray<TArray<ZRemappedSavedVariantData>> m_variantStorageFragments;
};

int32_t HashFunctionForZAccessorRef(const ZAccessorRef &key);

class ZAccessorRefSaveDataStorage : public ZRemappedFragmentedVariantDataStorage {
    public:
    static void RegisterType();
    ZAccessorRefSaveDataStorage(const ZAccessorRefSaveDataStorage &other);
    ZAccessorRefSaveDataStorage(ZAccessorRefSaveDataStorage *other);
    ZAccessorRefSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                const ZString &storageName,
                                uint32_t maxValuesToRemapDuringRemapJobs);
    ZAccessorRefSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                uint32_t maxValuesToRemapDuringRemapJobs);
    ZAccessorRefSaveDataStorage();
    ZAccessorRefSaveDataStorage &operator=(const ZAccessorRefSaveDataStorage &other);
    virtual ~ZAccessorRefSaveDataStorage();
    virtual uint32_t GetSizeOfReuseCache();
    virtual void Trim();
    virtual bool NeedsToPreRemapStoredValues();
    virtual void InternalClear();
    virtual bool IsValueReuseSupported();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual void AssignStoredValueFromRawData(void *storedValue, void *pData, STypeID *typeId);
    ZAccessorRef ExtractAccessorRefFromData(void *pData, STypeID *typeId);
    TUnorderedLinearHashMap<ZAccessorRef, unsigned int, &HashFunctionForZAccessorRef, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

int32_t HashFunctionForZArrayElementRefRef(const ZArrayElementRef &key);

class ZArrayElementRefSaveDataStorage : public ZRemappedFragmentedVariantDataStorage {
    public:
    static void RegisterType();
    ZArrayElementRefSaveDataStorage(const ZArrayElementRefSaveDataStorage &other);
    ZArrayElementRefSaveDataStorage(ZArrayElementRefSaveDataStorage *other);
    ZArrayElementRefSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                    const ZString &storageName,
                                    uint32_t maxValuesToRemapDuringRemapJobs);
    ZArrayElementRefSaveDataStorage(bool bLargeReserverForReuseMap, bool reuseExistingValues,
                                    uint32_t maxValuesToRemapDuringRemapJobs);
    ZArrayElementRefSaveDataStorage();
    ZArrayElementRefSaveDataStorage &operator=(const ZArrayElementRefSaveDataStorage &other);
    virtual ~ZArrayElementRefSaveDataStorage();
    virtual uint32_t GetSizeOfReuseCache();
    virtual void Trim();
    virtual bool NeedsToPreRemapStoredValues();
    virtual void InternalClear();
    virtual bool IsValueReuseSupported();
    virtual void PutReusedValuesIntoStorage();
    virtual uint32_t FindOrCreateReusedRawValueIndex(void *pData, STypeID *typeId);
    virtual void AssignStoredValueFromRawData(void *storedValue, void *pData, STypeID *typeId);
    ZArrayElementRef ExtractArrayElementRefFromData(void *pData, STypeID *typeId);
    TUnorderedLinearHashMap<ZArrayElementRef, unsigned int, &HashFunctionForZArrayElementRefRef, 31,
                            DefaultHashAllocator>
        m_valueToStoredIndex;
};

class ZPathedSavedVariantData {
    public:
    static void RegisterType();
    ZPathedSavedVariantData(ZPathedSavedVariantData *rhs);
    ZPathedSavedVariantData(const ZPathedSavedVariantData &rhs);
    ZPathedSavedVariantData(const ZSubEntityPath &propertyPath, uint32_t valueIndex);
    ZPathedSavedVariantData(const ZSubEntityPath &propertyPath);
    ZPathedSavedVariantData();
    ZPathedSavedVariantData &operator=(ZPathedSavedVariantData *);
    ZPathedSavedVariantData &operator=(const ZPathedSavedVariantData &rhs);
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
    ZSubEntityPath m_propertyPath;
    uint32_t m_valueIndex;
};

class ZEntityIDPathAccumulator {
    public:
    ZEntityIDPathAccumulator();
    ZSubEntityPath GenerateIDPath();
    void PushNewId(uint32_t newId);
    void PopLastId();
    uint32_t m_aSubEntityIds[32];
    uint32_t m_nSize;
};

class ZEntityIDPathBuilder {
    public:
    ZEntityIDPathBuilder(ZEntityIDPathAccumulator &sourceAccumulator);
    ZEntityIDPathBuilder(ZEntityIDPathAccumulator &sourceAccumulator, uint32_t entityId);
    ZEntityIDPathBuilder(ZEntityIDPathBuilder &parentBuilder, uint32_t entityId);
    ~ZEntityIDPathBuilder();
    ZEntityIDPathAccumulator &GetSourceAccumulator();
    ZSubEntityPath GenerateIDPath();
    ZEntityIDPathAccumulator &m_sourceAccumulator;
    bool m_idPushed;
};

class ZComplexSavedVariantData : public ZRemappedSavedVariantData {
    public:
    static void RegisterType();
    ZComplexSavedVariantData(ZComplexSavedVariantData *rhs);
    ZComplexSavedVariantData(const ZComplexSavedVariantData &rhs);
    ZComplexSavedVariantData(ZVariant *value);
    ZComplexSavedVariantData();
    virtual ~ZComplexSavedVariantData();
    ZComplexSavedVariantData &operator=(ZComplexSavedVariantData *);
    ZComplexSavedVariantData &operator=(const ZComplexSavedVariantData &rhs);
    virtual void
    RemapStoredValueToInMemoryType(ZLocalIDPathToEntityRefStorage *pEntityPathToEntityRefMapping,
                                   ZSaveDataValueStorage &saveDataValueStorage,
                                   SResourcePtrsToLoad &resPtrsToLoad);
    virtual bool TryReferenceValueOfDesiredType(ZVariantRef &propertyValue,
                                                STypeID *const &desiredType);
    virtual bool
    RemapMemoryValueToStoredValue(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                  ZSaveDataValueStorage &saveDataValueStorage);
    TArray<ZPathedSavedVariantData> m_pathedPropertiesToRemap;
    virtual void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                  uint64_t &dataSizeAccumulator);
    virtual bool CheckValidityOfStoredReferences();
    int32_t m_lastFoundPathedPropertyIndex;
    bool CheckValidityOfStoredReferencesInType(const ZVariantRef &typeValue, const IType *pType);
    bool CheckValidityOfStoredReferencesInClass(const ZVariantRef &classValue,
                                                const IClassType *pClassType);
    ZPathedSavedVariantData *GetPathedPropertyToRemap(const ZSubEntityPath &propertyPath);
    bool RemapReferencePropertiesInType(const ZVariantRef &typeValue, const IType *pType,
                                        ZEntityIDPathBuilder &pathBuilder,
                                        ZSaveDataValueStorage &saveDataValueStorage,
                                        SResourcePtrsToLoad &resPtrsToLoad);
    bool RemapReferencePropertiesInClass(const ZVariantRef &classValue,
                                         const IClassType *pClassType,
                                         ZEntityIDPathBuilder &pathBuilder,
                                         ZSaveDataValueStorage &saveDataValueStorage,
                                         SResourcePtrsToLoad &resPtrsToLoad);
    bool StoreReferencePropertiesInTypeForRemapping(
        const ZVariantRef &typeValue, const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
        ZEntityIDPathBuilder &pathBuilder, ZSaveDataValueStorage &saveDataValueStorage);
    bool StoreReferencePropertiesInClassForRemapping(
        const ZVariantRef &classValue, const IClassType *pClassType,
        const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
        ZEntityIDPathBuilder &pathBuilder, ZSaveDataValueStorage &saveDataValueStorage);
};

class ZPreRemappedComplexFragmentedVariantDataStorage
    : public TBaseFragmentedVariantDataStorage<ZComplexSavedVariantData> {
    public:
    static void RegisterType();
    ZPreRemappedComplexFragmentedVariantDataStorage(
        const ZPreRemappedComplexFragmentedVariantDataStorage &other);
    ZPreRemappedComplexFragmentedVariantDataStorage(
        ZPreRemappedComplexFragmentedVariantDataStorage *other);
    ZPreRemappedComplexFragmentedVariantDataStorage(const ZString &storageName,
                                                    uint32_t maxValuesToRemapDuringRemapJobs);
    ZPreRemappedComplexFragmentedVariantDataStorage();
    ZPreRemappedComplexFragmentedVariantDataStorage &
    operator=(const ZPreRemappedComplexFragmentedVariantDataStorage &other);
    virtual ~ZPreRemappedComplexFragmentedVariantDataStorage();
    virtual bool NeedsToRemapStoredValues();
    virtual bool NeedsToPreRemapStoredValues();
    virtual void RemapStoredValueToInMemoryValue(
        void *storedValue, ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void PreRemapStoredValueToInMemoryValue(void *storedValue,
                                                    ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad);
    virtual const TArray<TArray<ZComplexSavedVariantData>> &GetStorageFragments() const;
    virtual TArray<TArray<ZComplexSavedVariantData>> &GetStorageFragments();
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    TArray<TArray<ZComplexSavedVariantData>> m_variantStorageFragments;
};

class ZComplexFragmentedVariantDataStorage
    : public TBaseFragmentedVariantDataStorage<ZComplexSavedVariantData> {
    public:
    static void RegisterType();
    ZComplexFragmentedVariantDataStorage(const ZComplexFragmentedVariantDataStorage &other);
    ZComplexFragmentedVariantDataStorage(ZComplexFragmentedVariantDataStorage *other);
    ZComplexFragmentedVariantDataStorage(const ZString &storageName,
                                         uint32_t maxValuesToRemapDuringRemapJobs);
    ZComplexFragmentedVariantDataStorage();
    ZComplexFragmentedVariantDataStorage &
    operator=(const ZComplexFragmentedVariantDataStorage &other);
    virtual ~ZComplexFragmentedVariantDataStorage();
    virtual const TArray<TArray<ZComplexSavedVariantData>> &GetStorageFragments() const;
    virtual TArray<TArray<ZComplexSavedVariantData>> &GetStorageFragments();
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    TArray<TArray<ZComplexSavedVariantData>> m_variantStorageFragments;
};

class ZGenericBufferDataStorage : public TBaseFragmentedSaveDataStorage<TArray<char>> {
    public:
    static void RegisterType();
    ZGenericBufferDataStorage(const ZGenericBufferDataStorage &other);
    ZGenericBufferDataStorage(ZGenericBufferDataStorage *other);
    ZGenericBufferDataStorage(const ZString &storageName);
    ZGenericBufferDataStorage();
    ZGenericBufferDataStorage &operator=(const ZGenericBufferDataStorage &other);
    virtual ~ZGenericBufferDataStorage();
    virtual const TArray<TArray<TArray<char>>> &GetStorageFragments() const;
    virtual TArray<TArray<TArray<char>>> &GetStorageFragments();
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    void BaseAssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
    uint32_t m_maxValuesToRemapDuringRemapJobs;
    TArray<TArray<TArray<char>>> m_variantStorageFragments;
};

class ZArrayOfEntityRefSaveDataStorage : public ZBaseFragmentedSaveDataStorage {
    public:
    static void RegisterType();
    ZArrayOfEntityRefSaveDataStorage(const ZArrayOfEntityRefSaveDataStorage &other);
    ZArrayOfEntityRefSaveDataStorage(ZArrayOfEntityRefSaveDataStorage *other);
    ZArrayOfEntityRefSaveDataStorage(const ZString &storageName);
    ZArrayOfEntityRefSaveDataStorage();
    ZArrayOfEntityRefSaveDataStorage &operator=(const ZArrayOfEntityRefSaveDataStorage &other);
    virtual ~ZArrayOfEntityRefSaveDataStorage();
    virtual uint32_t StoreVariantData(ZVariant *saveData);
    virtual void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                  uint64_t &dataSizeAccumulator);
    virtual uint64_t GetPerElementSaveDataStorageOverhead();
    virtual STypeID *GetStoredType();
    virtual STypeID *GetTypeOfStoredValue(const void *storedValue);
    virtual bool TryGetStoredVariantData(uint32_t index, ZVariantRef &outValue,
                                         STypeID *propertyType);
    virtual void Clear();
    virtual void Trim();
    virtual void RemapStoredValueToMemoryValueInPartOfFragment(
        uint32_t fragmentIndex, uint32_t startValueIndexToRemap,
        uint32_t maxNumberOfValueIndicesToRemap,
        ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void RemapMemoryValueToStoredValueInPartOfFragment(
        uint32_t fragmentIndex, uint32_t startValueIndexToRemap,
        uint32_t maxNumberOfValueIndicesToRemap,
        const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
        ZSaveDataValueStorage &saveDataValueStorage);
    virtual bool IsValueReuseSupported();
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    virtual void
    RemapMemoryValuesToStoredValues(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage,
                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual void
    RemapStoredValuesToMemoryValues(ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
                                    ZSaveDataValueStorage &saveDataValueStorage,
                                    SResourcePtrsToLoad &resPtrsToLoad,
                                    ZSaveGameJobManager *pSaveGameJobManager);
    virtual bool NeedsToRemapStoredValues();
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
    TArray<TArray<TArray<unsigned int>>> m_entityRefValueIndexArrayStorageFragments;
    TArray<TArray<ZVariant>> m_unconvertedArrayValues;
    TArray<TArray<TArray<ZEntityRef>>> m_remappedValues;
    static const ZEntityRef InvalidEntityRef;
};

class ZCustomSaveDataStorageDataStorage
    : public TBaseFragmentedSaveDataStorage<ZCustomSaveDataStorage> {
    public:
    static void RegisterType();
    ZCustomSaveDataStorageDataStorage(const ZCustomSaveDataStorageDataStorage &other);
    ZCustomSaveDataStorageDataStorage(ZCustomSaveDataStorageDataStorage *other);
    ZCustomSaveDataStorageDataStorage(const ZString &storageName);
    ZCustomSaveDataStorageDataStorage();
    ZCustomSaveDataStorageDataStorage &operator=(const ZCustomSaveDataStorageDataStorage &other);
    virtual ~ZCustomSaveDataStorageDataStorage();
    virtual const TArray<TArray<ZCustomSaveDataStorage>> &GetStorageFragments() const;
    virtual TArray<TArray<ZCustomSaveDataStorage>> &GetStorageFragments();
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    void BaseAssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
    uint32_t m_maxValuesToRemapDuringRemapJobs;
    virtual bool NeedsToRemapStoredValues();
    virtual bool NeedsToPreRemapStoredValues();
    virtual void
    RemapInMemoryValueToStoredValue(void *storedValue,
                                    const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage);
    virtual void RemapStoredValueToInMemoryValue(
        void *storedValue, ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void PreRemapStoredValueToInMemoryValue(void *storedValue,
                                                    ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad);
    virtual uint32_t GetMaxValuesToRemapDuringRemapJobs();
    TArray<TArray<ZCustomSaveDataStorage>> m_variantStorageFragments;
};

class ZCustomSaveDataStorageWithEntityReferenceStorage : public ZCustomSaveDataStorage {
    public:
    static void RegisterType();
    ZCustomSaveDataStorageWithEntityReferenceStorage(
        ZCustomSaveDataStorageWithEntityReferenceStorage *rhs);
    ZCustomSaveDataStorageWithEntityReferenceStorage(
        const ZCustomSaveDataStorageWithEntityReferenceStorage &rhs);
    ZCustomSaveDataStorageWithEntityReferenceStorage(int32_t dataVersion);
    ZCustomSaveDataStorageWithEntityReferenceStorage();
    ZCustomSaveDataStorageWithEntityReferenceStorage &
    operator=(ZCustomSaveDataStorageWithEntityReferenceStorage *rhs);
    ZCustomSaveDataStorageWithEntityReferenceStorage &
    operator=(const ZCustomSaveDataStorageWithEntityReferenceStorage &rhs);
    ZCustomSaveDataEntityReferenceStorage m_referenceStorage;
};

class ZCustomSaveDataStorageWithResourcePtrStorage : public ZCustomSaveDataStorage {
    public:
    static void RegisterType();
    ZCustomSaveDataStorageWithResourcePtrStorage(ZCustomSaveDataStorageWithResourcePtrStorage *rhs);
    ZCustomSaveDataStorageWithResourcePtrStorage(
        const ZCustomSaveDataStorageWithResourcePtrStorage &rhs);
    ZCustomSaveDataStorageWithResourcePtrStorage(int32_t dataVersion);
    ZCustomSaveDataStorageWithResourcePtrStorage();
    ZCustomSaveDataStorageWithResourcePtrStorage &
    operator=(ZCustomSaveDataStorageWithResourcePtrStorage *rhs);
    ZCustomSaveDataStorageWithResourcePtrStorage &
    operator=(const ZCustomSaveDataStorageWithResourcePtrStorage &rhs);
    ZCustomSaveDataRuntimeResourceIDStorage m_rridStorage;
};

class ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage
    : public TBaseFragmentedSaveDataStorage<ZCustomSaveDataStorageWithEntityReferenceStorage> {
    public:
    static void RegisterType();
    ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage(
        const ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage &other);
    ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage(
        ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage *other);
    ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage(const ZString &storageName);
    ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage();
    ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage &
    operator=(const ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage &other);
    virtual ~ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage();
    virtual const TArray<TArray<ZCustomSaveDataStorageWithEntityReferenceStorage>> &
    GetStorageFragments() const;
    virtual TArray<TArray<ZCustomSaveDataStorageWithEntityReferenceStorage>> &GetStorageFragments();
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    void BaseAssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
    uint32_t m_maxValuesToRemapDuringRemapJobs;
    virtual bool NeedsToRemapStoredValues();
    virtual bool NeedsToPreRemapStoredValues();
    virtual void
    RemapInMemoryValueToStoredValue(void *storedValue,
                                    const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage);
    virtual void RemapStoredValueToInMemoryValue(
        void *storedValue, ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void PreRemapStoredValueToInMemoryValue(void *storedValue,
                                                    ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad);
    virtual uint32_t GetMaxValuesToRemapDuringRemapJobs();
    TArray<TArray<ZCustomSaveDataStorageWithEntityReferenceStorage>> m_variantStorageFragments;
};

class ZCustomSaveDataStorageWithResourcePtrStorageDataStorage
    : public TBaseFragmentedSaveDataStorage<ZCustomSaveDataStorageWithResourcePtrStorage> {
    public:
    static void RegisterType();
    ZCustomSaveDataStorageWithResourcePtrStorageDataStorage(
        const ZCustomSaveDataStorageWithResourcePtrStorageDataStorage &other);
    ZCustomSaveDataStorageWithResourcePtrStorageDataStorage(
        ZCustomSaveDataStorageWithResourcePtrStorageDataStorage *other);
    ZCustomSaveDataStorageWithResourcePtrStorageDataStorage(const ZString &storageName);
    ZCustomSaveDataStorageWithResourcePtrStorageDataStorage();
    ZCustomSaveDataStorageWithResourcePtrStorageDataStorage &
    operator=(const ZCustomSaveDataStorageWithResourcePtrStorageDataStorage &other);
    virtual ~ZCustomSaveDataStorageWithResourcePtrStorageDataStorage();
    virtual const TArray<TArray<ZCustomSaveDataStorageWithResourcePtrStorage>> &
    GetStorageFragments() const;
    virtual TArray<TArray<ZCustomSaveDataStorageWithResourcePtrStorage>> &GetStorageFragments();
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    void BaseAssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
    uint32_t m_maxValuesToRemapDuringRemapJobs;
    virtual bool NeedsToRemapStoredValues();
    virtual bool NeedsToPreRemapStoredValues();
    virtual void
    RemapInMemoryValueToStoredValue(void *storedValue,
                                    const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage);
    virtual void RemapStoredValueToInMemoryValue(
        void *storedValue, ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void PreRemapStoredValueToInMemoryValue(void *storedValue,
                                                    ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad);
    virtual uint32_t GetMaxValuesToRemapDuringRemapJobs();
    TArray<TArray<ZCustomSaveDataStorageWithResourcePtrStorage>> m_variantStorageFragments;
};

class ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage
    : public ZCustomSaveDataStorageWithEntityReferenceStorage {
    public:
    static void RegisterType();
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage(
        ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage *rhs);
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage(
        const ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage &rhs);
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage(int32_t dataVersion);
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage();
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage &
    operator=(ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage *rhs);
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage &
    operator=(const ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage &rhs);
    ZCustomSaveDataRuntimeResourceIDStorage m_rridStorage;
};

class ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage
    : public TBaseFragmentedSaveDataStorage<
          ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage> {
    public:
    static void RegisterType();
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage(
        const ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage &other);
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage(
        ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage *other);
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage(
        const ZString &storageName);
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage();
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage &operator=(
        const ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage &other);
    virtual ~ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage();
    virtual const TArray<TArray<ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage>> &
    GetStorageFragments() const;
    virtual TArray<TArray<ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage>> &
    GetStorageFragments();
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    void BaseAssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
    uint32_t m_maxValuesToRemapDuringRemapJobs;
    virtual bool NeedsToRemapStoredValues();
    virtual bool NeedsToPreRemapStoredValues();
    virtual void
    RemapInMemoryValueToStoredValue(void *storedValue,
                                    const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage);
    virtual void RemapStoredValueToInMemoryValue(
        void *storedValue, ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void PreRemapStoredValueToInMemoryValue(void *storedValue,
                                                    ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad);
    virtual uint32_t GetMaxValuesToRemapDuringRemapJobs();
    TArray<TArray<ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage>>
        m_variantStorageFragments;
};

class ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage
    : public ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorage {
    public:
    static void RegisterType();
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage(
        ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage *rhs);
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage(
        const ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage &rhs);
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage(int32_t dataVersion);
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage();
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage &
    operator=(ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage *rhs);
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage &
    operator=(const ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage &rhs);
    ZCustomSaveDataTypeIDStorage m_typeIDStorage;
};

class ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage
    : public TBaseFragmentedSaveDataStorage<
          ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage> {
    public:
    static void RegisterType();
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage(
        const ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage
            &other);
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage(
        ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage *other);
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage(
        const ZString &storageName);
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage();
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage &
    operator=(const ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage
                  &other);
    virtual ~ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage();
    virtual const TArray<
        TArray<ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage>> &
    GetStorageFragments() const;
    virtual TArray<TArray<ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage>> &
    GetStorageFragments();
    virtual bool ExtractStoredValueIntoVariantRef(const void *storedValue, ZVariantRef &outValue,
                                                  STypeID *propertyType);
    virtual void AssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    void BaseAssignStoredValueFromVariant(void *storedValue, ZVariant *saveData);
    virtual void DumpStoredValueSaveGameInfo(const void *storedValue,
                                             ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                                             uint64_t &dataSizeAccumulator);
    uint32_t m_maxValuesToRemapDuringRemapJobs;
    virtual bool NeedsToRemapStoredValues();
    virtual bool NeedsToPreRemapStoredValues();
    virtual void
    RemapInMemoryValueToStoredValue(void *storedValue,
                                    const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                    ZSaveDataValueStorage &saveDataValueStorage);
    virtual void RemapStoredValueToInMemoryValue(
        void *storedValue, ZLocalIDPathToEntityRefStorage &entityPathToEntityRefMapping,
        ZSaveDataValueStorage &saveDataValueStorage, SResourcePtrsToLoad &resPtrsToLoad);
    virtual void PreRemapStoredValueToInMemoryValue(void *storedValue,
                                                    ZSaveDataValueStorage &saveDataValueStorage,
                                                    SResourcePtrsToLoad &resPtrsToLoad);
    virtual uint32_t GetMaxValuesToRemapDuringRemapJobs();
    TArray<TArray<ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorage>>
        m_variantStorageFragments;
};

class ZSaveDataValueStorage {
    public:
    static void RegisterType();
    enum EStorageState {
        eStorageInitialized,
        eStorageCompletedFinalizationOfComplexSaveData,
        eStorageReadyForWriteToDisk,
        eStoragePreReadyToRestoreComplexSaveDataToMemory,
        eStoragePreReadyToRestoreToMemory,
        eStorageReadyToRestoreComplexSaveDataToMemory,
        eStorageReadyToRestoreToMemory,
    };
    ZSaveDataValueStorage(const ZSaveDataValueStorage &other);
    ZSaveDataValueStorage(ZSaveDataValueStorage *other);
    ZSaveDataValueStorage(bool bLargeReserverForReuseMap);
    ZSaveDataValueStorage();
    static const uint32_t CUSTOM_SERIALIZER_VERSION;
    bool SerializeTo(ZSaveGameDataSerializer *pSerializer, IOutputStream *pStreamToSerializeWith);
    bool DeserializeFrom(ZSaveGameDataDeserializer *pDeserializer,
                         IInputStream *pStreamToDeserializeWith, uint32_t dataVersion);
    ZSaveDataValueStorage &operator=(const ZSaveDataValueStorage &other);
    ~ZSaveDataValueStorage();
    static void RegisterSaveDataValueStorageType();
    void StoreSaveDataValue(void *pDataValue, STypeID *valueTypeId, uint32_t &outStoredIndex,
                            uint64_t &actualSizeStored);
    bool StoreSaveDataValue(ZVariant *saveData, uint32_t &outStoredIndex,
                            bool bValueCanBeStoredInIndex, uint64_t &actualSizeStored);
    bool IsEmpty();
    bool IsStorageReadyToBeWrittenToDisk();
    bool IsStorageReadyToBeRestoredToMemory();
    bool TryGetTypedStoredForValue(uint64_t storedValueTypeKey, STypeID *&outTypeId);
    bool CheckTypeStoredIsCompatibleWithExpectedType(uint64_t storedValueTypeKey,
                                                     STypeID *expectedTypeId);
    const SOrderedClassSaveOperations *FindSaveOperationsForClass(uint32_t classId);
    bool TryGetStoredSaveDataValue(STypeID *typeId, const uint32_t &index, ZVariantRef &outValue);
    bool TryGetStoredSaveDataValue(uint64_t storedValueTypeKey, const uint32_t &index,
                                   ZVariantRef &outValue);
    static bool CanTypeBeStoredInIndex(const IType *pType);
    static bool ShouldCheckValidityOfEntityReferences();
    static void
    SetValidSceneContextReferences(const TSet<unsigned __int64> &validSceneContextReferences);
    static void CheckIsSceneContextReferenceValid(const ZEntityRef &ref);
    static void SetCurrentSavingTypeName(const ZString &currentSavingTypeName);
    static void
    SetCurrentDataInfoForCheckValidityOfEntityReferences(const ZString &currentSavingData);
    static void
    SetCurrentSubDataInfoForCheckValidityOfEntityReferences(const ZString &currentSavingSubData);
    static const char *GetCurrentSavingTypeName();
    static const char *GetCurrentSavingDataDescription();
    static const char *GetCurrentSavingSubDataDescription();
    void
    FinalizeComplexSaveDataValueStorage(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage);
    void FinalizeSaveDataValueStorage(const ZLocalEntityRefToIDPathStorage &entityIDPathStorage,
                                      ZSaveGameJobManager *pSaveGameJobManager,
                                      bool bWaitForJobsToComplete);
    void Trim();
    void Clear();
    void SetGlobalSaveInfo(
        TMap<unsigned __int64, STypeID *> *pStoredValueTypeInfo,
        TMap<unsigned int, SOrderedClassSaveOperations> *pBlueprintToOrderedSaveOperationsInfo);
    void PrePrepareToRestoreStoredData(SResourcePtrsToLoad &resPtrsToLoad,
                                       ZSaveGameJobManager *pSaveGameJobManager,
                                       bool bWaitForJobsToComplete);
    void PrePrepareComplexSaveDataForRestore(SResourcePtrsToLoad &resPtrsToLoad,
                                             ZSaveGameJobManager *pSaveGameJobManager,
                                             bool bWaitForJobsToComplete);
    void PrepareToRestoreStoredData(ZLocalIDPathToEntityRefStorage &entityPathToRefMapping,
                                    SResourcePtrsToLoad &resPtrsToLoad,
                                    ZSaveGameJobManager *pSaveGameJobManager,
                                    bool bWaitForJobsToComplete);
    void PrepareComplexSaveDataForRestore(ZLocalIDPathToEntityRefStorage &entityPathToRefMapping,
                                          SResourcePtrsToLoad &resPtrsToLoad,
                                          ZSaveGameJobManager *pSaveGameJobManager,
                                          bool bWaitForJobsToComplete);
    void DumpSaveGameInfo(ZSaveGameDumpInfoWriter *pWriter, uint64_t depth,
                          uint64_t &dataSizeAccumulator);
    ZSaveDataValueStorageStatsStorage m_statsStorage;
    void ClearTLSData();
    const ZBaseFragmentedSaveDataStorage *
    GetCorrectExistingStorageForSaveData(STypeID *originalTypeId);
    ZBaseFragmentedSaveDataStorage *GetCorrectStorageForSaveData(STypeID *originalTypeId);
    void ConvertValueToSimplerStorageType(ZVariant &originalValue);
    ZColorRGBFragmentedSaveDataStorage m_ColorRGBFragmentedSaveDataStorage;
    ZFloat4FragmentedSaveDataStorage m_Float4FragmentedSaveDataStorage;
    ZVector2FragmentedSaveDataStorage m_Vector2FragmentedSaveDataStorage;
    ZVector3FragmentedSaveDataStorage m_Vector3FragmentedSaveDataStorage;
    ZVector4FragmentedSaveDataStorage m_Vector4FragmentedSaveDataStorage;
    ZMatrixFragmentedSaveDataStorage m_MatrixFragmentedSaveDataStorage;
    ZMatrix43FragmentedSaveDataStorage m_Matrix43FragmentedSaveDataStorage;
    ZUInt64FragmentedSaveDataStorage m_UInt64FragmentedSaveDataStorage;
    ZRuntimeResourceIDSaveDataStorage m_RuntimeResourceIDFragmentedSaveDataStorage;
    ZStringFragmentedSaveDataStorage m_StringFragmentedSaveDataStorage;
    ZSimpleFragmentedVariantDataStorage m_VoidFragmentedSaveDataStorage;
    ZResourcePtrSaveDataStorage m_ResourcePtrFragmentedSaveDataStorage;
    ZEntityRefSaveDataStorage m_EntityRefFragmentedSaveDataStorage;
    ZAccessorRefSaveDataStorage m_AccessorRefFragmentedSaveDataStorage;
    ZArrayElementRefSaveDataStorage m_ArrayElementRefFragmentedSaveDataStorage;
    ZRemappedFragmentedVariantDataStorage m_uncommonSimpleSaveDataValuesWithReferencesStorage;
    ZSimpleFragmentedVariantDataStorage m_uncommonSimpleSaveDataValuesStorage;
    ZPreRemappedComplexFragmentedVariantDataStorage
        m_uncommonPreRemappedComplexSaveDataValuesStorage;
    ZComplexFragmentedVariantDataStorage m_uncommonComplexSaveDataValuesStorage;
    ZGenericBufferDataStorage m_genericBufferSaveDataValueStorage;
    ZArrayOfEntityRefSaveDataStorage m_entityRefArraySaveDataValueStorage;
    ZCustomSaveDataStorageDataStorage m_customSaveDataStorageDataStorage;
    ZCustomSaveDataStorageWithResourcePtrStorageDataStorage
        m_customSaveDataStorageWithResourcePtrStorageDataStorage;
    ZCustomSaveDataStorageWithEntityReferenceStorageDataStorage
        m_customSaveDataStorageWithEntityReferenceStorageDataStorage;
    ZCustomSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage
        m_customSaveDataStorageWithEntityReferenceAndResourcePtrStorageDataStorage;
    ZCustomSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage
        m_customSaveDataStorageWithEntityReferenceResourcePtrAndTypeIDStorageDataStorage;
    TSet<STypeID *> m_typesStoredInStorage;
    TMap<unsigned __int64, STypeID *> *m_pStoredValueTypeInfo;
    TMap<unsigned int, SOrderedClassSaveOperations> *m_pBlueprintToOrderedSaveOperationsInfo;
    struct STypeToStorageToUseCache {
        STypeToStorageToUseCache(const ZSaveDataValueStorage::STypeToStorageToUseCache &other);
        STypeToStorageToUseCache();
        ZSaveDataValueStorage::STypeToStorageToUseCache &
        operator=(const ZSaveDataValueStorage::STypeToStorageToUseCache &other);
        void Clear();
        TMap<STypeID *, ZBaseFragmentedSaveDataStorage *> m_storageToUseForTypeCache;
    };
    ZSaveDataValueStorage::STypeToStorageToUseCache m_typeToStorageCache;
    ZSaveDataValueStorage::EStorageState m_storageState;
};

struct SSaveEntityInfo {
    SSaveEntityInfo(SEntityInstantiationSaveData &rootEntitySaveData,
                    ZSaveDataValueStorage &saveDataValueStorage);
    SEntityInstantiationSaveData &m_rootEntitySaveData;
    ZSaveDataValueStorage &m_saveDataValueStorage;
};

class ZSubEntitySaveInfoAccumulator {
    public:
    ZSubEntitySaveInfoAccumulator(TArray<SSubEntitySaveInfo> &automaticSubSavedEntities);
    void AddAutomaticSubEntitySaveInfo(const SSubEntitySaveInfo &newSaveInfo);
    uint32_t GetTotalExpectedNumberOfAutomaticallySavedSubPaths();
    uint32_t GetTotalNumberOfPropertySaveOperationsNeedingIndices();
    uint32_t GetTotalNumberOfCustomSaveOperationsNeedingIndices();
    TArray<SSubEntitySaveInfo> &m_automaticallySavedSubEntities;
    TUnorderedLinearHashSet<unsigned __int64, &Hash_DefaultHash<unsigned __int64>, 31,
                            DefaultHashAllocator>
        m_offsetsAlreadyAutomaticallySaved;
    uint32_t m_totalExpectedNumberOfAutomaticallySavedSubPaths;
    uint32_t m_nPropertySaveOperationsNeedingIndices;
    uint32_t m_nCustomSaveOperationsNeedingIndices;
};

class ZPathMappingCollector {
    public:
    ZPathMappingCollector(uint64_t nReservePathMappings);
    void UnsafeAddOffsetToPathMapping(const SEntityOffsetToIDPath &mapping);
    void SafeMergeCollector(const ZPathMappingCollector &other);
    void TransferMappings(TArray<SEntityOffsetToIDPath> &otherMapping);
    ZAssertlock m_assertLock;
    ZMutex m_mappingMutex;
    TUniquePtr<TArray<SEntityOffsetToIDPath>> m_pPathMappingStorage;
};

struct SEntityStats {
    TMap<STypeID *, int> leafTypeMap;
};

class IEntityBlueprintFactory : public IComponentInterface {
    public:
    enum EEntityFunctionImplementationFlagIndex {
        eEntityHasInit,
        eEntityHasOnRemove,
        eEntityFunctionImplementationCount,
    };
    static const uint64_t EXTERN_ENTITY_INDEX_MASK;
    static const uint64_t INVALID_ENTITY_INDEX;
    static bool IsIndexExtern(uint64_t nIndex);
    static uint64_t ExtractExternIndex(uint64_t nIndex);
    virtual ~IEntityBlueprintFactory();
    virtual void GetMemoryRequirements(uint64_t *, uint64_t *, uint64_t *);
    virtual ZEntityType *GetFactoryEntityType();
    virtual ZEntityType **CreateEntity(ISceneContext *, uint8_t *, ZEntityType *,
                                       const SEntityCreationInfo &,
                                       ZThreadedFactoryTraversalJobInfo *);
    virtual void ConstructEntity(ISceneContext *, uint8_t *, ZEntityType *);
    virtual void RemoveEntity(ZEntityType **);
    virtual void InitializeEntity(ZEntityType **);
    virtual void PostInitializeEntity(ZEntityType **, EEntityLifeCycleStates,
                                      ZThreadedFactoryTraversalJobInfo *);
    virtual void ChangePostInitializeEntity(ZEntityType **, EEntityLifeCycleStates);
    virtual bool UninitializeEntity(ZEntityType **, SEntityFactoryTimeslicingParameter &);
    virtual void DestroyEntity(ZEntityType **, ZThreadedFactoryTraversalJobInfo *);
    virtual void DestructEntity(ZEntityType **);
    virtual bool HasSaveInformation();
    virtual bool IsSavable(bool bOverridingSave);
    virtual void SaveEntityState(ZEntityType **, SSaveEntityInfo &, const ZSubEntityPath &);
    virtual void LoadEntityState(ZEntityType **, const SLoadEntityInfo &, const ZSubEntityPath &);
    virtual void PerformPostLoadOfEntityState(ZEntityType **, const SLoadEntityInfo &,
                                              const ZSubEntityPath &);
    virtual void PopulateSavableSubEntityData();
    virtual void GatherSavableSubEntities(uint64_t entityOffset, ZEntityIDPathBuilder &pathBuilder,
                                          ZSubEntitySaveInfoAccumulator &saveInfoAccumulator,
                                          bool bOverridingSave);
    virtual void GatherEntityToPathMapping(uint64_t entityOffset, ZEntityIDPathBuilder &pathBuilder,
                                           ZPathMappingCollector &entityPathMappingCollector,
                                           ZThreadedFactoryTraversalJobInfo *pTraversalJobInfo);
    virtual uint64_t GetNumberOfPathMappingsStored();
    virtual uint64_t GetEstimatedNumberOfEntitiesToSave();
    virtual bool GetManualSaveInfoForSubEntity(const ZEntityRef &rootEntity,
                                               uint64_t addressOfEntityToSearch,
                                               SSubEntitySaveInfo &outSaveInfo);
    virtual bool GetPathOfSubEntity(const ZEntityRef &rootEntity, uint64_t addressOfSubEntity,
                                    ZEntityIDPathBuilder &pathBuilder, uint64_t currentOffset,
                                    SEntityOffsetToIDPath &offsetToPath,
                                    const IEntityBlueprintFactory **pRealFactoryOfEntity,
                                    TArray<ZEntityRef> *pRootEntityHierarchy);
    virtual uint32_t GetNumberOfIndicesNeededForAllSaveOperations(const ZEntityRef &rootEntity);
    virtual void DisableReloadOfConstAfterStartResources(ZEntityType **);
    virtual bool AreAllResourcesReady(ZEntityType **);
    virtual void OnEnterStagingEntity(ZEntityType **);
    virtual void OnExitStagingEntity(ZEntityType **);
    virtual void StartEntity(ZEntityType **);
    virtual void OnEnterEditEntity(ZEntityType **);
    virtual void OnExitEditEntity(ZEntityType **);
    virtual void CollectInternalEntities(ZEntityType **, TArray<ZEntityRef> &);
    virtual void CollectInternalEntitiesThatImplementFunction(
        ZEntityType **, SEntityFunctionCallCollector &,
        IEntityBlueprintFactory::EEntityFunctionImplementationFlagIndex);
    virtual bool ShouldPrecalculateInternalEntitiesThatImplementFunction(uint32_t functionIndex);
    virtual void CollectEntityStats(SEntityStats &);
    virtual void ClearAllEntityReferences(ZEntityType **);
    virtual void ClearAllEntityReferencesOnChildren(ZEntityType **);
    virtual void OnOrphanedResource();
    virtual ZRuntimeResourceID GetRuntimeResourceID();
    virtual bool IsRuntimeResourceIDCompatible(const ZRuntimeResourceID &runtimeResourceID);
    virtual uint8_t *TypePtrToMemBlock(ZEntityType **);
    virtual ZEntityType **MemBlockToTypePtr(uint8_t *);
    virtual bool IsReusableEntity();
    virtual void SetEntityBlueprintFactory(ZEntityType *pEntity);
    void AddToSavedInstanceCount(SSaveEntityInfo &saveInfo, bool isRenderMaterial,
                                 ZRuntimeResourceID runtimeResourceID, uint64_t timeSpent);
    void AddCurrentBlueprintToEntityPathMapping(uint64_t entityOffset,
                                                ZEntityIDPathBuilder &pathBuilder,
                                                ZPathMappingCollector &entityPathMappingCollector);
    void IncrementPathMappingCountInStats(const SEntityCreationInfo &entityCreationInfo);
    void GatherDefaultEntityToPathMapping(const IEntityBlueprintFactory *pBluePrintFactoryToUse,
                                          uint32_t subPathID, uint64_t entityOffset,
                                          ZEntityIDPathBuilder &pathBuilder,
                                          ZPathMappingCollector &entityPathMappingCollector,
                                          ZThreadedFactoryTraversalJobInfo *pTraversalJobInfo);
    bool GetDefaultPathOfSubEntityFromBlueprint(
        const IEntityBlueprintFactory *pBluePrintFactoryToUse, uint32_t subPathID,
        const ZEntityRef &rootEntity, uint64_t addressOfSubEntity,
        ZEntityIDPathBuilder &pathBuilder, uint64_t currentOffset,
        SEntityOffsetToIDPath &offsetToPath, const IEntityBlueprintFactory **pRealFactoryOfEntity,
        TArray<ZEntityRef> *pRootEntityHierarchy);
    void GenerateSubOffsetToPath(ZEntityIDPathBuilder &pathBuilder, uint64_t currentOffset,
                                 SEntityOffsetToIDPath &offsetToPath);
    void RegisterOrderedSaveOperations(
        uint32_t classId,
        const TArrayRef<TPair<unsigned int, TPair<bool, unsigned int>>> orderedSaveOperations);
    void UnregisterOrderedSaveOperations(uint32_t classId);
    const SOrderedClassSaveOperations *FindSaveOperationsForClass(const SLoadEntityInfo &loadInfo,
                                                                  uint32_t classId);
    bool
    TryGetIndexOfPropertySaveOperation(const SOrderedClassSaveOperations *pOrderedSaveOperations,
                                       uint32_t classId, uint32_t propertyId,
                                       uint32_t &lastFetchedIndex);
    bool TryGetIndexOfCustomSaveOperation(const SOrderedClassSaveOperations *pOrderedSaveOperations,
                                          uint32_t classId, uint32_t &lastFetchedIndex);
    bool CheckSubEntityPathEmpty(const ZSubEntityPath &path);
    void RegisterSavedPropertyTypes(const ZString &typeName, uint32_t classID, uint32_t propertyID,
                                    STypeID *propertyTypeId);
    void UnregisterSavedPropertyTypes(const ZString &typeName, uint32_t classID,
                                      uint32_t propertyID);
    uint32_t
    CalculateNumberOfIndicesNeededForSaveOperations(uint32_t numberOfPropertieSaveOperations,
                                                    uint32_t numberOfCustomSaveOperations);
    ZEntityOperationValueIndexContainer *GetIndexContainerToUse(SSaveEntityInfo &saveInfo,
                                                                uint32_t numberOfIndices);
    const ZEntityOperationValueIndexContainer *
    GetIndexContainerToRead(const SLoadEntityInfo &loadInfo,
                            const SOrderedClassSaveOperations *pOrderedOperations);
    SInstantiationSubEntitySaveInfo &CreateSubEntitySaveInfo(SSaveEntityInfo &saveInfo,
                                                             const ZSubEntityPath &subEntityPath);
    void SavePropertyValue(SSaveEntityInfo &saveInfo,
                           SInstantiationSubEntitySaveInfo &subEntitySaveInfo,
                           bool isRenderMaterial,
                           ZEntityOperationValueIndexContainer *pIndexContainer,
                           const ZSubEntityPath &subEntityPath, const SPropertyData *pPropertyData,
                           const ZEntityRef &propertyEntitySource, const ZString &typeName);
    void SavePropertyValueUsingTemproraryBuffer(
        SSaveEntityInfo &saveInfo, SInstantiationSubEntitySaveInfo &subEntitySaveInfo,
        bool isRenderMaterial, ZEntityOperationValueIndexContainer *pIndexContainer,
        const ZSubEntityPath &subEntityPath, void *pPropertyAddress, const IType *pPropertyType,
        const ZString &typeName);
    bool TryGetPropertyValueToSet(const SLoadEntityInfo &loadInfo,
                                  const SOrderedClassSaveOperations *pOrderedOperations,
                                  const ZEntityOperationValueIndexContainer *pIndexContainer,
                                  uint32_t &lastFetchedIndex, const ZSubEntityPath &subEntityPath,
                                  uint32_t classID, uint32_t propertyId, STypeID *propertyType,
                                  ZVariantRef &value);
    bool GetRootManualEntitySaveInfo(uint64_t entityOffset, const ZEntityRef &rootEntity,
                                     uint64_t addressOfEntityToSearch,
                                     SSubEntitySaveInfo &outSaveInfo,
                                     uint32_t numberOfPropertieSaveOperations,
                                     uint32_t numberOfCustomSaveOperations);
    void GatherSubBlueprintAsSavableEntity(uint64_t entityOffset, ZEntityIDPathBuilder &pathBuilder,
                                           uint32_t subPathID,
                                           ZSubEntitySaveInfoAccumulator &saveInfoAccumulator,
                                           const IEntityBlueprintFactory *pBluePrintFactory,
                                           bool bOverridingSave);
    ZRuntimeResourceID m_ridResource;
};

class IEntityFactory : public IComponentInterface {
    public:
    virtual ~IEntityFactory();
    virtual void GetMemoryRequirements(uint64_t *, uint64_t *, uint64_t *);
    virtual ZEntityType *GetFactoryEntityType();
    virtual ZEntityType **CreateEntity(ISceneContext *pContext, uint8_t *pMemAddr,
                                       ZEntityType *pEntityType,
                                       const SEntityCreationInfo &entityCreationInfo,
                                       ZThreadedFactoryTraversalJobInfo *pTraversalJobInfo);
    virtual void ConstructEntity(ISceneContext *pContext, uint8_t *pMemAddr,
                                 ZEntityType *pEntityType);
    virtual void ConfigureEntity(ZEntityType **, ZThreadedFactoryTraversalJobInfo *);
    virtual bool InitializeEntityPass1NonThreaded(ZEntityType **,
                                                  SEntityFactoryTimeslicingParameter &);
    virtual bool InitializeEntityPass2Threaded(ZEntityType **, ZThreadedFactoryTraversalJobInfo *);
    virtual void PostInitializeEntity(ZEntityType **pEntity, EEntityLifeCycleStates postInitState,
                                      ZThreadedFactoryTraversalJobInfo *pTraversalJobInfo);
    virtual void ChangePostInitializeEntity(ZEntityType **pEntity,
                                            EEntityLifeCycleStates postInitState);
    virtual void RemoveEntity(ZEntityType **pEntity);
    virtual void UninitializeEntity(ZEntityType **pEntity);
    virtual void DestroyEntity(ZEntityType **pEntity,
                               ZThreadedFactoryTraversalJobInfo *pTraversalJobInfo);
    virtual void DestructEntity(ZEntityType **pEntity);
    virtual void OnEnterStagingEntity(ZEntityType **);
    virtual void OnExitStagingEntity(ZEntityType **);
    virtual void DisableReloadOfConstAfterStartResources(ZEntityType **);
    virtual bool AreAllResourcesReady(ZEntityType **);
    virtual void StartEntity(ZEntityType **);
    virtual void OnEnterEditEntity(ZEntityType **);
    virtual void OnExitEditEntity(ZEntityType **);
    virtual void CollectInternalEntities(ZEntityType **, TArray<ZEntityRef> &);
    virtual void CollectInternalEntitiesThatImplementFunction(
        ZEntityType **pEntity, SEntityFunctionCallCollector &result,
        IEntityBlueprintFactory::EEntityFunctionImplementationFlagIndex functionIndex);
    virtual IEntityBlueprintFactory *GetBlueprint();
    virtual TResourcePtr<IEntityBlueprintFactory> GetBlueprintResource();
    ZRuntimeResourceID GetOriginRuntimeResourceID();
    ZRuntimeResourceID GetFinalRuntimeResourceID();
    bool IsSavable(bool);
    void SaveEntityState(ZEntityType **, SSaveEntityInfo &, const ZSubEntityPath &);
    void LoadEntityState(ZEntityType **, const SLoadEntityInfo &, const ZSubEntityPath &);
    void PerformPostLoadOfEntityState(ZEntityType **, const SLoadEntityInfo &,
                                      const ZSubEntityPath &);
    ZRuntimeResourceID m_originRRID;
    ZRuntimeResourceID m_ridResource;
};

enum EEntityTypeMapFlags {
    ENTITYTYPE_PROPERTY_MAP,
    ENTITYTYPE_INTERFACE_MAP,
    ENTITYTYPE_EXPENT_MAP,
    ENTITYTYPE_SUBSET_MAP,
    ENTITYTYPE_INPUTPIN_MAP,
    ENTITYTYPE_OUTPUTPIN_MAP,
    ENTITYTYPE_OWNED_BY_ENTITY,
};

class ZEntityType {
    public:
    enum EntityTypeOwner {
        EOWNED_BY_FACTORY,
        EOWNED_BY_ENTITY,
    };
    ZEntityType(ZEntityType::EntityTypeOwner eOwner, ZEntityType *pType);
    ZEntityType(ZEntityType::EntityTypeOwner eOwner);
    ZEntityType();
    ~ZEntityType();
    bool IsOwnedByEntity();
    bool HasBorrowedPointer(EEntityTypeMapFlags eMap);
    void ClearBorrowedPointerFlag(EEntityTypeMapFlags eMap);
    void SetBorrowedPointerFlag(EEntityTypeMapFlags);
    int32_t GetValidMapsFlag();
    bool ImplementsInterface(STypeID *);
    void RemoveInterfaceData(STypeID *);
    SInterfaceData *GetInterfaceData(STypeID *type);
    SExposedEntityData *GetExposedEntityData(uint32_t nExposedEntityNameID);
    SExposedEntityData *GetExposedEntityData(const ZString &sExposedEntityNameID);
    SSubsetData *GetSubsetData(uint32_t nSubsetID);
    static uint32_t GetSubsetID(const ZString &sName);
    bool FindFirstProperty(uint32_t nPropertyID, TArrayIterator<SPropertyData> *it);
    bool FindFirstStreamedProperty(uint32_t nPropertyID, TArrayIterator<unsigned int> *it);
    bool FindProperties(uint32_t nPropertyID, TArrayIterator<SPropertyData> *itBegin,
                        TArrayIterator<SPropertyData> *itEnd);
    bool FindFirstInputPin(uint32_t nPinID, TArrayIterator<TPair<unsigned int, SPinData>> *it);
    bool FindInputPins(uint32_t nPinID, TArrayIterator<TPair<unsigned int, SPinData>> *itBegin,
                       TArrayIterator<TPair<unsigned int, SPinData>> *itEnd);
    bool FindFirstOutputPin(uint32_t nPinID, TArrayIterator<TPair<unsigned int, SPinData>> *it);
    uint64_t FindOutputPins(uint32_t nPinID, TArrayIterator<TPair<unsigned int, SPinData>> *itBegin,
                            TArrayIterator<TPair<unsigned int, SPinData>> *itEnd);
    bool IsPropertyStreamable(uint32_t);
    bool IsPropertyMediaStreamable(uint32_t);
    static bool IsOffsetAbsolute(int64_t nOffset);
    static void *GetOffsetAddress(ZEntityType **pSourceEntity, int64_t nOffset);
    static int64_t MakeOffsetAbsolute(int64_t nOffset);
    int32_t m_nBorrowedPointersMask;
    TArray<SPropertyData> *m_pPropertyData;
    TArray<unsigned int> *m_pStreamablePropertyData;
    TArray<SInterfaceData> *m_pInterfaceData;
    TArray<SExposedEntityData> *m_pExposedEntityData;
    TArray<SSubsetData> *m_pSubsets;
    TArray<TPair<unsigned int, SPinData>> *m_pInputPins;
    TArray<TPair<unsigned int, SPinData>> *m_pOutputPins;
    IEntityBlueprintFactory *m_pEntityBlueprint;
    static const int64_t ABSOLUTE_OFFSET_MASK;
    ZString m_sDebugName;
    int64_t m_nOwningEntityOffset;
};

struct SExternReference {
    SExternReference();
    ~SExternReference();
    ZRuntimeResourceID sceneRid;
    uint32_t entityId;
    int32_t propertyIndex;
};

class ZExternReferenceMap {
    public:
    static const uint64_t SPATIALPARENT_INDEX;
    ZExternReferenceMap();
    ~ZExternReferenceMap();
    bool IsResolved(uint64_t nTargetIndex);
    ISceneContext *ResolveContextOfEntity(uint64_t nTargetIndex);
    ZEntityType **ResolveEntity(uint64_t nTargetIndex);
    int32_t ResolvePropertyIndex(uint64_t nTargetIndex);
    void SetDebugID(ZRuntimeResourceID rrid);
    enum EReservedIndex {
        EReservedIndex_SpatialParent,
        EReservedIndex_Count,
    };
    const SExternReference *Resolve(uint64_t nTargetIndex);
    TArray<SExternReference> m_xrefMap;
    ZRuntimeResourceID m_debugID;
};

void ImportZRuntimeEntityModule();