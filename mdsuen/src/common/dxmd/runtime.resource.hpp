// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "g2base.hpp"
#include "runtime.core.hpp"
#include "system.connection.hpp"
#include "system.job.hpp"

class ZRuntimeResourceDatabase;
class ZResourceStub;
class ZResourcePtr;
class IResourceInstaller;
class ZResourceManager;
class IResourceDevice;
class ZResourceDevice;

class ZRuntimeResourceID {
    public:
    static void RegisterType();
    ZRuntimeResourceID();
    bool operator==(const ZRuntimeResourceID &rhs) { return this->m_ID == rhs.m_ID; }
    bool operator!=(const ZRuntimeResourceID &rhs);
    bool operator<(const ZRuntimeResourceID &rhs);
    bool IsEmpty();
    bool IsValid();
    ZString ToDebugString() const;
    ZString ToHexDebugString() const;
    ZString TryGetResolvedResourceID(bool returnPrettyResourceID);
    uint64_t ToCString(char *pBuffer, uint64_t nSize, const ZString &sFormat);
    uint32_t GetHashCode();
    uint64_t GetID() const;
    static bool HasRemoteDatabase();
    static ZRuntimeResourceID QueryRuntimeResourceID(const ZResourceID &idResource);
    static ZResourceID QueryResourceID(const ZRuntimeResourceID &ridResource,
                                       bool returnPrettyResourceID);
    static bool CanRuntimeResourceIDBeResolved(const ZRuntimeResourceID &ridResource,
                                               bool haltOnUnresolvable);
    static bool RecordMapping(const ZRuntimeResourceID &ridResource, const ZResourceID &idResource,
                              const ZResourceID &prettyIDResource);
    static ZRuntimeResourceID CreateLibraryResourceID(ZRuntimeResourceID ridLibrary,
                                                      int32_t indexInLibrary);
    static ZRuntimeResourceID DO_NOT_USE_ME_Create(uint64_t nResource);
    bool IsLibraryResource() const;
    bool IsLibrary() const;
    ZRuntimeResourceID GetLibraryRuntimeResourceID() const;
    int32_t GetIndexInLibrary() const;
    static ZResourceID InternalResolveResourceID(const ZRuntimeResourceID &ridResource,
                                                 bool haltOnUnresolvable,
                                                 bool returnPrettyResourceID);
    static const uint64_t INVALID_RUNTIME_RESOURCE_ID;
    static const uint64_t BITMASK_STANDALONERESOURCE_ID;
    static const uint64_t BITMASK_LIBRARYRESOURCE_LIBRARYID;
    static const uint64_t BITMASK_LIBRARYRESOURCE_INDEX;
    static const uint32_t SHIFTBITS_LIBRARYRESOURCE_INDEX;
    static const uint64_t BITMASK_FLAGS;
    static const uint64_t FLAG_BIT_LIBRARYRESOURCE;
    static const uint64_t FLAG_BIT_LIBRARY;
    uint64_t m_ID;
    static void InitResolver(ZRuntimeResourceDatabase *runtimeResourceDatabase,
                             ZComponentManager *componentManager);
    static void ShutdownResolver();
    static ZRuntimeResourceID Create(uint64_t nResource);
};

int32_t Hash_ZRuntimeResourceID(const ZRuntimeResourceID &key);

class ZRuntimeResourceDatabase {
    public:
    ZRuntimeResourceDatabase(IRemoteCallManager *remoteCallManager,
                             ZComponentManager *componentManager,
                             ZConnectionManager *connectionManager);
    ~ZRuntimeResourceDatabase();
    void RaisePriority(const ZRuntimeResourceID &ridResource, int32_t nPriorty);
    void LoadResourceAsync(const ZRuntimeResourceID &ridResource,
                           const ZDelegate<void(ZRuntimeResourceID const &, ZString const &,
                                                __int64, int, int)> &callback,
                           int32_t nPriority);
    int32_t LoadMonitoredResourceAsync(
        const ZRuntimeResourceID &ridResource,
        const ZDelegate<void(ZRuntimeResourceID const &, ZString const &, __int64, int, int)>
            &callback,
        int32_t nPriority);
    void BeginMonitoring(int32_t monitorID,
                         const ZDelegate<void(ZRuntimeResourceID const &, int)> &callback);
    void ReleaseMonitoredResource(int32_t monitorID);
    void OnResourceUnloading(const ZRuntimeResourceID &resourceID);
    ZResourceID ResourceIDFromRuntimeResourceID(const ZRuntimeResourceID &ridResource);
    ZResourceID PrettyResourceIDFromRuntimeResourceID(const ZRuntimeResourceID &ridResource);
    void RecordForResolution(const ZResourceID &idResource);
    void OnRuntimeResourceBuildLog(const ZRuntimeResourceID &requesterResourceID,
                                   const ZResourceID &resource, bool failed,
                                   const ZString &message);
    void ClearMessageBoardFor(const ZRuntimeResourceID &resourceID);
    void ConnectToResourceServer(ZComponentManager *componentManager,
                                 ZConnectionManager *connectionManager);
    void LoadResourceAsyncInternal(const ZRuntimeResourceID &ridResource,
                                   const ZDelegate<void(ZRuntimeResourceID const &, ZString const &,
                                                        __int64, int, int)> &callback,
                                   int32_t nPriority, int32_t monitorID);
    int32_t GetNewMonitorID();
    void OnRuntimeResourceNeedsReload(int32_t nMonitor, const ZRuntimeResourceID &ridResource);
    ZMutex m_reloadCallMutex;
    int64_t m_nextFreeMonitorID;
    THashMap<int, ZDelegate<void(ZRuntimeResourceID const &, int)>,
             TDefaultHashMapPolicy<int, ZDelegate<void(ZRuntimeResourceID const &, int)>>>
        m_runtimeResourceReloadMonitors;
    ZMutex m_reloadMonitorMutex;
    IRemoteCallManager *m_remoteCallManager;
    ZConnection *m_pConnection;
    ZMutex m_LogMutex;
    THashMap<ZRuntimeResourceID, TList<ZResourceID>,
             TDefaultHashMapPolicy<ZRuntimeResourceID, TList<ZResourceID>>>
        m_parentRequestorToChildren;
    TMap<ZResourceID, ZTraceMessageBoard> m_resourceToLoadMessages;
    static ZRuntimeResourceDatabase *instance;
};

class ZResourceDataBuffer {
    public:
    ~ZResourceDataBuffer();
    void Insert(uint8_t *pData, uint64_t size);
    void Clear();
    uint64_t GetCapacity();
    uint8_t *GetData();
    static TSharedPtr<ZResourceDataBuffer> Create(void *data, uint64_t capacity);
    static TSharedPtr<ZResourceDataBuffer> CreateFromBorrowedData(void *buffer, uint64_t capacity);
    static TSharedPtr<ZResourceDataBuffer> Wrap(void *data, uint32_t size);
    ZResourceDataBuffer(void *buffer, uint64_t capacity, uint64_t size, bool bOwnDataPtr);
    uint8_t *m_pData;
    uint64_t m_nSize;
    uint64_t m_nCapacity;
    bool m_bOwnsDataPtr;
};

enum EResourceReferenceFlags {
    RESOURCE_REFERENCE_NO_FLAGS,
    RESOURCE_REFERENCE_OFFSET_BITS,
    RESOURCE_REFERENCE_OFFSET_BITCOUNT,
    RESOURCE_REFERENCE_FLAG_BITS,
    RESOURCE_REFERENCE_TYPE_OF_STREAMING_ENTITY,
    RESOURCE_REFERENCE_STREAMED,
    RESOURCE_REFERENCE_MEDIA_STREAMED,
    RESOURCE_REFERENCE_INSTALL_DEPENDENCY,
    RESOURCE_REFERENCE_MANUAL_INSTALL_DEPENDENCY,
    RESOURCE_REFERENCE_RESOURCE_ALIAS,
    RESOURCE_REFERENCE_CYCLIC,
    RESOURCE_REFERENCE_LOCALIZATION_DEPENDENCY,
    RESOURCE_REFERENCE_LOCALIZATION_TEXT_DEPENDENCY,
    RESOURCE_REFERENCE_ONDEMAND_STREAMED,
    RESOURCE_LOCALIZATION_MASK,
};

class ZResourceReader {
    public:
    ZResourceReader(const ZResourceReader &);
    ZResourceReader(ZResourceStub *pStub, TSharedPtr<ZResourceDataBuffer> pData, uint32_t dataSize);
    virtual ~ZResourceReader();
    const void *GetResourceData(uint64_t nOffset);
    TSharedPtr<ZResourceDataBuffer> GetResourceDataPtr();
    uint64_t GetResourceDataSize();
    virtual uint32_t GetResourceType();
    virtual uint64_t GetNumResourceIdentifiers();
    virtual ZRuntimeResourceID GetResourceIdentifier(uint64_t lResourceIdentifierIndex);
    virtual EResourceReferenceFlags GetResourceFlags(uint64_t lResourceIdentifierIndex);
    virtual void FillResourceIdentifierArray(TArray<ZRuntimeResourceID> &resourceIDs);
    virtual ZResourcePtr GetInstallTimeDependency(uint64_t lResourceIdentifierIndex);
    virtual ZRuntimeResourceID GetRuntimeResourceID();
    void *Deserialize();
    ZResourceStub *m_pStub;
    TSharedPtr<ZResourceDataBuffer> m_pResourceData;
    ZResourcePtr &operator=(const ZResourceReader &);
    uint32_t m_nResourceDataSize;
};

enum EResourceStatus {
    RESOURCE_STATUS_UNLOADED,
    RESOURCE_STATUS_LOADING,
    RESOURCE_STATUS_INSTALLING,
    RESOURCE_STATUS_FAILED,
    RESOURCE_STATUS_VALID,
    RESOURCE_STATUS_HEADER_LOADED,
};

class ZResourcePtr {
    public:
    struct ZNull {};
    ZResourcePtr(ZResourceStub *pResourceHeader);
    ZResourcePtr(const ZRuntimeResourceID &rid);
    ZResourcePtr(ZResourcePtr *rhs);
    ZResourcePtr(const ZResourcePtr &rhs);
    ZResourcePtr();
    ~ZResourcePtr();
    ZResourcePtr &operator=(ZResourcePtr::ZNull *__formal);
    ZResourcePtr &operator=(ZResourcePtr *rhs);
    ZResourcePtr &operator=(const ZResourcePtr &rhs);
    bool operator==(const ZResourcePtr &rhs);
    bool operator==(ZResourcePtr::ZNull *__formal);
    bool operator!=(const ZResourcePtr &rhs);
    bool operator!=(ZResourcePtr::ZNull *__formal);
    void Release();
    ZRuntimeResourceID GetRuntimeResourceID() const;
    bool IsLoading();
    bool IsReady();
    bool Exists();
    bool IsValid();
    bool Failed();
    ZResourcePtr GetNewestVersion();
    bool HasNewerVersion();
    bool Rebind();
    bool RebindEvenIfNotReady();
    bool IsResourceRequestedWithStreamedDependencies();
    void ReleaseStreamedDependencies();
    uint32_t GetResourceTag() const;
    void AddStatusChangedListener(const ZDelegate<void(ZRuntimeResourceID const &)> &d,
                                  bool bNotifyIfResourceAlreadyLoaded);
    void RemoveStatusChangedListener(const ZDelegate<void(ZRuntimeResourceID const &)> &d);
    bool AreAllReferencesLoaded();
    bool operator<(const ZResourcePtr &rhs);
    bool operator>(const ZResourcePtr &rhs);
    void SetInstallSucceeded(void *pResourceData);
    void SetInstallFailed();
    void SetResourceData(void *pResourceData);
    void ReplaceResourceData(void *pResourceData);
    void SetResourceDataSize(uint64_t dataSize, uint64_t gpuDataSize);
    uint64_t GetResourceDataSize();
    uint64_t GetResourceDataSizeRecursive();
    uint64_t GetResourceGPUDataSize();
    uint64_t GetResourceGPUDataSizeRecursive();
    void *GetRawPointer() const;
    void *GetInstallingRawPointer();
    uint64_t GetHashCode();
    static bool IsLeakTrackingEnabled();
    static void SetLeakTrackingEnabled(bool bEnabled);
    static ZResourcePtr Create(ZResourceStub *stub);
    void AddRefStub(ZResourceStub *pStub);
    void ReleaseStub(ZResourceStub *pStub);
    EResourceStatus GetResourceStatus() const;
    IResourceInstaller *GetResourceInstaller();
    void *GetRawPointerInternal();
    ZResourceStub *m_pResourceStub;
    static bool m_bLeakTrackingEnabled;
};

class ZResourcePending {
    public:
    ZResourcePending(ZResourcePending *pSrc);
    ZResourcePending(const ZResourcePending &pSrc);
    ZResourcePending(const ZResourcePtr &pResource,
                     const TSharedPtr<ZResourceReader> &pResourceReader, int32_t nPriority);
    ~ZResourcePending();
    ZResourcePending &operator=(ZResourcePending *rhs);
    ZResourcePtr m_pResource;
    TSharedPtr<ZResourceReader> m_pResourceReader;
    int32_t m_nPriority;
    int64_t m_ResourceHandle;
    int32_t m_nDataStartPosition;
    bool m_bInGameLoad;
};

enum EResourceInstallerState {
    Installing,
    Done,
    Blocked,
    WaitingForDevice,
    ThreadRestriction,
    BudgetExceeded,
};

class IResourceInstaller : public IComponentInterface {
    public:
    virtual void Release(const ZRuntimeResourceID &, void *, uint32_t);
    virtual void *Allocate(uint64_t);
    virtual EResourceInstallerState Install(ZResourcePending &);
    virtual bool IsStreamInstaller();
    virtual bool IsIndirectionInstaller();
    virtual bool SupportsAllocate();
    virtual void OnOrphanedResource(const ZRuntimeResourceID &, void *, uint32_t);
    virtual uint32_t GetInstallerId();
};

enum EResourceLoadPriority : uint8_t {
    None,
    Lowest,
    BelowNormal3,
    BelowNormal2,
    BelowNormal1,
    Normal,
    AboveNormal1,
    AboveNormal2,
    AboveNormal3,
    AboveNormal4,
    AboveNormal5,
    WellAboveNormal,
    Hasty,
    High,
};

enum EResourceStubType {
    NonLibrary,
    Library,
    LibraryStreamed,
};

struct SResourceHeaderHeader {
    uint32_t m_type;
    uint32_t m_nReferencesChunkSize;
    uint32_t m_nStatesChunkSize;
    uint32_t m_nDataSize;
    uint32_t m_nSystemMemoryRequirement;
    uint32_t m_nVideoMemoryRequirement;
};

struct SResourceHeaderData {
    SResourceHeaderData();
    SResourceHeaderHeader m_HeaderHeader;
    const uint8_t *m_pReferencesChunk;
};

class ZResourceHeaderReader {
    public:
    ZResourceHeaderReader(SResourceHeaderHeader headerHeader, const uint8_t *pReferencesChunk);
    uint32_t GetResourceType() const;
    uint32_t GetNumResourceIdentifiers() const;
    ZRuntimeResourceID GetResourceIdentifier(uint32_t lResourceIdentifierIndex) const;
    EResourceReferenceFlags GetResourceFlags(uint32_t lResourceIdentifierIndex) const;
    uint32_t GetResourceLocalizationFlags(uint32_t lResourceIdentifierIndex) const;
    bool HasIntegerFormat() const;
    bool HasOffsetTooBig() const;
    SResourceHeaderData m_data;
};

enum EResourceLoadStatus {
    LOAD_STATUS_NONE,
    LOAD_STATUS_HEADER,
    LOAD_STATUS_FULL,
    LOAD_STATUSES,
    LOAD_STATUS_OPTION_STREAMED,
    LOAD_STATUS_OPTIONS,
};

class ZResourceStub {
    public:
    ZResourceStub(const ZRuntimeResourceID &ResourceID, int8_t type,
                  EResourceLoadPriority nInitialPriority);
    virtual ~ZResourceStub();
    bool ReleaseResourceData();
    EResourceStatus GetResourceStatus() const;
    virtual bool HasNewerVersion();
    virtual ZResourcePtr GetNewestVersion();
    virtual bool IsReloadable();
    virtual int32_t GetMonitorID();
    virtual void SetMonitorID(int32_t);
    const ZRuntimeResourceID &GetRuntimeResourceID() const;
    virtual ZRuntimeResourceID GetStreamingRID();
    void *GetResourceData();
    uint32_t GetResourceTag() const;
    IResourceInstaller *GetResourceInstaller() const;
    uint64_t GetResourceDataSize();
    uint64_t GetResourceDataSizeRecursive();
    uint64_t GetResourceGPUDataSize();
    uint64_t GetResourceGPUDataSizeRecursive();
    void AddRef();
    void Release();
    void AddRefInternal();
    void ReleaseInternal();
    void AddRefHeader();
    void ReleaseHeader();
    void AddStatusChangedListener(const ZDelegate<void(ZRuntimeResourceID const &)> &d,
                                  bool bNotifyIfResourceAlreadyLoaded);
    void RemoveStatusChangedListener(const ZDelegate<void(ZRuntimeResourceID const &)> &d);
    void SetResourceDataSize(uint64_t dataSize, uint64_t gpuDataSize);
    void SetResourceTag(uint32_t nResourceTag);
    uint32_t GetHeaderRefCount();
    uint32_t GetRefCount();
    EResourceStubType GetType();
    EResourceLoadPriority GetPriority();
    void RaisePriority(EResourceLoadPriority nPriority);
    bool HasHeaderResourceReferences();
    void AcquireHeaderResourceReferences(const ZResourceHeaderReader &header,
                                         bool bIncludeStreamedDependencies);
    void RebindHeaderResourceReference(uint64_t index);
    bool AcquireResourceReferences(bool bIncludeStreamedDependencies);
    void ReleaseResourceReferences(bool bReleaseDependencies);
    bool ShouldReacquireHeaderResourceReferences(const ZResourceHeaderReader &header);
    void ReacquireHeaderResourceReferences(const ZResourceHeaderReader &header,
                                           bool bIncludeStreamedDependencies);
    void SetInstallSucceeded(void *pResourceData);
    void SetInstallFailed();
    EResourceStatus GetInstallTimeDependenciesStatus();
    TArray<TPair<ZRuntimeResourceID, enum EResourceStatus>> GetInstallTimeDependencies();
    void AcquireStreamedDependencies(EResourceLoadPriority nLoadPriority);
    void ReleaseStreamedDependencies();
    bool HasInstallTimeDependency(const ZRuntimeResourceID &resourceID);
    ZResourcePtr GetInstallDependency(uint64_t nIndex);
    TArray<ZResourceStub *> GetManualInstallDependencies();
    bool IsResourceAlias();
    bool IsFullyLoaded();
    uint64_t GetResourceReferenceCount();
    ZRuntimeResourceID GetResourceReferenceID(uint64_t index);
    EResourceReferenceFlags GetResourceReferenceFlags(uint64_t idx);
    EResourceLoadStatus GetRequestedLoadStatus();
    uint8_t GetRequestedLoadOptions();
    void SetRequestedLoadStatus(EResourceLoadStatus newStatus);
    void SetRequestedLoadOptions(EResourceLoadStatus options);
    bool AreAllReferencesLoaded();
    void GetBackReferences(TArray<ZResourceStub *> &references);
    void RemoveBackReferences();
    bool IsCyclicReference(ZResourceStub *pStub);
    void AddReferenceInfo(void *pPtr, const ZCallStack &callStack);
    void RemoveReferenceInfo(void *pPtr);
    bool IsLeakTrackingEnabled();
    THashMap<void *, ZCallStack, TDefaultHashMapPolicy<void *, ZCallStack>> m_ReferenceInfo;
    static ZMutex m_ReferenceInfoMutex;
    void ReleaseHeaderResourceReferences();
    void SetResourceData(void *pResourceData);
    void ReplaceResourceData(void *pResourceData);
    void SetResourceStatus(EResourceStatus eResourceStatus);
    void AddBackReference(ZResourceStub *pStub);
    void RemoveBackReference(ZResourceStub *pStub);
    void SetResourceAlias(ZResourceStub *pStub);
    void GetResourceDataSizeRecursiveInternal(uint64_t &size, bool gpu);
    ZRuntimeResourceID m_ridResource;
    uint8_t m_eResourceStatus;
    uint8_t m_RequestedLoadStatus;
    EResourceLoadPriority m_nPriority;
    uint8_t m_eType;
    uint32_t m_nResourceTag;
    union {
        ZResourceStub *m_pAliasedResourceStub;
        void *m_pResourceData;
    };
    int64_t m_nRefCount;
    struct SResourceReference {
        SResourceReference();
        ZResourceStub *m_pStub;
        ZRuntimeResourceID *m_pridMediaResource;
        ZResourcePtr m_pResource;
        uint32_t m_Flags;
        ZResourceStub *GetStub();
        uint32_t GetLocalizationFlags();
        bool IsStreamed();
        bool IsTypeOfStreamingEntity();
        bool IsInstallDependency();
        bool IsNonLocalizedInstallDependency();
        bool IsLocalizedInstallDependency();
        bool IsManualInstallDependency();
        bool IsMediaStreamed();
        bool IsCyclic();
        bool IsResourceAlias();
        bool IsNoFlags();
        bool IsOnDemandStreamed();
    };
    TArray<ZResourceStub::SResourceReference> m_resourceReferences;
    TArray<ZResourceStub *> m_backReferences;
    ZTraceMessageSlot m_resourceLoadError;
    uint64_t m_nResourceDataSize;
    uint64_t m_nResourceGPUDataSize;
};

class ZLibraryResourceStub : public ZResourceStub {
    public:
    ZLibraryResourceStub(const ZRuntimeResourceID &ResourceID,
                         const ZRuntimeResourceID &LibraryResourceID, uint8_t type,
                         EResourceLoadPriority nInitialPriority);
    ZLibraryResourceStub(const ZRuntimeResourceID &ResourceID,
                         const ZRuntimeResourceID &LibraryResourceID,
                         EResourceLoadPriority nInitialPriority);
    virtual ~ZLibraryResourceStub();
    void InitializeReferences(ZResourceHeaderReader &reader);
    virtual bool IsReloadable();
    virtual int32_t GetMonitorID();
    virtual void SetMonitorID(int32_t value);
    ZRuntimeResourceID GetLibraryRuntimeResourceID();
    virtual ZRuntimeResourceID GetStreamingRID();
    virtual bool HasNewerVersion();
    virtual ZResourcePtr GetNewestVersion();
    void SetNewestVersion(ZResourcePtr pResource);
    bool SetResourceStreamerID(uint32_t id, bool &alreadySet);
    bool SetResourceStreamerID(uint32_t id);
    bool HasResourceStreamerID();
    bool IsLoadedByResourceLibrary();
    ZRuntimeResourceID m_libraryID;
    uint32_t m_nResourceStreamerID;
};

struct SResourceLibraryEntry {
    SResourceLibraryEntry();
    ZLibraryResourceStub *pStub;
    uint32_t nDataSize;
    bool bExternal;
};

class ZHashedString {
    public:
    static void RegisterType();
    ZHashedString(ZHashedString *);
    ZHashedString(const ZHashedString &);
    ZHashedString(uint32_t);
    ZHashedString(const ZString &);
    ZHashedString(const char *);
    ZHashedString();
    bool IsValid();
    bool IsEmpty();
    ZHashedString &operator=(ZHashedString *);
    ZHashedString &operator=(const ZHashedString &);
    bool operator==(uint32_t);
    bool operator==(const ZHashedString &);
    bool operator!=(uint32_t);
    bool operator!=(const ZHashedString &);
    bool operator<(uint32_t);
    bool operator<(const ZHashedString &);
    operator unsigned int();
    uint32_t GetHashCode();
    bool FromString(const ZString &);
    uint64_t ToCString(char *, uint64_t, const ZString &);
    static ZHashedString Empty;
    ZString GetResolvableTraceString();
    static const uint32_t s_InvalidHash;
    static const uint32_t s_EmptyHash;
    uint32_t m_hashedValue;
};

class ZResourceLibraryInfo {
    public:
    ZResourceLibraryInfo(const ZResourceID &rid, ZResourceStub *pLibraryStub,
                         uint32_t nStartFilePos, uint64_t nChunkSize, char flags,
                         ZRuntimeResourceID ridSource, ZRuntimeResourceID ridMap,
                         const TArray<ZHashedString> &streamGroups, const int4 &radialPosition,
                         const TArray<ZString> &languages, int32_t localizationCategory);
    ~ZResourceLibraryInfo();
    void Initialize(int64_t nEntries);
    const ZRuntimeResourceID &GetSourceRuntimeResourceID() const;
    const ZRuntimeResourceID &GetMapResourceID() const;
    bool IsInitLibrary();
    bool IsEntityTypeLibrary();
    bool IsMediaStreamed();
    bool IsDynamic();
    bool IsStreamed();
    bool IsRadial();
    void SetSourceRuntimeResourceID(const ZRuntimeResourceID &rid);
    bool CanBeLoadedInCurrentConfiguration();
    bool IsLoadedInState(const int4 cell);
    bool IsLoadedInState(int32_t nState);
    bool IsCancelRequestPending();
    void CancelLoading();
    void AbortCancelRequest();
    void StartProcessingCancelRequest();
    void FinalizeCancelRequest();
    uint32_t GetStartFilePosition();
    const ZResourceID &GetResourceID() const;
    const ZRuntimeResourceID &GetRuntimeResourceID() const;
    uint32_t GetLibraryIndex();
    uint64_t GetEntryCount() const;
    const SResourceLibraryEntry &GetEntry(uint64_t nIndex) const;
    uint32_t GetEntryFilePosition(uint32_t nIndex);
    void SetEntryFilePosition(uint32_t nIndex, uint32_t nPosition);
    uint64_t GetChunkSize();
    bool IsValid(uint32_t nIndex);
    bool IsExternal(uint32_t nIndex);
    void SetEntry(uint32_t nIndex, const SResourceLibraryEntry &entry);
    void SetResourceStub(uint32_t nIndex, ZLibraryResourceStub *pStub, bool external);
    EResourceInstallerState InstallResource(uint32_t nIndex,
                                            TSharedPtr<ZResourceDataBuffer> pResourceData);
    void SetInstallProgress(int32_t nIndex);
    void ReleaseEntries();
    static ZString GetFriendlyStateName(const int4 cell);
    static ZString GetFriendlyStateName(const int32_t cell);
    int32_t GetLocalizationCategory();
    void ReleaseEntryHeaders();
    ZResourceStub *m_pLibraryStub;
    ZResourceID m_ResourceID;
    ZRuntimeResourceID m_MapResourceID;
    ZRuntimeResourceID m_SourceResourceID;
    TArray<SResourceLibraryEntry> m_Entries;
    int32_t m_nInstallIndex;
    char m_Flags;
    uint8_t m_nCancelRequest;
    uint32_t m_nStartFilePosition;
    uint64_t m_nChunkSize;
    TArray<unsigned int> m_EntryFilePositions;
    TArray<ZHashedString> m_StreamGroups;
    TArray<ZString> m_Languages;
    int32_t m_LocalizationCategory;
    int4 m_radialPosition;
};

template <typename T>
class TResourcePtr : public ZResourcePtr {
    public:
    TResourcePtr<T>(const ZRuntimeResourceID &);
    TResourcePtr<T>(TResourcePtr<T> *);
    TResourcePtr<T>(ZResourcePtr *rhs);
    TResourcePtr<T>(const TResourcePtr<T> &rhs);
    TResourcePtr<T>(const ZResourcePtr &rhs) : ZResourcePtr(rhs) {}
    TResourcePtr<T>(void *);
    TResourcePtr<T>();
    TResourcePtr<T> &operator=(ZResourcePtr::ZNull *);
    TResourcePtr<T> &operator=(TResourcePtr<T> *);
    TResourcePtr<T> &operator=(ZResourcePtr *);
    TResourcePtr<T> &operator=(const TResourcePtr<T> &);
    TResourcePtr<T> &operator=(const ZResourcePtr &);
    T *operator->() const;
    T &operator*();
    T *GetRawPointer() const;
    T *GetInstallingRawPointer();
};

class ZResourceInstallerBase : public IResourceInstaller {
    public:
    virtual ~ZResourceInstallerBase();
    virtual void *Allocate(uint64_t nSize);
    virtual bool IsStreamInstaller();
    virtual bool IsIndirectionInstaller();
    virtual bool SupportsAllocate();
    virtual void OnOrphanedResource(const ZRuntimeResourceID &resourceID, void *pResourceData,
                                    uint32_t nResourceTag);
};

class ZIDMapInstaller : public ZResourceInstallerBase {
    public:
    struct ZIDMap {
        TArray<ZRuntimeResourceID> ResourceLibraryIDs;
    };
    virtual int64_t AddRef();
    static void Release(ZIDMapInstaller::ZIDMap *pIDMap);
    virtual void Release(const ZRuntimeResourceID &resourceID, void *pResourceData,
                         uint32_t nResourceTag);
    virtual int64_t Release();
    ZIDMapInstaller();
    ZIDMapInstaller(const ZIDMapInstaller &);
    ZIDMapInstaller &operator=(const ZIDMapInstaller &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZIDMapInstaller();
    static ZIDMapInstaller::ZIDMap *Install(ZResourcePending &ResourcePending,
                                            const TArrayRef<unsigned char> &data,
                                            int64_t &installMemory);
    virtual EResourceInstallerState Install(ZResourcePending &ResourcePending);
    virtual uint32_t GetInstallerId();
};

class ZHeaderLibrary {
    public:
    ~ZHeaderLibrary();
    void ReleaseLibraries();
    void LoadLibrariesPresentIn(const int4 cell);
    void LoadLibrariesPresentIn(int32_t nNewStateIndex);
    void ReleaseLibrariesPresentIn(const int4 cell);
    void ReleaseLibrariesPresentIn(int32_t nNewStateIndex);
    void AddSharingRef();
    void ReleaseSharingRef();
    bool AreLibrariesValid();
    bool AreLibrariesReady(const int4 cell, bool bConsiderParents);
    bool AreLibrariesReady(int32_t nStateIndex, bool bConsiderParents);
    bool AreLibrariesReady() const;
    bool ContainsLibrary(const ZRuntimeResourceID &ridLibrary);
    ZResourcePtr GetSourceResource();
    ZResourcePtr GetHeaderLibResource();
    const ZString &GetParentLibraryResourceToLoad();
    uint64_t GetLibraryInfoCount() const;
    float GetRadialCellSizeInMeters();
    ZResourceLibraryInfo *GetLibraryInfo(uint64_t index) const;
    ZResourceLibraryInfo *GetLibraryInfo(const ZRuntimeResourceID &ridLibrary);
    uint64_t GetLoadedLibraryCount() const;
    uint64_t GetLoadingLibraryCount() const;
    bool IsWaitingForCancel();
    void OnCancelDone(const ZResourcePtr &pResource);
    void CancelLoadings();
    void CancelLoading(ZResourceLibraryInfo *pLibrary, ZResourcePtr pResource);
    void AbortCancellation(ZResourceLibraryInfo *pLibrary, ZRuntimeResourceID rID);
    void OnLibraryStatusChanged(const ZRuntimeResourceID &rid);
    struct SParentReference {
        SParentReference(const TResourcePtr<ZHeaderLibrary> &parent, bool shared);
        TResourcePtr<ZHeaderLibrary> headerLibrary;
        bool bIsShared;
    };
    struct SResourceLibraryReference {
        SResourceLibraryReference(uint32_t state, const ZResourcePtr &resource);
        bool IsEmpty();
        TSet<unsigned int> states;
        ZResourcePtr resourceLibrary;
    };
    struct SResourceLibraryCancellation {
        SResourceLibraryCancellation(const ZResourcePtr &resourceLib,
                                     const ZResourcePtr &headerLib);
        ZResourcePtr headerLibrary;
        ZResourcePtr resourceLibrary;
    };
    ZHeaderLibrary(const TArray<ZResourceLibraryInfo *> &libraries, ZResourceStub *pHeaderLibrary,
                   const TArrayRef<ZHeaderLibrary::SParentReference> &parentHeaderLibrairies,
                   ZIDMapInstaller::ZIDMap *pIDMap, const ZString &parentScene,
                   float radialCellSizeInMeters);
    TArray<ZHeaderLibrary::SParentReference> m_Parents;
    TArray<ZResourceLibraryInfo *> m_LibraryLoadOrder;
    TMap<ZRuntimeResourceID, ZResourceLibraryInfo *> m_LibraryLookup;
    TMap<ZRuntimeResourceID, ZHeaderLibrary::SResourceLibraryReference> m_LoadedLibraryPtrs;
    TMap<ZRuntimeResourceID, ZHeaderLibrary::SResourceLibraryReference> m_LoadingLibraryPtrs;
    TMap<ZRuntimeResourceID, ZHeaderLibrary::SResourceLibraryCancellation> m_LibraryCancellations;
    TMap<unsigned int, unsigned __int64> m_StateRefCounts;
    ZResourceStub *m_pHeaderLibraryStub;
    ZIDMapInstaller::ZIDMap *m_pIDMap;
    uint64_t m_nSharingCount;
    float m_radialCellSizeInMeters;
    ZString m_parentLibraryResourceToLoad;
};

struct SHeaderLibraryChunk {
    ZString sLibraryID;
    ZString sInstallID;
    uint32_t nOffset;
    uint64_t nChunkSize;
    char nFlags;
    TArray<ZString> sLanguages;
    uint32_t localizationCategory;
    TArray<unsigned int> streamGroups;
    int32_t x;
    int32_t y;
    int32_t z;
    TArray<unsigned char> chunkHeader;
    TArray<TArray<unsigned char>> resourceHeaders;
    TArray<unsigned int> ridMappingIndices;
    TArray<unsigned __int64> ridMappingIDs;
};

struct SHeaderLibrary {
    TArray<ZString> streamGroups;
    TArray<SHeaderLibraryChunk> chunks;
    TArray<ZString> externalResourceIds;
    int64_t ridSource;
    int64_t ridIDMapDebug;
    TArray<unsigned char> idmap;
    ZString parentScene;
    TArray<int> sharedLibraries;
    float radialCellSizeInMeters;
};

class ZHeaderLibraryInstaller : public ZResourceInstallerBase {
    public:
    virtual int64_t AddRef();
    virtual void Release(const ZRuntimeResourceID &resourceID, void *pResourceData,
                         uint32_t nResourceTag);
    virtual int64_t Release();
    ZHeaderLibraryInstaller();
    ZHeaderLibraryInstaller(const ZHeaderLibraryInstaller &);
    ZHeaderLibraryInstaller &operator=(const ZHeaderLibraryInstaller &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZHeaderLibraryInstaller();
    virtual EResourceInstallerState Install(ZResourcePending &ResourcePending);
    virtual uint32_t GetInstallerId();
    void ParseHeaderLibrary(SHeaderLibrary *pHeaderLib, TSharedPtr<ZResourceReader> resourceReader,
                            TArray<ZResourceLibraryInfo *> &createdLibraries);
    bool Validate(const TArray<ZResourceLibraryInfo *> &libraries);
    void CreateLibraryStubs(ZResourceLibraryInfo *pLibrary, const SHeaderLibraryChunk &chunk);
    void ReadResourceHeader(SResourceLibraryEntry &entry, const uint8_t *pHeader);
    void SetupResourceIDMapping(const ZRuntimeResourceID &);
};

struct SResourceInstallInfo {
    ZResourceManager *manager;
    int64_t endTick;
    bool abort;
    bool bAllowInstallersDuringUpdateOnly;
    bool bAllowInstallersNotDuringUpdate;
    uint64_t iInstallingResourcesCursor;
};

struct SResourceLibraryInstallInfo {
    ZResourceManager *manager;
};

class ZResourceManager : public IComponentInterface {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZResourceManager();
    ZResourceManager(const ZResourceManager &);
    ZResourceManager &operator=(const ZResourceManager &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    static const int64_t ResourceManagerBudgetTime;
    virtual ~ZResourceManager();
    void Deactivate();
    IResourceInstaller *GetResourceInstaller(uint32_t nResourceType) const;
    bool IsResourceLoading();
    bool IsEmpty();
    bool AreManualInstallDependenciesValid(const ZResourcePtr &pResource);
    TArray<ZResourcePtr> GetManualInstallDependenciesPtr(const ZResourcePtr &pResource);
    ZResourcePtr GetResourcePtr(const ZRuntimeResourceID &ridResource,
                                EResourceLoadPriority nPriority, bool loadStreamedDependencies);
    ZResourcePtr GetResourcePtrHeader(const ZRuntimeResourceID &ridResource,
                                      EResourceLoadPriority nPriority);
    void GetResourcePtrs(const TArrayRef<ZRuntimeResourceID> ridResources,
                         TArrayRef<ZResourcePtr> pResources, EResourceLoadPriority nPriority);
    ZResourcePtr LoadResource(const ZRuntimeResourceID &ridResource, bool loadStreamedDependencies);
    bool HasResourcePtr(const ZRuntimeResourceID &resourceID);
    void HandleResourceChanged(const ZRuntimeResourceID &resourceID);
    void RegisterResourceInstaller(IResourceInstaller *pResourceInstaller, uint32_t nResourceType);
    void UnregisterResourceInstaller(IResourceInstaller *pResourceInstaller);
    void StartInstallingResources(bool bAllowInstallersDuringUpdateOnly,
                                  bool bAllowInstallersNotDuringUpdate, int64_t iEndTick);
    void StopInstallingResources(bool bForceStop);
    void Update(bool bExecuteAnyInstalls, bool bSendStatusChangedNotifications,
                int64_t FrameStartTicks);
    void StartResourceLibraryInstallThread();
    void StopResourceLibraryInstallThread();
    void SendStatusChangedNotifications();
    void CheckIfDoneLoading();
    void Clear();
    void ReleaseUnusedResources();
    ZEvent<ZRuntimeResourceID const &, enum EResourceStatus, ZEventNull, ZEventNull, ZEventNull,
           ZEventNull> &
    GetAnyResourceStatusChangedEvent();
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetFinishedLoadingEvent();
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetTransferOutputPinEvent();
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetSignalOutputPinEvent();
    void AddStatusChangedListener(const ZRuntimeResourceID &resourceID,
                                  const ZDelegate<void(ZRuntimeResourceID const &)> &d,
                                  bool bNotifyIfResourceAlreadyLoaded);
    void RemoveStatusChangedListener(const ZRuntimeResourceID &resourceID,
                                     const ZDelegate<void(ZRuntimeResourceID const &)> &d);
    void ResourceStatusChanged(const ZRuntimeResourceID &ridResource, EResourceStatus oldStatus,
                               EResourceStatus newStatus);
    void AddPendingResource(const ZResourcePending &resourcePending);
    void ReleaseStub(ZResourceStub *stub, bool bDataOnly);
    void RaisePriority(const ZRuntimeResourceID &resourceID, EResourceLoadPriority nPriority);
    ZMutex &GetMutex();
    void WaitAndAddLoadingMemorySize(const uint32_t dataSize);
    bool TryAddLoadingMemorySize(uint32_t dataSize);
    void SubLoadingMemorySize(const uint64_t dataSize);
    void InstallMultilanguageResource(const ZResourcePtr &pResource, bool bTextResource,
                                      TArray<ZString> locales, TArray<int> indices);
    void UninstallMultilanguageResource(void *pResourceData);
    bool IsLocaleDirty();
    bool ApplyLocaleChanges();
    void SetActiveLocale(uint32_t index, const ZString &sName);
    const ZString &GetActiveLocale();
    bool IsActiveLocale(uint32_t mask);
    void SetActiveTextLocale(uint32_t index, const ZString &sName);
    const ZString &GetActiveTextLocale();
    bool IsActiveTextLocale(uint32_t mask);
    void SuspendReleasing();
    void ResumeReleasing();
    void PauseResourceReload();
    void ResumeResourceReload();
    bool IsResourceReloadPaused();
    bool IsSuspended();
    int64_t GetNumProcessing();
    bool HasWorkPending(bool bAllowedToExecuteDuringUpdate, bool bAllowedToExecuteNotDuringUpdate);
    bool DoneLoading();
    void Trim();
    void RegisterGlobalResources(const ZString &projectName,
                                 TArrayRef<ZResourcePtr *> pResourceArray);
    void AssignGlobalResource(const ZString &projectName, int32_t index,
                              const ZResourcePtr &pResource);
    void ReleaseGlobalResources();
    void DumpResourceHolders();
    struct ZResourcePendingPriorityCmp {
        bool operator()(const ZResourcePending &, const ZResourcePending &);
    };
    static int32_t
    HashSet_ZResourceStatusChangedDelegate(const ZDelegate<void(ZRuntimeResourceID const &)> &key);
    bool IsChunkDataAvailable(const ZRuntimeResourceID &rid, float &fProgress, bool bFromHDDOnly);
    void PrioritizeChunk(const ZRuntimeResourceID &rid);
    void ResourceInstallThread(SResourceInstallInfo *info);
    void ResourceLibraryInstallThread(SResourceLibraryInstallInfo *info);
    void SetHeartBeatDelegate(ZDelegate<void(void)> heartBeatDelegate);
    bool IsInstallBudgetElapsed();
    bool IsResourceLibraryInstallThreadRunning();
    bool IsInstallRestrictedToUpdate(uint32_t tag);
    bool InstallRestrictedOnly();
    void CancelLibrary(const ZResourcePtr &cancelled);
    void AbortCancelLibrary(const ZResourcePtr &abort);
    void SignalLibraryThreadEvent();
    void HeartBeat();
    ZResourcePtr GetResourcePtrInternal(const ZRuntimeResourceID &ridResource,
                                        EResourceLoadPriority nPriority,
                                        EResourceLoadStatus requiredLoadStatus,
                                        bool loadStreamedDependencies);
    bool HasPendingResourceReleases();
    void RaiseResourceStatusChanged(const ZRuntimeResourceID &resourceID);
    void ReserveStubCapacity(uint64_t nNewStubCount);
    ZLibraryResourceStub *CreateLibraryResourceStub(const ZRuntimeResourceID &resourceID,
                                                    EResourceLoadPriority nPriority, bool streamed);
    ZResourceStub *GetResourceStub(const ZRuntimeResourceID &ridResource,
                                   EResourceLoadPriority nPriority,
                                   EResourceLoadStatus requiredLoadStatus,
                                   bool loadStreamedDependencies, bool &bOutStartLoading);
    ZResourceStub *TryFindResourceStub(const ZRuntimeResourceID &resourceID);
    void GatherDependentResources(ZResourceStub *pStub, TArray<ZResourceStub *> &dependentList,
                                  int32_t nDepth);
    void ReloadResource(const ZRuntimeResourceID &resourceID);
    uint64_t NumPending();
    bool HasInstallDependencies(const ZResourcePending &pending);
    ZEvent<ZRuntimeResourceID const &, enum EResourceStatus, ZEventNull, ZEventNull, ZEventNull,
           ZEventNull>
        m_AnyResourceStatusChangedEvent;
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull>
        m_FinishedLoadingEvent;
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull>
        m_TransferOutputPinEvent;
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull>
        m_SignalOutputPinEvent;
    void UpdateWaitingResources();
    void UpdatePendingLibraries();
    void UpdatePendingResources(bool bExecuteAnyInstalls, int64_t iEndTick);
    void UpdateInstallingLibraries(bool threaded);
    void UpdateCancellingLibraries();
    void UpdateDownloadingResources();
    void UpdateChangedResources(int64_t iEndTick);
    void UpdatePendingUninstalls(int64_t iEndTick);
    void PrintStatus();
    void DrawDebugUpdate();
    struct SQueuedChangeDelegate {
        ZRuntimeResourceID rid;
        ZDelegate<void(ZRuntimeResourceID const &)> d;
        SQueuedChangeDelegate(const ZRuntimeResourceID &rid,
                              const ZDelegate<void(ZRuntimeResourceID const &)> &d);
        SQueuedChangeDelegate();
        ZResourceManager::SQueuedChangeDelegate &
        operator=(const ZResourceManager::SQueuedChangeDelegate &rhs);
        bool operator==(const ZResourceManager::SQueuedChangeDelegate &rhs);
        bool operator<(const ZResourceManager::SQueuedChangeDelegate &);
    };
    TUnorderedLinearHashMap<
        ZRuntimeResourceID,
        TUnorderedLinearHashSet<ZResourceStub *, &Hash_DefaultHash<ZResourceStub *>, 31,
                                DefaultHashAllocator> *,
        &Hash_ZRuntimeResourceID, 31, DefaultHashAllocator>
        m_Resources;
    TUnorderedLinearHashMap<unsigned int, IResourceInstaller *, &Hash_DefaultHash<unsigned int>, 31,
                            DefaultHashAllocator>
        m_ResourceInstallers;
    ZMutex m_statusChangedTableMutex;
    ZMutex m_statusChangedCallbackBarrier;
    TUnorderedLinearHashMap<
        ZRuntimeResourceID,
        TUnorderedLinearHashSet<ZDelegate<void(ZRuntimeResourceID const &)>,
                                &ZResourceManager::HashSet_ZResourceStatusChangedDelegate, 31,
                                DefaultHashAllocator> *,
        &Hash_ZRuntimeResourceID, 31, DefaultHashAllocator>
        m_statusChangedTable;
    TUnorderedLinearHashMap<
        ZRuntimeResourceID,
        TUnorderedLinearHashSet<ZDelegate<void(ZRuntimeResourceID const &)>,
                                &ZResourceManager::HashSet_ZResourceStatusChangedDelegate, 31,
                                DefaultHashAllocator> *,
        &Hash_ZRuntimeResourceID, 31, DefaultHashAllocator>
        m_frameStatusChangedTable;
    ZMutex m_queuedChangeEventsMutex;
    TUnorderedLinearHashSet<ZRuntimeResourceID, &Hash_ZRuntimeResourceID, 31, DefaultHashAllocator>
        m_queuedChangeEvents;
    static int32_t
    HashSet_SQueuedChangeDelegate(const ZResourceManager::SQueuedChangeDelegate &key);
    TUnorderedLinearHashSet<ZResourceManager::SQueuedChangeDelegate,
                            &ZResourceManager::HashSet_SQueuedChangeDelegate, 31,
                            DefaultHashAllocator>
        m_queuedChangeDelegates;
    int64_t m_nNumValid;
    int64_t m_nNumHeaderLoaded;
    int64_t m_nNumInstalling;
    int64_t m_nNumLoading;
    int64_t m_nNumFailed;
    int64_t m_nNumProcessing;
    int64_t m_nLoadTime;
    ZMutex m_IncomingResourcesLock;
    TArray<ZResourcePending> m_IncomingResources;
    ZMutex m_IncomingLibrariesLock;
    TArray<ZResourcePending> m_IncomingLibraries;
    TArray<ZResourcePending> m_InstallingLibraries;
    TArray<ZResourcePending> m_InstallingResources;
    TArray<ZResourcePending> m_WaitingResources;
    ZMutex m_CancelledLibrariesLock;
    TSet<ZResourcePtr> m_CancelledLibraries;
    uint64_t m_iInstallingLibrariesCursor;
    std::atomic<unsigned __int64> m_iInstallingResourcesCursor;
    uint64_t m_iWaitingResourcesCursor;
    int64_t m_nReleaseSuspendCount;
    TArray<ZResourceStub *> m_pendingUninstalls;
    TArray<ZResourceStub *> m_pendingDataReleases;
    int64_t m_nLoadingMemoryUsed;
    ZThreadEvent m_nLoadingMemoryUsedReduced;
    uint32_t m_nMaxLoadingMemory;
    bool m_bLibrariesAreWaiting;
    bool m_bThreadRestriction;
    uint32_t m_uiLibrariesBlocked;
    ZMutex m_ResourceManagerMutex;
    ZMutex m_LoadResourceManagerMutex;
    ZTraceMessageSlot m_resourceLoadMessage;
    uint32_t m_nActiveLocaleMask;
    uint32_t m_nActiveTextLocaleMask;
    ZString m_sActiveLocaleName;
    ZString m_sActiveTextLocaleName;
    uint32_t m_nRequestedLocaleMask;
    uint32_t m_nRequestedTextLocaleMask;
    ZString m_sRequestedLocaleName;
    ZString m_sRequestedTextLocaleName;
    TMap<ZString, TArrayRef<ZResourcePtr *>> m_GlobalResources;
    ZDelegate<void(void)> m_heartBeatDelegate;
    struct SMultiLanguageResourceInfo {
        ZResourceStub *pResourceStub;
        TArray<ZString> locales;
        TArray<int> indices;
    };
    TArray<ZResourceManager::SMultiLanguageResourceInfo> m_multiLanguageResources;
    TArray<ZResourceManager::SMultiLanguageResourceInfo> m_multiLanguageTextResources;
    void UpdateMultilanguageResource(const ZResourceManager::SMultiLanguageResourceInfo &info,
                                     const ZString &localeName);
    int64_t m_nTickToBeginReloading;
    uint32_t m_nResourceReloadPaused;
    TSet<ZRuntimeResourceID> m_ChangedResources;
    TSimpleBlockAllocator<TUnorderedLinearHashSet<
        ZDelegate<void(ZRuntimeResourceID const &)>,
        &ZResourceManager::HashSet_ZResourceStatusChangedDelegate, 31, DefaultHashAllocator>>
        m_poolChangeDelegate;
    TSimpleBlockAllocator<TUnorderedLinearHashSet<
        ZResourceStub *, &Hash_DefaultHash<ZResourceStub *>, 31, DefaultHashAllocator>>
        m_poolResourceStubTree;
    void PopulateRestrictedThreadedInstall();
    void AddRestrictedInstall(const TArray<unsigned int> &group);
    TUnorderedLinearHashMap<unsigned int, TArray<unsigned int>, &Hash_DefaultHash<unsigned int>, 31,
                            DefaultHashAllocator>
        m_restrictedThreadedInstall;
    TUnorderedLinearHashSet<unsigned int, &Hash_DefaultHash<unsigned int>, 31, DefaultHashAllocator>
        m_InstallersDuringUpdateOnly;
    TMap<IResourceInstaller *, __int64> m_installTimeStats;
    ZJobChainThread *m_pJobChain;
    ZJobThread *m_pJobs[6];
    bool m_bThreadedInstallRunning;
    bool m_bInstallBudgetElapsed;
    uint64_t m_installTimeStart;
    int64_t m_InstallBudgetEndTick;
    TArray<unsigned int> m_installationToErase;
    TArray<bool> m_installationProcessed;
    TArray<unsigned int> m_installationCurrentlyInstalling;
    ZMutex m_installationLock;
    ZAssertlock m_installationAssertLock;
    ZAssertlock m_globalResourceAssertLock;
    bool m_bInstallingLibrariesJobRunning;
    bool m_bInstallingLibrariesJobRanLastFrame;
    ZThreadEvent m_InstallingLibrariesEvent;
    ZJobChainThread *m_pJobChainRLIB;
    ZJobThread *m_pJobRLIB;
};

class ZResourceLibraryManager : public IComponentInterface {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZResourceLibraryManager();
    ZResourceLibraryManager(const ZResourceLibraryManager &);
    ZResourceLibraryManager &operator=(const ZResourceLibraryManager &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZResourceLibraryManager();
    bool IsLibraryModeEnabled() const;
    void Clear();
    void RegisterLibrarySet(ZHeaderLibrary *librarySet);
    void UnregisterLibrarySet(ZHeaderLibrary *pLibrarySet);
    void StartLoadingLibraries(uint32_t nCount);
    void FinishLoadingLibrary();
    bool IsLoading();
    ZHeaderLibrary *FindLibrarySet(const ZRuntimeResourceID &chunkLibraryId);
    uint32_t GetResourceFilePosition(const ZRuntimeResourceID &ridResource);
    ZString ResolveResourceID(ZRuntimeResourceID ridResource, ZRuntimeResourceID ridMap);
    ZString ResolveResourceID(ZRuntimeResourceID ridResource);
    void RegisterIDMappings(ZRuntimeResourceID ridLibraryResource,
                            TArrayRef<ZRuntimeResourceID> ridLibraryList,
                            TArrayRef<ZRuntimeResourceID> ridList);
    void UnregisterIDMappings(ZRuntimeResourceID ridLibraryResource);
    ZRuntimeResourceID GetOriginalRID(ZRuntimeResourceID ridLibrary);
    void RegisterMediaStreamedID(ZRuntimeResourceID ridLibrary);
    ZRuntimeResourceID FindLoadedMediaStreamedID(ZRuntimeResourceID rid);
    void LoadMediaStreamablePatches();
    ZResourceLibraryInfo *FindMediaStreamedLibrary(ZRuntimeResourceID ridLibrary);
    TArray<ZHeaderLibrary *> m_LibrarySets;
    TUnorderedLinearHashMap<ZRuntimeResourceID,
                            TArray<TPair<ZRuntimeResourceID, ZRuntimeResourceID>>,
                            &Hash_ZRuntimeResourceID, 31, DefaultHashAllocator>
        m_IDMap;
    THashSet<ZRuntimeResourceID, TDefaultHashSetPolicy<ZRuntimeResourceID>>
        m_MediaStreamedLibraryIDs;
    TUnorderedLinearHashMap<ZRuntimeResourceID, TArray<ZRuntimeResourceID>,
                            &Hash_ZRuntimeResourceID, 31, DefaultHashAllocator>
        m_MediaStreamedIDs;
    uint32_t m_nNumLoadingLibraries;
    bool m_bEnabled;
    ZSharedMutex m_IDMapMutex;
    TArray<TResourcePtr<ZHeaderLibrary>> m_MediaStreamablePatches;
};

// Windows.h decided that conditional functions are too sensible, so the file
// defined functions as macros, breaking methods...
// TODO: Figure out if we can disable this madness centrally
#undef UpdateResource

class IResourceBuildMediator {
    public:
    virtual ~IResourceBuildMediator();
    // XXX: MSVC for some reason chooses the first overload when calling the
    // second, resulting in mayhem and brokenness. To "fix" this problem, we
    // rename the overloads to something distinct. This works since they are
    // virtual methods. Just keep in mind the _ suffixes aren't there in the
    // original code
    virtual void UpdateResource_Rrid(
        const ZRuntimeResourceID &, int32_t,
        ZDelegate<void __cdecl(ZRuntimeResourceID, ZResourceStub *, int, ZString const &,
                               unsigned __int64, unsigned __int64)>);
    virtual void UpdateResource_RStub(
        ZResourceStub *, int32_t,
        ZDelegate<void __cdecl(ZRuntimeResourceID, ZResourceStub *, int, ZString const &,
                               unsigned __int64, unsigned __int64)>);
    virtual void
    UpdateResources(const TArrayRef<ZResourceStub *> &, int32_t,
                    ZDelegate<void __cdecl(ZRuntimeResourceID, ZResourceStub *, int,
                                           ZString const &, unsigned __int64, unsigned __int64)>);
    virtual void RaisePriority(const ZRuntimeResourceID &, int32_t);
    virtual void PrintStatus();
    virtual ZString GetResourceFileName(const ZRuntimeResourceID &);
    virtual void RegisterDLCFiles(const TArray<ZString> &, const ZString &);
    virtual void UnregisterDLCFiles(const ZString &);
};

class ZLocalResourceFileMediator : public IResourceBuildMediator {
    public:
    ZLocalResourceFileMediator(const ZString &runtimePath);
    ZLocalResourceFileMediator();
    virtual ~ZLocalResourceFileMediator();
    virtual void
    UpdateResource_Rrid(const ZRuntimeResourceID &ridResource, int32_t nPriority,
                        ZDelegate<void __cdecl(ZRuntimeResourceID, ZResourceStub *, int,
                                               ZString const &, unsigned __int64, unsigned __int64)>
                            callback);
    virtual void UpdateResource_RStub(
        ZResourceStub *pResource, int32_t nPriority,
        ZDelegate<void __cdecl(ZRuntimeResourceID, ZResourceStub *, int, ZString const &,
                               unsigned __int64, unsigned __int64)>
            callback);
    virtual void
    UpdateResources(const TArrayRef<ZResourceStub *> &resources, int32_t nPriority,
                    ZDelegate<void __cdecl(ZRuntimeResourceID, ZResourceStub *, int,
                                           ZString const &, unsigned __int64, unsigned __int64)>
                        callback);
    virtual void RaisePriority(const ZRuntimeResourceID &ridResource, int32_t nPriority);
    virtual void PrintStatus();
    virtual ZString GetResourceFileName(const ZRuntimeResourceID &resourceID);
    virtual void RegisterDLCFiles(const TArray<ZString> &filenames, const ZString &sDLCPath);
    virtual void UnregisterDLCFiles(const ZString &sDLCPath);
    ZString CalcResourceFileName(const ZRuntimeResourceID &ridResource);
    void GetResourceFileNameAndOffset(const ZRuntimeResourceID &ridResource, ZString &outString,
                                      uint32_t &outOffset);
    bool m_bIsSushi;
    ZString m_RuntimePath;
    TUnorderedLinearHashMap<unsigned int, ZString, &Hash_DefaultHash<unsigned int>, 31,
                            DefaultHashAllocator>
        m_FileNameToDLCPathMap;
    ZSpinlock m_FileNameToDLCPathMapMutex;
};

class LocalResourceIDsResolver {
    public:
    LocalResourceIDsResolver();
    enum EResolveResult {
        Unresolved,
        ResolvedResourceID,
        ResolvedPrettyResourceID,
    };
    LocalResourceIDsResolver::EResolveResult TryResolve(const ZRuntimeResourceID &ridResource,
                                                        ZResourceID &idResource,
                                                        bool returnPrettyResourceID);
    bool RecordMapping(const ZRuntimeResourceID &ridResource, const ZResourceID &idResource,
                       const ZResourceID &prettyIDResource);
    uint64_t UsedMemoryInBytes();
    uint64_t NumberOfEntries();
    ZSpinlock m_Mutex;
    struct SResourceIDRecord {
        SResourceIDRecord();
        SResourceIDRecord(const ZResourceID &resourceID, const ZResourceID &prettyResourceID);
        SResourceIDRecord(const ZResourceID &);
        void SetPrettyResourceID(const ZResourceID &prettyResourceID);
        const ZResourceID &GetResourceID() const;
        const ZResourceID &GetPrettyResourceID() const;
        bool HasDifferentPrettyResourceID() const;
        bool GetPrettyResourceIDResolved();
        ZResourceID m_ResourceID;
        ZResourceID m_PrettyResourceID;
        bool m_PrettyResourceIDResolved;
        bool m_PrettyResourceIDIdentical;
    };
    TUnorderedLinearHashMap<ZRuntimeResourceID, LocalResourceIDsResolver::SResourceIDRecord,
                            &Hash_ZRuntimeResourceID, 31, DefaultHashAllocator>
        m_RuntimeResourceIDsToResourceIDs;
};

class ZRuntimeResourceCacheHandle {};

struct SCachedResourceFileInfo {
    ZString fileName;
    int64_t latestVersion;
    bool dirty;
    SCachedResourceFileInfo();
};

class ZRuntimeResourceCache {
    public:
    const SCachedResourceFileInfo *GetCachedResourceFileInfo(const ZRuntimeResourceID &resourceID,
                                                             const ZString &sFileName,
                                                             int64_t nFileVersion);
    ZRuntimeResourceCacheHandle *BeginUpdateCache(const ZRuntimeResourceID &resourceID);
    void AddCacheData(ZRuntimeResourceCacheHandle *pHandle, void *pBuffer, uint64_t nSize);
    void EndUpdate(ZRuntimeResourceCacheHandle *pHandle);
    void CancelUpdate(ZRuntimeResourceCacheHandle *pHandle);
    void PrintInfo(bool);
};

class ZResourceStreamer : public IComponentInterface {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZResourceStreamer(IResourceDevice *pDevice);
    ZResourceStreamer();
    ZResourceStreamer(const ZResourceStreamer &);
    ZResourceStreamer &operator=(const ZResourceStreamer &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    enum EResourceStreamerStatus {
        OPENING,
        READY,
        INVALID,
    };
    enum EStreamType {
        FULLSCREEN_VIDEO,
        DATA,
        MEDIA,
        NUM_STREAMTYPES,
        STREAMTYPE_MASK,
        STREAMTYPE_BITS,
    };
    enum EOpenResourceFlags {
        NO_FLAG,
        HINT_SEQUENTIAL,
        HINT_HIGH_THROUGHPUT,
        DEFAULT_FLAGS,
    };
    static const int64_t INVALID_HANDLE;
    static const int32_t IO_PRIORITY_HIGH_THRESHOLD;
    static const int32_t IO_PRIORITY_HIGH;
    enum EStreamedResourceLoadPriority {
        LOAD_PRIORITY_THROTTLING,
        LOAD_PRIORITY_VERYHIGH,
        LOAD_PRIORITY_HIGHEST,
    };
    static int32_t
    ConvertPriority(ZResourceStreamer::EStreamedResourceLoadPriority nResourcePriority);
    static int32_t ConvertPriority(EResourceLoadPriority nResourcePriority);
    virtual ~ZResourceStreamer();
    bool UseResourceServer();
    void Deactivate();
    void Suspend();
    void Resume();
    void Trim();
    void Load(TArrayRef<ZResourceStub *> pResources, EResourceLoadPriority nResourcePriority);
    void Load(ZResourceStub *pResource, EResourceLoadPriority nResourcePriority);
    void LoadStreamedResource(
        const ZRuntimeResourceID &resourceID,
        ZResourceStreamer::EStreamedResourceLoadPriority nResourcePriority,
        ZDelegate<void __cdecl(ZRuntimeResourceID, ZResourceStub *, int, ZString const &,
                               unsigned __int64, unsigned __int64)>
            callback);
    void Update();
    void UpdateLoadQueue();
    int64_t OpenResource(const ZString &sFileName, int32_t prio,
                         ZResourceStreamer::EStreamType streamType, bool shareable,
                         ZResourceStreamer::EOpenResourceFlags flags);
    void CloseResource(int64_t handle);
    void ReadFromResource(int64_t handle, void *pBuffer, int64_t nPosition, uint64_t nSize,
                          const ZDelegate<void __cdecl(__int64, void *, unsigned __int64,
                                                       unsigned __int64)> &callback);
    void CancelReadRequests(int64_t handle);
    void RaisePriority(const ZRuntimeResourceID &resourceID, int32_t nPriority);
    void SuspendStreamType(ZResourceStreamer::EStreamType streamType, int32_t duration);
    void ResumeStreamType(ZResourceStreamer::EStreamType streamType);
    void RegisterDLCFiles(const TArray<ZString> &filenames, const ZString &sDLCPath);
    void UnregisterDLCFiles(const ZString &sDLCPath);
    virtual uint64_t GetDebugMaxOpenStreamCount(ZResourceStreamer::EStreamType streamType);
    virtual int64_t GetDebugOpenStreamCount(ZResourceStreamer::EStreamType streamType);
    virtual bool IsHandleAvailable(ZResourceStreamer::EStreamType streamType, int32_t priority);
    void PrintStatus();
    void AddCallback(IInputStream *cb);
    void RemoveCallback(IInputStream *cb);
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetDeviceReadFailEvent();
    void StartStreamingCapture();
    void EndStreamingCapture();
    void ReadHeaderLibHeaders();
    void RemoveDependencies(
        ZResourceStub *pStub,
        THashSet<ZRuntimeResourceID, TDefaultHashSetPolicy<ZRuntimeResourceID>> &dependencies,
        THashSet<ZRuntimeResourceID, TDefaultHashSetPolicy<ZRuntimeResourceID>> &processed);
    void GetStreamedDependencyCount(
        ZResourceStub *pStub, uint32_t &count,
        const THashSet<ZRuntimeResourceID, TDefaultHashSetPolicy<ZRuntimeResourceID>>
            &streamedResources,
        THashSet<ZRuntimeResourceID, TDefaultHashSetPolicy<ZRuntimeResourceID>> &processed);
    void AddResourceToQueue(const ZRuntimeResourceID id, ZResourceStub *pResource,
                            int32_t nPriority, const ZString &sFilename, uint64_t nFileVersion,
                            uint64_t nFileOffset);
    struct SLoadQueueEntry {
        enum ELoadStatus {
            HEADER_LOADING,
            HEADER_LOADED,
            THROTTLED_WAITING,
            DATA_LOADING,
        };
        ZString m_sFileName;
        uint64_t m_nFileOffset;
        ZResourceStub *m_pResource;
        int64_t m_ResourceHandle;
        int32_t nPriority;
        ZResourceStreamer::SLoadQueueEntry::ELoadStatus m_Status;
        SResourceHeaderHeader m_HeaderHeader;
        uint8_t *m_pReferencesChunk;
        TSharedPtr<ZResourceDataBuffer> m_pResourceData;
        bool m_bRescheduled;
        uint8_t *m_pHeaderdata;
        void SetCacheHandle(ZRuntimeResourceCacheHandle *__formal);
        ZRuntimeResourceCacheHandle *GetCacheHandle();
        SLoadQueueEntry(ZResourceStub *resource, int32_t priority, const ZString &fileName,
                        uint64_t nFileOffset);
        ~SLoadQueueEntry();
        int32_t operator<(const ZResourceStreamer::SLoadQueueEntry &);
    };
    class ZLoadQueue {
        public:
        void Enqueue(ZResourceStreamer::SLoadQueueEntry *entry);
        ZResourceStreamer::SLoadQueueEntry *Dequeue();
        void ClearEmptyQueues();
        int32_t GetTopPriority();
        bool IsEmpty();
        void PrintInfo();
        TMap<int, TQueue<ZResourceStreamer::SLoadQueueEntry *,
                         TBlockArray<ZResourceStreamer::SLoadQueueEntry *>>>
            m_QueueMap;
    };
    ZMutex m_callbackMutex;
    ZMutex &GetMutex();
    uint32_t ProcessQueue(void *__formal);
    void AcquireReferences(ZResourceStreamer::SLoadQueueEntry *pEntry, bool bLoadHeaderOnly,
                           bool bIncludeStreamedDependencies);
    void DeleteStream(ZResourceStreamer::SLoadQueueEntry *pEntry);
    void OnStreamFailed(ZResourceStreamer::SLoadQueueEntry *pEntry);
    int64_t GenerateUniqueHandle();
    void StartLoadingResourceData(ZResourceStreamer::SLoadQueueEntry *pEntry);
    void AddPendingResource(ZResourceStreamer::SLoadQueueEntry *pEntry, bool bStreamedResource);
    void OnHeaderReadComplete(ZResourceStreamer::SLoadQueueEntry *pEntry, void *buffer,
                              uint64_t nPosition, uint64_t nSize);
    void OnHeaderReadComplete(int64_t handle, void *buffer, uint64_t nPosition, uint64_t nSize);
    void OnResourceTableReadComplete(ZResourceStreamer::SLoadQueueEntry *pEntry, void *buffer,
                                     uint64_t nPosition, uint64_t nSize);
    void OnResourceTableReadComplete(int64_t handle, void *buffer, uint64_t nPosition,
                                     uint64_t nSize);
    void OnResourceDataReadComplete(int64_t handle, void *buffer, uint64_t nPosition,
                                    uint64_t nSize);
    ZResourceStreamer::SLoadQueueEntry *FindLoadQueueEntry(int64_t handle);
    IResourceDevice *m_ResourceDevice;
    TArray<IInputStream *> m_OpenResourceCallbackDispatcherQueue;
    TMap<__int64, ZResourceStreamer::SLoadQueueEntry *> m_OpenStreams;
    TQueue<ZResourceStreamer::SLoadQueueEntry *, TBlockArray<ZResourceStreamer::SLoadQueueEntry *>>
        m_StreamQueue;
    ZMutex m_OpenStreamsMutex;
    bool m_bTerminate;
    int32_t m_priorityOverride;
    ZResourceStreamer::ZLoadQueue m_Queue;
    ZThread m_QueueThread;
    ZMutex m_QueueMutex;
    ZThreadEvent m_QueueEvent;
    IResourceBuildMediator *m_pResourceBuildMediator;
    ZResourceStreamer *m_pCacheStreamer;
    ZRuntimeResourceCache m_Cache;
    TMap<ZRuntimeResourceID, TSharedPtr<unsigned char>> m_HeaderLibraryHeaders;
    bool m_bUseResourceServer;
};

class IResourceDevice {
    public:
    virtual ~IResourceDevice();
    virtual void Suspend();
    virtual void Resume();
    virtual int64_t OpenResource(const ZString &, int32_t, ZResourceStreamer::EStreamType, bool,
                                 ZResourceStreamer::EOpenResourceFlags);
    virtual void CloseResource(int64_t);
    virtual void ReadFromResource(
        int64_t, void *, int64_t, uint64_t,
        const ZDelegate<void __cdecl(__int64, void *, unsigned __int64, unsigned __int64)> &);
    virtual void CancelReadRequests(int64_t);
    virtual void SuspendStreamType(ZResourceStreamer::EStreamType, int32_t);
    virtual void ResumeStreamType(ZResourceStreamer::EStreamType);
    virtual void Trim();
    virtual uint64_t GetDebugMaxOpenHandleCount(ZResourceStreamer::EStreamType);
    virtual int64_t GetDebugOpenHandleAndRequestCount(ZResourceStreamer::EStreamType);
    virtual bool IsHandleAvailable(ZResourceStreamer::EStreamType, int32_t);
    virtual void RegisterForIsHandleAvailableChanged(const ZDelegate<void __cdecl(void)> &);
    virtual void UnregisterForIsHandleAvailableChanged(const ZDelegate<void __cdecl(void)> &);
    virtual ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetReadFailEvent();
};

class ZDeviceEventLog {
    public:
    ZDeviceEventLog();
    ~ZDeviceEventLog();
    void FrameUpdate(const SGameUpdateEvent &event);
    void WaitingRequest(uint8_t streamType);
    void ProcessingRequest(int64_t handle, int64_t requestSize, int64_t createdTicks);
    void RequestProcessed();
    struct SLogEntry {
        int64_t nStartTicks;
        int64_t nEndTicks;
        int64_t nHandle;
        int64_t nRequestCreatedTicks;
        int64_t nSize;
    };
    class SStreamTypeStatistics {
        public:
        uint32_t requestCount;
        int64_t dataSizeSum;
        int64_t latencySum;
        int64_t maxLatency;
        TSet<__int64 volatile> handles;
        int64_t waitTicks;
        int64_t processTicks;
        SStreamTypeStatistics();
    };
    void UpdateStatistics();
    void AddEntry(const ZDeviceEventLog::SLogEntry &entry);
    int32_t m_FrameCounter;
    int64_t m_nInitTicks;
    bool m_bWaiting;
    int64_t m_nWaitStartTicks;
    uint8_t m_nWaitStreamType;
    int64_t m_nCurrentHandle;
    int64_t m_nCurrentSize;
    int64_t m_nCurrentCreatedTicks;
    int64_t m_nCurrentStartedTicks;
    TArray<ZDeviceEventLog::SLogEntry> m_LogEntries;
    TFixedArray<TArray<ZString>, 3> m_StatStrings;
    ZMutex m_LogMutex;
};

class ZResourceDevice : public IResourceDevice {
    public:
    ZResourceDevice();
    virtual ~ZResourceDevice();
    virtual void Suspend();
    virtual void Resume();
    virtual int64_t OpenResource(const ZString &fileName, int32_t priority,
                                 ZResourceStreamer::EStreamType streamType, bool shareable,
                                 ZResourceStreamer::EOpenResourceFlags flags);
    virtual void CloseResource(int64_t handle);
    virtual void ReadFromResource(int64_t handle, void *pBuffer, int64_t nPosition, uint64_t nSize,
                                  const ZDelegate<void __cdecl(__int64, void *, unsigned __int64,
                                                               unsigned __int64)> &callback);
    virtual void CancelReadRequests(int64_t handle);
    virtual void SuspendStreamType(ZResourceStreamer::EStreamType streamType, int32_t duration);
    virtual void ResumeStreamType(ZResourceStreamer::EStreamType streamType);
    virtual void Trim();
    virtual uint64_t GetDebugMaxOpenHandleCount(ZResourceStreamer::EStreamType streamType);
    virtual int64_t GetDebugOpenHandleAndRequestCount(ZResourceStreamer::EStreamType streamType);
    virtual bool IsHandleAvailable(ZResourceStreamer::EStreamType streamType, int32_t priority);
    virtual void RegisterForIsHandleAvailableChanged(const ZDelegate<void __cdecl(void)> &callback);
    virtual void
    UnregisterForIsHandleAvailableChanged(const ZDelegate<void __cdecl(void)> &callback);
    virtual ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> &
    GetReadFailEvent();
    struct SDeviceRequest {
        uint32_t requestType;
        int64_t handle;
        void *buffer;
        int64_t position;
        uint64_t size;
        ZDelegate<void __cdecl(__int64, void *, unsigned __int64, unsigned __int64)> m_Callback;
        uint64_t createdTicks;
        bool shareable;
        bool bHintSequential;
        bool bHintHighThroughput;
        int32_t openPriority;
        SDeviceRequest();
    };
    void Start();
    void Stop();
    void AddRequest(const ZResourceDevice::SDeviceRequest &request);
    bool GetNextRequest(ZResourceDevice::SDeviceRequest &outRequest);
    void ProcessRequest(const ZResourceDevice::SDeviceRequest &);
    int64_t GenerateUniqueHandle(ZResourceStreamer::EStreamType streamType);
    virtual bool ProcessOpenRequest(const ZResourceDevice::SDeviceRequest &);
    virtual bool ProcessCloseRequest(const ZResourceDevice::SDeviceRequest &);
    virtual void ProcessReadRequest(const ZResourceDevice::SDeviceRequest &);
    virtual bool CanProcessAdditionalRequest(const ZResourceDevice::SDeviceRequest &request);
    virtual bool ProcessAsyncRequests();
    virtual int32_t GetPriority(const ZResourceDevice::SDeviceRequest &);
    virtual uint64_t GetMaxOpenHandleCount(ZResourceStreamer::EStreamType, int32_t);
    static ZResourceStreamer::EStreamType GetStreamType(int64_t handle);
    void InvokeCallback(const ZResourceDevice::SDeviceRequest &request, uint64_t actualSize);
    ZThreadEvent m_RequestEvent;
    int64_t m_nResourceHandleSeed;
    ZMutex m_RequestQueueMutex;
    ZMutex m_CurrentCallbackMutex;
    TFixedArray<TArray<ZResourceDevice::SDeviceRequest>, 3> m_RequestQueue;
    TFixedArray<__int64, 3> m_ReservedHandleCount;
    int64_t m_CurrentProcessingHandle;
    int64_t m_nSliceEndTicks;
    uint8_t m_CurrentStreamType;
    struct StreamStatus {
        bool bSuspended;
        int64_t nSuspendedExpiryTicks;
        int64_t nBudgetSliceTicks;
    };
    TFixedArray<ZResourceDevice::StreamStatus, 3> m_StreamsStatus;
    void UpdateScheduling();
    uint32_t ProcessRequestQueue(void *__formal);
    void CheckForUpdate();
    ZThread m_RequestThread;
    std::atomic<unsigned int> m_bSuspendRequested;
    bool m_bIsSuspended;
    bool m_bTerminateRequestThread;
    ZDeviceEventLog *m_pEventLog;
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull>
        m_isHandleAvailableChanged;
    ZEvent<ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull>
        m_ReadFailedEvent;
    enum {
        REQUEST_OPEN,
        REQUEST_CLOSE,
        REQUEST_READ,
    };
};

class ZResourceInputStream : public IInputStream {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZResourceInputStream(ZComponentCreateInfo &info);
    ZResourceInputStream(const ZResourceInputStream &);
    ZResourceInputStream &operator=(const ZResourceInputStream &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZResourceInputStream();
    virtual bool IsValid();
    virtual bool IsSeekable();
    virtual uint64_t GetLength();
    virtual uint64_t GetPosition();
    virtual uint64_t Read(void *buffer, uint64_t count);
    virtual uint64_t Seek(int64_t offset, IBaseInputStream::ESeekOrigin origin);
    virtual uint64_t Skip(uint64_t count);
    virtual void Close();
    virtual uint64_t GetAvailableSize();
    virtual uint64_t GetBufferSize();
    virtual ZString GetLastErrorMessage();
    void FillBuffer();
    void ResourceReadCallback(int64_t handle, void *buffer, uint64_t nPosition, uint64_t nSize);
    virtual void OpenCallback();
    bool IsClosing();
    void ResourceReady(const ZRuntimeResourceID id, ZResourceStub *pStub, int32_t nPriority,
                       const ZString &sFileName, uint64_t nFileVersion, uint64_t nFileOffset);
    void ParseHeader(int64_t handle, void *buffer, uint64_t nPosition, uint64_t nSize);
    ZRuntimeResourceID m_ResourceID;
    ZDelegate<void __cdecl(ZRuntimeResourceID const &, bool, IInputStream *, void *, void *)>
        m_OpenCallBack;
    int64_t m_ResourceHandle;
    bool m_bPrefetching;
    bool m_bClosing;
    bool m_bReadyHandled;
    uint64_t m_nDataStartPosition;
    uint64_t m_nDataSize;
    uint64_t m_nStreamPositionDataReadyStart;
    uint64_t m_nStreamPositionDataReadyEnd;
    uint64_t m_nStreamPositionNextWriteStart;
    uint8_t *m_pBufferStart;
    uint8_t *m_pBufferEnd;
    TArray<unsigned char> m_Buffer;
    uint64_t m_nBufferSize;
    void *m_pUserData1;
    void *m_pUserData2;
};

class ZBlockingResourceInputStream : public IInputStream {
    public:
    ZRefCount m_refcount;
    virtual int64_t Release();
    virtual int64_t AddRef();
    int64_t GetRefCount();
    ZBlockingResourceInputStream(ZComponentCreateInfo &info);
    ZBlockingResourceInputStream(const ZBlockingResourceInputStream &);
    ZBlockingResourceInputStream &operator=(const ZBlockingResourceInputStream &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZBlockingResourceInputStream();
    virtual bool IsValid() const;
    virtual bool IsSeekable() const;
    virtual uint64_t GetLength() const;
    virtual uint64_t GetPosition() const;
    virtual uint64_t Read(void *buffer, uint64_t count);
    virtual uint64_t GetChar();
    virtual uint64_t Seek(int64_t offset, IBaseInputStream::ESeekOrigin origin);
    virtual uint64_t Skip(uint64_t count);
    virtual void Close();
    virtual ZString GetLastErrorMessage();
    void ResourceReady(const ZRuntimeResourceID id, ZResourceStub *pStub, int32_t nPriority,
                       const ZString &sFileName, uint64_t fileVersion, uint64_t nFileOffset);
    void ReadComplete(int64_t handle, void *buffer, uint64_t nPosition, uint64_t nSize);
    int64_t m_Handle;
    bool m_Ready;
    ZMutex m_Mutex;
    ZThreadEvent m_Event;
    int64_t m_FilePosition;
    uint64_t m_BeginPosition;
    uint64_t m_EndPosition;
    ZString m_LastError;
    ZResourceStreamer::EStreamType m_StreamType;
};

void ImportZRuntimeResourceModule();
ZResourceManager *GetResourceManager();
ZResourceLibraryManager *GetResourceLibraryManager();
ZResourceStreamer *GetResourceStreamer();

extern "C" {
__declspec(dllimport) ZHeaderLibraryInstaller **g_pHeaderLibraryInstallerPtr;
// Unfortunately this useful global is in a C++ anonymous namespace
__declspec(dllimport) LocalResourceIDsResolver **_anonymous_namespace___g_LocalResolver;
}