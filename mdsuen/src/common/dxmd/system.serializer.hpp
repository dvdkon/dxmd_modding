// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "g2base.hpp"

class ICustomDeserializer {
	public:
	virtual void HandleCustomData(uint32_t, const void *, uint64_t, void *, uint64_t);
	virtual ~ICustomDeserializer(); // addr=0xc430
};

class ZBinaryDeserializer: public IComponentInterface {
	public:
	ZRefCount m_refcount;
	virtual int64_t Release(); // addr=0x22f90
	virtual int64_t AddRef(); // addr=0xeb80
	int64_t GetRefCount(); // addr=0x19b30
	ZBinaryDeserializer(ZComponentCreateInfo &createInfo); // addr=0xb240
	ZBinaryDeserializer(const ZBinaryDeserializer &);
	ZBinaryDeserializer &operator=(const ZBinaryDeserializer &);
	virtual ZVariantRef GetVariantRef(); // addr=0x19f50
	virtual void *QueryInterface(STypeID *iid); // addr=0x1fcc0
	const SComponentMapEntry *GetComponentInterfacesInternal(); // addr=0x19360
	void SetInputStream(IInputStream *inputStream); // addr=0x24570
	uint64_t RequiredAlignment(); // addr=0x23250
	uint64_t BytesNeededForDeserialization(); // addr=0x10b90
	void DeserializeInto(void *memory); // addr=0x13f80
	void *DeserializeInPlace(); // addr=0x13c10
	void RegisterCustomDeserializer(ICustomDeserializer *customSerializer); // addr=0x22ba0
	void UnRegisterCustomDeserializers(); // addr=0x26360
	IInputStream *stream;
	uint8_t headerBuffer[16];
	TMaxArray<ICustomDeserializer *,2> customSerializers;
};

void ImportZSerializerModule();

ZBinaryDeserializer *CreateBinaryDeserializer();