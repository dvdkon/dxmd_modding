// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include "g2base.hpp"

class ZGameTime {
	public:
	static void RegisterType(); // addr=0x45570
	static const int64_t FOREVER; // addr=0x500b8
	static const int64_t SEC; // addr=0x500c8
	static const float INV_SEC; // addr=0x52cd0
	ZGameTime(int64_t nTicks); // addr=0xe200
	ZGameTime(int32_t nSeconds); // addr=0xe190
	ZGameTime(double fSeconds); // addr=0xe1d0
	ZGameTime(float fSeconds); // addr=0xe1b0
	ZGameTime(const ZGameTime &tGameTime); // addr=0xe180
	ZGameTime(); // addr=0xe1f0
	ZGameTime &operator=(const ZGameTime &lhs); // addr=0xf720
	operator float(); // addr=0xff50
	operator __int64();
	ZGameTime &operator+=(float);
	ZGameTime &operator+=(const ZGameTime &rhs); // addr=0x10cf0
	ZGameTime &operator-=(float);
	ZGameTime &operator-=(const ZGameTime &rhs); // addr=0x10d10
	ZGameTime &operator*=(float rhs); // addr=0x10c80
	ZGameTime &operator*=(const ZGameTime &rhs); // addr=0x10c60
	ZGameTime &operator/=(float);
	ZGameTime &operator/=(const ZGameTime &rhs); // addr=0x3efc0
	ZGameTime PostMul(const ZGameTime &);
	ZGameTime PreMul(const ZGameTime &);
	ZGameTime PostDiv(const ZGameTime &);
	ZGameTime PreDiv(const ZGameTime &);
	ZGameTime PostAdd(const ZGameTime &);
	ZGameTime PreAdd(const ZGameTime &);
	ZGameTime PostSub(const ZGameTime &);
	ZGameTime PreSub(const ZGameTime &);
	float ToFloat32(); // addr=0x3c310
	bool FromString(const ZString &sSrc); // addr=0x37980
	ZString ToString(); // addr=0x3c330
	uint64_t ToCString(char *pBuffer, uint64_t nSize, const ZString &sFormat); // addr=0x3c2e0
	void HumanReadableTime(uint32_t &hours, uint32_t &mins, uint32_t &secs, uint32_t &mili); // addr=0x384b0
	ZString GetHumanReadableString(); // addr=0x37ce0
	bool IsForever(); // addr=0x39560
	static int64_t GetTicksFromSeconds(const double fSeconds); // addr=0x19460
	static int64_t GetTicksFromSeconds(const float fSeconds); // addr=0x19440
	int64_t m_nTicks;
};

struct SGameUpdateEvent {
	SGameUpdateEvent();
	void ModifyGameTime(const ZGameTime &, const ZGameTime &, const ZGameTime &, float);
	void ClearTime();
	ZGameTime m_GameTimeDelta;
	ZGameTime m_GameTimeCurrent;
	ZGameTime m_GameTimePrevious;
	ZGameTime m_RealTimeDelta;
	ZGameTime m_RealTimeCurrent;
	ZGameTime m_RealTimePrevious;
	float m_fGameTimeMultiplier;
};

void ImportZRuntimeCoreModule();