// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include <Windows.h>
#include <d3d11.h>
#include <dxgi.h>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

class CCallbackBase {
    public:
    CCallbackBase(const CCallbackBase &);
    CCallbackBase();
    virtual void Run(void *, bool, uint64_t);
    virtual void Run(void *);
    int32_t GetICallback();
    virtual int32_t GetCallbackSizeBytes();
    enum {
        k_ECallbackFlagsRegistered,
        k_ECallbackFlagsGameServer,
    };
    uint8_t m_nCallbackFlags;
    int32_t m_iCallback;
    CCallbackBase &operator=(const CCallbackBase &);
};

enum EUniverse {
    k_EUniverseInvalid,
    k_EUniversePublic,
    k_EUniverseBeta,
    k_EUniverseInternal,
    k_EUniverseDev,
    k_EUniverseMax,
};

enum EAccountType {
    k_EAccountTypeInvalid,
    k_EAccountTypeIndividual,
    k_EAccountTypeMultiseat,
    k_EAccountTypeGameServer,
    k_EAccountTypeAnonGameServer,
    k_EAccountTypePending,
    k_EAccountTypeContentServer,
    k_EAccountTypeClan,
    k_EAccountTypeChat,
    k_EAccountTypeConsoleUser,
    k_EAccountTypeAnonUser,
    k_EAccountTypeMax,
};

class CSteamID {
    public:
    CSteamID(int32_t);
    CSteamID(uint32_t);
    CSteamID(const char *, EUniverse);
    CSteamID(uint64_t ulSteamID);
    CSteamID(uint32_t, uint32_t, EUniverse, EAccountType);
    CSteamID(uint32_t, EUniverse, EAccountType);
    CSteamID();
    void Set(uint32_t, EUniverse, EAccountType);
    void InstancedSet(uint32_t, uint32_t, EUniverse, EAccountType);
    void FullSet(uint64_t, EUniverse, EAccountType);
    void SetFromUint64(uint64_t ulSteamID);
    void Clear();
    uint64_t ConvertToUint64();
    uint64_t GetStaticAccountKey();
    void CreateBlankAnonLogon(EUniverse);
    void CreateBlankAnonUserLogon(EUniverse);
    bool BBlankAnonAccount();
    bool BGameServerAccount();
    bool BPersistentGameServerAccount();
    bool BAnonGameServerAccount();
    bool BContentServerAccount();
    bool BClanAccount();
    bool BChatAccount();
    bool IsLobby();
    bool BIndividualAccount();
    bool BAnonAccount();
    bool BAnonUserAccount();
    bool BConsoleUserAccount();
    void SetAccountID(uint32_t);
    void SetAccountInstance(uint32_t);
    void ClearIndividualInstance();
    bool HasNoIndividualInstance();
    uint32_t GetAccountID();
    uint32_t GetUnAccountInstance();
    EAccountType GetEAccountType();
    EUniverse GetEUniverse();
    void SetEUniverse(EUniverse);
    bool IsValid();
    static const char *Render(uint64_t);
    const char *Render();
    void SetFromString(const char *, EUniverse);
    bool SetFromStringStrict(const char *, EUniverse);
    bool SetFromSteam2String(const char *, EUniverse);
    bool operator==(const CSteamID &);
    bool operator!=(const CSteamID &);
    bool operator<(const CSteamID &val);
    bool operator>(const CSteamID &);
    bool BValidExternalSteamID();
    union SteamID_t {
        struct SteamIDComponent_t {
            uint32_t m_unAccountID;
            uint32_t m_unAccountInstance;
            uint32_t m_EAccountType;
            EUniverse m_EUniverse;
        } m_comp;
        uint64_t m_unAll64Bits;
    };
    CSteamID::SteamID_t m_steamid;
};

struct GameOverlayActivated_t {
    enum {
        k_iCallback,
    };
    uint8_t m_bActive;
};

struct PersonaStateChange_t {
    enum {
        k_iCallback,
    };
    uint64_t m_ulSteamID;
    int32_t m_nChangeFlags;
};

struct D3DInitArgs {
    ID3D11Device *m_pDevice;
};

namespace nx {

// Unfortunately, (probably) due to ABI changes, we can't use NxApp.dll's
// std::strings. As a workaround, they're used as if they were "NxStrings"
class NxString {
    public:
    NxString(std::string str) { this->operator=(str); }

    NxString &operator=(std::string str) {
        this->len = str.size();
        this->ptr = new char[this->len];
        std::copy(str.begin(), str.end(), this->ptr);
        return *this;
    }

    char *ptr;
    size_t unk1;
    size_t len;
    size_t unk2;
};

class NxWString {
    public:
    NxWString(std::wstring str) { this->operator=(str); }

    NxWString &operator=(std::wstring str) {
        this->len = str.size();
        this->ptr = new wchar_t[this->len];
        std::copy(str.begin(), str.end(), this->ptr);
        return *this;
    }

    wchar_t *ptr;
    size_t unk1;
    size_t len;
    size_t unk2;
};

namespace Allocator {
    template <typename T>
    class NxStlAllocator : public std::allocator<T> {
        public:
        NxStlAllocator<T>(const nx::Allocator::NxStlAllocator<T> &);
        NxStlAllocator<T>();
        ~NxStlAllocator<T>();
        T *allocate(uint64_t count, const void *hint);
        void deallocate(T *data, uint64_t count);
    };

    class GameInterface {
        public:
        virtual ~GameInterface(){};
        virtual void *Allocate(uint64_t) = 0;
        virtual void Free(void *) = 0;
    };
}; // namespace Allocator

enum TextureQuality {
    kTextureQuality_Lowest,
    kTextureQuality_Low,
    kTextureQuality_Medium,
    kTextureQuality_High,
    kTextureQuality_VeryHigh,
    kTextureQuality_NumValues,
};

enum TextureFiltering {
    kTextureFiltering_Anisotropic1x,
    kTextureFiltering_Anisotropic2x,
    kTextureFiltering_Anisotropic4x,
    kTextureFiltering_Anisotropic8x,
    kTextureFiltering_Anisotropic16x,
    kTextureFiltering_NumValues,
};

enum ShadowQuality {
    kShadowQuality_Low,
    kShadowQuality_Normal,
    kShadowQuality_High,
    kShadowQuality_NumValues,
};

enum CHSQuality {
    kCHSQuality_Off,
    kCHSQuality_Medium,
    kCHSQuality_High,
    kCHSQuality_NumValues,
};

enum SSAAQuality {
    kSSAAQuality_Off,
    kSSAAQuality_Medium,
    kSSAAQuality_High,
    kSSAAQuality_NumValues,
};

enum MSAAQuality {
    kMSAAQuality_Off,
    kMSAAQuality_2x,
    kMSAAQuality_4x,
    kMSAAQuality_8x,
    kMSAAQuality_NumValues,
};

enum TessellationQuality {
    kTessellation_Off,
    kTessellation_Low,
    kTessellation_NumValues,
};

enum POMQuality {
    kPOM_Off,
    kPOM_Low,
    kPOM_Medium,
    kPOM_NumValues,
};

enum DOFQuality {
    kDOF_Off,
    kDOF_Low,
    kDOF_Medium,
    kDOF_High,
    kDOF_NumValues,
};

enum AmbientOcclusionQuality {
    kAmbientOcclusionQuality_Off,
    kAmbientOcclusionQuality_Medium,
    kAmbientOcclusionQuality_High,
    kAmbientOcclusionQuality_NumValues,
};

enum VolumetricLightingQuality {
    kVolumetricLightingQuality_Off,
    kVolumetricLightingQuality_Medium,
    kVolumetricLightingQuality_High,
    kVolumetricLightingQuality_NumValues,
};

enum SSRQuality {
    kSSRQuality_Off,
    kSSRQuality_Medium,
    kSSRQuality_High,
    kSSRQuality_NumValues,
};

enum LODQuality {
    kLOD_Lowest,
    kLOD_Low,
    kLOD_Medium,
    kLOD_High,
    kLOD_NumValues,
};

enum ForceEnableDX12AsyncCompute {
    kForceEnableDX12AsyncCompute_Disabled,
    kForceEnableDX12AsyncCompute_Enabled,
    kForceEnableDX12AsyncCompute_Auto,
    kForceEnableDX12AsyncCompute_NumValues,
};

enum Stereoscopic3DMode {
    kStereoscopic3DMode_Disabled,
    kStereoscopic3DMode_SideBySide,
    kStereoscopic3DMode_VendorSpecific,
    kStereoscopic3DMode_VR,
    kStereoscopic3DMode_NumValues,
};

struct DisplaySettings {
    DisplaySettings();
    bool IsNonExclusiveFullscreen();
    bool IsExclusiveFullscreen();
    bool IsMultiGPUEnabled();
    uint32_t GetMaxMultiGPUCount();
    uint32_t ComputeShadowMapSize(uint32_t nOriginalSize);
    uint32_t ConvertToSSAA(uint32_t nValue);
    uint32_t GetMSAASampleCount();
    float AdjustTessellationFactor(float fValue);
    float AdjustTessellationRange(float fValue);
    bool FromFile(const wchar_t *path, NxString *outErrorString);
    bool FromString(const char *str, NxString *outErrorString);
    bool ToString(NxString &outString);
    bool operator==(const nx::DisplaySettings &rhs);
    bool operator!=(const nx::DisplaySettings &rhs);
    bool m_bFullscreen;
    bool m_bExclusiveFullscreen;
    uint32_t m_nMonitor;
    int32_t m_nWindowLeft;
    int32_t m_nWindowTop;
    uint32_t m_nWindowWidth;
    uint32_t m_nWindowHeight;
    uint32_t m_nUserWindowWidth;
    uint32_t m_nUserWindowHeight;
    bool m_bMaximized;
    bool m_bMultiMonitorEnabled;
    bool m_bIsBezelCorrected;
    float m_fAspectRatio;
    uint32_t m_nFullscreenWidth;
    uint32_t m_nFullscreenHeight;
    uint32_t m_nRefreshRate;
    bool m_bVSync;
    bool m_bTripleBuffering;
    bool m_bHighPrecisionRT;
    float m_fFOVCorrection;
    float m_fNormalizedGammaDecodingPower;
    nx::TextureQuality m_nTextureQuality;
    nx::TextureFiltering m_nTextureFiltering;
    nx::ShadowQuality m_nShadowQuality;
    nx::CHSQuality m_nCHSQuality;
    bool m_bTAA;
    nx::SSAAQuality m_nSSAAQuality;
    nx::MSAAQuality m_nMSAAQuality;
    nx::TessellationQuality m_nTessellationQuality;
    nx::POMQuality m_nPOMQuality;
    nx::DOFQuality m_nDepthOfFieldQuality;
    nx::AmbientOcclusionQuality m_nAmbientOcclusionQuality;
    bool m_bMotionBlur;
    nx::VolumetricLightingQuality m_nVolumetricLightingQuality;
    bool m_bChromaticAberration;
    bool m_bSharpen;
    bool m_bBloom;
    bool m_bLensFlares;
    nx::SSRQuality m_nScreenSpaceReflectionQuality;
    bool m_bSubSurfaceScattering;
    bool m_bClothPhysics;
    nx::LODQuality m_nLevelOfDetail;
    bool m_bEnableDX12;
    bool m_bEnableDX12StablePowerState;
    bool m_bEnableDX12AsyncCompute;
    bool m_bDisableDX12BufferPooling;
    bool m_bDisableDXGISwapChain1;
    uint32_t m_nMaxMultiGPUCount;
    bool m_bDebugDevice;
    bool m_bDisableDX12Paging;
    uint32_t m_nEmulatedNodes;
    uint32_t GetEmulatedNodeCount();
    nx::ForceEnableDX12AsyncCompute m_nForceEnableDX12AsyncCompute;
    nx::Stereoscopic3DMode m_nStereoscopic3DMode;
    float m_fStereoSeparation;
    float m_fStereoPopOut;
    float m_fStereoNearSeparationMultiplier;
    bool m_bStereoscopic3DAMDSwapEyes;
    bool HasTextureQualityChanged();
    bool m_bTextureQualityChanged;
};

struct RenderRect {
    RenderRect();
    uint32_t m_nLeft;
    uint32_t m_nTop;
    uint32_t m_nWidth;
    uint32_t m_nHeight;
    float m_fAspectRatio;
    float m_fMinAspectRatio;
    float m_fMaxAspectRatio;
    uint32_t m_nParentRectID;
    bool operator==(const nx::RenderRect &other);
    bool operator!=(const nx::RenderRect &other);
};

struct InitParams {
    InitParams();
    HINSTANCE__ *m_appInstance;
    NxString m_commandLine;
    NxString m_registryPath;
    NxWString m_localDataPath;
    uint32_t m_steamAppId;
    void (*m_pPreEnforceDisplaySettingsCallback)(const nx::DisplaySettings &,
                                                 const nx::RenderRect &);
    void (*m_pEnforceDisplaySettingsCallback)(const nx::DisplaySettings &, const nx::RenderRect &);
    void (*m_pRenderThreadEnforceDisplaySettingsCallback)(const nx::DisplaySettings &,
                                                          const nx::RenderRect &);
};

namespace Steam {
    enum Result {
        Okay,
        Restarting,
        NotRunning,
        FailedToInitialize,
        NotSubscribed,
    };

    class GameInterface {
        public:
        virtual ~GameInterface();
        virtual bool ProcessResult(nx::Steam::Result);
    };

    enum Overlay {
        Profile,
        Chat,
        Trade,
        Statistics,
        Achievements,
        FriendAdd,
        FriendRemove,
        FriendRequestAccept,
        FriendRequestIgnore,
    };

    class CallbackBase : public CCallbackBase {
        public:
        virtual ~CallbackBase();
        virtual void Run(void *pvParam, bool bIOFailure, uint64_t hSteamAPICall);
        virtual void Run(void *pvParam);
        virtual void QueuedRun(void *, bool, uint64_t);
        virtual void QueuedRun(void *);
    };

    template <typename O, typename P, int64_t N>
    class Callback : public nx::Steam::CallbackBase {
        public:
        Callback<O, P, N>(O *pObj, void (*func)(P *));
        virtual ~Callback<O, P, N>();
        void Register(O *pObj, void (*func)(P *));
        void Unregister();
        void SetGameserverFlag();
        virtual void QueuedRun(void *pvParam, bool __formal, uint64_t);
        virtual void QueuedRun(void *pvParam);
        virtual int32_t GetCallbackSizeBytes();
        O *m_pObj;
        void (*m_Func)(P *);
    };
}; // namespace Steam

class INxSteam {
    public:
    virtual ~INxSteam();
    virtual bool IsRunning();
    virtual void SetGameInterface(nx::Steam::GameInterface &);
    virtual void ParseCommandLine(const char *);
    virtual void Initialize(uint32_t);
    virtual bool IsInitialized();
    virtual bool ProcessResult();
    virtual void Poll(float);
    virtual CSteamID GetProfileIdentifier();
    virtual NxString GetProfileName();
    virtual uint32_t GetGameAppId();
    virtual std::vector<CSteamID, nx::Allocator::NxStlAllocator<CSteamID>> GetFriendsList(uint64_t);
    virtual void GetFriendUsername(
        const CSteamID &,
        std::function<void __cdecl(std::basic_string<char, std::char_traits<char>,
                                                     nx::Allocator::NxStlAllocator<char>> const &)>,
        bool);
    virtual bool IsOverlayActive();
    virtual void ActivateOverlay(nx::Steam::Overlay, const CSteamID &);
    virtual bool IsBigPictureModeActive();
    virtual bool IsUsingSteamController();
    virtual void ActivateControllerActionSet(NxString &);
    virtual void QueueCallback(nx::Steam::CallbackBase *, void *, bool, uint64_t);
    virtual void QueueCallback(nx::Steam::CallbackBase *, void *);
    virtual void CancelCallbacks(nx::Steam::CallbackBase *);
};

class NxSteamImpl : public nx::INxSteam {
    public:
    NxSteamImpl();
    virtual ~NxSteamImpl();
    virtual bool IsRunning();
    virtual void SetGameInterface(nx::Steam::GameInterface &gameInterface);
    virtual void ParseCommandLine(const char *commandLine);
    virtual void Initialize(uint32_t appIdentifier);
    virtual bool IsInitialized();
    virtual bool ProcessResult();
    virtual void Poll(float deltaTime);
    virtual CSteamID GetProfileIdentifier();
    virtual NxString GetProfileName();
    virtual uint32_t GetGameAppId();
    virtual std::vector<CSteamID, nx::Allocator::NxStlAllocator<CSteamID>>
    GetFriendsList(uint64_t flags);
    virtual void GetFriendUsername(
        const CSteamID &identifier,
        std::function<void __cdecl(std::basic_string<char, std::char_traits<char>,
                                                     nx::Allocator::NxStlAllocator<char>> const &)>
            callback,
        bool bRequireNameOnly);
    virtual bool IsOverlayActive();
    virtual void ActivateOverlay(nx::Steam::Overlay overlay, const CSteamID &identifier);
    virtual bool IsBigPictureModeActive();
    virtual bool IsUsingSteamController();
    virtual void ActivateControllerActionSet(
        const std::basic_string<char, std::char_traits<char>, nx::Allocator::NxStlAllocator<char>>
            &actionSet);
    virtual void QueueCallback(nx::Steam::CallbackBase *callback, void *pvParam, bool bIOFailure,
                               uint64_t hSteamAPICall);
    virtual void QueueCallback(nx::Steam::CallbackBase *callback, void *pvParam);
    virtual void CancelCallbacks(nx::Steam::CallbackBase *callback);
    nx::Steam::Callback<nx::NxSteamImpl, GameOverlayActivated_t, 0> m_pOverlayActivated;
    void OnOverlayActivated(GameOverlayActivated_t *parameter);
    nx::Steam::Callback<nx::NxSteamImpl, PersonaStateChange_t, 0> m_pPersonaStateChange;
    void OnPersonaStateChange(PersonaStateChange_t *parameter);
    uint32_t m_gameAppId;
    nx::Steam::GameInterface *m_pGameInterface;
    nx::Steam::Result m_eResult;
    bool m_bInitialized;
    bool m_bEnabled;
    bool m_bOverlayActive;
    bool m_bBigPictureModeActive;
    std::map<CSteamID, std::function<void __cdecl(NxString const &)>, std::less<CSteamID>,
             nx::Allocator::NxStlAllocator<
                 std::pair<CSteamID const, std::function<void __cdecl(NxString const &)>>>>
        m_mUsernames;
    bool m_bControllerInitialized;
    int32_t m_nControllerConnected;
    uint64_t m_aControllerHandles[16];
    std::map<NxString, unsigned __int64, std::less<NxString>,
             std::allocator<std::pair<NxString const, unsigned __int64>>>
        m_ControllerActionSets;
    struct CallbackStruct {
        CallbackStruct(const nx::NxSteamImpl::CallbackStruct &rhs);
        CallbackStruct(nx::Steam::CallbackBase *callback, void *pvParam, bool bIOFailure,
                       uint64_t hSteamAPICall);
        CallbackStruct(nx::Steam::CallbackBase *callback, void *pvParam);
        ~CallbackStruct();
        void Run();
        nx::Steam::CallbackBase *m_callback;
        void *m_pvParam;
        bool m_extendedCall;
        bool m_bIOFailure;
        uint64_t m_hSteamAPICall;
    };
    _RTL_CRITICAL_SECTION m_lock;
    std::vector<nx::NxSteamImpl::CallbackStruct, std::allocator<nx::NxSteamImpl::CallbackStruct>>
        m_callbacks;
};

enum NxRenderFeature {
    HighPrecisionRenderTargets,
    DX12MultiGPU,
};

class INxRenderer {
    public:
    enum QuitResult {
        Pending,
        Okay,
        Cancel,
    };
    virtual ~INxRenderer() = default;
    virtual bool NxIsFirstRun() = 0;
    virtual bool IsFeatureSupported(nx::NxRenderFeature) = 0;
    virtual void NxWindowMessage(HWND__ *, uint32_t, uint64_t, int64_t) = 0;
    virtual void NxWindowActivated(bool) = 0;
    virtual nx::INxRenderer::QuitResult NxGetQuitResult() = 0;
    virtual void NxAskUserForQuitConfirmation() = 0;
    virtual void NxSettingsChanged(const nx::DisplaySettings &) = 0;
    virtual void NxInitialize(const D3DInitArgs &) = 0;
    virtual void NxSetRenderThreadParams(void *) = 0;
    virtual void NxDestroy() = 0;
};

struct NxDebugOptions {
    NxDebugOptions();
    bool m_EmulateEyefinity;
    float m_fEyefinityAspectRatio;
    bool m_bShowPrimaryMonitor;
    uint32_t m_nDisplayCountX;
    uint32_t m_nDisplayCountY;
    bool m_bEnableDX12Profiling;
    bool m_bEnableDX12PixEventMarkers;
    bool m_bPauseBenchmarkDuringQuitConfirmation;
};

enum StereoDuplicationMode {
    SDM_Off,
    SDM_RightEye,
    SDM_LeftEye,
};

class INxApp {
    public:
    virtual ~INxApp();
    virtual bool UseNixxesPCFeatures();
    virtual bool PreInit(const nx::InitParams &);
    virtual void ParseCommandLine(const char *);
    virtual bool Init(nx::INxRenderer &);
    virtual bool InitializeRendering(HWND__ *, uint64_t);
    virtual void Quit();
    virtual void Destroy();
    virtual bool IsReady();
    virtual void SetIsReady(bool);
    virtual bool Poll(float);
    virtual void JoinMessageThread();
    virtual void TerminateStatisticsThread();
    virtual bool Flush();
    virtual void FinalizeBackBuffers();
    virtual bool Present();
    virtual void BeginRenderFrame();
    virtual void EndRenderFrame();
    virtual void Shutdown();
    virtual void ShutdownAfterError();
    virtual nx::INxRenderer *GetRenderer();
    virtual const char *GetGameName();
    virtual NxString GetSafeGameName();
    virtual const wchar_t *GetGameDirectory();
    virtual const wchar_t *GetLocalDataDirectory();
    virtual uint32_t GetBuildIdentifier();
    virtual const char *GetBuildString();
    virtual bool IsRunning64Bit();
    virtual bool IsWindows10OrLater();
    virtual double GetWindowsVersion();
    virtual bool IsPausedByUser();
    virtual void RenderThreadEnforceDisplaySettings(bool);
    virtual const nx::DisplaySettings &GetDisplaySettings();
    virtual void ApplyDisplaySettings(const nx::DisplaySettings &);
    virtual bool NeedsToEnforceDisplaySettings();
    virtual bool EnforceDisplaySettings();
    virtual bool EnforceDisplaySettingsStartup();
    virtual nx::DisplaySettings ValidateSettings(const nx::DisplaySettings &);
    virtual void SetPreEnforceDisplaySettingsCallback(void (*)(const nx::DisplaySettings &,
                                                               const nx::RenderRect &));
    virtual void SetEnforceDisplaySettingsCallback(void (*)(const nx::DisplaySettings &,
                                                            const nx::RenderRect &));
    virtual void SetRenderThreadEnforceDisplaySettingsCallback(void (*)(const nx::DisplaySettings &,
                                                                        const nx::RenderRect &));
    virtual void InvalidateExclusiveFullscreen(bool, nx::Stereoscopic3DMode);
    virtual uint32_t GetNumMonitors();
    virtual const char *GetMonitorName(uint32_t);
    virtual nx::RenderRect ComputeScaleformRect(const nx::RenderRect &);
    virtual bool IsWindowActive();
    virtual bool FormatSupported(DXGI_FORMAT, uint32_t);
    virtual HWND__ *GetWindowHandle();
    virtual bool IsD3D10Forced();
    virtual bool IsD3D11Supported();
    virtual bool IsD3D12Supported();
    virtual bool IsMultiMonitorEnabled();
    virtual void GetMultiMonitorGridSize(uint32_t &, uint32_t &);
    virtual bool IsStereoscopic3DDisplayAvailable();
    virtual bool IsStereoscopic3DVRAvailable();
    virtual bool IsStereoscopic3DEnabled();
    virtual bool IsVREnabled();
    virtual nx::StereoDuplicationMode GetStereoDuplicationMode();
    virtual void SetStereoDuplicationMode(nx::StereoDuplicationMode);
    virtual uint32_t GetStereoOffset();
    virtual void SetActiveStereoEye(bool);
    virtual bool IsDesktopMultiMonitorEnabled();
    virtual bool IsDesktopMultiMonitorSurroundEnabled();
    virtual bool IsDesktopMultiMonitorEyefinityEnabled();
    virtual bool IsDesktopBezelCorrected();
    virtual bool IsMantleSupported(bool);
    virtual void ShowWindow(bool);
    virtual void ForceStereoscopic3DEnabled(bool);
    virtual bool IsForceStereoscopic3DEnabled();
    virtual const nx::NxDebugOptions &GetDebugOptions();
    virtual void SetDebugOptions(const nx::NxDebugOptions &);
    virtual void InitStatisticsClient();
    virtual void InitRTDebugSettingsClient();
    virtual void SendStatistics(bool);
    virtual bool NeedsSceneRenderTargetsUpdate(bool);
    virtual bool NeedsRecreateCachedSamplerStates(bool);
    virtual uint32_t ConvertShadowResolution(uint32_t);
    virtual uint32_t ConvertMaxAnisotropy(uint32_t);
    virtual const nx::RenderRect &GetBackBufferRect();
    virtual const nx::RenderRect &GetPrimaryScreenRect();
    virtual int32_t GetPendingExclusiveFullscreenSwitch();
    virtual int32_t GetMultiGPUCount();
    virtual bool IsBenchmarkEnabled();
    virtual const nx::InitParams &GetInitParams();
    virtual const NxString &GetGPUDriverVersion();
    virtual const NxString &GetGPUDriverDescription();
    virtual ID3D11Device *GetDummyD3D11Device();
    virtual void RecreateDummyD3D11Device(const nx::DisplaySettings &);
    virtual void GetDefaultSettings(nx::DisplaySettings &);
};

class INxLog {
    public:
    virtual ~INxLog();
    virtual bool Initialize();
    virtual bool Open();
    virtual void OpenConsoleWindow();
    virtual void Close();
    virtual const char *GetStdOutAsString();
    virtual void LogMessage(const char *);
    virtual void LogChannel(const char *, const char *, ...);
    virtual void LogChannelW(const wchar_t *, const wchar_t *, ...);
    virtual void LogMemoryUsage(const char *);
    virtual void LogOSVersion(const char *);
};

class NxLogImpl : public nx::INxLog {
    public:
    NxLogImpl();
    virtual ~NxLogImpl();
    virtual bool Initialize();
    virtual bool Open();
    virtual void OpenConsoleWindow();
    virtual void Close();
    virtual const char *GetStdOutAsString();
    virtual void LogMessage(const char *message);
    virtual void LogChannel(const char *channel, const char *format, ...);
    virtual void LogChannelW(const wchar_t *channel, const wchar_t *format, ...);
    virtual void LogMemoryUsage(const char *channel);
    virtual void LogOSVersion(const char *channel);
    bool GetWindowsVersionString(NxString &windowsStr);
    _iobuf *m_File;
    int32_t m_StdOutPrevious;
    int32_t m_StdErrPrevious;
    static const uint64_t s_StdOutBufferSize;
    char m_StdOutBuffer[1048576];
    void *m_ConsoleWindow;
    _RTL_CRITICAL_SECTION m_Lock;
    static const uint64_t s_BufferSize;
};

namespace ResourceLoader {
    class GameInterface {
        public:
        virtual ~GameInterface();
        virtual bool GetResourceIdentifier(const char *, HINSTANCE__ *&, wchar_t *&);
    };
}; // namespace ResourceLoader

class INxResourceLoader {
    public:
    virtual ~INxResourceLoader();
    virtual void SetGameInterface(nx::ResourceLoader::GameInterface &);
    virtual bool IsResourceAvailable(const char *);
    virtual void *LoadImageResource(const char *, uint32_t, int32_t, int32_t, uint32_t);
    virtual HBITMAP__ *LoadBitmapResource(const char *);
    virtual HICON__ *LoadIconResource(const char *);
    virtual HICON__ *LoadCursorResource(const char *);
};

namespace GameState {
    enum Override {
        Override_None,
        Override_On,
        Override_Off,
    };

    class GameStateDefinition {
        public:
        GameStateDefinition(const char *name, uint64_t _priority);
        const char *GetName();
        uint64_t GetPriority();
        bool IsActive();
        uint64_t GetActiveCount();
        void SetActiveMinimum(uint64_t count);
        void SetActiveMaximum(uint64_t count);
        void PushActive();
        void PopActive();
        const char *GetCursorResourceName();
        void SetCursorResourceName(const char *resourceName);
        HICON__ *GetCursorGraphic();
        void LoadCursorGraphic(nx::INxResourceLoader &loader);
        nx::GameState::Override GetShowCursorOverride();
        void SetShowCursorOverride(nx::GameState::Override value);
        nx::GameState::Override GetClipCursorOverride();
        void SetClipCursorOverride(nx::GameState::Override value);
        nx::GameState::Override GetForceShowGameOverride();
        void SetForceShowGameOverride(nx::GameState::Override value);
        nx::GameState::Override GetEnableFOVMultiplierOverride();
        void SetEnableFOVMultiplierOverride(nx::GameState::Override value);
        nx::GameState::Override GetDisableExpensivePostFiltersOverride();
        void SetDisableExpensivePostFiltersOverride(nx::GameState::Override value);
        nx::GameState::Override GetLetterBoxOverride();
        float GetLetterBoxAspectRatio();
        void SetLetterBoxOverride(nx::GameState::Override value, float fAspectRatio);
        NxString &GetChromaScheme();
        void SetChromaScheme(const char *scheme);
        const NxString &GetControllerActionSet();
        void SetControllerActionSet(const char *actionSet);
        char m_Name[128];
        uint64_t m_Priority;
        uint64_t m_Active;
        uint64_t m_ActiveMinimum;
        uint64_t m_ActiveMaximum;
        char m_CursorResourceName[128];
        HICON__ *m_CursorGraphic;
        nx::GameState::Override m_ShowCursor;
        nx::GameState::Override m_ClipCursor;
        nx::GameState::Override m_EnableFOVMultiplier;
        nx::GameState::Override m_LetterBox;
        float m_fLetterBoxAspectRatio;
        nx::GameState::Override m_ForceShowGame;
        nx::GameState::Override m_DisableExpensivePostFilters;
        NxString m_ChromaScheme;
        NxString m_ControllerActionSet;
    };
}; // namespace GameState

class INxGameState {
    public:
    virtual ~INxGameState();
    virtual void Initialize();
    virtual bool IsStateListFinalized();
    virtual void FinalizeStateList();
    virtual void AddState(const nx::GameState::GameStateDefinition &);
    virtual void Update(float);
    virtual void UpdateCursor(HWND__ *, bool);
    virtual bool IsCursorVisible();
    virtual bool IsCursorClipped();
    virtual HICON__ *GetCursorGraphic();
    virtual bool GetForceShowGame();
    virtual bool GetLetterBoxingEnabled();
    virtual bool GetDisableExpensivePostFilters();
    virtual float GetFOVMultiplier();
    virtual nx::GameState::GameStateDefinition *GetStateByName(const char *);
    virtual bool IsStateRegistered(const char *);
    virtual void PushState(const char *);
    virtual void PushState(nx::GameState::GameStateDefinition &);
    virtual void PopState(const char *, bool);
    virtual void PopState(nx::GameState::GameStateDefinition &, bool);
};

}; // namespace nx

/*struct NxAppInterfaces {
        nx::INxApp *app;
        nx::INxResourceLoader *resourceLoader;
        nx::INxAssert *assert;
        nx::INxExceptions *exceptions;
        nx::INxCallstack *callstack;
        nx::INxLog *log;
        nx::INxSettings *settings;
        nx::INxLocalization *localization;
        nx::INxMonitor *monitor;
        nx::INxDXGI *dxgi;
        nx::INxD3D *d3d;
        nx::INxADL *adl;
        nx::INxNvidia *nvidia;
        nx::INxSwapChain *swapChain;
        nx::INxSteam *steam;
        nx::INxMiniDump *miniDump;
        nx::INxFailDialog *failDialog;
        nx::INxGameWindow *gameWindow;
        nx::INxInput *input;
        nx::INxDebugKeys *debugKeys;
        nx::INxRazerChroma *razer;
        nx::INxEyeTracker *eyetracker;
        nx::INxActions *actions;
        nx::INxGameState *gameState;
        nx::INxLauncher *launcher;
        nx::INxBenchmark *benchmark;
        nx::INxConfigDialog *configDialog;
        nx::INxScaleform *scaleform;
};*/

extern "C" {
bool NxApp_Initialize(nx::InitParams *parameters);
bool NxApp_Create(nx::Allocator::GameInterface *pAllocatorOverride);
void NxApp_Destroy();
nx::INxApp *NxApp_GetApp();
nx::INxSteam *NxApp_GetSteam();
nx::INxLog *NxApp_GetLog();
nx::INxGameState *NxApp_GetGameState();
}