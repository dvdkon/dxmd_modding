// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include <stdint.h>

enum ERenderFormat {
    RENDER_FORMAT_UNKNOWN,
    RENDER_FORMAT_R32G32B32A32_TYPELESS,
    RENDER_FORMAT_R32G32B32A32_FLOAT,
    RENDER_FORMAT_R32G32B32A32_UINT,
    RENDER_FORMAT_R32G32B32A32_SINT,
    RENDER_FORMAT_R32G32B32_TYPELESS,
    RENDER_FORMAT_R32G32B32_FLOAT,
    RENDER_FORMAT_R32G32B32_UINT,
    RENDER_FORMAT_R32G32B32_SINT,
    RENDER_FORMAT_R16G16B16A16_TYPELESS,
    RENDER_FORMAT_R16G16B16A16_FLOAT,
    RENDER_FORMAT_R16G16B16A16_UNORM,
    RENDER_FORMAT_R16G16B16A16_UINT,
    RENDER_FORMAT_R16G16B16A16_SNORM,
    RENDER_FORMAT_R16G16B16A16_SINT,
    RENDER_FORMAT_R32G32_TYPELESS,
    RENDER_FORMAT_R32G32_FLOAT,
    RENDER_FORMAT_R32G32_UINT,
    RENDER_FORMAT_R32G32_SINT,
    RENDER_FORMAT_R32G8X24_TYPELESS,
    RENDER_FORMAT_D32_FLOAT_S8X24_UINT,
    RENDER_FORMAT_R32_FLOAT_X8X24_TYPELESS,
    RENDER_FORMAT_X32_TYPELESS_G8X24_UINT,
    RENDER_FORMAT_R10G10B10A2_TYPELESS,
    RENDER_FORMAT_R10G10B10A2_UNORM,
    RENDER_FORMAT_R10G10B10A2_UINT,
    RENDER_FORMAT_R11G11B10_FLOAT,
    RENDER_FORMAT_R8G8B8A8_TYPELESS,
    RENDER_FORMAT_R8G8B8A8_UNORM,
    RENDER_FORMAT_R8G8B8A8_UNORM_SRGB,
    RENDER_FORMAT_R8G8B8A8_UINT,
    RENDER_FORMAT_R8G8B8A8_SNORM,
    RENDER_FORMAT_R8G8B8A8_SINT,
    RENDER_FORMAT_B8G8R8A8_UNORM_SRGB,
    RENDER_FORMAT_R16G16_TYPELESS,
    RENDER_FORMAT_R16G16_FLOAT,
    RENDER_FORMAT_R16G16_UNORM,
    RENDER_FORMAT_R16G16_UINT,
    RENDER_FORMAT_R16G16_SNORM,
    RENDER_FORMAT_R16G16_SINT,
    RENDER_FORMAT_R32_TYPELESS,
    RENDER_FORMAT_D32_FLOAT,
    RENDER_FORMAT_R32_FLOAT,
    RENDER_FORMAT_R32_UINT,
    RENDER_FORMAT_R32_SINT,
    RENDER_FORMAT_R24G8_TYPELESS,
    RENDER_FORMAT_D24_UNORM_S8_UINT,
    RENDER_FORMAT_R24_UNORM_X8_TYPELESS,
    RENDER_FORMAT_X24_TYPELESS_G8_UINT,
    RENDER_FORMAT_R9G9B9E5_SHAREDEXP,
    RENDER_FORMAT_R8G8_B8G8_UNORM,
    RENDER_FORMAT_G8R8_G8B8_UNORM,
    RENDER_FORMAT_R8G8_TYPELESS,
    RENDER_FORMAT_R8G8_UNORM,
    RENDER_FORMAT_R8G8_UINT,
    RENDER_FORMAT_R8G8_SNORM,
    RENDER_FORMAT_R8G8_SINT,
    RENDER_FORMAT_R16_TYPELESS,
    RENDER_FORMAT_R16_FLOAT,
    RENDER_FORMAT_D16_UNORM,
    RENDER_FORMAT_R16_UNORM,
    RENDER_FORMAT_R16_UINT,
    RENDER_FORMAT_R16_SNORM,
    RENDER_FORMAT_R16_SINT,
    RENDER_FORMAT_B5G6R5_UNORM,
    RENDER_FORMAT_B5G5R5A1_UNORM,
    RENDER_FORMAT_R8_TYPELESS,
    RENDER_FORMAT_R8_UNORM,
    RENDER_FORMAT_R8_UINT,
    RENDER_FORMAT_R8_SNORM,
    RENDER_FORMAT_R8_SINT,
    RENDER_FORMAT_A8_UNORM,
    RENDER_FORMAT_R1_UNORM,
    RENDER_FORMAT_BC1_TYPELESS,
    RENDER_FORMAT_BC1_UNORM,
    RENDER_FORMAT_BC1_UNORM_SRGB,
    RENDER_FORMAT_BC2_TYPELESS,
    RENDER_FORMAT_BC2_UNORM,
    RENDER_FORMAT_BC2_UNORM_SRGB,
    RENDER_FORMAT_BC3_TYPELESS,
    RENDER_FORMAT_BC3_UNORM,
    RENDER_FORMAT_BC3_UNORM_SRGB,
    RENDER_FORMAT_BC4_TYPELESS,
    RENDER_FORMAT_BC4_UNORM,
    RENDER_FORMAT_BC4_SNORM,
    RENDER_FORMAT_BC5_TYPELESS,
    RENDER_FORMAT_BC5_UNORM,
    RENDER_FORMAT_BC5_SNORM,
    RENDER_FORMAT_BC6H_UF16,
    RENDER_FORMAT_BC6H_SF16,
    RENDER_FORMAT_BC7_TYPELESS,
    RENDER_FORMAT_BC7_UNORM,
    RENDER_FORMAT_BC7_UNORM_SRGB,
    RENDER_FORMAT_R16G16B16_FLOAT,
    RENDER_FORMAT_INDEX_32,
    RENDER_FORMAT_INDEX_16,
    RENDER_FORMAT_LE_X2R10G10B10_UNORM,
    RENDER_FORMAT_LE_X8R8G8B8_UNORM,
    RENDER_FORMAT_X16Y16Z16_SNORM,
    RENDER_FORMAT_D16_UNORM_S8_UINT,
    RENDER_FORMAT_R16_UNORM_X8_TYPELESS,
    RENDER_FORMAT_X16_TYPELESS_G8_UINT,
    RENDER_FORMAT_B10G10R10A2_UNORM,
};

class ZTextureMap {
	public:
	enum ETextureFlags {
		TEXF_SWIZZLED,
		TEXF_DEFERRED,
		TEXF_MEMORY_READY_XBOX360,
		TEXF_GAMMA,
		TEXF_EMISSIVE,
		TEXF_PLATFORM_READY,
	};
	enum EInterpretAs {
		INTERPRET_AS_COLOR,
		INTERPRET_AS_NORMAL,
		INTERPRET_AS_HEIGHT,
		INTERPRET_AS_CONE,
	};
	enum EDimensions {
		DIMENSIONS_2D,
		DIMENSIONS_CUBE,
		DIMENSIONS_VOLUME,
	};
	struct SMipLevel {
		uint32_t nWidth;
		uint32_t nHeight;
		uint32_t nSizeInBytes;
		const uint8_t *pData;
	};
	struct STextureMapHeader {
		uint32_t nTotalSize;
		uint32_t nFlags;
		uint16_t nWidth;
		uint16_t nHeight;
		uint16_t nDepth;
		uint16_t nFormat;
		uint8_t nNumMipLevels;
		uint8_t nDefaultMipLevel;
		uint8_t nInterpretAs;
		uint8_t nDimensions;
		uint8_t nMipInterpolation;
		uint32_t nIADataSize;
		uint32_t nIADataOffset;
	};
	ZTextureMap(const uint8_t *);
	uint32_t GetTotalSize() const;
	uint32_t GetWidth() const;
	uint32_t GetHeight() const;
	uint32_t GetDepth() const;
	ERenderFormat GetFormat() const;
	uint32_t GetNumMipLevels() const;
	uint32_t GetDefaultMipLevel() const;
	ZTextureMap::EInterpretAs GetInterpretAs() const;
	ZTextureMap::EDimensions GetDimensions() const;
	uint32_t GetFlags() const;
	uint8_t GetMipInterpolation() const;
	void GetMipLevel(ZTextureMap::SMipLevel *, uint32_t) const;
	const ZTextureMap::STextureMapHeader *GetHeader();
	const void *GetRawTextureData();
	const bool HasIAData();
	const uint32_t GetIADataSize();
	const uint8_t *GetIAData();
	void DeleteData();
	void SetData(const uint8_t *);
	uint32_t GetDataSize(uint32_t, uint32_t, ERenderFormat);
	const uint8_t *m_pData;
};
