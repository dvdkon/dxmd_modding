// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
// This file describes APIs not created as a part of MDsuen
#pragma once
#include <WinSock2.h>
#include <atomic>
#include <emmintrin.h>
#include <functional>
#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

class ZString;
class IType;
class IClassType;
class IEnumType;
class IContainerType;
class IArrayType;

struct STypeID {
    uint16_t flags;
    uint16_t typeNum;
    const IType *pTypeInfo;
    STypeID *pSourceType;
};

template <typename T>
class TSerializablePointer {
    public:
    TSerializablePointer<T>(const TSerializablePointer<T> &);
    TSerializablePointer<T>(T *);
    TSerializablePointer<T> &operator=(const TSerializablePointer<T> &);
    TSerializablePointer<T> &operator=(uint64_t);
    operator T *();
    T &operator*() {
        return *this->m_pointer;
    }
    T *operator->() {
        return this->m_pointer;
    }
    T *Get() {
        return this->m_pointer;
    }
    bool operator==(int32_t);
    bool operator==(T *);
    bool operator==(const TSerializablePointer<T> &);
    T *operator++(int32_t);
    T *operator++();
    T *operator--(int32_t);
    T *operator--();
    T *operator+=(const uint64_t);
    union {
        T *m_pointer;
        uint64_t m_address;
    };
};

struct SSerializablePointer {
    SSerializablePointer(void *pointer);
    operator void *();
    void *Get();
    void *m_pointer;
    uint64_t m_address;
};

class ZObjectRef {
    public:
    ZObjectRef(void *pObject, STypeID *const typeID);
    ZObjectRef();
    STypeID *GetTypeID() const;
    const IType *GetType();
    static bool CanConvert(STypeID *from, STypeID *to);
    TSerializablePointer<STypeID> m_TypeID;
    SSerializablePointer m_pData;
};

class ZVariantRef;

class ZVariant : public ZObjectRef {
    public:
    ZVariant(const char *string);
    ZVariant(ZVariant *rhs);
    ZVariant(const ZVariantRef &rhs);
    ZVariant(const ZVariant &rhs);
    ZVariant(STypeID *type);
    ZVariant(STypeID *type, const void *pData);
    ZVariant();
    void Set(STypeID *type, const void *pData);
    void Set(const ZVariantRef &rhs);
    void Set(const ZVariant &rhs);
    void Set(STypeID *type);
    ZVariant &operator=(const ZVariant &rhs);
    ZVariant &operator=(ZVariant *rhs);
    void Allocate(STypeID *type);
    void PlacementAllocate(STypeID *type, void *pMem);
    ~ZVariant();
    bool IsNull();
    void *GetData();
    void Assign(ZVariantRef rhs);
    bool operator==(const ZVariant &rhs);
    bool operator!=(const ZVariant &rhs);
    bool operator<(const ZVariant &rhs);
    bool operator>(const ZVariant &rhs);
    bool operator<=(const ZVariant &rhs);
    bool operator>=(const ZVariant &rhs);
    ZVariant operator-(const ZVariant &rhs);
    ZVariant operator+(const ZVariant &rhs);
    ZVariant operator*(const ZVariant &rhs);
    ZVariant operator/(const ZVariant &rhs);
    void Clear();
    void ClearNoFree();
    bool FromString(const ZString &sData);
    bool FromString(STypeID *type, const ZString &sData);
    ZVariant &Get();
    static const ZVariant &GetEmpty();
    ZString ToString(const ZString &sFormat);
    ZString ToString();
    int64_t ToCString(char *pBuffer, uint64_t nSize, const ZString &sFormat);
    bool IsSmallTypeSize(STypeID *type);
    bool IsSmallTypeSize();
    void DestroyNoFree();
    void Destroy();
    static const ZVariant g_EmptyVariant;
};

class ZVariantRef : public ZObjectRef {
    public:
    ZVariantRef(const char *string);
    ZVariantRef(const ZVariant &rhs);
    ZVariantRef(const ZVariantRef &rhs);
    ZVariantRef(void *pObject, STypeID *const typeID);
    ZVariantRef();
    ZString ToString(const ZString &sFormat);
    ZString ToString();
    int64_t ToCString(char *pBuffer, uint64_t nSize, const ZString &sFormat);
    bool FromString(const ZString &sData);
    void *GetData();
    bool IsNull();
    void Assign(ZVariantRef rhs);
};

template <typename T>
class TIterator {
    public:
    TIterator<T>(T *p) : m_pCurrent(p) {}
    TIterator<T>() {}
    TIterator<T> &operator=(const TIterator<T> &);
    T &operator*() { return *m_pCurrent; }
    T *operator->() { return m_pCurrent; }
    bool operator==(const TIterator<T> &rhs) { return rhs.m_pCurrent == m_pCurrent; }
    bool operator!=(const TIterator<T> &rhs) { return rhs.m_pCurrent != m_pCurrent; }
    T *m_pCurrent;
};

template <typename T>
class TArrayIterator : public TIterator<T> {
    public:
    TArrayIterator<T>(const TArrayIterator<T> &);
    TArrayIterator<T>(const T *ptr) { this->m_pCurrent = ptr; }
    TArrayIterator<T>(T *ptr) { this->m_pCurrent = ptr; }
    TArrayIterator<T>();
    operator TArrayIterator<T>();
    TArrayIterator<T> operator++(int32_t) {
        auto copy = *this;
        this->m_pCurrent++;
        return copy;
    };
    TArrayIterator<T> &operator++() {
        this->m_pCurrent++;
        return *this;
    }
    TArrayIterator<T> operator--(int32_t);
    TArrayIterator<T> &operator--();
    TArrayIterator<T> &operator+=(int64_t);
    TArrayIterator<T> &operator-=(int64_t);
    TArrayIterator<T> operator+(int64_t);
    TArrayIterator<T> operator-(int64_t);
};

template <typename T>
class TEnumeratorBase {
    public:
    static void RegisterType();
    TEnumeratorBase<T>(const TEnumeratorBase<T> &);
    TEnumeratorBase<T>(T *pCurrent, T *pEnd);
    TEnumeratorBase<T>();
    T &Current();
    T *m_pCurrent;
    T *m_pEnd;
};

class IEnumeratorImpl {
    public:
    virtual void *GetNext(const void *);
    virtual void Release();
    virtual ~IEnumeratorImpl();
};

template <typename T>
class TArrayEnumerator : public TEnumeratorBase<T> {
    public:
    TArrayEnumerator<T>(T *pBeforeFirst, T *pEnd);
    operator TArrayEnumerator<T const>();
    bool MoveNext();
    class ZEnumeratorImpl : public IEnumeratorImpl {
        public:
        virtual void *GetNext(const void *pCurrent);
        virtual void Release();
        static TArrayEnumerator<T>::ZEnumeratorImpl s_impl;
    };
    operator TArrayEnumerator<T>();
};

template <typename T, typename D>
class TFixedArrayBase : public D {
    public:
    TFixedArrayBase<T, D>(T *pStart, uint64_t uiCount);
    TFixedArrayBase<T, D>();
    bool operator==(const TFixedArrayBase<T, D> &);
    const T &operator[](uint64_t nIndex) const;
    T &operator[](uint64_t);
    TArrayIterator<T const> Begin() const;
    TArrayIterator<T> Begin();
    TArrayIterator<T const> End() const;
    TArrayIterator<T> End();
    const T &Front() const;
    T &Front();
    const T &Back() const;
    T &Back();
    bool IsEmpty();
    uint64_t Size();
    TArrayEnumerator<T const> GetEnumerator() const;
    TArrayEnumerator<T> GetEnumerator();
    TArrayIterator<T const> Find(const T &) const;
    TArrayIterator<T> Find(const T &);
    uint64_t FindIndex(const T &);
    void Prefetch();
    const T *GetPointer() const;
    T *GetPointer();
    const T &GetElementUnsafe(const uint64_t) const;
    T &GetElementUnsafe(const uint64_t);
};

template <typename T, size_t S>
struct ZFixedArrayData {
    uint8_t m_pStart[S];
    uint8_t *GetEnd();
    const uint8_t *GetEnd() const;
};

struct STemplatedTypeName1WithSizetConstant {
    STypeID *arg0;
    uint64_t arg1;
    char argCount;
    char name[255];
};

template <typename T, size_t S>
class TFixedArray : public TFixedArrayBase<T, ZFixedArrayData<T, S>> {
    public:
    enum { ArraySize };
    static STemplatedTypeName1WithSizetConstant s_typeName;
    static IContainerType s_typeInfo;
    TFixedArray<T, S>(TFixedArray<T, S> *rhs);
    TFixedArray<T, S>(const TFixedArray<T, S> &);
    TFixedArray<T, S>();
    TFixedArray<T, S> &operator=(TFixedArray<T, S> *);
    TFixedArray<T, S> &operator=(const TFixedArray<T, S> &);
    void Clear();
    void Fill(const uint8_t &);
};

template <typename T>
struct ZArrayRefData {
    ZArrayRefData<T>(const uint8_t *pStart, uint64_t uiCount);
    const uint8_t *m_pStart;
    const uint8_t *GetEnd();
    const uint8_t *GetConstEnd();
    uint32_t m_uiCount;
};

struct STemplatedTypeName1 {
    STypeID *arg0;
    char argCount;
    char name[255];
};

template <typename T>
class TArrayRef : public TFixedArrayBase<T, ZArrayRefData<T>> {
    public:
    static IArrayType s_typeInfo;
    static STemplatedTypeName1 s_typeName;
    TArrayRef<T>(const TArrayRef<T> &);
    TArrayRef<T>(T *pStart, uint64_t nSize);
    TArrayRef<T>();
    operator class TArrayRef<T>();
    TArrayRef<T> &operator=(const TArrayRef<T> &);
    void Assign(const TArrayRef<T> &);
    void Insert(T *);
    void Insert(const T &);
    void Clear();
};

template <typename T>
class TArray {
    public:
    static STemplatedTypeName1 s_typeName;
    static IArrayType s_typeInfo;
    TArray<T>(TArray<T> *);
    TArray<T>(const TArrayRef<T const> &);
    TArray<T>(const TArrayRef<T> &);
    TArray<T>(const TArray<T> &rhs);
    TArray<T>(uint64_t ninitialCapacity);
    ~TArray<T>() {
        if(this->m_pStart.Get() != NULL) {
            for(size_t i = 0; i < this->m_uiCount; i++) {
                this->m_pStart.Get()[i].~T();
            }
            GetMemoryManager()->Free(this->m_pStart.Get());
        }
    }
    TArray<T> &operator=(TArray<T> *);
    TArray<T> &operator=(const TArrayRef<T const> &);
    TArray<T> &operator=(const TArrayRef<T> &);
    TArray<T> &operator=(const TArray<T> &rhs);
    bool operator==(const TArray<T> &);
    bool operator!=(const TArray<T> &);
    const T &operator[](uint64_t nIndex) const;
    T &operator[](uint64_t nIndex);
    operator class TArrayRef<T>();
    operator class TArrayRef<T>() const;
    operator class TArrayRef<T const>();
    TArrayIterator<T const> Begin() const { return TArrayIterator<T const>(m_pStart.m_pointer); }
    TArrayIterator<T> Begin() { return TArrayIterator<T>(m_pStart.m_pointer); }
    TArrayIterator<T const> End() const {
        return TArrayIterator<T const>(m_pStart.m_pointer + m_uiCount);
    }
    TArrayIterator<T> End() { return TArrayIterator<T>(m_pStart.m_pointer + m_uiCount); }
    // Convenience methods for range-based for loop
    TArrayIterator<T const> begin() const { return Begin(); }
    TArrayIterator<T> begin() { return Begin(); }
    TArrayIterator<T const> end() const { return End(); }
    TArrayIterator<T> end() { return End(); }
    const T &Front() const;
    T &Front();
    const T &Back() const;
    T  &Back();
    bool IsEmpty();
    uint64_t Size() const {
        return this->m_uiCount;
    }
    uint64_t Capacity() const {
        return this->m_uiCapacity;
    }
    TArrayEnumerator<T const> GetEnumerator() const;
    TArrayEnumerator<T> GetEnumerator();
    TArrayIterator<T const> Find(const T &) const;
    TArrayIterator<T> Find(const T &);
    TArrayIterator<T const> GetIterator(uint64_t) const;
    TArrayIterator<T> GetIterator(uint64_t);
    uint64_t FindIndex(const T &);
    bool Contains(const T &);
    void Assign(const TArrayRef<T const> &);
    void Assign(const TArrayRef<T> &);
    void Assign(const TArray<T> &rhs);
    void Clear();
    void Swap(TArray<T> &);
    void Reserve(uint64_t nNewcapacity, bool bAllowReduce);
    void Resize(uint64_t nSize, const T &element);
    void Resize(uint64_t);
    void ResizeGrowNoConstruct(uint64_t);
    void Trim();
    void ResizeUnsafe(uint64_t);
    void ReserveUnsafe(uint64_t);
    T *Grow(T);
    T *GrowUnsafe(T);
    T *PushBack(T *);
    T *PushBack(const TArrayRef<T const> &);
    T *PushBack(const T &);
    T *PushBackRange(const T *, uint64_t);
    void PopBack();
    TArrayIterator<T> Insert(const TArrayIterator<T> &, T *);
    TArrayIterator<T> Insert(const TArrayIterator<T> &where, const T &element);
    TArrayIterator<T> Insert(T *);
    TArrayIterator<T> Insert(const T &);
    TArrayIterator<T> InsertAt(uint64_t, T *);
    TArrayIterator<T> InsertAt(uint64_t, const T &);
    void Erase(uint64_t);
    TArrayIterator<T> Erase(const TArrayIterator<T> &, const TArrayIterator<T> &);
    TArrayIterator<T> Erase(const TArrayIterator<T> &where);
    void EraseSwap(uint64_t);
    TArrayIterator<T> EraseSwap(const TArrayIterator<T> &);
    void Sort();
    void Prefetch();
    T *GetPointer();
    const T *GetPointer() const;
    const T &GetElementUnsafe(uint64_t);
    T &GetElementUnsafe(uint64_t) const;
    ZString ToString();
    uint64_t ToCString(char *, uint64_t, const ZString &);
    void InitFromArray(const TArrayRef<T const> &rhs);
    bool CanUsePtrAsStorage();
    bool IsUsingPtrAsStorage();
    T *GetStart();
    T *PushBackResizeHelper(uint64_t *);
    T *PushBackResizeHelper(const T &);
    T *Allocate(uint64_t nCount);
    void Free(T *pStart);
    void Cleanup();
    void DestroyElements();
    void ClearMembers();
    void AssignMembers(const TArray<T> &);
    TSerializablePointer<T> m_pStart;
    uint32_t m_uiCount;
    uint32_t m_uiCapacity : 31;
    uint32_t m_bIsUsingPtrAsStorage : 1;
};

class ZString {
    public:
    static const uint32_t STATIC_STRING;
    static const uint32_t VOLATILE_STRING;
    static const uint32_t FLAGS_MASK;
    ZString(uint64_t);
    ZString(ZString *rhs);
    ZString(const ZString &rhs);
    // Added for convenience, DOES NOT COPY!
    ZString(const char *str) : m_chars(const_cast<char *>(str)), m_capacity(0) {
        m_length = strlen(str) | 0x80000000;
    }
    ZString(const std::string &str) : ZString() {
        this->Reserve(str.size() + 1);
        this->m_chars.Get()[str.size()] = '\0';
        memcpy(this->m_chars.Get(), str.data(), str.size());
        this->m_length = str.size();
    }
    operator std::string() const {
        // Not returning directly seems to fix some weird "free"-related bug?
        std::string s(m_chars.m_pointer, m_chars.m_pointer + Length());
        return s;
    }
    operator const char *() const { return m_chars.m_pointer; }
    ZString();
    ~ZString();
    static ZString ReferenceCString(const char *pStringLiteral, uint64_t nLength);
    static ZString ReferenceCString(const char *pStringLiteral);
    static ZString AllocateFromCString(const char *rhs, uint64_t maxLength);
    static ZString AllocateFromCString(const char *rhs);
    void FromCArray(const char *rhs, uint64_t length);
    void FromCString(const char *rhs, uint64_t maxLength);
    void FromCString(const char *rhs);
    static ZString AllocateFromCArray(const char *rhs, uint64_t length);
    ZString &GetCStringCopy(const char *rhs);
    void PlacementReserve(char *ptr, uint64_t capacity);
    void Reserve(uint64_t newCapacity);
    void ReserveFromLength(uint64_t length);
    ZString &operator=(ZString *rhs);
    ZString &operator=(const ZString &rhs);
    bool operator==(int32_t) const;
    bool operator==(const char *rhs) const;
    bool operator==(const ZString &rhs) const;
    bool operator!=(int32_t);
    bool operator!=(const char *rhs);
    bool operator!=(const ZString &rhs);
    bool operator<(const char *rhs);
    bool operator<(const ZString &rhs);
    bool operator>(const char *rhs);
    bool operator>(const ZString &rhs);
    char operator[](uint64_t index);
    ZString &operator+=(const char rhs);
    ZString &operator+=(const char *rhs);
    ZString &operator+=(const ZString &rhs);
    bool IsEmpty() const;
    uint64_t Length() const;
    void Clear();
    uint64_t IndexOf(const char *rhs, uint64_t fromIndex);
    uint64_t IndexOf(const ZString &rhs, uint64_t fromIndex);
    uint64_t IndexOf(const char *rhs);
    uint64_t IndexOf(const ZString &rhs);
    uint64_t IndexOf(char c, uint64_t fromIndex);
    uint64_t IndexOf(char c);
    uint64_t LastIndexOf(const char *rhs);
    uint64_t LastIndexOf(const ZString &rhs);
    uint64_t LastIndexOf(char c, uint64_t fromIndex);
    uint64_t LastIndexOf(char c);
    uint64_t IIndexOf(const char *rhs, uint64_t fromIndex);
    uint64_t IIndexOf(const ZString &rhs, uint64_t fromIndex);
    uint64_t IIndexOf(const char *rhs);
    uint64_t IIndexOf(const ZString &rhs);
    bool StartsWith(char rhs);
    bool StartsWith(const ZString &rhs);
    bool IStartsWith(const ZString &rhs);
    bool EndsWith(char rhs);
    bool EndsWith(const ZString &rhs);
    bool IEndsWith(const ZString &rhs);
    int32_t ICompare(const ZString &rhs);
    int32_t IWCompare(const ZString &rhs);
    ZString Substring(uint64_t start, uint64_t length);
    ZString SubstringUnicode(uint64_t start, uint64_t length);
    bool Contains(const ZString &rhs, bool caseSensitive);
    ZString &Append(const char rhs);
    ZString &Append(const char *rhs);
    ZString &Append(const ZString &rhs);
    ZString &Prepend(const char rhs);
    ZString &Prepend(const char *rhs);
    ZString &Prepend(const ZString &rhs);
    ZString &Replace(char match, char replacement);
    ZString &Replace(const ZString &match, const ZString &replacement);
    ZString CopyReplace(char match, char replacement);
    ZString CopyReplace(const ZString &match, const ZString &replacement);
    ZString &ReplaceChar(uint64_t index, char replacement);
    ZString CopyReplaceChar(uint64_t index, char replacement);
    ZString &Remove(uint64_t nStart, uint64_t nLength);
    ZString CopyRemove(uint64_t nStart, uint64_t nLength);
    ZString &Trim();
    ZString CopyTrim();
    ZString &Insert(const ZString &sData, uint64_t nPosition);
    ZString CopyInsert(const ZString &sData, uint64_t nPosition);
    ZString &Reverse();
    ZString CopyReverse();
    ZString &ToLower();
    ZString CopyToLower();
    bool IsLower();
    ZString &ToUpper();
    ZString &ToUpperUnicode();
    uint64_t GetUnicodeLength();
    ZString CopyToUpper();
    ZString CopyToUpperUnicode();
    bool IsUpper();
    const char *ToCString() const;
    static ZString Format(const ZString &sFormat, const ZVariantRef &a0__, const ZVariantRef &a1__,
                          const ZVariantRef &a2__, const ZVariantRef &a3__, const ZVariantRef &a4__,
                          const ZVariantRef &a5__, const ZVariantRef &a6__, const ZVariantRef &a7__,
                          const ZVariantRef &a8__, const ZVariantRef &a9__,
                          const ZVariantRef &a10__);
    void FormatInplace(const ZString &sFormat, const ZVariantRef *aArgs, uint32_t nArgSize);
    void FormatInplace(const ZString &sFormat, const ZVariantRef &a0__, const ZVariantRef &a1__,
                       const ZVariantRef &a2__, const ZVariantRef &a3__, const ZVariantRef &a4__,
                       const ZVariantRef &a5__, const ZVariantRef &a6__, const ZVariantRef &a7__,
                       const ZVariantRef &a8__, const ZVariantRef &a9__, const ZVariantRef &a10__);
    static ZString FormatOld(const ZString &sFormat, const ZVariantRef *aArgs, uint32_t nArgs);
    TArray<ZString> Split(char delimiter) const;
    uint64_t FindMatchingParentheses(uint64_t startIndex, const ZString &parentheses);
    uint64_t FindNextSplit(uint64_t startIndex, char seperator, const ZString &parentheses);
    TArray<ZString> SplitWithParentheses(uint64_t startIndex, char seperator,
                                         const ZString &parentheses);
    static ZString Join(const ZString &sSeparator, TArrayRef<ZString> aValues);
    static ZString FromWideChars(const wchar_t *pWCString);
    const ZString &ToString();
    uint32_t GetHashCode();
    uint32_t GetCaseInsensitiveHashCode();
    static const uint64_t npos;
    static const ZString Empty;
    void Free();
    TSerializablePointer<char> m_chars;
    uint32_t m_length;
    uint32_t m_capacity;
};

struct STypeFunctions {
    void (*placementConstruct)(void *);
    void (*placementCopyConstruct)(void *, const void *);
    void (*destruct)(void *);
    void (*assign)(void *, const void *);
    void (*save)(const void *, ZVariant &, int32_t &);
    void (*load)(void *, const ZVariantRef &, int32_t);
    void (*initEntity)(void *);
    void (*removeFromScene)(void *);
    bool (*equal)(const void *, const void *);
    bool (*smaller)(const void *, const void *);
    void (*minus)(const void *, const void *, void *);
    void (*plus)(const void *, const void *, void *);
    void (*mult)(const void *, const void *, void *);
    void (*div)(const void *, const void *, void *);
};

class IType {
    public:
    const char *GetTypeName();
    STypeID *GetTypeID();
    void PlacementConstruct(void *pObject, const void *pSource);
    void PlacementConstruct(void *pObject);
    void Destruct(void *pObject);
    void Assign(void *pDestination, const void *pSource);
    void Assign(void *pObject, ZVariantRef rhs);
    bool FromString(void *pObject, const ZString &sSource);
    ZString ToString(const void *pObject, const ZString &sFormat);
    ZString ToString(const void *pObject);
    int64_t ToCString(char *pBuffer, uint64_t nSize, const void *pObject, const ZString &sFormat);
    void *Construct();
    void DestructAndFree(void *pObject);
    bool OperatorEqual(const void *a, const void *b);
    bool OperatorSmaller(const void *a, const void *b);
    void OperatorMinus(const void *a, const void *b, void *pResult);
    void OperatorPlus(const void *a, const void *b, void *pResult);
    void OperatorMult(const void *a, const void *b, void *pResult);
    void OperatorDiv(const void *a, const void *b, void *pResult);
    bool CustomSaveData(const void *, ZVariant &, int32_t &);
    bool CustomLoadData(void *, const ZVariantRef &, int32_t);
    bool HasCustomSaveFunctions();
    uint32_t GetTypeSize();
    uint8_t GetTypeAlignment();
    bool IsTEntityRef();
    bool IsTAcccessorRef();
    bool IsTArrayElementRef();
    bool IsTResourcePtr();
    bool IsIEnumType();
    bool IsTFixedArrayType();
    const IClassType *QueryClassInterface() const;
    const IEnumType *QueryEnumType() const;
    const IContainerType *QueryContainerType() const;
    const IArrayType *QueryArrayType() const;
    const STypeFunctions *m_pTypeFunctions;
    uint16_t m_nTypeSize;
    uint8_t m_nTypeAlignment;
    uint16_t m_nTypeInfoFlags;
    const char *pszTypeName;
    STypeID *typeID;
    bool (*fromString)(void *, const IType *, const ZString &);
    int64_t (*toString)(const void *, const IType *, char *, uint64_t, const ZString &);
};

struct SContainerTypeVTable {
    void *(*begin)(void *);
    void *(*end)(void *);
    void *(*next)(void *, void *);
    uint64_t (*getSize)(void *);
    void (*setDeserializedElement)(void *, ZVariantRef, int32_t);
    void (*clear)(void *);
};

class IContainerType {
    public:
    IType type;
    STypeID *elementType;
    SContainerTypeVTable *pVTab;
    STypeID *GetElementType();
    void *Begin(void *pContainer);
    void *End(void *);
    void *Next(void *, void *);
    uint64_t GetSize(void *pContainer);
    void SetDeserializedElement(void *, ZVariantRef, int32_t);
    void Clear(void *);
};

class IArrayType {
    public:
    IContainerType containerType;
    uint64_t GetSize(void *);
    void SetSize(void *pContainer, uint64_t nSize);
    STypeID *GetElementType();
    ZVariantRef GetElement(void *pContainer, uint64_t nIndex);
    void SetElement(void *, uint64_t, ZVariantRef);
    void (*setSize)(void *, uint64_t);
};

struct SPropertyInfo {
    bool IsPropertyEverSavable();
    bool IsPropertyLoadable();
    STypeID *m_Type;
    uint64_t m_nExtraData;
    uint32_t m_Flags;
    void (*m_PropertySetCallBack)(void *, void *, uint64_t, bool);
    void (*m_PropertyGetCallBack)(void *, void *, STypeID *, uint64_t);
};

struct SNamedPropertyInfo {
    const char *m_pszPropertyName;
    uint32_t m_nPropertyID;
    const char *GetPropertyName();
    SPropertyInfo m_propertyInfo;
};

struct SBaseClassInfo {
    STypeID *m_Type;
    uint64_t m_nOffset;
};

class ZPinFunctor {
    public:
    ZPinFunctor();
    void Invoke(void *, const ZVariantRef &, uint64_t);
    bool IsArgumentValid(void *, const ZVariantRef &, uint64_t);
    bool IsValid();
    bool operator==(const ZPinFunctor &);
    bool operator!=(const ZPinFunctor &);
    void (*pfInvoke)(const void *, void *, const ZVariantRef &, uint64_t);
    bool (*pfIsArgumentValid)(const void *, void *, const ZVariantRef &, uint64_t);
    char aData[16];
};

struct SInputPinEntry {
    ZString m_sPinName;
    STypeID *m_Type;
    ZPinFunctor m_functor;
};

class IMethod {
    public:
    virtual ~IMethod();
    virtual void Call(void *, ZVariant &, const TArrayRef<ZVariantRef>);
    virtual const TArrayRef<STypeID *const> GetArgumentTypes();
};

class ZConstructorInfo {
    public:
    ~ZConstructorInfo();
    TArrayRef<STypeID *const> GetArgumentTypes();
    bool IsViable(const TArrayRef<STypeID *> argtypes);
    bool IsViable(const TArrayRef<ZVariantRef>);
    void Call(void *pObject, const TArrayRef<ZVariantRef> args);
    const IMethod &GetMethod();
    char MethodImpl[8];
};

struct SComponentMapEntry {
    STypeID *type;
    uint64_t nOffset;
};

class ZPropertyRef {
    public:
    ZPropertyRef(void *pObject, const SNamedPropertyInfo &PropertyInfo);
    ZPropertyRef();
    operator bool();
    STypeID *GetPropertyTypeID();
    const IType *GetPropertyType();
    bool IsRuntimeEditableOrConstAfterStart();
    void *GetData();
    void *m_pObject;
    const SNamedPropertyInfo *m_pPropertyInfo;
};

class ZConstructorRef {
    public:
    ZConstructorRef(TArrayRef<ZConstructorInfo const> aConstructors);
    ZConstructorRef();
    uint64_t GetNumberOfConstructors();
    TArrayRef<STypeID *const> GetArgumentTypes(uint32_t);
    bool Call(void *, const TArrayRef<ZVariantRef>);
    void Call(void *pObject, uint32_t nIndex, const TArrayRef<ZVariantRef> args);
    int32_t GetBestViable(const TArrayRef<STypeID *> argtypes);
    int32_t GetBestViable(const TArrayRef<ZVariantRef>);
    TArrayRef<ZConstructorInfo const> m_aConstructors;
};

class IClassType {
    public:
    IType type;
    uint16_t m_nProperties;
    uint16_t m_nConstructors;
    uint16_t m_nBaseClasses;
    uint16_t m_nInterfaces;
    uint16_t m_nInputPins;
    const SNamedPropertyInfo *m_pProperties;
    const ZConstructorInfo *m_pConstructors;
    const SBaseClassInfo *m_pBaseClasses;
    const SComponentMapEntry *m_pInterfaces;
    const SInputPinEntry *m_pInputPins;
    uint32_t GetTypeSize();
    uint32_t GetTypeAlignment();
    STypeID *GetTypeID();
    void PlacementConstruct(void *pObject, const void *pSource);
    void PlacementConstruct(void *pObject);
    void Destruct(void *pObject);
    const char *GetTypeName();
    TArrayRef<SNamedPropertyInfo const> GetProperties();
    TArrayRef<SComponentMapEntry const> GetInterfaces();
    TArrayRef<SInputPinEntry const> GetInputPins();
    uint32_t GetNumberOfProperties();
    uint32_t GetNumberOfInputPins();
    ZPropertyRef GetPropertyRef(void *pObject, const ZString &sName);
    ZConstructorRef GetConstructors();
    uint32_t GetNumberOfDirectBaseClasses();
    const IClassType *GetDirectBaseClassType(uint64_t nBaseClass);
    uint64_t GetDirectBaseClassOffset(uint64_t nBaseClass);
    uint64_t GetBaseClassOffset(STypeID *id);
};

class IEnumType {
    public:
    IType type;
    struct SEnumItem {
        const char *szName;
        int32_t nValue;
        uint32_t nameStrCrc;
    };
    TArrayRef<IEnumType::SEnumItem const> items;
    bool ConvertNameToValue(const ZString &, int32_t &);
    const char *ConvertValueToName(int32_t);
    const TArrayRef<IEnumType::SEnumItem const> &GetItems();
    int32_t GetValue(const void *);
    void Set(void *, const int32_t);
};

class ZTypeIDRegistry {
    public:
    struct SElement {
        const char *pszName;
        STypeID *type;
    };
    ZTypeIDRegistry();
    STypeID *Find(const char *name);
    void AddType(const char *pszName, STypeID *type);
    int32_t m_nCount;
    ZTypeIDRegistry::SElement m_aElements[32768];
};

class ZMutex {
    public:
    ZMutex(const ZMutex &);
    ZMutex(int32_t spincount);
    ~ZMutex();
    void Lock();
    void Unlock();
    bool TryLock();
    bool IsLockedByCurrentThread();
    ZMutex &operator=(const ZMutex &);
    static const int32_t IMPL_SIZE;
    uint64_t m_impl[5];
};

class ZSpinlock {
    public:
    ZSpinlock(ZSpinlock *rhs);
    ZSpinlock(const ZSpinlock &);
    ZSpinlock();
    ~ZSpinlock();
    void Lock();
    void Unlock();
    bool TryLock();
    bool IsLocked();
    ZSpinlock &operator=(ZSpinlock *rhs);
    ZSpinlock &operator=(const ZSpinlock &);
    ZMutex m_mutex;
};

class ITypeConverter {
    public:
    virtual ~ITypeConverter();
    virtual void Convert(void *, ZVariantRef);
};

class ZArrayTypeConverter : public ITypeConverter {
    public:
    ZArrayTypeConverter();
    virtual void Convert(void *pTarget, ZVariantRef value);
    void Set(const IArrayType *pFromType, const IArrayType *pToType,
             ITypeConverter *pElementConverter);
    const IArrayType *m_pFromType;
    const IArrayType *m_pToType;
    ITypeConverter *m_pElementConverter;
};

class ZConstructorTypeConverter : public ITypeConverter {
    public:
    ZConstructorTypeConverter();
    virtual void Convert(void *pTarget, ZVariantRef value);
    void Set(const IClassType *pToClassType, int32_t nBestViableConstructorIndex);
    const IClassType *m_pToClassType;
    int32_t m_nBestViableConstructorIndex;
};

struct STemplatedTypeName2 {
    STypeID *arg0;
    STypeID *arg1;
    char argCount;
    char name[255];
};

template <typename K, typename V>
class TPair {
    public:
    static const SNamedPropertyInfo s_aProperties[2];
    static STemplatedTypeName2 s_typeName;
    static IClassType s_typeInfo;
    TPair<K, V>(K *, V *);
    TPair<K, V>(K &, V *);
    TPair<K, V>(K *, V const &);
    TPair<K, V>(K &, V const &);
    TPair<K, V>(TPair<K, V> *);
    TPair<K, V>(const TPair<K, V> &);
    TPair<K, V>();
    bool operator==(const TPair<K, V> &);
    bool operator<(const TPair<K, V> &);
    TPair<K, V> &operator=(TPair<K, V> *);
    TPair<K, V> &operator=(const TPair<K, V> &);
    K &Key() const;
    K &Key();
    V const &Value() const;
    V &Value();
    static void RegisterTypeDescriptor();
    K m_key;
    V m_value;
};

template <typename P>
struct THashMapNode {
    // Methods and inner type omitted, because they seem to change based on
    // template params?
    uint64_t m_iNext;
    P m_data;
    // Given pointer to m_data, gives the enclosing THashMapNode
    static THashMapNode<P> *GetNodeFromData(P *data_ptr) {
        return reinterpret_cast<THashMapNode<P> *>(reinterpret_cast<uintptr_t>(data_ptr) - 8);
    }
};

template <typename P>
struct SHashMapInfo {
    uint64_t m_nBuckets;
    uint64_t *m_pBuckets;
    THashMapNode<P> *m_pNodes;
    SHashMapInfo<P> &operator=(const SHashMapInfo<P> &);
};

template <typename P>
class THashMapIterator : public TIterator<P> {
    public:
    THashMapIterator<P>(const THashMapIterator<P> &);
    THashMapIterator<P>(SHashMapInfo<P> *info, P *pairs, uint64_t bucket)
        : TIterator<P>(pairs), m_info(info), m_nBucket(bucket) {
        this->m_pCurrent = pairs; // XXX: Why?
        if(this->m_pCurrent != NULL) {
            // Skip to first item
            for (; m_nBucket < m_info->m_nBuckets; m_nBucket++) {
                if (m_info->m_pBuckets[m_nBucket] != -1ull) {
                    uint64_t nextIdx = m_info->m_pBuckets[m_nBucket];
                    this->m_pCurrent = &m_info->m_pNodes[nextIdx].m_data;
                    break;
                }
            }
        }
    }
    THashMapIterator<P>();
    ~THashMapIterator<P>() = default;
    operator THashMapIterator<P>();
    THashMapIterator<P> &operator=(const THashMapIterator<P> &);
    THashMapNode<P> &Node() { return *THashMapNode<P>::GetNodeFromData(this->m_pCurrent); }
    THashMapIterator<P> operator++(int32_t) { return operator++(); }
    THashMapIterator<P> &operator++() {
        uint64_t nextIdx = Node().m_iNext;
        if (nextIdx == -1ull) {
            for (m_nBucket++; m_nBucket < m_info->m_nBuckets; m_nBucket++) {
                if (m_info->m_pBuckets[m_nBucket] != -1ull) {
                    nextIdx = m_info->m_pBuckets[m_nBucket];
                    break;
                }
            }
            if (m_nBucket >= m_info->m_nBuckets) {
                this->m_pCurrent = NULL;
                return *this;
            }
        }
        this->m_pCurrent = &m_info->m_pNodes[nextIdx].m_data;

        return *this;
    }
    SHashMapInfo<P> *m_info;
    uint64_t m_nBucket;
};

template <typename P>
class THashMapEnumerator; // TODO

template <typename K, typename V>
class TDefaultHashMapPolicy {
    public:
    static uint32_t GetHashCode(const K &);
    static bool IsEqual(const K &, const K &);
    static bool IsValueEqual(V const &, V const &);
    static uint32_t InternalGetHashCode(uint64_t);
    static uint32_t InternalGetHashCode(uint32_t);
    static uint32_t InternalGetHashCode(int32_t);
    static uint32_t InternalGetHashCode(void *);
};

template <typename K, typename V, typename P>
class THashMap {
    public:
    static STemplatedTypeName2 s_typeName;
    static IContainerType s_typeInfo;
    THashMap<K, V, P>(THashMap<K, V, P> *);
    THashMap<K, V, P>(const THashMap<K, V, P> &);
    THashMap<K, V, P>(uint64_t);
    THashMap<K, V, P>(void *, uint64_t);
    ~THashMap<K, V, P>();
    THashMap<K, V, P> &operator=(THashMap<K, V, P> *);
    THashMap<K, V, P> &operator=(const THashMap<K, V, P> &);
    THashMapIterator<TPair<K const, V> const> Begin() const {
        return THashMapIterator<TPair<K const, V> const>(&m_info, &m_info.m_pNodes->m_data, 0);
    }
    THashMapIterator<TPair<K const, V>> Begin() {
        return THashMapIterator<TPair<K const, V>>(&m_info, &m_info.m_pNodes->m_data, 0);
    }
    THashMapIterator<TPair<K const, V> const> End() const {
        return THashMapIterator<TPair<K const, V> const>(&m_info, NULL, m_info.m_nBuckets);
    }
    THashMapIterator<TPair<K const, V>> End() {
        return THashMapIterator<TPair<K const, V>>(&m_info, NULL, m_info.m_nBuckets);
    }
    // Convenience functions for for loops
    THashMapIterator<TPair<K const, V> const> begin() const { return this->Begin(); }
    THashMapIterator<TPair<K const, V>> begin() { return this->Begin(); }
    THashMapIterator<TPair<K const, V> const> end() const { return this->End(); }
    THashMapIterator<TPair<K const, V>> end() { return this->End(); }
    THashMapEnumerator<TPair<K const, V> const> GetEnumerator() const;
    THashMapEnumerator<TPair<K const, V>> GetEnumerator();
    bool IsEmpty();
    uint64_t Size();
    uint64_t Capacity();
    void GetSplittedIterators(THashMapIterator<TPair<K const, V> const> *, uint64_t);
    void GetSplittedIterators(THashMapIterator<TPair<K const, V>> *, uint64_t);
    THashMapIterator<TPair<K const, V> const> Find(const K &) const;
    THashMapIterator<TPair<K const, V>> Find(const K &);
    THashMapIterator<TPair<K const, V> const> Find(const TPair<K const, V> &) const;
    THashMapIterator<TPair<K const, V>> Find(const TPair<K const, V> &);
    V const &GetValue(const K &, V const &);
    bool Contains(const K &);
    void Assign(const THashMap<K, V, P> &);
    void Clear();
    void Reserve(uint64_t, bool);
    V &operator[](const K &) const;
    V &operator[](K *);
    V &operator[](const K &);
    THashMapIterator<TPair<K const, V>> Insert(K *, V *);
    THashMapIterator<TPair<K const, V>> Insert(const K &, V *);
    THashMapIterator<TPair<K const, V>> Insert(K *, V const &);
    THashMapIterator<TPair<K const, V>> Insert(const K &, V const &);
    THashMapIterator<TPair<K const, V>> Insert(const TPair<K const, V> &);
    THashMapIterator<TPair<K const, V>> Erase(THashMapIterator<TPair<K const, V>>);
    bool Remove(const K &);
    void Swap(THashMap<K, V, P> &);
    void Trim();
    uint64_t m_nSize;
    uint64_t m_iFree;
    SHashMapInfo<TPair<K const, V>> m_info;
    bool m_bFixed;
    uint64_t AllocNode(K *, V *);
    uint64_t AllocNode(const K &, V *);
    uint64_t AllocNode(K *, V const &);
    uint64_t AllocNode(const K &, V const &);
    void FreeNode(uint64_t);
    void EnsureCapacity(uint64_t);
    void Rehash(uint64_t);
    void Init(uint64_t);
    void Cleanup();
    void DestroyElements();
    void ClearMembers();
    void AssignMembers(const THashMap<K, V, P> &);
};

template <typename T, uint64_t N>
class TArrayFixedHeap {
    public:
    TArrayFixedHeap<T, N>();
    ~TArrayFixedHeap<T, N>();
    bool IsEmpty();
    bool IsFull();
    bool Contains(void *p);
    T *Allocate();
    void Free(T *);
    uint64_t GetFreeElementCount();
    uint64_t GetIndex(const T *);
    const T *GetElement(uint64_t) const;
    T *GetElement(uint64_t);
    struct ZElement {
        char m_memory[24];
    };
    TArrayFixedHeap<T, N>::ZElement m_memory[N];
    void **m_pFreeElement;
    bool IsFree(const TArrayFixedHeap<T, N>::ZElement *);
};

class ZTypeRegistry {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZTypeRegistry();
    ZTypeRegistry(const ZTypeRegistry &);
    ZTypeRegistry &operator=(const ZTypeRegistry &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZTypeRegistry();
    const IType *GetType(const ZString &sTypeName);
    static const IType *GetType(STypeID *type);
    bool IsTypeRegistered(const ZString &sTypeName);
    bool IsTypeRegistered(STypeID *type);
    void Register(IType *pType);
    void RegisterCustomConverter(STypeID *from, STypeID *to, ITypeConverter *pConverter);
    ITypeConverter *GetExistingConverter(STypeID *from, STypeID *to);
    ITypeConverter *GetConverter(STypeID *from, STypeID *to);
    const IType *FindType(const ZString &sTypeName);
    ZSpinlock m_mutex;
    class ZTypeMapHashPolicy {
        public:
        static uint64_t GetHashCode(const ZString &key);
        static bool IsEqual(const ZString &a, const ZString &b);
    };
    THashMap<ZString, STypeID *, ZTypeRegistry::ZTypeMapHashPolicy> m_typeNameMap;
    TArray<IType const *> m_unnamedTypes;
    struct STypeIDPair {
        STypeIDPair(STypeID *from, STypeID *to);
        STypeIDPair();
        bool operator<(const ZTypeRegistry::STypeIDPair &);
        STypeID *m_From;
        STypeID *m_To;
    };
    class ZTypeConverterHashPolicy {
        public:
        static uint32_t GetHashCode(const ZTypeRegistry::STypeIDPair &key);
        static bool IsEqual(const ZTypeRegistry::STypeIDPair &a,
                            const ZTypeRegistry::STypeIDPair &b);
    };
    THashMap<ZTypeRegistry::STypeIDPair, ITypeConverter *, ZTypeRegistry::ZTypeConverterHashPolicy>
        m_Converters;
    TArray<ITypeConverter *> m_aCustomConverters;
    TArrayFixedHeap<ZArrayTypeConverter, 256> m_ArrayConverterPool;
    TArrayFixedHeap<ZConstructorTypeConverter, 2048> m_ConstructorConverterPool;
};

class IComponentInterface {
    public:
    static void RegisterType();
    virtual ~IComponentInterface(){};
    virtual ZVariantRef GetVariantRef() = 0;
    virtual int64_t AddRef() = 0;
    virtual int64_t Release() = 0;
    virtual void *QueryInterface(STypeID *) = 0;
};

struct SAllocatorStatus {
    uint64_t nTotalMemory;
    uint64_t nAvailableMemory;
    uint64_t nTotalAllocCalls;
    uint64_t nTotalFreeCalls;
    uint64_t nLargestFreeBlock;
    uint64_t nFreeSizesSquared;
    uint64_t nTotalFreeBlocks;
};

struct SAllocatorUsage {
    uint64_t nTotalMemory;
    uint64_t nAvailableMemory;
    uint64_t nUsedMemory;
};

class IAllocator : public IComponentInterface {
    public:
    IAllocator();
    virtual uint64_t GetDefaultAlignment();
    virtual bool SupportsAlignedAllocations();
    virtual void *Allocate(uint64_t, uint32_t);
    virtual void *AllocateAligned(uint64_t, uint64_t, uint32_t);
    virtual void Free(void *);
    virtual void FreeAligned(void *pData);
    virtual uint64_t GetAllocationSize(void *);
    virtual void *ReAllocate(void *, uint64_t, uint32_t);
    virtual void *ReAllocateAligned(void *, uint64_t, uint64_t, uint32_t);
    virtual SAllocatorStatus GetStatus();
    virtual SAllocatorUsage GetUsage();
    virtual void CheckHeap();
    void Uninstall();
    void RegisterForUninstall(void (*func)());
    void UnregisterForUninstall(void (*func)());
    virtual void ResetThreadMemTracking();
    virtual int64_t GetThreadMemTracking();
    virtual void InitializeHeapCheck();
    virtual void ShutdownHeapCheck();
    virtual void EnableHeapCheck();
    virtual void DisableHeapCheck();
    void (*m_dtorListenerFunc[64])();
};

struct SPageAllocatorStats {
    uint64_t nTotalMemory;
    uint64_t nAvailableMemory;
    uint64_t nLowestFreeMemory;
    uint64_t nCachedPages;
    uint64_t nPagesReassigned;
};

class IPageAllocator {
    public:
    virtual ~IPageAllocator();
    virtual uint64_t GetPageSize();
    virtual uint64_t GetReserveGranularity();
    virtual SPageAllocatorStats GetStats();
    virtual void ResetLowestFreeStat();
    virtual void *Reserve(uint64_t);
    virtual bool Release(void *, uint64_t);
    virtual bool Release(void *);
    virtual bool Commit(void *, uint64_t);
    virtual bool Decommit(void *, uint64_t);
    virtual void *GetPhysicalPoolBase();
    virtual uint64_t GetPhysicalPoolMaxSize();
    virtual uint64_t ResizePhysicalPool(uint64_t);
    virtual void *GetSwappedPoolBase();
    virtual uint64_t ResizeSwappedPool(uint64_t);
    virtual void RegisterAllocatorAddress(void *, uint64_t, IAllocator *);
    virtual void UnregisterAllocatorAddress(void *);
    virtual IAllocator *GetAllocator(void *);
};

template <uint64_t N>
class TAddressToAllocatorMap {
    public:
    TAddressToAllocatorMap<N>(void *__formal);
    void Add(void *pAddress, uint64_t nSize, IAllocator *pAllocator);
    void Remove(void *pAddress);
    IAllocator *GetAllocator(void *pAddress);
    uint64_t GetAllocatorIdx(void *pAddress);
    struct SAllocatorEntry {
        IAllocator *m_pAllcator;
        uint64_t m_startAddress;
        uint64_t m_endAddress;
    };
    static const uint64_t NUMBER_ALLOCATORS;
    uint64_t m_nNbUsedAllocators;
    TAddressToAllocatorMap<N>::SAllocatorEntry m_aAllocators[255];
};

class ZPageAllocator : public IPageAllocator {
    public:
    ZPageAllocator();
    virtual ~ZPageAllocator();
    virtual uint64_t GetPageSize();
    virtual uint64_t GetReserveGranularity();
    virtual SPageAllocatorStats GetStats();
    virtual void ResetLowestFreeStat();
    virtual void *Reserve(uint64_t nSize);
    virtual bool Release(void *pAddress, uint64_t nSize);
    virtual bool Release(void *pAddress);
    virtual bool Commit(void *pAddress, uint64_t nSize);
    virtual bool Decommit(void *pAddress, uint64_t nSize);
    virtual void *GetPhysicalPoolBase();
    virtual uint64_t ResizePhysicalPool(uint64_t newPhysicalPoolSize);
    virtual void *GetSwappedPoolBase();
    virtual uint64_t GetPhysicalPoolMaxSize();
    virtual uint64_t ResizeSwappedPool(uint64_t newSwappedSize);
    virtual void RegisterAllocatorAddress(void *pAddress, uint64_t nSize, IAllocator *pAllocator);
    virtual void UnregisterAllocatorAddress(void *pAddress);
    virtual IAllocator *GetAllocator(void *pAddress);
    void UpdateLowestFree();
    ZMutex m_Mutex;
    void *m_pPhysicalBase;
    uint64_t m_nTotalMemory;
    uint64_t m_nPhysicalSize;
    uint64_t m_nPhysicalPoolSize;
    uint64_t m_nPhysicalLowestFreeSize;
    TAddressToAllocatorMap<1048576> m_AddressToAllocatorMap;
};

class ZAllocatorDL : public IAllocator {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZAllocatorDL(bool enableMemoryGuard);
    ZAllocatorDL(const ZAllocatorDL &);
    ZAllocatorDL &operator=(const ZAllocatorDL &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    void InitMemory(char *pMemory, uint64_t nSize);
    void UninitMemory();
    virtual ~ZAllocatorDL();
    virtual uint64_t GetDefaultAlignment();
    virtual bool SupportsAlignedAllocations();
    virtual void OutOfMemory(uint64_t nSize);
    virtual void *Allocate(uint64_t nSize, uint32_t nSP);
    virtual void *AllocateAligned(uint64_t nSize, uint64_t nAlignment, uint32_t nSP);
    virtual void Free(void *object);
    virtual uint64_t GetAllocationSize(void *pData);
    virtual void *ReAllocate(void *pData, uint64_t nNewSize, uint32_t nSP);
    virtual void *ReAllocateAligned(void *pData, uint64_t nNewSize, uint64_t nAlignment,
                                    uint32_t nSP);
    void Reset();
    virtual SAllocatorStatus GetStatus();
    virtual SAllocatorUsage GetUsage();
    virtual void CheckHeap();
    bool GetMemoryGuardEnabled();
    virtual bool MoreCore(const int64_t nSize, void **pNew);
    virtual void OnFreeChunk(void *pStart, int64_t nSize);
    virtual void CommitPages(void *from, void *to);
    virtual void DeCommitPages(void *from, void *to);
    uint32_t IncrementAllocNumber();
    void *GetCheckHeapData();
    virtual void ResetThreadMemTracking();
    virtual int64_t GetThreadMemTracking();
    virtual void InitializeHeapCheck();
    virtual void ShutdownHeapCheck();
    virtual void EnableHeapCheck();
    virtual void DisableHeapCheck();
    void PrintStatus();
    void *m_msp;
    int64_t m_nAllocCount;
    int64_t m_nFreeCount;
    int64_t m_nUsedBytes;
    int64_t m_nTotalBytes;
    int64_t m_nDecommitted;
    int64_t m_AllocNumber;
    bool m_MemoryGuardEnabled;
    void *m_CheckHeapData;
    char *m_OOMBuffer;
    uint64_t m_nAllocatorID;
};

class ZAllocator_DLMalloc_Virtual_Fragmented : public ZAllocatorDL {
    public:
    ZAllocator_DLMalloc_Virtual_Fragmented(IPageAllocator *pPageAllocator, uint64_t maxSize,
                                           const char *name, bool enableMemoryGuard);
    virtual ~ZAllocator_DLMalloc_Virtual_Fragmented();
    void ReportChunkInfo();
    virtual bool MoreCore(const int64_t nSize, void **pNew);
    IPageAllocator *m_pPageAllocator;
    uint64_t m_nPageSize;
    struct SChunk {
        uint8_t *m_pReserveAddress;
        uint8_t *m_pBase;
        uint64_t m_nSize;
        uint64_t m_nUsed;
    };
    static const int32_t MAX_NUM_CHUNKS;
    ZAllocator_DLMalloc_Virtual_Fragmented::SChunk m_aChunks[16];
    int32_t m_nNumChunks;
    int32_t m_nCurrentChunk;
};

class ZMemAllocatorScope {
    public:
    ZMemAllocatorScope(uint64_t memSize);
    ~ZMemAllocatorScope();
    uint64_t GetUsage();
    IPageAllocator *GetPageAllocator();
    IAllocator *GetAllocator();
    ZPageAllocator m_pageAllocator;
    ZAllocator_DLMalloc_Virtual_Fragmented m_allocator;
    IPageAllocator *m_previousPageAllocator;
    IAllocator *m_previousAllocator;
};

class ZRefCount {
    public:
    ZRefCount();
    int64_t operator--();
    int64_t operator++();
    operator __int64();
    int64_t m_nCount;
};

class ZComponentCreateInfo {
    public:
    ZComponentCreateInfo();
    int32_t ArgCount();
    STypeID *GetArgumentTypeID(int32_t);
    void AddArgument(STypeID *type, const void *pData);
    struct SArgumentInfo {
        STypeID *m_type;
        const void *m_pData;
    };
    static const int32_t MAX_ARGUMENTS;
    int32_t m_nCount;
    ZComponentCreateInfo::SArgumentInfo m_aArguments[5];
};

/*template <typename T>
class TSharedPtr : public std::shared_ptr<T> {
    public:
    TSharedPtr<T>(T *ptr) : std::shared_ptr<T>(ptr) { }
    TSharedPtr<T>(void *);
    TSharedPtr<T>();
    ~TSharedPtr<T>();
    void Reset();
    T *Get();
    T &operator*();
    T *operator->() const;
    int32_t UseCount();
    bool Unique();
    operator class std::shared_ptr<T>();
};*/

// Specific to MDSuen reimplementation of TSharedPtr
// (mirrors DXMD's std::_Ref_count_base)
struct TSharedPtrRefCount {
    uint32_t uses;
    uint32_t weak_uses;
};

template <typename T>
class TSharedPtr {
    public:
    // Creates a TSharedPtr that is supposedly used a lot, so it never tries to
    // delete the inner pointer, which is useful for creating a TSharedPtr to
    // locals
    static TSharedPtr<T> FromLocal(T *ptr) {
        auto ref_count = new TSharedPtrRefCount;
        ref_count->uses = 1 << 20;
        ref_count->weak_uses = 0;
        return TSharedPtr(ptr, ref_count);
    }

    TSharedPtr<T>(T *ptr, TSharedPtrRefCount *ref_count) : ptr(ptr), ref_count(ref_count) {}
    TSharedPtr<T>(void *);
    TSharedPtr<T>();
    ~TSharedPtr<T>();
    void Reset();
    T *Get();
    T &operator*();
    T *operator->() const;
    int32_t UseCount();
    bool Unique();
    operator class std::shared_ptr<T>();
    T *ptr;
    TSharedPtrRefCount *ref_count;
};

template <typename T>
class TListNode {
    public:
    TListNode<T>(T *);
    TListNode<T>(const T &);
    TListNode<T>();
    const T &Data() const;
    T &Data();
    TListNode<T> *&Previous();
    const TListNode<T> *Previous() const;
    TListNode<T> *&Next();
    const TListNode<T> *Next() const;
    static TListNode<T> *GetNodeFromData(T *);
    TListNode<T> *m_pNext;
    TListNode<T> *m_pPrevious;
    T m_data;
};

template <typename T>
class TListIterator : public TIterator<T> {
    public:
    TListIterator<T>(const TListIterator<T> &rhs);
    TListIterator<T>(T *pElement);
    operator class TListIterator<T const>();
    TListNode<T> &Node();
    TListIterator<T> operator++(int32_t __formal);
    TListIterator<T> &operator++();
    TListIterator<T> operator--(int32_t);
    TListIterator<T> &operator--();
};

template <typename T>
class TListEnumerator; // TODO

template <typename T>
class TList {
    public:
    static STemplatedTypeName1 s_typeName;
    static IContainerType s_typeInfo;
    TList<T>(TList<T> *);
    TList<T>(const TList<T> &rhs);
    TList<T>();
    ~TList<T>();
    TList<T> &operator=(TList<T> *);
    TList<T> &operator=(const TList<T> &rhs);
    bool operator==(const TList<T> &rhs);
    bool operator!=(const TList<T> &);
    TListIterator<T const> Begin() const;
    TListIterator<T> Begin();
    TListIterator<T const> End() const;
    TListIterator<T> End();
    // Convenience methods for range-based for loop
    TListIterator<T const> begin() const { return Begin(); }
    TListIterator<T> begin() { return Begin(); }
    TListIterator<T const> end() const { return End(); }
    TListIterator<T> end() { return End(); }
    const T &Front() const;
    T &Front();
    const T &Back() const;
    T &Back();
    bool IsEmpty();
    uint64_t Size();
    TListEnumerator<T const> GetEnumerator() const;
    TListEnumerator<T> GetEnumerator();
    void Assign(const TList<T> &rhs);
    void Clear();
    TListIterator<T const> Find(const T &) const;
    TListIterator<T> Find(const T &);
    TListIterator<T> Insert(TListIterator<T> where, T *element);
    TListIterator<T> Insert(TListIterator<T>, const T &);
    TListIterator<T> Insert(T *element);
    TListIterator<T> Insert(const T &);
    TListIterator<T> Insert();
    TListIterator<T> Erase(TListIterator<T>);
    void PushFront(T *);
    void PushFront(const T &);
    void PushBack(T *);
    void PushBack(const T &);
    TListIterator<T> InsertNodeAt(TListIterator<T> where, TListNode<T> *pNode);
    struct ZFakeListNode {
        TListNode<T> *m_pFirst;
        TListNode<T> *m_pLast;
    };
    uint64_t m_nSize;
    TList<T>::ZFakeListNode m_list;
};

template <typename T>
class TBinaryTreeNode {
    public:
    TBinaryTreeNode<T>(T *data);
    TBinaryTreeNode<T>(const T &data);
    const T &Data() const;
    T &Data();
    int32_t Balance();
    TBinaryTreeNode<T> *&Parent();
    const TBinaryTreeNode<T> *Parent() const;
    TBinaryTreeNode<T> *&Left();
    const TBinaryTreeNode<T> *Left() const;
    TBinaryTreeNode<T> *&Right();
    const TBinaryTreeNode<T> *Right() const;
    static const TBinaryTreeNode<T> *GetNextNode(const TBinaryTreeNode<T> *);
    static TBinaryTreeNode<T> *GetNextNode(TBinaryTreeNode<T> *pNode);
    static const TBinaryTreeNode<T> *GetPreviousNode(const TBinaryTreeNode<T> *);
    static TBinaryTreeNode<T> *GetPreviousNode(TBinaryTreeNode<T> *);
    static TBinaryTreeNode<T> *GetNodeFromData(T *pData);
    TBinaryTreeNode<T> *GetChild(int32_t) const;
    TBinaryTreeNode<T> *&GetChild(int32_t n);
    int32_t m_nBalance;
    TBinaryTreeNode<T> *m_pParent;
    TBinaryTreeNode<T> *m_pLeft;
    TBinaryTreeNode<T> *m_pRight;
    T m_data;
};

template <typename T>
class TBinaryTreeIterator : public TIterator<T> {
    public:
    TBinaryTreeIterator<T>(const TBinaryTreeIterator<T> &);
    TBinaryTreeIterator<T>(T *);
    operator class TBinaryTreeIterator<T>();
    const TBinaryTreeNode<T> &Node() const;
    TBinaryTreeNode<T> &Node();
    TBinaryTreeIterator<T> operator++(int32_t);
    TBinaryTreeIterator<T> &operator++();
    TBinaryTreeIterator<T> operator--(int32_t);
    TBinaryTreeIterator<T> &operator--();
};

template <typename T>
class TEnumerator : public TEnumeratorBase<T> {
    public:
    TEnumerator<T>(const TEnumerator<T> &);
    TEnumerator<T>(T *, T *, IEnumeratorImpl *);
    TEnumerator<T>();
    ~TEnumerator<T>();
    TEnumerator<T> &operator=(const TEnumerator<T> &);
    operator class TEnumerator<T const>();
    bool MoveNext();
    static TEnumerator<T> GetEmptyEnumerator();
    IEnumeratorImpl *m_pEnumeratorImpl;
};

template <typename T>
class TBinaryTreeEnumerator : public TEnumeratorBase<T> {
    public:
    TBinaryTreeEnumerator<T>(T *, T *);
    operator class TBinaryTreeEnumerator<T>();
    TBinaryTreeNode<T> Node();
    bool MoveNext();
};

template <typename K, typename V>
class TMapKeyEnumerator : public TEnumeratorBase<K const> {
    public:
    TMapKeyEnumerator<K, V>(const K *pBeforeFirst, const K *pEnd);
    operator class TEnumerator<K const>();
    TEnumerator<K> GetMutableEnumerator();
    bool MoveNext();
    class ZEnumeratorImpl : public IEnumeratorImpl {
        public:
        virtual void *GetNext(const void *pCurrent);
        virtual void Release();
        static TMapKeyEnumerator<K, V>::ZEnumeratorImpl s_impl;
    };
};

template <typename K, typename V>
class TMapValueEnumerator : public TEnumeratorBase<V> {
    public:
    TMapValueEnumerator<K, V>(V *, V *);
    operator class TMapValueEnumerator<K, V const>();
    bool MoveNext();
    class ZEnumeratorImpl : public IEnumeratorImpl {
        public:
        virtual void *GetNext(const void *);
        virtual void Release();
        static TMapValueEnumerator<K, V>::ZEnumeratorImpl s_impl;
    };
    static void *GetNextInternal(const void *);
    operator class TEnumerator<V>();
};

template <typename T>
class TBinaryTree {
    public:
    TBinaryTree<T>(TBinaryTree<T> *);
    TBinaryTree<T>(const TBinaryTree<T> &rhs);
    TBinaryTree<T>();
    ~TBinaryTree<T>();
    bool operator==(const TBinaryTree<T> &);
    bool operator!=(const TBinaryTree<T> &);
    TBinaryTree<T> &operator=(TBinaryTree<T> *);
    TBinaryTree<T> &operator=(const TBinaryTree<T> &rhs);
    TBinaryTreeIterator<T const> Begin() const;
    TBinaryTreeIterator<T> Begin();
    TBinaryTreeIterator<T const> End() const;
    TBinaryTreeIterator<T> End();
    TBinaryTreeNode<T> *Root();
    bool IsEmpty();
    uint64_t Size();
    TBinaryTreeEnumerator<T const> GetEnumerator() const;
    TBinaryTreeEnumerator<T> GetEnumerator();
    TBinaryTreeIterator<T const> Find(const T &) const;
    TBinaryTreeIterator<T> Find(const T &element);
    TBinaryTreeIterator<T const> FindFirst(const T &) const;
    TBinaryTreeIterator<T> FindFirst(const T &);
    void FindRange(TBinaryTreeIterator<T const> &, TBinaryTreeIterator<T const> &, const T &);
    void FindRange(TBinaryTreeIterator<T> &, TBinaryTreeIterator<T> &, const T &);
    void Clear();
    void Assign(const TBinaryTree<T> &rhs);
    void Swap(TBinaryTree<T> &);
    void Cleanup();
    struct SFakeTreeNode {
        int32_t m_reserved1;
        TBinaryTreeNode<T> *m_pNULL;
        TBinaryTreeNode<T> *m_pRightRoot;
        TBinaryTreeNode<T> *m_pLeftRoot;
        SFakeTreeNode();
    };
    TBinaryTree<T>::SFakeTreeNode m_tree;
    int32_t m_nSize;
};

template <typename T>
class TRedBlackTree : public TBinaryTree<T> {
    public:
    TRedBlackTree<T>(TRedBlackTree<T> *);
    TRedBlackTree<T>(const TRedBlackTree<T> &rhs);
    TRedBlackTree<T>();
    ~TRedBlackTree<T>();
    TRedBlackTree<T> &operator=(TRedBlackTree<T> *);
    TRedBlackTree<T> &operator=(const TRedBlackTree<T> &rhs);
    TBinaryTreeIterator<T> Insert(T *element);
    TBinaryTreeIterator<T> Insert(const T &);
    TBinaryTreeIterator<T> Erase(TBinaryTreeIterator<T>);
};

template <typename K, typename V>
class TMap {
    public:
    static STemplatedTypeName2 s_typeName;
    static IContainerType s_typeInfo;
    TMap<K, V>(TMap<K, V> *);
    TMap<K, V>(const TMap<K, V> &);
    TMap<K, V>();
    TMap<K, V> &operator=(TMap<K, V> *);
    TMap<K, V> &operator=(const TMap<K, V> &);
    bool operator==(const TMap<K, V> &);
    bool operator!=(const TMap<K, V> &);
    const V &operator[](const K &) const;
    V &operator[](K *);
    V &operator[](const K &key);
    const V &GetValue(const K &, const V &);
    TBinaryTreeIterator<TPair<K, V> const> Begin() const;
    TBinaryTreeIterator<TPair<K, V>> Begin();
    TBinaryTreeIterator<TPair<K, V> const> End() const;
    TBinaryTreeIterator<TPair<K, V>> End();
    bool IsEmpty();
    uint64_t Size();
    TBinaryTreeEnumerator<TPair<K, V> const> GetEnumerator() const;
    TBinaryTreeEnumerator<TPair<K, V>> GetEnumerator();
    TMapKeyEnumerator<K, V> GetKeyEnumerator();
    TMapValueEnumerator<K, V const> GetValueEnumerator() const;
    TMapValueEnumerator<K, V> GetValueEnumerator();
    void Assign(const TMap<K, V> &);
    void Clear();
    TBinaryTreeIterator<TPair<K, V> const> Find(const K &) const;
    TBinaryTreeIterator<TPair<K, V>> Find(const K &key);
    bool Contains(const K &);
    TBinaryTreeIterator<TPair<K, V>> Insert(K *, V *);
    TBinaryTreeIterator<TPair<K, V>> Insert(const K &, V *);
    TBinaryTreeIterator<TPair<K, V>> Insert(K *, const V &);
    TBinaryTreeIterator<TPair<K, V>> Insert(const K &, const V &);
    TBinaryTreeIterator<TPair<K, V>> Insert(TPair<K, V> *);
    TBinaryTreeIterator<TPair<K, V>> Insert(const TPair<K, V> &);
    TBinaryTreeIterator<TPair<K, V>> Erase(TBinaryTreeIterator<TPair<K, V>>);
    bool Remove(const K &);
    void Swap(TMap<K, V> &);
    TRedBlackTree<TPair<K, V>> m_container;
};

class ZPath {
    public:
    static void RegisterType();
    ZPath(ZString *rhs);
    ZPath(const ZString &rhs);
    ZPath(ZPath *rhs);
    ZPath(const ZPath &rhs);
    ZPath();
    ZPath &operator=(ZString *rhs);
    ZPath &operator=(const ZString &rhs);
    ZPath &operator=(ZPath *rhs);
    ZPath &operator=(const ZPath &rhs);
    bool operator==(const ZString &rhs);
    bool operator==(const ZPath &rhs);
    bool operator!=(const ZString &rhs);
    bool operator!=(const ZPath &rhs);
    bool operator<(const ZString &rhs);
    bool operator<(const ZPath &rhs);
    bool operator>(const ZString &rhs);
    bool operator>(const ZPath &rhs);
    uint64_t Length();
    bool IsEmpty();
    uint64_t Depth();
    const ZString &ToString();
    ZString GetName();
    ZPath GetChildPath();
    ZPath GetParentPath();
    ZPath &Append(const ZString &sPath);
    ZPath &Append(const ZPath &path);
    ZPath &Prepend(const ZString &sPath);
    ZPath &Prepend(const ZPath &path);
    void Normalize();
    ZString m_value;
};

class ZFilePath {
    public:
    static void RegisterType();
    ZFilePath(ZString *rhs);
    ZFilePath(const ZString &rhs);
    ZFilePath(ZPath *rhs);
    ZFilePath(const ZPath &rhs);
    ZFilePath(ZFilePath *rhs);
    ZFilePath(const ZFilePath &rhs);
    ZFilePath();
    ZFilePath &operator=(const ZFilePath &rhs);
    bool operator==(const ZFilePath &rhs);
    bool operator!=(const ZFilePath &rhs);
    bool operator<(const ZFilePath &rhs);
    bool operator>(const ZFilePath &rhs);
    uint64_t Length();
    bool IsEmpty();
    void Assign(const ZFilePath &path);
    bool StartsWith(const ZFilePath &path);
    bool IStartsWith(const ZFilePath &path);
    bool HasExtension(const ZString &ext);
    bool HasDirectory(const ZString &directory);
    bool IsAbsolute();
    int32_t GetDirectoryCount();
    int32_t GetDirectoryIndex(const ZString &directoryName);
    ZFilePath GetParentDirectory();
    ZString GetExtension();
    void GetDirectory(char *szBuffer, uint64_t bufSize);
    ZFilePath GetDirectory();
    ZString GetDirectoryName(int32_t index);
    ZFilePath ReplaceDirectoryName(int32_t index, const ZString &newName);
    ZFilePath GetFileName();
    void GetFileNameNoExtension(char *szBuffer, uint64_t bufSize);
    ZString GetFileNameNoExtension();
    ZFilePath ReplaceFileName(const ZString &newName);
    ZFilePath RemoveExtension();
    ZFilePath ReplaceExtension(const ZString &ext);
    ZFilePath AddExtension(const ZString &ext);
    ZFilePath RemoveLeadingPath(const ZFilePath &path);
    ZFilePath IRemoveLeadingPath(const ZFilePath &path);
    ZFilePath IReplaceLeadingPath(const ZFilePath &path, const ZString &replacement);
    ZFilePath IRemoveMatchingPath(const ZFilePath &path);
    ZFilePath RemoveLeadingSlash();
    ZFilePath Append(const ZFilePath &path);
    ZFilePath Prepend(const ZFilePath &path);
    const ZString &ToString();
    uint64_t ToCString(char *pBuffer, uint64_t nSize);
    ZPath m_path;
};

class IIniFileSection {
    public:
    virtual ~IIniFileSection();
    virtual ZString GetName();
    virtual ZString GetValue(const ZString &);
    virtual ZString GetValueSource(const ZString &);
    virtual TEnumerator<ZString> GetOptions();
};

class IIniFile : public IComponentInterface {
    public:
    virtual bool Load(const ZFilePath &, bool);
    virtual bool LoadFromString(const ZString &);
    virtual IIniFileSection *GetSection(const ZString &);
    virtual TEnumerator<IIniFileSection *> GetSections();
    virtual ZString GetValue(const ZString &, const ZString &);
    virtual void SetValue(const ZString &, const ZString &, const ZString &, const ZString &);
    virtual void AddConsoleCmd(const ZString &);
    virtual TEnumerator<ZString> GetConsoleCmds();
};

namespace details {
class ZIniFileSection : public IIniFileSection {
    public:
    ZIniFileSection(const ZString &name);
    virtual ZString GetName();
    virtual ZString GetValue(const ZString &sOptionName);
    virtual ZString GetValueSource(const ZString &sOptionName);
    virtual TEnumerator<ZString> GetOptions();
    void SetOption(const ZString &sOptionName, const ZString &sValue, const ZString &sSource);
    struct SOptionValue {
        SOptionValue(const ZString &sValue, const ZString &sSource);
        SOptionValue();
        ZString m_value;
        ZString m_source;
    };
    const ZIniFileSection::SOptionValue *GetOptionValue(const ZString &sOptionName);
    ZString m_name;
    TMap<ZString, ZIniFileSection::SOptionValue> m_options;
};
}; // namespace details

class ZIniFile : public IIniFile {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZIniFile();
    ZIniFile(const ZIniFile &);
    ZIniFile &operator=(const ZIniFile &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    virtual ~ZIniFile();
    virtual bool Load(const ZFilePath &path, bool bNixxesForceAllowPlainTextIniFile);
    virtual bool LoadFromString(const ZString &sSettings);
    virtual IIniFileSection *GetSection(const ZString &sName);
    virtual TEnumerator<IIniFileSection *> GetSections();
    virtual ZString GetValue(const ZString &sSectionName, const ZString &sOptionName);
    virtual void SetValue(const ZString &sSectionName, const ZString &sOptionName,
                          const ZString &sValue, const ZString &sSource);
    virtual void AddConsoleCmd(const ZString &sCommand);
    virtual TEnumerator<ZString> GetConsoleCmds();
    bool LoadInternal(const ZFilePath &path, bool bNixxesForceAllowPlainTextIniFile);
    static bool LoadIniFileContent(const ZFilePath &plainTextIniFilePath,
                                   TArray<unsigned char> &aBuffer, bool bPlainText);
    static ZString Unscramble(TArrayRef<unsigned char> aBuffer);
    static uint32_t GenerateHash(const ZString &s);
    bool LoadFromStringInternal(const ZString &sIniFileContent, const ZFilePath &path);
    static bool ParseFileName(const ZFilePath &currentFile, const char *pszRaw, ZFilePath &result);
    TMap<ZString, IIniFileSection *> m_sections;
    details::ZIniFileSection *m_pCurrentSection;
    TArray<ZString> m_ConsoleCmds;
    TArray<ZString> m_IniFilesLoaded;
};

class IModule : public IComponentInterface {
    public:
    virtual const ZString &Name();
    virtual bool Initialize();
    virtual void Shutdown();
    virtual void Activate();
    virtual void OnAfterActivation();
    virtual void OnBeforeDeactivation();
    virtual void Deactivate();
    virtual const TMap<ZString, ZString> &GetOptions();
    virtual ZString GetOption(const ZString &);
    virtual bool HasOptions();
    virtual bool GetOptionBool(const ZString &);
    virtual void SetOption(const ZString &, const ZString &);
    virtual void BeginInitialization();
    virtual void EndInitialization();
    virtual void WaitForInitialization();
    virtual IComponentInterface *CreateComponent(const ZString &, ZComponentCreateInfo &);
};

enum ERunMode {
    RUN_MODE_STANDALONE,
    RUN_MODE_ACTIVE,
    RUN_MODE_PASSIVE,
    RUN_MODE_PACKER,
};

enum EEditorMode {
    EDITOR_MODE_DEFAULT,
    EDITOR_MODE_DEBUG,
    EDITOR_MODE_RELEASE,
};

enum EAssertResult {
    EAssertResult_Ignore,
    EAssertResult_IgnoreAll,
    EAssertResult_Break,
    EAssertResult_Abort,
};

class ITraceFilter;

class ITraceListener : public IComponentInterface {
    public:
    enum ETraceLevel {
        TRACE_PRINT,
        TRACE_ERROR,
        TRACE_WARNING,
        TRACE_MESSAGE,
        TRACE_NONE,
    };
    virtual void Flush() = 0;
    virtual void SetLevel(ITraceListener::ETraceLevel) = 0;
    virtual ITraceListener::ETraceLevel GetLevel() = 0;
    virtual void Write(uint32_t nBoardID, const ZString &pszFile, uint32_t nLine,
                       const char *pszCommand, ITraceListener::ETraceLevel eLevel,
                       const ZString &pszChannel, const ZString &pszString, bool bAppendNewline)
        = 0;
    virtual const char *GetStoredLine(uint64_t) = 0;
    virtual void SetFilter(ITraceFilter *) = 0;
    virtual ITraceFilter *GetFilter() = 0;
};

class ITraceFilter {
    public:
    virtual ~ITraceFilter();
    virtual bool FilterMessage(const ZString &, uint32_t, const char *, ITraceListener::ETraceLevel,
                               const ZString &, const ZString &);
};

template <typename F>
class ZDelegate;

template <typename R, typename... A>
class ZDelegate<R (A...)> {
    public:

    // Custom struct for MDSuen, seems to be represented as a 128bit pointer in
    // the original source code (Ghidra says so)
    struct MFPInner {
        R (*func)(void *, A...);
        int32_t inst_offset;
    };

    static R call_stdfunc(std::function<R (A...)> *f, A... args) {
        return (*f)(args...);
    }

    struct SStaticInvoke {};
    ZDelegate<R (A...)>(R (*)(A...));
    ZDelegate<R (A...)>(void *, R (*)(A...));
    ZDelegate<R (A...)>();
    ZDelegate<R (A...)>(std::function<R (A...)> &fn) {
        this->m_mfp.func = reinterpret_cast<R (*)(void *, A...)>(&call_stdfunc);
        this->m_mfp.inst_offset = 0;
        // TODO: Can I do this?
        //this->pInst = &fn;
        this->pInst = new std::function<R (A...)>(fn); // Yes, memory leak...
    }
    void operator()(A... args) {
        if(this->pInst == 0) {
            this->pStaticFunc(args...);
        } else {
            this->m_mfp(
                (void *) ((uintptr_t) this->pInst + this->m_mfp.inst_offset),
                args...);
        }
    }
    bool IsValid();
    bool operator==(const ZDelegate<R (A...)> &);
    bool operator!=(const ZDelegate<R (A...)> &);
    bool operator<(const ZDelegate<R (A...)> &);
    bool operator<=(const ZDelegate<R (A...)> &);
    bool operator>(const ZDelegate<R (A...)> &);
    bool operator>=(const ZDelegate<R (A...)> &);
    void PrefetchInstance();
    static auto (*GetNullMFP)() -> R (*)(A...);
    MFPInner m_mfp;
    R (*pStaticFunc)(A...);
    void *pInst;
};

struct SDelegateBaseInvoker {
    uint32_t argCount;
    ZVariant (*pfInvoke)(void *, const TArrayRef<ZVariant> &);
    STypeID *retType;
    STypeID *a0Type;
};

class ZDelegateBase {
    public:
    ZDelegateBase();
    bool IsValid();
    bool CanInvoke(const TArrayRef<ZVariant> &);
    ZVariant Invoke(const TArrayRef<ZVariant> &);
    ZDelegate<void(void)> d;
    const SDelegateBaseInvoker *pInvokeData;
};

class ICrashHandler {
    public:
    virtual ~ICrashHandler();
    virtual void GetSessionUid(char *, uint64_t);
    virtual void GetModuleName(char *, uint64_t);
    virtual void GetUsername(char *, uint64_t);
    virtual void GetMachineName(char *, uint64_t);
    virtual void GetPlatformName(char *, uint64_t);
    virtual void GetCmdLine(char *, uint64_t);
};

class IApplication {
    public:
    virtual ~IApplication() = 0;
    virtual void OnBeforeInitialize() = 0;
    virtual void OnAfterInitialize() = 0;
    virtual void OnBeforeShutdown() = 0;
    virtual void OnAfterShutdown() = 0;
    virtual void OnBeforeModuleInitialize(IModule *) = 0;
    virtual void OnAfterModuleInitialize(IModule *) = 0;
    virtual void OnBeforeModuleShutdown(IModule *) = 0;
    virtual void OnAfterModuleShutdown(IModule *) = 0;
    virtual void OnModuleNotFound(const ZString &) = 0;
    virtual void SetOption(const ZString &sOption, const ZString &sValue,
                           const ZString &sValueSource)
        = 0;
    virtual const ZString &GetOption(const ZString &) = 0;
    virtual bool GetOptionBool(const ZString &) = 0;
    virtual bool GetOptionOrDefaultBool(const ZString &, bool) = 0;
    virtual IIniFile *GetIniFile() = 0;
    virtual ERunMode GetRunMode() = 0;
    virtual ZString GetExePath() = 0;
    virtual EEditorMode GetEditorMode() = 0;
    virtual ZString GetEditorPath() = 0;
    virtual void Exit() = 0;
    virtual void SetDebugContextInfo(const ZString &, const ZString &) = 0;
    virtual ZString GetDebugContextInfo(const ZString &) = 0;
    virtual auto GetAssertHandlerFunction() -> EAssertResult (*)(const char *, const char *) = 0;
    virtual void SetAssertHandlerFunction(EAssertResult (*)(const char *, const char *));
    virtual auto GetAssertReporterFunction() -> void (*)(const char *, const char *) = 0;
    virtual void SetAssertReporterFunction(void (*)(const char *, const char *)) = 0;
    virtual auto GetNotificationReporterFunction()
        -> void (*)(ITraceListener::ETraceLevel, int32_t, const char *, const char *, const char *)
        = 0;
    virtual void SetNotificationReporterFunction(void (*)(ITraceListener::ETraceLevel, int32_t,
                                                          const char *, const char *, const char *))
        = 0;
    virtual ZDelegateBase &GetExceptionHandlerFunction() = 0;
    virtual void SetExceptionHandlerFunction(const ZDelegateBase &) = 0;
    virtual ICrashHandler *GetCrashHandler() = 0;
    virtual bool IsDemo() = 0;
    virtual void StartBenchmark() = 0;
    virtual void InitVRDemo(bool) = 0;
    virtual ZString GetBenchmarkScene() = 0;
    virtual int32_t GetBenchmarkSpawnPoint() = 0;
    virtual ZString GetVRDemoScene() = 0;
    virtual int32_t GetVRDemoSpawnPoint() = 0;
    static const char *GetEditorConnectionPortCommandLineDebug();
    static const char *GetEditorConnectionPortCommandLineDebugUseEditorDebug();
    static const char *GetEditorConnectionPortCommandLineDebugUseEditorRelease();
};

class ZScopedHeapChecker {
    public:
    ZScopedHeapChecker(IAllocator *allocator);
    ~ZScopedHeapChecker();
    IAllocator *m_Allocator;
};

class ZApplicationBase : public IApplication {
    public:
    ZApplicationBase();
    virtual ERunMode GetRunMode();
    virtual ZString GetExePath();
    virtual ZString GetEditorPath();
    virtual EEditorMode GetEditorMode();
    virtual void SetDebugContextInfo(const ZString &sKey, const ZString &sValue);
    virtual ZString GetDebugContextInfo(const ZString &sKey);
    virtual auto GetAssertHandlerFunction() -> EAssertResult (*)(const char *, const char *);
    virtual void SetAssertHandlerFunction(EAssertResult (*)(const char *, const char *));
    virtual auto GetAssertReporterFunction() -> void (*)(const char *, const char *);
    virtual void SetAssertReporterFunction(void (*)(const char *, const char *));
    virtual auto GetNotificationReporterFunction()
        -> void (*)(ITraceListener::ETraceLevel, int32_t, const char *, const char *, const char *);
    virtual void SetNotificationReporterFunction(void (*)(ITraceListener::ETraceLevel, int32_t,
                                                          const char *, const char *,
                                                          const char *));
    virtual ZDelegateBase &GetExceptionHandlerFunction();
    virtual void SetExceptionHandlerFunction(const ZDelegateBase &exceptionHandlerFunc);
    virtual ICrashHandler *GetCrashHandler();
    virtual bool IsDemo();
    ZScopedHeapChecker heapCheckerScope;
    virtual ZString ReadVersionInfo();
    THashMap<ZString, ZString, TDefaultHashMapPolicy<ZString, ZString>> m_debugContextMap;
    EAssertResult (*m_pAssertHandlerFunc)(const char *, const char *);
    void (*m_pAssertReporterFunc)(const char *, const char *);
    void (*m_pNotifReporterFunc)(ITraceListener::ETraceLevel, int32_t, const char *, const char *,
                                 const char *);
    ZDelegateBase m_g2ExceptionHandlerFunc;
};

struct SModuleDesc {
    SModuleDesc();
    ZString m_pszName;
    IModule *m_pModule;
};

class ZModuleTable {
    public:
    ZModuleTable();
    int32_t Length();
    IModule *operator[](int32_t nIndex);
    IModule *Find(const ZString &pszName, uint64_t nNameLength);
    void Add(const ZString &pszName, IModule *pModule);
    static const int32_t MAX_MODULES;
    int32_t m_nLength;
    SModuleDesc m_modules[100];
};

enum EComponentManagerState {
    CMS_UNINITIALIZED,
    CMS_RUNNING,
    CMS_CLOSING,
    CMS_DISPOSED,
};

class ZComponentManager {
    public:
    ZComponentManager();
    ~ZComponentManager();
    void SetApplication(IApplication *application);
    IApplication *GetApplication();
    void Initialize();
    void Shutdown();
    void Activate();
    void Deactivate();
    bool IsRunning();
    void RegisterModule(IModule *pModule);
    IModule *GetModule(const ZString &sName);
    bool PrepareCreateComponent(const ZString &pszFullName);
    void *CreateComponent(const ZString &pszFullName, STypeID *requestedInterface,
                          ZComponentCreateInfo &Info);
    IModule *FindModuleByComponentName(const ZString &pszFullName);
    IModule *GetModuleInternal(const ZString &pszName, uint64_t nModuleNameLength);
    IApplication *m_pApplication;
    EComponentManagerState m_state;
    ZModuleTable m_modules;
    TMap<ZString, IComponentInterface *> *m_pSingletons;
    ZMutex m_ModuleLoadMutex;
};

class ZThread {
    public:
    ZThread(const ZDelegate<unsigned int(void *)> &function, const ZString &sName, bool bJoinable);
    ZThread(bool bJoinable);
    ~ZThread();
    void SetFunction(const ZDelegate<unsigned int(void *)> &function, const ZString &sName);
    void SetJoinable(bool bJoinable);
    void SetStackSize(const int32_t nSizeInBytes);
    void Start(void *pParam);
    void Join();
    static uint64_t GetCurrentThreadHandle();
    static uint64_t GetCurrentThreadID();
    static uint64_t QueryPerformanceFrequency();
    static uint64_t GetCPUTimeStamp();
    static void DoYield();
    static void Sleep(uint32_t nMsecs);
    bool IsRunning();
    static void Terminate(uint32_t nExitCode);
    static void Suspend(uint64_t nThreadHandle);
    static void Resume(uint64_t nThreadHandle);
    static void SetCPU(uint64_t handle, uint32_t nAffinityMask);
    void SetCPU(uint32_t nAffinityMask);
    static uint64_t GetCPU();
    static void GetCPU(uint64_t, uint32_t *);
    static uint32_t GetCPUCount();
    enum eThreadPriority {
        eThreadPriority_Low,
        eThreadPriority_Medium,
        eThreadPriority_High,
    };
    static void SetPriority(uint64_t handle, ZThread::eThreadPriority priority);
    void SetPriority(ZThread::eThreadPriority priority);
    static ZThread::eThreadPriority GetPriority(uint64_t handle);
    static void LowerPriorityAndYield(ZThread::eThreadPriority destinationPriority);
    bool IsCurrent();
    uint32_t GetExitCode();
    static uint32_t AllocateDataSlot();
    static void FreeDataSlot(uint32_t nSlot);
    static void SetData(uint32_t nSlot, void *pData);
    static void *GetData(uint32_t nSlot);
    uint64_t GetThreadHandle();
    uint64_t GetThreadID();
    ZDelegate<unsigned int(void *)> m_function;
    uint64_t m_handle;
    ZString m_sName;
    uint32_t m_nExitCode;
    int32_t m_nStackSize;
    uint8_t m_nJoinState;
};

class ZThreadEvent {
    public:
    ZThreadEvent();
    void WaitEvent();
    static void SignalEvent(ZThreadEvent &event);
    void SignalEvent();
    static bool WaitForSingleEvent(ZThreadEvent &event, int32_t nTimeOutMSecs);
    TSharedPtr<IComponentInterface> m_pImpl;
};

class ZSmallBlockAllocator : public IAllocator {
    public:
    virtual int64_t AddRef();
    virtual int64_t Release();
    ZSmallBlockAllocator(const char *name);
    ZSmallBlockAllocator(const ZSmallBlockAllocator &);
    ZSmallBlockAllocator &operator=(const ZSmallBlockAllocator &);
    virtual ZVariantRef GetVariantRef();
    virtual void *QueryInterface(STypeID *iid);
    const SComponentMapEntry *GetComponentInterfacesInternal();
    enum SmallBlockAllocatorParameters {
        TotalVirtualSize,
        VirtualPageSize,
        MaxNumBuckets,
        PlatformAlignment,
    };
    enum SmallBlockAllocatorDerivedParameters {
        PageSize,
        NumVirtualPages,
        NumPagesPerVirtualPage,
        NumPages,
    };
    virtual ~ZSmallBlockAllocator();
    virtual void *Allocate(uint64_t nSize, uint32_t unused);
    virtual void *AllocateAligned(uint64_t nSize, uint64_t nAlignment, uint32_t unused);
    virtual void Free(void *pObject);
    virtual void *ReAllocate(void *pData, uint64_t nNewSize, uint32_t ra);
    virtual void *ReAllocateAligned(void *pData, uint64_t nNewSize, uint64_t nAlignment,
                                    uint32_t ra);
    virtual uint64_t GetDefaultAlignment();
    virtual bool SupportsAlignedAllocations();
    virtual uint64_t GetAllocationSize(void *pData);
    uint64_t GetManagedMemorySize();
    bool IsInAddressSpace(void *pObject);
    void DumpStats();
    virtual SAllocatorStatus GetStatus();
    virtual SAllocatorUsage GetUsage();
    virtual void CheckHeap();
    virtual void ResetThreadMemTracking();
    virtual int64_t GetThreadMemTracking();
    void *m_pAllocator;
};

class ZMemoryManager {
    public:
    ZMemoryManager();
    ~ZMemoryManager();
    void InstallPageAllocator(IPageAllocator *pPageAllocator);
    void UnInstallPageAllocator();
    IPageAllocator *GetPageAllocator();
    void InstallAllocator(uint64_t nID, IAllocator *pAllocator);
    void UnInstallAllocator(uint64_t nID);
    void UnInstallAllocators();
    IAllocator &GetAllocator(uint64_t nID);
    IAllocator *TryGetAllocator(uint64_t nID);
    ZSmallBlockAllocator *TryGetSmallBlockAllocator();
    void Initialize();
    void Shutdown();
    void *Allocate(uint64_t nSize, uint32_t tag);
    void *AllocateAligned(uint64_t nSize, uint64_t nAlignment, uint32_t tag);
    void Free(void *pObject);
    void FreeAligned(void *pObject);
    void InitializeHeapCheck();
    void ShutdownHeapCheck();
    uint32_t HeapCheckWatchdog(void *params);
    static const int32_t MAX_ALLOCATORS;
    IPageAllocator *m_pPageAllocator;
    bool m_Initialized;
    IAllocator *m_InstalledAllocators[32];
    IAllocator *m_Allocators[32];
    ZThread *m_threadHeapCheckWatchdog;
    static bool m_bQuitHeapCheckWatchdog;
};

class ZResourceID {
    public:
    static void RegisterType();
    ZResourceID(ZString *rhs);
    ZResourceID(ZResourceID *rhs);
    ZResourceID(const ZString &rhs);
    ZResourceID(const ZResourceID &rhs);
    ZResourceID();
    ZResourceID &operator=(ZResourceID *rhs);
    ZResourceID &operator=(const ZResourceID &rhs);
    bool operator==(const ZString &rhs);
    bool operator==(const ZResourceID &rhs);
    bool operator!=(const ZResourceID &rhs);
    bool operator<(const ZResourceID &rhs);
    bool IsValid();
    bool IsEmpty();
    bool IsDerived() const;
    bool IsLibraryResource();
    bool IsLibrary();
    int32_t GetIndexInLibrary();
    void GetExtension(ZString &sExtensionOut);
    ZString GetExtension() const;
    ZString GetAllExtensions();
    void GetPlatformNeutralExtension(ZString &aOut);
    ZString GetPlatformNeutralExtension();
    void GetPlatformFromExtension(const ZString &sExtension, ZString &sPlatformOut);
    ZString GetPlatformFromExtension(const ZString &sExtension);
    ZString GetPlatformFromExtension();
    ZResourceID ReplaceProtocol(const ZString &sProtocol);
    void ReplacePlatform(const ZString &sTargetPlatform, ZResourceID &resourceIdOut);
    ZResourceID ReplacePlatform(const ZString &sTargetPlatform);
    void ReplaceExtension(const ZString &sExtension, ZString &sOut);
    ZResourceID ReplaceExtension(const ZString &sExtension);
    ZResourceID ReplacePlaformNeutralExtension(const ZString &sExtension);
    ZResourceID ReplaceRootPath(const ZString &sRootPath);
    ZResourceID ReplaceRootParameters(const ZString &sRootParameters);
    ZResourceID ReplaceCharacteristicName(const ZString &sCharacteristicName);
    bool FromString(ZString *sSource);
    bool FromString(const ZString &sSource);
    const ZString &ToString() const;
    uint64_t ToCString(char *pBuffer, uint64_t nSize, const ZString &sFormat);
    ZResourceID GetRoot() const;
    ZString GetProtocolName() const;
    ZString GetRootPath() const;
    ZString GetRootExtension();
    ZString GetRootParameters() const;
    ZString GetCharacteristicName();
    ZResourceID CreateDerivedResourceID(const ZString &sExtension, const ZString &sParameters,
                                        const ZString &sPlatform);
    bool IsAspectTypeResource();
    ZResourceID GetBaseType();
    ZResourceID GetSourceResourceID() const;
    void SetToInnermostSourceResourceID();
    ZResourceID GetInnermostSourceResourceID() const;
    TEnumerator<ZString> GetParameters();
    uint64_t GetParameterCount() const;
    ZString GetParameter(uint64_t nIndex) const;
    ZResourceID ReplaceParameters(const ZString &sParameters);
    ZString GetStrippedName();
    static ZResourceID GetEntityHeaderLibRID(const ZResourceID &entityID);
    static ZResourceID GetEntityHeaderLibRID(const ZString &szEntityName);
    static void GetEntityDerivedRIDs(const ZResourceID &entityID, ZResourceID &headerlib,
                                     ZResourceID &entityTemplate, ZResourceID &sceneBlueprint);
    static void GetEntityDerivedRIDs(const ZString &szEntityName, ZResourceID &headerlib,
                                     ZResourceID &entityTemplate, ZResourceID &sceneBlueprint);
    static uint64_t FindMatchingParentheses(const ZString &str, uint64_t startIndex, char open,
                                            char close);
    uint64_t GetDerivedEndIndex();
    static const ZResourceID m_sAspectBaseResource;
    static const int32_t m_sBaseAspectIndex;
    ZString m_uri;
};

class IBaseInputStream : public IComponentInterface {
    public:
    enum ESeekOrigin {
        ORIGIN_END,
        ORIGIN_BEGIN,
        ORIGIN_CURRENT,
        ORIGIN_LAST,
    };
    virtual ~IBaseInputStream();
    virtual bool IsValid();
    virtual bool IsSeekable();
    virtual uint64_t GetLength();
    virtual uint64_t GetPosition();
    virtual void Close();
    virtual ZString GetLastErrorMessage();
    virtual void OpenCallback();
};

class IInputStream : public IBaseInputStream {
    public:
    virtual uint64_t Read(void *, uint64_t);
    virtual uint64_t GetChar();
    virtual uint64_t Skip(uint64_t);
    virtual uint64_t Seek(int64_t, IBaseInputStream::ESeekOrigin);
};

class IOutputStream : public IComponentInterface {
    public:
    enum ESeekOrigin {
        ORIGIN_END,
        ORIGIN_BEGIN,
        ORIGIN_CURRENT,
    };
    virtual bool IsValid();
    virtual uint64_t Write(const void *, uint64_t);
    virtual bool Flush();
    virtual bool Close();
    virtual bool IsSeekable();
    virtual uint64_t Seek(int64_t, IOutputStream::ESeekOrigin);
    virtual int64_t GetPosition();
    virtual ZString GetLastErrorMessage();
};

class ZBuffer {
    public:
    ZBuffer(const ZBuffer &rhs);
    ZBuffer(IInputStream *pInStream);
    ZBuffer(const void *pData, uint64_t nSize);
    ZBuffer(uint64_t nSize);
    ZBuffer();
    ~ZBuffer();
    ZBuffer &operator=(const ZBuffer &rhs);
    bool operator==(const ZBuffer &rhs);
    bool operator!=(const ZBuffer &rhs);
    uint64_t Size();
    bool IsEmpty();
    const void *Get() const;
    void *Get();
    void Resize(uint64_t nSize);
    void Clear();
    bool IsShared();
    void MakeUnique();
    uint64_t FillFromInStream(IInputStream *pInStream, uint64_t nLength);
    uint64_t FillFromInStream(IInputStream *pInStream);
    IInputStream *GetInputStream();
    IOutputStream *GetOutputStream();
    IOutputStream *GetSeekableOutputStream();
    static IInputStream *CreateInputStream(IInputStream *pInStream);
    IInputStream *GetInputStreamWithOwnership();
    struct SBufferData {
        uint64_t m_nRefCount;
        uint64_t m_nSize;
    };
    ZBuffer::SBufferData *m_pData;
    void IncReference();
    void DecReference();
    void Create(const void *pData, uint64_t nSize);
};

template <typename T>
class TComponentPtr {
    public:
    TComponentPtr<T>(TComponentPtr<T> &);
    TComponentPtr<T>(T *);
    ~TComponentPtr<T>();
    TComponentPtr<T> &operator=(T *);
    TComponentPtr<T> &operator=(TComponentPtr<T> &);
    bool operator!=(const T *);
    bool operator==(const T *);
    T *Get();
    T *Detach();
    T *operator->();
    T *m_pComponent;
    void Cleanup();
};

template <typename T>
class TComponentSharedPtr {
    public:
    struct ZNull {};
    TComponentSharedPtr<T>(const TComponentSharedPtr<T> &);
    TComponentSharedPtr<T>(const TComponentPtr<T> &);
    TComponentSharedPtr<T>(T *);
    TComponentSharedPtr<T>();
    ~TComponentSharedPtr<T>();
    TComponentSharedPtr<T> &operator=(TComponentSharedPtr<T>::ZNull *);
    TComponentSharedPtr<T> &operator=(const TComponentSharedPtr<T> &);
    void Attach(T *);
    TComponentSharedPtr<T> &Assign(T *pComponent);
    operator T *();
    T *operator->();
    T &operator*();
    T *m_pComponent;
    void Initialize(T *pComponent);
    void Cleanup();
};

class ZTraceMessagePostDestination {
    public:
    static const uint32_t INVALID_ID;
    static const uint32_t PERMANENT_ID;
    uint32_t GetID();
};

class ZTraceMessageBoard : public ZTraceMessagePostDestination {
    public:
    ZTraceMessageBoard();
    ~ZTraceMessageBoard();
    void SetUsed();
    bool m_bIsUsed;
};

class ZTraceMessageSlot : public ZTraceMessagePostDestination {
    public:
    static const uint32_t FNV_OFFSET_BASIS;
    static const uint32_t EMPTY_MESSAGE_HASH;
    ZTraceMessageSlot();
    ~ZTraceMessageSlot();
    bool UpdateMessageHash(uint32_t nHash);
    static uint32_t GetMessageHash(const ZString &message);
    int64_t m_nLastMessageHash;
    static const uint32_t FNV_PRIME;
};

class ZCallStack {
    public:
    ZCallStack(const ZCallStack &aOther);
    ZCallStack();
    ~ZCallStack();
    const char *ToCString();
    uint64_t GetHashCode();
    uint64_t Size();
    bool operator==(const ZCallStack &other);
    ZCallStack &operator=(const ZCallStack &aOther);
    void TrimTopOfStack(const char *aszSymbol);
    void TrimTopOfStack(uint32_t howManySymbolsToTrim);
    void TrimBottomOfStack(uint32_t howManySymbolsToKeep);
    uint64_t m_StackLength;
    uint64_t m_Stack[64];
    static uint32_t sDefaultTrimValue;
};

template <typename T>
class TSet : public TRedBlackTree<T> {
    public:
    static STemplatedTypeName1 s_typeName;
    static IContainerType s_typeInfo;
    TSet<T>(TSet<T> *);
    TSet<T>(const TSet<T> &);
    TSet<T>();
    ~TSet<T>();
    TSet<T> &operator=(TSet<T> *);
    TSet<T> &operator=(const TSet<T> &);
    bool Contains(const T &);
    TBinaryTreeIterator<T> Insert(const T &element);
    bool Remove(const T &);
};

template <typename T>
struct THashSetNode {
    THashSetNode<T>(const THashSetNode<T> &rhs);
    THashSetNode<T>(T &data);
    ~THashSetNode<T>();
    int64_t m_iNext;
    T m_data;
    static THashSetNode<T> *GetNodeFromData(T *pData);
};

template <typename T>
struct SHashSetInfo {
    uint64_t m_nBuckets;
    int64_t *m_pBuckets;
    THashSetNode<T> *m_pNodes;
};

template <typename T>
class THashSetIterator : public TIterator<T> {
    public:
    THashSetIterator<T>(const THashSetIterator<T> &rhs);
    THashSetIterator<T>(SHashSetInfo<T> &info, T *pCurrent, uint64_t nBucket);
    THashSetIterator<T>();
    ~THashSetIterator<T>();
    operator class THashSetIterator<T>();
    THashSetIterator<T> &operator=(const THashSetIterator<T> &);
    THashSetNode<T> &Node();
    THashSetIterator<T> operator++(int32_t __formal);
    THashSetIterator<T> &operator++();
    SHashSetInfo<T> *m_info;
    uint64_t m_nBucket;
};

template <typename T>
class THashSetEnumerator; // TODO

template <typename T, typename P>
class THashSet {
    public:
    static STemplatedTypeName1 s_typeName;
    static IContainerType s_typeInfo;
    THashSet<T, P>(THashSet<T, P> *);
    THashSet<T, P>(const THashSet<T, P> &rhs);
    THashSet<T, P>(void *, uint64_t);
    THashSet<T, P>(uint64_t nBuckets);
    ~THashSet<T, P>();
    THashSet<T, P> &operator=(THashSet<T, P> *);
    THashSet<T, P> &operator=(const THashSet<T, P> &);
    THashSetIterator<T const> Begin() const;
    THashSetIterator<T const> Begin();
    THashSetIterator<T const> End() const;
    THashSetIterator<T const> End();
    THashSetEnumerator<T const> GetEnumerator() const;
    THashSetEnumerator<T const> GetEnumerator();
    void GetSplittedIterators(THashSetIterator<T const> *, uint64_t);
    bool IsEmpty();
    uint64_t Size();
    uint64_t Capacity();
    bool Contains(const T &key);
    bool TryGet(const T &);
    void Assign(const THashSet<T, P> &rhs);
    void Clear();
    void Reserve(uint64_t, bool);
    THashSetIterator<T const> Insert(const T &key);
    THashSetIterator<T const> Erase(THashSetIterator<T const> where);
    bool Remove(const T &);
    void Swap(THashSet<T, P> &other);
    void Trim();
    bool m_bFixed;
    uint64_t m_nSize;
    int64_t m_iFree;
    SHashSetInfo<T const> m_info;
    uint64_t AllocNode(const T &key);
    void FreeNode(uint64_t node);
    void EnsureCapacity(uint64_t nElements);
    void Rehash(uint64_t nNewBuckets);
    void Init(uint64_t nBuckets);
    void Cleanup();
    void ClearMembers();
    void DestroyElements();
};

template <typename T>
class TDefaultHashSetPolicy {
    public:
    static uint64_t GetHashCode(const T &key);
    static bool IsEqual(const T &a, const T &b);
    static uint64_t InternalGetHashCode(uint64_t);
    static uint64_t InternalGetHashCode(uint32_t);
    static uint64_t InternalGetHashCode(int32_t);
    static uint32_t InternalGetHashCode(void *);
};

struct int4 {
    int32_t operator[](uint64_t nSubscript);
    __m128i m;
    int32_t v[4];
    int32_t x;
    int32_t y;
    int32_t z;
    int32_t w;
};

class ZAssertlock {
    public:
    ZAssertlock(const ZAssertlock &);
    ZAssertlock();
    ~ZAssertlock();
    void LockRead();
    void UnlockRead();
    void LockWrite();
    void UnlockWrite();
    void EnableDebugMode(bool bEnable);
    ZAssertlock &operator=(const ZAssertlock &);
    struct AtomicMutexValues {
        uint32_t m_iLockedThreadId;
        uint32_t m_iRefCountRead;
        uint32_t m_iRefCountWrite;
    };
    union AtomicMutex {
        ZAssertlock::AtomicMutexValues m_atomicValues;
        uint64_t m_atomic;
    };
    std::atomic<unsigned __int64> m_atomicMutex;
    bool m_bDebugMode;
};

class ZSimpleBlockAllocator {
    public:
    class ZBlock {
        public:
        ZBlock(uint64_t itemSize, uint64_t capacity);
        ~ZBlock();
        uint8_t *Alloc();
        void Free(uint8_t *ptr);
        bool IsEmpty();
        bool HasFreeSlots();
        bool IsParent(uint8_t *ptr);
        uint64_t Capacity();
        uint64_t Size();
        void GetAllPtrs(TArrayRef<unsigned char *> &allPtrs);
        uint64_t GetAllocStatusCount();
        const uint64_t m_ItemSize;
        const uint64_t m_Capacity;
        uint64_t m_Size;
        uint8_t *m_BasePtr;
        uint64_t *m_AllocStatus;
        uint64_t m_FirstFreeAllocStatusCandidateIndex;
    };
    ZSimpleBlockAllocator();
    virtual ~ZSimpleBlockAllocator();
    virtual void Trim();
    static void RegisterAllocatorForDebug(const ZString &name, ZSimpleBlockAllocator *allocator);
    static void UnregisterAllocatorForDebug(const ZString &name);
};

template <typename T>
class TSimpleBlockAllocator : public ZSimpleBlockAllocator {
    public:
    TSimpleBlockAllocator<T>(uint64_t initialCapacity, uint64_t incrementCapacity,
                             bool autoInitialize, bool autoTrim);
    virtual ~TSimpleBlockAllocator<T>();
    uint8_t *Alloc();
    void Free(T *ptr);
    T *New();
    void Delete(T **, uint64_t);
    void Delete(T *);
    void DeleteAll();
    virtual void Trim();
    void Initialize(const ZString &name);
    void Uninitialize();
    ZSimpleBlockAllocator::ZBlock *GetValidBlock();
    ZSimpleBlockAllocator::ZBlock *GetParentBlock(T *ptr);
    ZSimpleBlockAllocator::ZBlock *AppendBlock();
    void BlockFree(ZSimpleBlockAllocator::ZBlock *block, T *ptr);
    const uint64_t m_InitialBlockCapacity;
    const uint64_t m_IncrementBlockCapacity;
    const bool m_AutoTrim;
    uint64_t m_EmptyBlockCount;
    TArray<ZSimpleBlockAllocator::ZBlock *> m_Blocks;
    ZSimpleBlockAllocator::ZBlock *m_pInitialBlock;
    ZMutex m_Mutex;
    uint64_t m_TotalAllocationCount;
    uint64_t m_MaxAllocationCount;
    uint64_t m_CurrentAllocationCount;
    ZString m_name;
};

class DefaultHashAllocator {
    public:
    static void DeleteArray(uint8_t *aArray);
    static uint8_t *NewArray(int32_t aCapacity);
};

template <typename T>
int32_t Hash_DefaultHash(const T &key);

template <typename T>
int32_t HashSet_ZDeletionDelegate(const ZDelegate<T> &key);

template <typename K, int32_t (*H)(const K &), int32_t N, typename A>
class TUnorderedLinearHashSet {
    public:
    struct Header {
        uint8_t m_bInUse;
        uint8_t m_bClashCount;
    };
    struct Iterator {
        Iterator(const TUnorderedLinearHashSet<K, H, N, A> *aOwner, int32_t aIndex);
        K &operator*();
        void operator++();
        bool operator==(const TUnorderedLinearHashSet<K, H, N, A>::Iterator &);
        bool operator!=(const TUnorderedLinearHashSet<K, H, N, A>::Iterator &aOther);
        const TUnorderedLinearHashSet<K, H, N, A> *mHashMap;
        int32_t mIndex;
    };
    struct ConstIterator {
        ConstIterator(const TUnorderedLinearHashSet<K, H, N, A> *aOwner, int32_t aIndex);
        K &operator*();
        void operator++();
        bool operator==(const TUnorderedLinearHashSet<K, H, N, A>::Iterator &);
        bool operator!=(const TUnorderedLinearHashSet<K, H, N, A>::Iterator &aOther);
        const TUnorderedLinearHashSet<K, H, N, A> *mHashMap;
        int32_t mIndex;
    };
    TUnorderedLinearHashSet<K, H, N, A>(const TUnorderedLinearHashSet<K, H, N, A> &);
    TUnorderedLinearHashSet<K, H, N, A>(TUnorderedLinearHashSet<K, H, N, A> *);
    TUnorderedLinearHashSet<K, H, N, A>(int32_t);
    TUnorderedLinearHashSet<K, H, N, A>();
    ~TUnorderedLinearHashSet<K, H, N, A>();
    TUnorderedLinearHashSet<K, H, N, A> &operator=(TUnorderedLinearHashSet<K, H, N, A> *);
    TUnorderedLinearHashSet<K, H, N, A> &operator=(const TUnorderedLinearHashSet<K, H, N, A> &);
    void Clear();
    void FullClear();
    void Insert(int32_t aHash, const K &aKey);
    void Insert(const K &aKey);
    K *GetTkAtIndex(TUnorderedLinearHashSet<K, H, N, A>::Header *aBuffer, int32_t aIndex,
                    int32_t aCapacity);
    TUnorderedLinearHashSet<K, H, N, A>::Iterator Find(const K &aKey);
    bool Contains(const K &aKey);
    TUnorderedLinearHashSet<K, H, N, A>::Iterator End();
    TUnorderedLinearHashSet<K, H, N, A>::Iterator Begin();
    TUnorderedLinearHashSet<K, H, N, A>::Iterator
        Erase(TUnorderedLinearHashSet<K, H, N, A>::Iterator);
    int32_t Capacity();
    int32_t Size();
    bool IsEmpty();
    void Swap(TUnorderedLinearHashSet<K, H, N, A> &);
    void Cleanup();
    void InitialCreate(int32_t aInitialSize);
    void CreateNewBuffer(TUnorderedLinearHashSet<K, H, N, A>::Header **aBuffer, int32_t aCapacity);
    static bool InsertVal(int32_t hash, const K &aKey,
                          TUnorderedLinearHashSet<K, H, N, A>::Header *aBuffer, int32_t aCapacity,
                          int32_t *aElementCount);
    void DestroyAllElements(TUnorderedLinearHashSet<K, H, N, A>::Header *aBuffer,
                            int32_t aCapacity);
    static K *GetTkPtr(TUnorderedLinearHashSet<K, H, N, A>::Header *aBuffer, int32_t aCapacity);
    static bool IsIndexOccupied(TUnorderedLinearHashSet<K, H, N, A>::Header *aBuffer,
                                int32_t aIndex);
    static void SetIndexOccupied(TUnorderedLinearHashSet<K, H, N, A>::Header *aBuffer,
                                 int32_t aIndex);
    static void SetIndexUnoccupied(TUnorderedLinearHashSet<K, H, N, A>::Header *, int32_t);
    static uint8_t GetClashCount(TUnorderedLinearHashSet<K, H, N, A>::Header *aBuffer,
                                 int32_t aIndex);
    static void IncrementClashCount(TUnorderedLinearHashSet<K, H, N, A>::Header *aBuffer,
                                    int32_t aIndex);
    static void DecrementClashCount(TUnorderedLinearHashSet<K, H, N, A>::Header *, int32_t);
    int32_t mCapacity;
    int32_t mElementCount;
    TUnorderedLinearHashSet<K, H, N, A>::Header *mBuffer;
    K *mElements;
};

template <typename K, typename V, int32_t (*H)(const K &), int32_t N, typename A>
class TUnorderedLinearHashMap {
    public:
    struct TK_TV {
        K &Key();
        V &Value();
        K first;
        V second;
    };
    struct Header {
        union {
            uint8_t m_bInUse;
            uint8_t m_bClashCount;
        };
    };
    struct Iterator {
        Iterator(const TUnorderedLinearHashMap<K, V, H, N, A> *hm, int32_t i)
            : mHashMap(hm), mIndex(i) {}
        Iterator(const TUnorderedLinearHashMap<K, V, H, N, A>::Iterator &);
        Iterator();
        TUnorderedLinearHashMap<K, V, H, N, A>::TK_TV *operator->() const {
            return mHashMap->GetTkTvAtIndex(
                mHashMap->mBuffer, mIndex, mHashMap->mCapacity);
        }
        TUnorderedLinearHashMap<K, V, H, N, A>::TK_TV &operator*() {
            return *mHashMap->GetTkTvAtIndex(
                mHashMap->mBuffer, mIndex, mHashMap->mCapacity);
        }
        void operator++() {
            auto next_idx = mIndex + 1;
            if (next_idx < mHashMap->mCapacity) {
                // XXX: This (and all accesses into mBuffer) looks significantly
                // different in Ghidra. Find out why, the Ghidra code is just
                // plain broken
                byte *header = reinterpret_cast<byte *>(mHashMap->mBuffer) + next_idx;
                do {
                    if ((*header & 1) != 0) {
                        mIndex = next_idx;
                        return;
                    }
                    next_idx++;
                    header++;
                } while (next_idx < mHashMap->mCapacity);
            }
            mIndex = mHashMap->mCapacity;
        }
        bool operator==(const TUnorderedLinearHashMap<K, V, H, N, A>::Iterator &other) {
            return mHashMap == other.mHashMap && mIndex == other.mIndex;
        }
        bool operator<(const TUnorderedLinearHashMap<K, V, H, N, A>::Iterator &);
        bool operator!=(const TUnorderedLinearHashMap<K, V, H, N, A>::Iterator &other) {
            return mHashMap != other.mHashMap || mIndex != other.mIndex;
        }
        const TUnorderedLinearHashMap<K, V, H, N, A> *mHashMap;
        int32_t mIndex;
    };
    struct ConstIterator {
        ConstIterator(const TUnorderedLinearHashMap<K, V, H, N, A> *, int32_t);
        ConstIterator(const TUnorderedLinearHashMap<K, V, H, N, A>::Iterator &);
        ConstIterator();
        TUnorderedLinearHashMap<K, V, H, N, A>::TK_TV *operator->();
        TUnorderedLinearHashMap<K, V, H, N, A>::TK_TV &operator*();
        void operator++();
        bool operator==(const TUnorderedLinearHashMap<K, V, H, N, A>::Iterator &);
        bool operator<(const TUnorderedLinearHashMap<K, V, H, N, A>::Iterator &);
        bool operator!=(const TUnorderedLinearHashMap<K, V, H, N, A>::Iterator &);
        const TUnorderedLinearHashMap<K, V, H, N, A> *mHashMap;
        int32_t mIndex;
    };
    TUnorderedLinearHashMap<K, V, H, N, A>(const TUnorderedLinearHashMap<K, V, H, N, A> &);
    TUnorderedLinearHashMap<K, V, H, N, A>(TUnorderedLinearHashMap<K, V, H, N, A> *);
    TUnorderedLinearHashMap<K, V, H, N, A>(int32_t);
    TUnorderedLinearHashMap<K, V, H, N, A>();
    TUnorderedLinearHashMap<K, V, H, N, A> &operator=(TUnorderedLinearHashMap<K, V, H, N, A> *);
    TUnorderedLinearHashMap<K, V, H, N, A> &
    operator=(const TUnorderedLinearHashMap<K, V, H, N, A> &);
    ~TUnorderedLinearHashMap<K, V, H, N, A>();
    void Clear();
    void FullClear();
    TUnorderedLinearHashMap<K, V, H, N, A>::Iterator Insert(int32_t, K const &, const uint16_t &);
    TUnorderedLinearHashMap<K, V, H, N, A>::Iterator Insert(K const &, const uint16_t &);
    TUnorderedLinearHashMap<K, V, H, N, A>::TK_TV * GetTkTvAtIndex(
            TUnorderedLinearHashMap<K, V, H, N, A>::Header *aBuffer,
            int32_t aIndex, int32_t aCapacity) const {
        return (TK_TV *) ((char *) aBuffer + aCapacity + aIndex*sizeof(TK_TV));
    }
    uint16_t &operator[](K const &);
    void GetSplittedIterators(TUnorderedLinearHashMap<K, V, H, N, A>::Iterator *, uint64_t);
    TUnorderedLinearHashMap<K, V, H, N, A>::Iterator Find(K const &);
    bool Contains(K const &);
    TUnorderedLinearHashMap<K, V, H, N, A>::Iterator End() { return {this, mCapacity}; }
    TUnorderedLinearHashMap<K, V, H, N, A>::Iterator
        Erase(TUnorderedLinearHashMap<K, V, H, N, A>::Iterator);
    TUnorderedLinearHashMap<K, V, H, N, A>::Iterator Begin() const {
        if (mElementCount == 0) {
            return {this, mCapacity};
        }
        if (mCapacity > 0) {
            int32_t iter_idx = 0;
            do {
                if ((*(reinterpret_cast<byte *>(mBuffer) + iter_idx) & 1) != 0) {
                    return {this, iter_idx};
                }
                iter_idx++;
            } while (iter_idx < mCapacity);
        }
        return {this, 0};
    }
    int32_t Capacity();
    int32_t Size();
    bool IsEmpty();
    void Swap(TUnorderedLinearHashMap<K, V, H, N, A> &);
    void Cleanup();
    void InitialCreate(int32_t);
    void CreateNewBuffer(TUnorderedLinearHashMap<K, V, H, N, A>::Header **, int32_t);
    static bool InsertVal(int32_t, K const &, const uint16_t &,
                          TUnorderedLinearHashMap<K, V, H, N, A>::Header *, int32_t, int32_t *,
                          int32_t *);
    void DestroyAllElements(TUnorderedLinearHashMap<K, V, H, N, A>::Header *, int32_t);
    static TUnorderedLinearHashMap<K, V, H, N, A>::TK_TV *
    GetTkTvPtr(TUnorderedLinearHashMap<K, V, H, N, A>::Header *, int32_t);
    static bool IsIndexOccupied(TUnorderedLinearHashMap<K, V, H, N, A>::Header *h, int32_t i);
    static void SetIndexOccupied(TUnorderedLinearHashMap<K, V, H, N, A>::Header *, int32_t);
    static void SetIndexUnoccupied(TUnorderedLinearHashMap<K, V, H, N, A>::Header *, int32_t);
    static uint8_t GetClashCount(TUnorderedLinearHashMap<K, V, H, N, A>::Header *, int32_t);
    static void IncrementClashCount(TUnorderedLinearHashMap<K, V, H, N, A>::Header *, int32_t);
    static void DecrementClashCount(TUnorderedLinearHashMap<K, V, H, N, A>::Header *, int32_t);
    int32_t mInitialCapacity;
    int32_t mCapacity;
    int32_t mElementCount;
    TUnorderedLinearHashMap<K, V, H, N, A>::Header *mBuffer;
    TUnorderedLinearHashMap<K, V, H, N, A>::TK_TV *mElements;
};

class ZSharedMutex {
    public:
    ZSharedMutex(const ZSharedMutex &);
    ZSharedMutex();
    ~ZSharedMutex();
    void SharedLock();
    bool TrySharedLock();
    void SharedUnlock();
    void ExclusiveLock();
    bool TryExclusiveLock();
    void ExclusiveUnlock();
    ZSharedMutex &operator=(const ZSharedMutex &);
    static const int32_t IMPL_SIZE;
    uint64_t m_impl[1];
};

class ZConditionVariable {
    public:
    ZConditionVariable();
    ~ZConditionVariable();
    void Sleep(ZSharedMutex &mutex);
    void WakeAll();
    void Wake();
    static const int32_t IMPL_SIZE;
    uint64_t m_impl[1];
};

template <typename T>
class TFutureState : public std::enable_shared_from_this<TFutureState<T>> {
    public:
    bool Wait();
    bool IsReady();
    bool IsCancelled();
    bool HasEnded();
    bool Cancel();
    bool SetData();
    void RegisterFulfillDelegate(const std::function<T(void)> &);
    void RegisterCancelDelegate(const std::function<T(void)> &);
    ZSharedMutex &GetMutex();
    ZSharedMutex m_Mutex;
    ZConditionVariable m_DataIsReady;
    std::vector<std::function<T(void)>, std::allocator<std::function<T(void)>>> m_FulfillDelegates;
    std::vector<std::function<T(void)>, std::allocator<std::function<T(void)>>> m_CancelDelegates;
    std::atomic<bool> m_IsCancelled;
    std::atomic<bool> m_IsFinished;
};

template <typename T>
class TFuture {
    public:
    TFuture<T>(const TSharedPtr<TFutureState<T>> &);
    TFuture<T>(const TFuture<T> &);
    TFuture<T>();
    bool IsValid();
    bool Wait();
    bool IsReady();
    bool Cancel();
    bool IsCancelled();
    TFuture<T> &operator+=(std::function<T(void)>);
    bool operator==(const TFuture<T> &);
    TSharedPtr<TFutureState<T>> m_State;
};

class ZSemaphore {
    public:
    ZSemaphore();
    ~ZSemaphore();
    int32_t Wait(int32_t);
    int32_t Signal(int32_t);
    uint64_t m_Handle;
};

class ZNetUtil {
    public:
    static bool StringToAddr(in_addr *pAddr, const ZString &sAddress);
    static ZString AddrToString(const in_addr &addr);
    static bool InitializeNetwork();
    static void DeinitializeNetwork();
    static bool IsNetworkInitialized();
    static bool InitializeNetworkInternal();
    static bool m_NetworkInitialized;
};

class ZEventNull;

template <typename A, typename B, typename C, typename D, typename E, typename F>
class ZEvent;

template <typename T>
class ZEvent1 {
    public:
    static const ZDelegate<void(T)> &ContainedTypeToDelegateType(const ZDelegate<void(T)> &);
    static ZDelegate<void(T)> &ContainedTypeToDelegateType(ZDelegate<void(T)> &);
    ZEvent1<T>();
    void operator()(T);
    void operator+=(const ZDelegate<void(T)> &);
    void operator-=(const ZDelegate<void(T)> &);
    bool Contains(const ZDelegate<void(T)> &, bool);
    bool IsEmpty();
    bool InvocationInProgress();
    uint64_t GetDelegateCount();
    void Add(const ZDelegate<void(T)> &);
    void Remove(const ZDelegate<void(T)> &);
    void Trim();
    struct SInvocationData {
        SInvocationData();
        ZEvent1<T>::SInvocationData *pNullOrDelegateAddedIndicator;
        uint64_t nRemoved;
        TList<ZDelegate<void(T)>> added;
    };
    static void InvokeAll(const TArray<ZDelegate<void(T)>> &, T);
    static TListIterator<ZDelegate<void(T)>> Find(TList<ZDelegate<void(T)>> &,
                                                  const ZDelegate<void(T)> &);
    static TListIterator<ZDelegate<void(T)> const> Find(const TList<ZDelegate<void(T)>> &,
                                                        const ZDelegate<void(T)> &);
    static TArrayIterator<ZDelegate<void(T)>> Find(TArray<ZDelegate<void(T)>> &,
                                                   const ZDelegate<void(T)> &);
    static TArrayIterator<ZDelegate<void(T)> const> Find(const TArray<ZDelegate<void(T)>> &,
                                                         const ZDelegate<void(T)> &);
    static bool IsSet(const ZEvent1<T>::SInvocationData *);
    TArray<ZDelegate<void(T)>> m_delegates;
    ZEvent1<T>::SInvocationData *m_pInvocation;
};

template <typename A>
class ZEvent<A, ZEventNull, ZEventNull, ZEventNull, ZEventNull, ZEventNull> : ZEvent1<A> {};

template <typename A, typename B>
class ZEvent2 {
    public:
    static const ZDelegate<void(A, B)> &ContainedTypeToDelegateType(const ZDelegate<void(A, B)> &);
    static ZDelegate<void(A, B)> &ContainedTypeToDelegateType(ZDelegate<void(A, B)> &);
    ZEvent2<A, B>();
    void operator()(A, B);
    void operator+=(const ZDelegate<void(A, B)> &);
    void operator-=(const ZDelegate<void(A, B)> &);
    bool Contains(const ZDelegate<void(A, B)> &, bool);
    bool IsEmpty();
    bool InvocationInProgress();
    uint64_t GetDelegateCount();
    void Add(const ZDelegate<void(A, B)> &);
    void Remove(const ZDelegate<void(A, B)> &);
    void Trim();
    struct SInvocationData {
        SInvocationData();
        ZEvent2<A, B>::SInvocationData *pNullOrDelegateAddedIndicator;
        uint64_t nRemoved;
        TList<ZDelegate<void(A, B)>> added;
    };
    static void InvokeAll(const TArray<ZDelegate<void(A, B)>> &, A, B);
    static TListIterator<ZDelegate<void(A, B)>> Find(TList<ZDelegate<void(A, B)>> &,
                                                     const ZDelegate<void(A, B)> &);
    static TListIterator<ZDelegate<void(A, B)> const> Find(const TList<ZDelegate<void(A, B)>> &,
                                                           const ZDelegate<void(A, B)> &);
    static TArrayIterator<ZDelegate<void(A, B)>> Find(TArray<ZDelegate<void(A, B)>> &,
                                                      const ZDelegate<void(A, B)> &);
    static TArrayIterator<ZDelegate<void(A, B)> const> Find(const TArray<ZDelegate<void(A, B)>> &,
                                                            const ZDelegate<void(A, B)> &);
    static bool IsSet(const ZEvent2<A, B>::SInvocationData *);
    TArray<ZDelegate<void(A, B)>> m_delegates;
    ZEvent2<A, B>::SInvocationData *m_pInvocation;
};

template <typename A, typename B>
class ZEvent<A, B, ZEventNull, ZEventNull, ZEventNull, ZEventNull> : ZEvent2<A, B> {};

/*template <typename A, typename B, typename C>
class ZEvent<A, B, C, ZEventNull, ZEventNull, ZEventNull> :
    ZEvent3<A, B, C> {};

template <typename A, typename B, typename C, typename D>
class ZEvent<A, B, C, D, ZEventNull, ZEventNull> :
    ZEvent4<A, B, C, D> {};

template <typename A, typename B, typename C, typename D, typename E>
class ZEvent<A, B, C, D, E, ZEventNull> :
    ZEvent5<A, B, C, D, E> {};

template <typename A, typename B, typename C, typename D, typename E, typename F>
class ZEvent<A, B, C, D, E, F> :
    ZEvent6<A, B, C, D, E, F> {};*/

struct SMatrix;

struct float4 {
    float Dot2(const float4 v);
    float Dot3(const float4 v);
    float Dot4(const float4 v);
    float4 Dot2Repl(const float4);
    float4 Dot3Repl(const float4);
    float4 Dot4Repl(const float4);
    float4 Cross(const float4 v);
    float4 MulAdd(const float4 mul, const float4 add);
    float4 MulAdd(float mul, const float4 add);
    float4 Normalize2();
    float4 Normalize3();
    float4 Normalize4();
    float4 Normalize2Exact();
    float4 Normalize3Exact();
    float4 Normalize4Exact();
    float4 Normalize3Checked();
    float Length2();
    float Length2Estimate();
    float Length3();
    float Length3Estimate();
    float Length4();
    float Length4Estimate();
    float LengthSq2();
    float LengthSq3();
    float LengthSq4();
    float4 Rotate(const SMatrix &m);
    float4 RotateInverse(const SMatrix &m);
    float4 Transform(const SMatrix &m);
    float4 TransformInverse(const SMatrix &m);
    bool IsValid2();
    bool IsValid3();
    bool IsValid4();
    static float4 One();
    static float4 Zero();
    static float4 UnitX();
    static float4 UnitY();
    static float4 UnitZ();
    static float4 UnitW();
    static float4 MinusX();
    static float4 MinusZ();
    float &operator[](uint64_t nSubscript);
    float &x();
    float &y();
    float &z();
    float &w();
    __m128 m;
};

struct SMatrix {
    float4 mat[4];
    SMatrix(const float4 R0, const float4 R1, const float4 R2, const float4 R3);
    SMatrix();
    const float4 XAxis() const;
    float4 &XAxis();
    const float4 YAxis() const;
    float4 &YAxis();
    const float4 ZAxis() const;
    float4 &ZAxis();
    const float4 Trans() const;
    float4 &Trans();
    void SetXAxis(const float4 f);
    void SetYAxis(const float4 f);
    void SetZAxis(const float4 f);
    void SetTrans(const float4 f);
    const float4 operator[](int32_t nSubscript) const;
    float4 &operator[](int32_t nSubscript);
    operator const struct SMatrix22();
    operator struct SMatrix33();
    operator struct SMatrix43();
    operator struct SMatrix44();
    static const SMatrix ZERO;
};

struct SVector3 {
    union {
        struct {
            float x;
            float y;
            float z;
        };
        float v[3];
    };
    ZString ToString();
    uint64_t ToCString(char *pBuffer, uint64_t nSize, const ZString &format);
};

struct SVector4 {
    union {
        struct {
            float x;
            float y;
            float z;
            float w;
        };
        struct {
            SVector3 n;
            float d;
        };
        float v[4];
    };
    ZString ToString();
    uint64_t ToCString(char *, uint64_t, const ZString &);
};

struct SVector2 {
    union {
        struct {
            float x;
            float y;
        };
        float v[2];
    };
};

struct SMatrix33 {
    SVector3 XAxis;
    SVector3 YAxis;
    SVector3 ZAxis;
    float m11;
    float m12;
    float m13;
    float m21;
    float m22;
    float m23;
    float m31;
    float m32;
    float m33;
    float v[9];
    SVector3 r[3];
    operator struct SMatrix();
};

struct SMatrix43 {
    SVector3 XAxis;
    SVector3 YAxis;
    SVector3 ZAxis;
    SMatrix33 Rot;
    SVector3 Trans;
    float m11;
    float m12;
    float m13;
    float m21;
    float m22;
    float m23;
    float m31;
    float m32;
    float m33;
    float m41;
    float m42;
    float m43;
    float v[12];
    SVector3 r[4];
    bool operator==(const SMatrix43 &rhs);
    bool operator!=(const SMatrix43 &rhs);
    SMatrix43 &operator=(const SMatrix &mInput);
    SMatrix43 &operator=(const SMatrix43 &mInput);
    ZString ToString();
    uint64_t ToCString(char *pBuffer, uint64_t nSize, const ZString &format);
    operator struct SMatrix();
};

struct SMatrix44 {
    float m11;
    float m12;
    float m13;
    float m14;
    float m21;
    float m22;
    float m23;
    float m24;
    float m31;
    float m32;
    float m33;
    float m34;
    float m41;
    float m42;
    float m43;
    float m44;
    SVector4 r[4];
    float v[16];
    ZString ToString();
    uint64_t ToCString(char *, uint64_t, const ZString &);
    operator struct SMatrix();
};

class ZParametricCurve {
    public:
    float4 m_vA;
    float4 m_vB;
    float4 m_vC;
    float4 m_vD;
    float m_fLength;
    ZParametricCurve();
    void Reset();
    void DefineBezier(const float4 &vStart, const float4 &vTangent0, const float4 &vTangent1,
                      const float4 &vEnd, const float fCalcLenStepSize);
    void DefineHermite(const float4 &p0, const float4 &p1, const float4 &v0, const float4 &v1,
                       const float fCalcLenStepSize);
    void Transform(const SMatrix &m0);
    float4 GetCurvePosition(const float t);
    float4 GetCurveVelocity(const float t);
    float4 GetCurveAcceleration(const float t);
    float4 GetEvenCurvePosition(const float t, const float fStepSize);
    float ToSplineTime(const float t, const float fStepSize);
    float ToSplineTimeFrom(const float fPct, const float fPct0, const float t0, float &fLen,
                           const float fStepSize);
    const float CalculateLength(const float fStepSize);
    const float GetStraightLineLength();
    const float GetLength();
    float GetClosestPoint(const float4 &p0, float t, float4 &vClosestPoint, int32_t iIterations);
    const float CalcVal(const float t);
};

class ZSplineKnot {
    public:
    float4 m_vPosition;
    float4 m_vTangent[2];
    void SetPosition(const float4 &p0);
    const float4 &GetPosition();
    void SetTangent(uint32_t iIdx, const float4 &v0);
    const float4 &GetTangent(uint32_t iIdx);
};

class ZSpline;

class ZSplinePosition {
    public:
    float m_fT;
    float m_fDone;
    float m_fLength;
    int32_t m_iCurveIdx;
    const ZSpline *m_pSpline;
    ZParametricCurve m_Curve;
    ZSplinePosition(const ZSpline *pSpline);
    ZSplinePosition();
    void Reset();
    void MoveToClosestDistanceToPoint(const float4 &p0, const float fDeltaIterateDistance);
    float Advance(float fDistance);
    void SetAtPoint(uint32_t iIdx);
    void Invalidate();
    bool IsValid();
    bool IsAtStart();
    bool IsAtEnd();
    void SetAtStart();
    void SetAtEnd();
    const ZParametricCurve &GetCurve() const;
    ZParametricCurve &GetCurve();
    float GetLength();
    void ReloadCurve();
    float4 GetPosition();
    float4 GetVelocity();
    int32_t GetCurveIdx();
    float GetDone();
    float CalcDeltaMovementToSplinePosition(const ZSplinePosition &p0);
    bool operator>=(const ZSplinePosition &rhs);
    bool operator>(const ZSplinePosition &rhs);
    bool operator<=(const ZSplinePosition &rhs);
    bool operator<(const ZSplinePosition &rhs);
};

class ZSpline {
    public:
    enum EType {
        TypeLinear,
        TypeHermite,
        TypeBezier,
        TypeManual,
        TypePartiallyManual,
    };
    ZSpline::EType m_eType;
    uint32_t m_iNumberOfKnots;
    ZSplineKnot *m_pKnots;
    bool m_bLooping;
    ZSpline(ZSpline::EType eType, uint32_t iNumberOfKnots);
    ZSpline(ZSpline::EType eType);
    ZSpline(const ZSpline &rhs);
    ZSpline();
    virtual ~ZSpline();
    ZSpline &operator=(const ZSpline &rhs);
    void SetType(ZSpline::EType eType);
    ZSpline::EType GetType();
    void SetNumberOfKnots(uint32_t iValue);
    uint32_t GetNumberOfKnots();
    void SetKnot(uint32_t iIdx, const float4 &p0, const float4 &v0, const float4 &v1);
    void SetKnot(uint32_t iIdx, const float4 &p0);
    const ZSplineKnot *GetKnot(uint32_t iIdx);
    void LoadCurve(ZParametricCurve &Curve, uint32_t iIdx);
    void LoadCurve(ZSplinePosition &SplinePosition, uint32_t iIdx);
    float4 GetTangent(uint32_t iIdx, int32_t iTangentIndex);
    float GetLength();
    void AllocateKnots(uint32_t i);
    ZSplinePosition CreateSplinePosition();
    void ResetSplinePosition(ZSplinePosition &SplinePosition);
    float AdvanceSplinerPosition(ZSplinePosition &SplinePosition, float fDistance);
    void SetPositionAtIdx(ZSplinePosition &SplinePosition, uint32_t iIdx);
    float4 GetLocalPositionFromSplinePosition(const ZSplinePosition &SplinePosition);
    float4 GetLocalVelocityFromSplinePosition(const ZSplinePosition &SplinePosition);
    void SetSplinePositionAtClosestDistanceToPoint(ZSplinePosition &SplinePosition,
                                                   const float4 &vPosition,
                                                   const float fDeltaIterateDistance);
    void SetSplinePositionAtStart(ZSplinePosition &SplinePosition);
    void SetSplinePositionAtEnd(ZSplinePosition &SplinePosition);
    bool IsSplinePositionAtStart(const ZSplinePosition &SplinePosition, const float fDelta);
    bool IsSplinePositionAtEnd(const ZSplinePosition &SplinePosition, const float fDelta);
    float CalcDeltaMovement(const ZSplinePosition &SplinePos0, const ZSplinePosition &SplinePos1);
    void CalculateBezierHelpers2(const float4 &vMiddle, const float4 &vPrev, const float4 &vNext,
                                 float4 &pv1, float4 &pv2);
    void CalculateBezierHelpers(const ZSplineKnot *pMiddle, const ZSplineKnot *pPrev,
                                const ZSplineKnot *pNext, float4 &pv1, float4 &pv2);
};

struct SQuaternion {
    bool IsNearEqual(const SQuaternion &rhs, const float fTolerance);
    const float4 &Float4() const;
    float4 &Float4();
    float4 w128;
    float x();
    float y();
    float z();
    float w();
    float operator[](int32_t nSubscript);
    enum {
        X,
        Y,
        Z,
        W,
    };
    ZString ToString();
    uint64_t ToCString(char *pBuffer, uint64_t nSize, const ZString &format);
    void Normalize();
    void NormalizeEst();
    void NormalizeConditionnal();
    void SetIdentity();
    float4 Rotate(const float4 vVector);
    float4 RotateInverse(const float4 vVector);
    SQuaternion GetConjugate();
    SQuaternion GetInverse();
    SQuaternion GetNormalized();
    bool IsValid();
    void FromMatrix(const SMatrix &matrix);
    float GetAngle();
    float4 GetAxisAngle();
    float DotProduct(const SQuaternion &other);
    float GetNorm();
    float GetNormSq();
};

struct SQV {
    enum EIdentity {
        IDENTITY,
    };
    SQV(const float4 rotation, const float4 vTranslation);
    SQV(const SQuaternion &rotation, const float4 vTranslation);
    SQV(const SMatrix &m);
    SQV(const float4 vAxis, float fAngle);
    SQV(SQV::EIdentity __formal);
    SQV();
    bool operator==(const SQV &rhs);
    bool operator!=(const SQV &rhs);
    void LoadIdentity();
    void Swap(SQV &lhs);
    bool IsNearEqual(const SQV &rhs, const float fTolerance);
    SQV operator*(const SQV &rhs);
    SQV SQVTransform(const SQV &rhs);
    SQV AffineInverse();
    SQV AffineMultiplyInverse(const SQV &rhs);
    SQuaternion m_Rotation;
    float4 m_Translation;
};

template <size_t S>
class TAlignedType {
    public:
    char dummy;
};

template <typename T, size_t S>
struct ZMaxArrayData {
    uint8_t m_data[sizeof(T) * S];
    TAlignedType<S> alignDummy;
    uint64_t Capacity();
};

template <typename T, typename C>
class TMaxArrayBase : public C {
    public:
    TMaxArrayBase<T, C>(T *data, uint64_t capacity);
    TMaxArrayBase<T, C>();
    ~TMaxArrayBase<T, C>();
    TMaxArrayBase<T, C> &operator=(const TMaxArrayBase<T, C> &);
    bool operator==(const TMaxArrayBase<T, C> &);
    bool operator!=(const TMaxArrayBase<T, C> &);
    const T &operator[](uint64_t) const;
    T &operator[](uint64_t nIndex);
    operator class TArrayRef<T>();
    operator const class TArrayRef<T>() const;
    TArrayIterator<T const> Begin() const;
    TArrayIterator<T> Begin();
    TArrayIterator<T const> End() const;
    TArrayIterator<T> End();
    const T &Front() const;
    T &Front();
    const T &Back() const;
    T &Back();
    bool IsEmpty();
    uint64_t Size();
    TArrayEnumerator<T const> GetEnumerator() const;
    TArrayEnumerator<T> GetEnumerator();
    TArrayIterator<T const> Find(const T &) const;
    TArrayIterator<T> Find(const T &);
    uint64_t FindIndex(const T &);
    bool Contains(const T &);
    const T &GetElementUnsafe(uint64_t) const;
    T &GetElementUnsafe(uint64_t);
    void Assign(const TArrayRef<T> &);
    void Assign(const TMaxArrayBase<T, C> &);
    void Clear();
    void Resize(uint64_t, const T &);
    void PushBack(const TMaxArrayBase<T, C> &);
    T *PushBack(T *);
    T *PushBack(const T &element);
    T *PushBackRange(const T *, uint64_t);
    void PopBack();
    TArrayIterator<T> Insert(const TArrayIterator<T> &, const T &);
    TArrayIterator<T> Insert(const T &);
    void Erase(uint64_t);
    TArrayIterator<T> Erase(const TArrayIterator<T> &, const TArrayIterator<T> &);
    TArrayIterator<T> Erase(const TArrayIterator<T> &);
    void EraseSwap(uint64_t);
    TArrayIterator<T> EraseSwap(const TArrayIterator<T> &);
    T *GetPointer();
    const T *GetPointer() const;
    uint64_t m_nSize;
};

template <typename T, size_t S>
class TMaxArray : public TMaxArrayBase<T, ZMaxArrayData<T, S>> {
    public:
    TMaxArray<T, S>(const TArrayRef<T> &);
    TMaxArray<T, S>(TMaxArray<T, S> *);
    TMaxArray<T, S>(const TMaxArray<T, S> &);
    TMaxArray<T, S>();
    TMaxArray<T, S> &operator=(TMaxArray<T, S> *);
    TMaxArray<T, S> &operator=(const TMaxArray<T, S> &);
};

template <typename T, size_t S>
struct TMinCapacityArrayInfo {
    TArray<T> m_secondaryContainer;
    TMaxArray<T, S> m_primaryContainer;
    TMinCapacityArrayInfo<T, S>(TMinCapacityArrayInfo<T, S> *);
    TMinCapacityArrayInfo<T, S>(const TMinCapacityArrayInfo<T, S> &rhs);
    TMinCapacityArrayInfo<T, S>();
    TMinCapacityArrayInfo<T, S> &operator=(TMinCapacityArrayInfo<T, S> *rhs);
    TMinCapacityArrayInfo<T, S> &operator=(const TMinCapacityArrayInfo<T, S> &);
};

template <typename T, size_t S>
class TMinCapacityArrayIterator : public TIterator<T> {
    public:
    TMinCapacityArrayIterator<T, S>(const TMinCapacityArrayIterator<T, S> &);
    TMinCapacityArrayIterator<T, S>(TMinCapacityArrayInfo<T, S> &info, T *p);
    TMinCapacityArrayIterator<T, S>();
    TMinCapacityArrayIterator<T, S> &operator=(const TMinCapacityArrayIterator<T, S> &);
    operator class TMinCapacityArrayIterator<T const, S>();
    bool operator==(const TMinCapacityArrayIterator<T, S> &rhs);
    bool operator!=(const TMinCapacityArrayIterator<T, S> &rhs);
    TMinCapacityArrayIterator<T, S> operator++(int32_t);
    TMinCapacityArrayIterator<T, S> &operator++();
    TMinCapacityArrayIterator<T, S> operator--(int32_t);
    TMinCapacityArrayIterator<T, S> &operator--();
    TMinCapacityArrayInfo<T, S> *m_pInfo;
};

template <typename T, size_t S>
class TMinCapacityArray {
    public:
    TMinCapacityArray<T, S>(TMinCapacityArray<T, S> *);
    TMinCapacityArray<T, S>(const TMinCapacityArray<T, S> &);
    TMinCapacityArray<T, S>();
    TMinCapacityArray<T, S> &operator=(TMinCapacityArray<T, S> *);
    TMinCapacityArray<T, S> &operator=(const TMinCapacityArray<T, S> &) const;
    bool IsEmpty();
    uint64_t Size();
    uint64_t Capacity();
    const T &operator[](uint64_t) const;
    T &operator[](uint64_t nIndex);
    TMinCapacityArrayIterator<T const, S> Begin() const;
    TMinCapacityArrayIterator<T, S> Begin();
    TMinCapacityArrayIterator<T const, S> End() const;
    TMinCapacityArrayIterator<T, S> End();
    T *PushBack(T *element);
    T *PushBack(const T &);
    void Erase(uint64_t);
    TMinCapacityArrayIterator<T, S> Erase(const TMinCapacityArrayIterator<T, S> &);
    void Clear();
    bool HasSecondaryArray();
    const TArrayRef<T> GetPrimaryArrayRef();
    const TArrayRef<T> GetSecondaryArrayRef();
    void CopyToArray(TArray<T> &);
    void AppendToArray(TArray<T> &);
    TMinCapacityArrayInfo<T, S> m_info;
};

template <typename T>
struct TBlockArrayInfo {
    uint64_t m_nOffset;
    uint64_t m_nSize;
    uint64_t m_nBlockCount;
    uint64_t m_nBlockSize;
    T **m_pBlockMap;
    T const &operator[](uint64_t) const;
    T &operator[](uint64_t);
    void Prefetch();
    void Reset();
    TBlockArrayInfo<T> &operator=(const TBlockArrayInfo<T> &);
};

template <typename T>
class TBlockArrayIterator : public TIterator<T> {
    public:
    TBlockArrayIterator<T>(const TBlockArrayIterator<T> &);
    TBlockArrayIterator<T>(TBlockArrayInfo<T> &, uint64_t);
    operator class TBlockArrayIterator<T const>();
    bool operator==(const TBlockArrayIterator<T> &);
    bool operator!=(const TBlockArrayIterator<T> &);
    TBlockArrayIterator<T> &operator=(const TBlockArrayIterator<T> &);
    TBlockArrayIterator<T> operator++(int32_t);
    TBlockArrayIterator<T> &operator++();
    TBlockArrayIterator<T> operator--(int32_t);
    TBlockArrayIterator<T> &operator--();
    TBlockArrayInfo<T> *m_pInfo;
    uint64_t m_nIndex;
};

template <typename T>
class TBlockArrayEnumerator : public TEnumeratorBase<T> {
    public:
    TBlockArrayEnumerator<T>(TBlockArrayInfo<T> &, uint64_t);
    operator class TBlockArrayEnumerator<T const>();
    bool MoveNext();
    class ZEnumeratorImpl {
        public:
    };
    TBlockArrayInfo<T> &m_info;
    uint64_t m_nCurrentIndex;
};

template <typename T>
class TBlockArray {
    public:
    TBlockArray<T>(TBlockArray<T> *);
    TBlockArray<T>(const TBlockArray<T> &);
    TBlockArray<T>(uint64_t);
    ~TBlockArray<T>();
    TBlockArray<T> &operator=(TBlockArray<T> *);
    TBlockArray<T> &operator=(const TBlockArray<T> &);
    bool operator==(const TBlockArray<T> &);
    T const &operator[](uint64_t) const;
    T &operator[](uint64_t);
    T const &Front() const;
    T &Front();
    T const &Back() const;
    T &Back();
    TBlockArrayIterator<T const> Begin() const;
    TBlockArrayIterator<T> Begin();
    TBlockArrayIterator<T const> End() const;
    TBlockArrayIterator<T> End();
    TBlockArrayIterator<T const> Find(T const &) const;
    TBlockArrayIterator<T> Find(T const &);
    bool IsEmpty();
    uint64_t Size();
    bool Constains(T const &);
    TBlockArrayEnumerator<T const> GetEnumerator() const;
    TBlockArrayEnumerator<T> GetEnumerator();
    void Assign(const TBlockArray<T> &);
    void Clear();
    void PushBack(T *);
    void PushBack(T const &);
    void PopBack();
    void PushFront(T *);
    void PushFront(T const &);
    void PopFront();
    TBlockArrayIterator<T> Insert(const TBlockArrayIterator<T> &, T *);
    TBlockArrayIterator<T> Insert(const TBlockArrayIterator<T> &, T const &);
    TBlockArrayIterator<T> Insert(T *);
    TBlockArrayIterator<T> Insert(T const &);
    TBlockArrayIterator<T> Erase(const TBlockArrayIterator<T> &);
    void Prefetch();
    void GrowBlockMap();
    T *PreparePushBack();
    T *PreparePushFront();
    void Cleanup();
    void DestroyElements();
    TBlockArrayInfo<T> m_info;
};

template <typename T, typename C>
class TQueue {
    public:
    TQueue<T, C>(TQueue<T, C> *);
    TQueue<T, C>(const TQueue<T, C> &);
    TQueue<T, C>();
    ~TQueue<T, C>();
    TQueue<T, C> &operator=(TQueue<T, C> *);
    TQueue<T, C> &operator=(const TQueue<T, C> &);
    bool operator==(const TQueue<T, C> &);
    TBlockArrayIterator<T const> Begin() const;
    TBlockArrayIterator<T> Begin();
    TBlockArrayIterator<T const> End() const;
    TBlockArrayIterator<T> End();
    TBlockArrayIterator<T const> Find(T const &) const;
    TBlockArrayIterator<T> Find(T const &);
    TBlockArrayEnumerator<T const> GetEnumerator() const;
    TBlockArrayEnumerator<T> GetEnumerator();
    bool IsEmpty();
    uint64_t Size();
    bool Contains(T const &);
    T const &Top() const;
    T &Top();
    T &Push(T *);
    T &Push(T const &);
    void Pop();
    TBlockArrayIterator<T> Erase(TBlockArrayIterator<T>);
    void Clear();
    void Assign(const TQueue<T, C> &);
    C m_container;
};

template <typename T>
class TUniquePtr {
    public:
    TUniquePtr<T>(const TUniquePtr<T> &);
    TUniquePtr<T>(TUniquePtr<T> *);
    TUniquePtr<T>(T *p);
    TUniquePtr<T>();
    ~TUniquePtr<T>();
    TUniquePtr<T> &operator=(const TUniquePtr<T> &);
    TUniquePtr<T> &operator=(void *);
    TUniquePtr<T> &operator=(TUniquePtr<T> *);
    T &operator*();
    T *operator->();
    T *Get();
    // TODO
    // operator class T * (TUniquePtr<T>::*)(void) const();
    T *Release();
    void Reset(T *p);
    void Swap(TUniquePtr<T> &);
    T *m_ptr;
};

// TODO: Find a reified instance
template <typename T>
class TAutoPtrRef {};

template <typename T>
class TAutoPtr {
    public:
    TAutoPtr<T>(TAutoPtrRef<T>);

    T *m_ptr;
};

template <typename T>
class TScopedPtr {
    public:
    TScopedPtr<T>(const TScopedPtr<T> &);
    TScopedPtr<T>(TAutoPtr<T> &);
    TScopedPtr<T>(T *ptr);
    ~TScopedPtr<T>();
    T &operator*();
    T *operator->();
    void operator!=(const TScopedPtr<T> &);
    bool operator!=(const T *rhs);
    void operator==(const TScopedPtr<T> &);
    bool operator==(const T *rhs);
    // TODO
    // operator class T *(TScopedPtr<T>::* )(void) const();
    T *Get();
    void Reset(T *ptr);
    void Swap(TScopedPtr<T> &b);
    T *m_ptr;
    TScopedPtr<T> &operator=(const TScopedPtr<T> &);
};

template <typename T>
class TPromise {
    public:
    TPromise<T>();
    bool IsCancelled();
    bool Cancel();
    TFuture<T> GetFuture();
    bool SetValue(T *value);
    bool SetValue(const T &value);
    const T &GetValue();
    bool IsCompleted();
    void RegisterFulfillDelegate(const std::function<void(T const &)> &);
    void RegisterCancelDelegate(const std::function<void(void)> &delegate);
    bool operator==(const TPromise<T> &);
    TSharedPtr<TFutureState<T>> m_State;
};

struct SColorRGB {
    float r;
    float g;
    float b;
    float4 GetAsfloat4();
    uint32_t GetAsUInt32();
    uint32_t GetAsRGBUInt32();
    bool operator==(const SColorRGB &rhs);
    bool operator!=(const SColorRGB &rhs);
    bool FromString(const ZString &sSrc);
    ZString ToString();
    uint64_t ToCString(char *pBuffer, uint64_t nSize, const ZString &sFormat);
};

class ZBitArray {
    public:
    class ZBitRef {
        public:
        ZBitArray::ZBitRef &operator=(bool);
        operator bool();
        ZBitRef(ZBitArray &, uint64_t);
        ZBitArray &m_Self;
        uint64_t m_nIndex;
    };
    class ZRef {};
    static void RegisterType();
    ZBitArray(const ZBitArray &rhs);
    ZBitArray();
    ZBitArray &operator=(const ZBitArray &rhs);
    ~ZBitArray();
    void Swap(ZBitArray &);
    uint64_t Size();
    uint64_t Capacity();
    void Resize(uint64_t);
    void Grow(uint64_t);
    void Clear();
    void Zero();
    void One();
    void Negate();
    bool Get(uint64_t);
    void Set(uint64_t, bool);
    bool operator==(const ZBitArray &rhs);
    bool operator!=(const ZBitArray &);
    ZBitArray::ZBitRef operator[](uint64_t);
    // TODO: Duplicate operator[]?
    // bool operator[](uint64_t);
    TArray<unsigned char> m_aBytes;
    uint64_t m_nSize;
    static uint64_t BitsToBytes(uint64_t);
    TArray<unsigned char> &GetBytes();
    const TArray<unsigned char> &GetBytes() const;
};

// TODO: What is this?
class ZVTablePaddingRemover {
    public:
    virtual ~ZVTablePaddingRemover();
};

template <typename T>
STypeID *GetTypeID();

ZComponentManager *GetComponentManager();
ZMemoryManager *GetMemoryManager();
ZTypeRegistry &GetTypeRegistry();

extern "C" {
__declspec(dllimport) int64_t *g_ThreadIdMainThread;
}