// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík

#pragma once
#include "common/dxmd/g2base.hpp"

// new/delete in overridden in the DXMD binaries, some code assumes memory is
// allocated using the G2 allocator.
// Overriding new/delete globally unfortunately leads to weird problems with the
// allocator failing (which doesn't happen without allocations from MDSuen), so
// we have to be careful to allocate DXMD objects with the custom
// functions/macros defined here

#define G2NEW(t) new(GetMemoryManager()->Allocate(sizeof(t), 0)) t

template <typename T>
void G2DELETE(T *ptr) {
    ptr->~T();
    GetMemoryManager()->Free(ptr);
}

struct DxmdDeleter {
    template <typename T>
    void operator ()(T *ptr) {
        G2DELETE(ptr);
    }
};

template <typename T>
using g2_unique_ptr = std::unique_ptr<T, DxmdDeleter>;

template <typename T, typename... Args>
g2_unique_ptr<T> g2_make_unique(Args&&... args) {
    auto buf = GetMemoryManager()->Allocate(sizeof(T), 0);
    return g2_unique_ptr<T>(new(buf) T(std::forward<Args>(args)...));
}

template <typename T, typename... Args>
std::shared_ptr<T> g2_make_shared(Args&&... args) {
    return std::shared_ptr<T>(
        G2NEW(T)(std::forward<Args>(args)...),
        DxmdDeleter());
}