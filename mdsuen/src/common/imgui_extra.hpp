// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík

#pragma once

#include <stdint.h>
#include <string>
#include <vector>
#include <memory>
#include <functional>

namespace ImGui {

class StandaloneWindow {
    public:
    bool shown = true;
    void draw_window();

    StandaloneWindow() {
        this->window_id = next_unique_id++;
    }
    virtual ~StandaloneWindow() = default;

    virtual std::string get_title() = 0;
    virtual void draw() = 0;

    private:
    uint64_t window_id;
    static uint64_t next_unique_id;
};

class StandaloneWindowSet {
    public:
    void add_window(std::unique_ptr<StandaloneWindow> win);
    void draw();

    private:
    std::vector<std::unique_ptr<StandaloneWindow>> windows;
};

template <typename T>
void clipper_vec(std::vector<T> &vec, std::function<void(T &)> draw) {
    ImGuiListClipper clipper;
    clipper.Begin(vec.size());
    while(clipper.Step()) {
        for(size_t i = clipper.DisplayStart; i < clipper.DisplayEnd; i++) {
            draw(vec[i]);
        }
    }
}

void BeginActionBar(const char *id);
bool ActionBarButton(const char *label);
void EndActionBar();

};