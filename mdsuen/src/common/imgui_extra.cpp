// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík

#include "imgui_extra.hpp"
#include "imgui.h"
#include "imgui_internal.h"

namespace ImGui {

uint64_t StandaloneWindow::next_unique_id = 0;

void StandaloneWindow::draw_window() {
    Begin((this->get_title()
           + "###standalone-win-"
           + std::to_string(this->window_id)).c_str(),
          &this->shown);
    this->draw();
    End();
}

void StandaloneWindowSet::add_window(std::unique_ptr<StandaloneWindow> w) {
    this->windows.push_back(std::move(w));
}

void StandaloneWindowSet::draw() {
    for(auto it = this->windows.begin(); it != this->windows.end(); ) {
        if(!(*it)->shown) {
            it = this->windows.erase(it);
        } else {
            (*it)->draw_window();
            ++it;
        }
    }
}

// If we ever want multi-threaded ImGui, this will need to be thread-local
// And any coroutines would break it
static ImGuiID current_actionbar_id = -1;

void BeginActionBar(const char *id) {
    current_actionbar_id = GetCurrentWindow()->GetID(id);
    auto height = GetStateStorage()->GetInt(current_actionbar_id, 0);
    auto style = GetStyle();
    auto fill = style.Colors[ImGuiCol_Button];
    GetWindowDrawList()->AddRectFilled(
        GetCursorScreenPos(),
        ImVec2(GetCursorScreenPos().x + GetWindowWidth() - style.WindowPadding.x * 2,
               GetCursorScreenPos().y + height),
        IM_COL32(fill.x * 256, fill.y * 256, fill.z * 256, fill.w * 256));
    BeginGroup();
    Dummy(ImVec2(0, 0));
}

bool ActionBarButton(const char *label) {
    // Wrapping code from demo window
    auto style = GetStyle();
    float window_visible_x2 = GetWindowPos().x + GetWindowContentRegionMax().x;
    float last_button_x2 = GetItemRectMax().x;
    float next_button_x2 =
        last_button_x2 + style.ItemSpacing.x
      + CalcTextSize(label).x + style.FramePadding.x * 2;
    if(next_button_x2 < window_visible_x2) {
        SameLine();
    } else {
        SetCursorPosX(GetCursorPosX() + style.ItemSpacing.x);
    }
    return ButtonEx(label, ImVec2(0, 20));
}

void EndActionBar() {
    EndGroup();
    GetStateStorage()->SetInt(current_actionbar_id, GetItemRectSize().y);
}

};