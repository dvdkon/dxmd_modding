// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include <string>
#include <iostream>
#include <fstream>
#include <codecvt>
#include <filesystem>
#include <vector>
#include <regex>
#include <unordered_set>
#include <assert.h>
#include <atlbase.h>
#include <comdef.h>
#include <dia2.h>

/* 
 * This program takes a DLL with a corresponding PDB, and creates a new DLL that
 * exposes all the functions and global variables in the original DLL. If the
 * exported symbol limit is reached, the program creates multiple "wrapper" DLLs.
 * Some symbols cause trouble when reexported, and this program has some crude
 * rules to filter them out.
 */

bool should_reexport_symbol(std::string name) {
    // Just some functions that could cause trouble
    // For some reason atexit is not imported, but is exported
    if(name == "DllMain" || name == "atexit") {
        return false;
    }

    // Catch-all for troublemakers
    if(name.rfind("_", 0) == 0) {
        return false;
    }

    // Don't reexport new and delete operators and deleting destructros
    if(std::regex_match(name, std::regex("\\?+(2|3|_U|_V|_E|_G)[^@]*@.*"))) {
        return false;
    }

    // Crude check not to reexport symbols from std namespace
    // Ideally we would never reexport any symbols from outside the DLL's
    // main codebase, but that seems impossible to automate
    if(name.find("@std@@") != std::string::npos) {
        return false;
    }

    // Sometimes this encounters really weird symbols like "==" which break
    // compilation, because the "=" is used in the linker command
    if(name.find("=") != std::string::npos) {
        return false;
    }

    return true;
}

size_t rva_to_file_offset(std::vector<IMAGE_SECTION_HEADER> sections, uint64_t rva) {
    for(auto section : sections) {
        if(section.VirtualAddress <= rva
                && section.VirtualAddress + section.SizeOfRawData >= rva) {
            return rva - section.VirtualAddress + section.PointerToRawData;
        }
    }
    assert(false);
    return -1;
}

std::unordered_set<std::string> get_dll_imports(char *dll_path) {
    std::ifstream dll_file(dll_path, std::ios::binary);
    dll_file.seekg(0, std::ios::end);
    size_t dll_length = dll_file.tellg();
    dll_file.seekg(0);

    IMAGE_DOS_HEADER dos_header;
    dll_file.read(reinterpret_cast<char *>(&dos_header), sizeof(dos_header));
    dll_file.seekg(dos_header.e_lfanew);

    IMAGE_NT_HEADERS64 nt_header;
    dll_file.read(reinterpret_cast<char *>(&nt_header), sizeof(nt_header));

    std::vector<IMAGE_SECTION_HEADER> sections;
    for(size_t i = 0; i < nt_header.FileHeader.NumberOfSections; i++) {
        IMAGE_SECTION_HEADER section;
        dll_file.read(reinterpret_cast<char *>(&section), sizeof(section));
        sections.push_back(section);
    }

    std::unordered_set<std::string> imports;

    auto import_dir_info = nt_header.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT];
    auto import_dir_offset = rva_to_file_offset(sections, import_dir_info.VirtualAddress);
    dll_file.seekg(import_dir_offset);
    IMAGE_IMPORT_DESCRIPTOR import_desc;
    while(true) {
        dll_file.read(reinterpret_cast<char *>(&import_desc), sizeof(import_desc));
        if(import_desc.Name == NULL) {
            break;
        }
        size_t next_offset = dll_file.tellg();
        char name[256];
        dll_file.seekg(rva_to_file_offset(sections, import_desc.Name));
        for(size_t i = 0; i < 256; i++) {
            dll_file.read(name + i, 1);
            if(name[i] == '\0') {
                break;
            }
        }

        dll_file.seekg(rva_to_file_offset(sections, import_desc.FirstThunk));
        while(true) {
            IMAGE_THUNK_DATA entry;
            dll_file.read(reinterpret_cast<char *>(&entry), sizeof(entry));
            size_t next_offset = dll_file.tellg();
            if(entry.u1.AddressOfData & 0x8000000000000000) {
                continue; // Skip over entries importing by ordinal
            }
            if(entry.u1.AddressOfData == NULL) {
                break;
            }
            dll_file.seekg(rva_to_file_offset(sections, entry.u1.AddressOfData) + sizeof(WORD));
            char name[256];
            for(size_t i = 0; i < 256; i++) {
                dll_file.read(name + i, 1);
                if(name[i] == '\0') {
                    break;
                }
            }
            imports.insert(name);
            dll_file.seekg(next_offset);
        }

        dll_file.seekg(next_offset);
    };

    return imports;
}

struct Symbol {
    std::string name;
    uint64_t rva;
    bool is_function;
};

std::regex data_invalid_chars("[^a-zA-Z0-9_]");

std::vector<Symbol> get_symbols_from_pdb(
        char *pdb_path, std::unordered_set<std::string> blacklist) {
    CoInitialize(NULL);

    CComPtr<IDiaDataSource> data_source;
    assert(SUCCEEDED(CoCreateInstance(
        _uuidof(DiaSource), NULL, CLSCTX_INPROC_SERVER,
        IID_PPV_ARGS(&data_source))));

    std::wstring pdb_path_w(pdb_path, pdb_path + strlen(pdb_path));
    assert(SUCCEEDED(data_source->loadDataFromPdb(pdb_path_w.c_str())));

    CComPtr<IDiaSession> session;
    assert(SUCCEEDED(data_source->openSession(&session)));

    CComPtr<IDiaSymbol> global_sym;
    assert(SUCCEEDED(session->get_globalScope(&global_sym)));

    std::vector<Symbol> result;
    std::unordered_set<std::string> names;

    CComPtr<IDiaEnumSymbols> sym_enum;
    assert(SUCCEEDED(global_sym->findChildrenEx(
        SymTagPublicSymbol, NULL, nsNone, &sym_enum)));
    while(true) {
        CComPtr<IDiaSymbol> sym;
        ULONG fetched;
        assert(SUCCEEDED(sym_enum->Next(1, &sym, &fetched)));
        if(fetched != 1) {
            break;
        }

        DWORD rva;
        assert(SUCCEEDED(sym->get_relativeVirtualAddress(&rva)));

        BOOL is_function;
        assert(SUCCEEDED(sym->get_function(&is_function)));

        bstr_t name_bstr;
        if(is_function) {
            assert(SUCCEEDED(sym->get_name(name_bstr.GetAddress())));
        } else {
            assert(SUCCEEDED(sym->get_undecoratedNameEx(
                /*UNDNAME_NAME_ONLY*/ 0x1000,
                name_bstr.GetAddress())));
        }
        std::string name =
            std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(
                name_bstr.GetBSTR(), name_bstr.GetBSTR() + name_bstr.length());

        DWORD access;
        assert(SUCCEEDED(sym->get_access(&access)));

        if(!should_reexport_symbol(name)
           || blacklist.find(name) != blacklist.end()) {
            continue;
        }

        if(!is_function) {
            name = std::regex_replace(name, data_invalid_chars, "_");
        } else if(access = CV_private || access == CV_protected) {
            // Ugly hack to change access level to public
            // XXX: Monitor if this breaks something
            auto pos = name.find("@@");
            if(pos != name.size() - 1) {
                auto access = name[pos + 2];
                if(access == 'A' || access == 'B' ||
                   access == 'I' || access == 'J') {
                    name[pos + 2] = 'Q';
                } else if(access == 'C' || access == 'D' ||
                          access == 'K' || access == 'L') {
                    name[pos + 2] = 'S';
                } else if(access == 'E' || access == 'F' ||
                          access == 'M' || access == 'N') {
                    name[pos + 2] = 'U';
                } else if(access == 'G' || access == 'H' ||
                          access == 'O' || access == 'P') {
                    name[pos + 2] = 'W';
                }
            }
        }

        if(names.find(name) != names.end()) {
            continue;
        }

        result.push_back({name, rva, static_cast<bool>(is_function)});
        names.insert(name);
    }

    return result;
}

std::vector<std::vector<Symbol>> partition_syms(std::vector<Symbol> syms) {
    size_t group_size = 65000;
    std::vector<std::vector<Symbol>> sym_groups;
    size_t group_count =
        syms.size() / group_size
        + (syms.size() % group_size > 0 ? 1 : 0); // Round up
    sym_groups.resize(group_count);
    for(size_t i = 0; i < syms.size() / group_size; i++) {
        sym_groups[i].reserve(group_size);
    }
    for(size_t i = 0; i < syms.size(); i++) {
        sym_groups[i / group_size].push_back(syms[i]);
    }

    return sym_groups;
}

void build_wrapper(std::string wrapped_dll, std::string prefix, std::vector<Symbol> syms) {
    auto cpp_filename = prefix + ".cpp";
    auto dll_filename = prefix + ".dll";

    std::ofstream cpp(cpp_filename);
    cpp << "#include <stdio.h>\n";
    cpp << "#include <Windows.h>\n";
    cpp << "#include <Psapi.h>\n";
    cpp << "\n";

    for(size_t i = 0; i < syms.size(); i++) {
        auto sym = syms[i];
        if(sym.is_function) {
            // This relies on the compiler not adding too much pre/post assembly
            cpp << "void (*func_" << i << "_ptr)();\n";
            cpp << "void func_" << i << "() {\n";
            cpp << "#pragma comment(linker, \"/export:" << sym.name << "=\" __FUNCDNAME__)\n";
            cpp << "    func_" << i << "_ptr();\n";
            cpp << "}\n";
        } else {
            cpp << "extern \"C\" { void *data_" << i << "; }\n";
            // The resulting pointers will have to be loaded with extern "C",
            // because otherwise they'd have the type in the mangled name, which
            // would mean this program would have to be able to add a pointer to
            // an existing mangled name
            cpp << "#pragma comment(linker, \"/export:" << sym.name << "=data_" << i <<"\")\n";
        }
    }

    cpp << "\n";
    cpp << "BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {\n";
    cpp << "    if(fdwReason != DLL_PROCESS_ATTACH) return TRUE;\n";
    cpp << "    HMODULE wrapped_dll = LoadLibraryA(\"" << wrapped_dll << "\");\n";
    cpp << "    if(wrapped_dll == NULL) {\n";
    cpp << "        DWORD error = GetLastError();\n";
    cpp << "        char msg[1024];\n";
    cpp << "        snprintf(msg, 1024, \"Failed to load wrapped DLL " << wrapped_dll << "! %x\", error);\n";
    cpp << "        OutputDebugStringA(msg);\n";
    cpp << "        return FALSE;\n";
    cpp << "    }\n";
    cpp << "    MODULEINFO dll_info;\n";
    cpp << "    if(!GetModuleInformation(GetCurrentProcess(), wrapped_dll, &dll_info, sizeof(dll_info))) {\n";
    cpp << "        return FALSE;\n";
    cpp << "    }\n";
    cpp << "    char *wrapped_dll_base = (char *) dll_info.lpBaseOfDll;\n";

    for(size_t i = 0; i < syms.size(); i++) {
        auto sym = syms[i];
        if(sym.is_function) {
            cpp << "    func_" << i << "_ptr = (void(*)()) (wrapped_dll_base + " << sym.rva << ");\n";
        } else {
            cpp << "    data_" << i << " = (void *) (wrapped_dll_base + "<< sym.rva << ");\n";
        }
    }

    cpp << "    return TRUE;\n";
    cpp << "}\n";

    cpp.close();

    system(("cl /D_USRDLL /D_WINDLL /DEBUG /Zi " + cpp_filename + " /MT /link /DLL /OUT:" + dll_filename).c_str());
}

int main(int argc, char *argv[]) {
    if(argc != 4) {
        std::cerr << "Usage: <dll file> <pdb file> <out prefix>\n";
        exit(1);
    }
    
    auto dll_path = std::filesystem::path(argv[1]);
    auto out_path = std::filesystem::path(argv[3]);
    auto out_prefix = out_path / dll_path.stem();
    auto imports = get_dll_imports(argv[1]);
    auto syms = get_symbols_from_pdb(argv[2], imports);
    auto sym_groups = partition_syms(syms);
    size_t i = 0;
    for(auto group : sym_groups) {
        auto prefix = out_prefix.string() + "." + std::to_string(i);
        build_wrapper(dll_path.filename().string(), prefix, group);
        i++;
    }
}