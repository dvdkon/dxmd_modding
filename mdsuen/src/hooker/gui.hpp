// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#pragma once
#include <string>
#include <vector>
#include <imgui.h>
#include "hooker/dxmd.hpp"

class TypeRegistryWindow {
    public:
    void draw();

    private:
    STypeID *selected_type = NULL;

    void draw_itype(IType *details);
};

class DeCrc32Window {
    public:
    void draw();
};

class GameConsoleWindow {
    public:
    void draw();
    void add_line(std::string line);

    private:
    std::vector<std::string> output;
    char input_line[256];
    bool scroll_to_bottom = false;
};

class MenuTransitionWindow {
    public:
    void draw();

    private:
    std::string transition;
};

class OverlayGui {
    public:
    TypeRegistryWindow type_registry_window;
    DeCrc32Window decrc32_window;
    GameConsoleWindow game_console_window;
    MenuTransitionWindow menu_transition_window;

    void init();
    void draw();
};

extern OverlayGui overlay_gui;

void init_overlay_hook();
