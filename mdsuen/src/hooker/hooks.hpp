// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#pragma once
#include "hooker/dxmd.hpp"

void *hook_WinMain();
void *hook_nx__NxDXGIImpl__Present(nx::NxDXGIImpl *self, bool unk1);
void *hook_ZApplicationEngineWin32__MainLoop(void *self);
void *hook_ZEngineAppCommon__MainLoopSequence(void *self);
void *hook_ZDebugConsole__AddLine(void *self, char *fmtstr, ...);
void *hook_ZEngineAppCommon__GetDefaultSettings(ZString *out);
void *hook_IResourceInstaller__Install(
        IResourceInstaller *self, ZResourcePending *pending_res);
void *hook_ZResourceManager__RegisterResourceInstaller(
        ZResourceManager *self, IResourceInstaller *inst, uint32_t restag);

void *hook_Crc32Generator__CaseInsensitiveCrc32_cstr(char *str);
void *hook_Crc32Generator__CaseInsensitiveCrc32_ZString(ZString *str);
void *hook_Crc32Generator__CaseSensitiveCrc32(char *str);
void *hook_Crc32Generator__Crc32(char *str, size_t len);

void *hook_SignalInputPin(ZEntityType **entity_type, uint32_t pin_crc32, ZVariantRef *value);

extern void(*orig_ZResourceManager__RegisterResourceInstaller)(
        ZResourceManager *self, IResourceInstaller *inst, uint32_t restag);
