// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2021 David Koňařík
#pragma once
#include <memory>
#include <string>
#include <optional>
#include <functional>
#include <pahil.hpp>

struct GameVersion {
    /** An identifier for this version of the executable */
    std::string name;
    /** We use the exact filesize of an executable instead of something sensible
     *  like a hash to avoid having to add another library */
    size_t filesize;
    /** Called when the hooker starts. Should set pointers into the game and
     *  create hooks */
    std::function<void ()> on_hook;
};

extern GameVersion linux_version;
extern GameVersion win_breach_version;
extern GameVersion win_gog_version;
extern GameVersion dbg_release_version;

/** Will find the correct version based on the current executable's size */
std::optional<GameVersion> find_game_version();

/** The pahil Module representing the main executable
 *  It's global so that other parts of MDSuen can hook dynamically */
extern std::unique_ptr<pahil::Module> prog;

