// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2021 David Koňařík
#include "hooker/version.hpp"
#include <pahil.hpp>
#include "hooker/hooks.hpp"

GameVersion dbg_release_version = {
    "dbg_release_801",
    3230720,
    []() {
        pahil::Module engine(0x140000000);
        engine.init_exec_allocator();
        engine.unprotect_pages();

        {
            auto p = engine.patch();
            p.hook(0x140006f41, hook_WinMain);
            p.apply();
        }

        pahil::Module runtime_resource(0x180000000, "runtime.resource.dll");
        runtime_resource.init_exec_allocator();
        runtime_resource.unprotect_pages();

        {
            auto p = runtime_resource.patch();
            p.hook(0x18008b3d0, hook_ZResourceManager__GetResourcePtrInternal);
            p.apply();
        }

        ZRuntimeResourceID__ToDebugString = reinterpret_cast<ZString *(*)(ZRuntimeResourceID *, ZString *)>(runtime_resource.translate_addr(0x1800e8290));
    },
};
