// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "hooker/version.hpp"
#include <pahil.hpp>
#include "hooker/dxmd.hpp"
#include "hooker/hooks.hpp"

static void init_post_main() {
    GetGlobalPointer = reinterpret_cast<void *(*)(const char*)>(prog->translate_addr(0x00833c20));
    ZDebugConsole__ExecuteCommand = reinterpret_cast<void (*)(void *, const char *)>(prog->translate_addr(0x02a4ab40));
    TriggerMenuScreenTransition__string = reinterpret_cast<void (*)(ZString *)>(prog->translate_addr(0x024bbbb0));

    g_pDebugConsoleSingleton = reinterpret_cast<void *>(prog->translate_addr(0x06887f50));
    globalPointerCount = reinterpret_cast<uint32_t *>(prog->translate_addr(0x05e69c60));
    globalPointers = reinterpret_cast<GlobalPointer *>(prog->translate_addr(0x05e69c68));

    // Finally call the generic hook
    hook_WinMain();
}

GameVersion linux_version = {
    "dxmd_linux",
    92578840,
    []() {
        prog = std::make_unique<pahil::Module>(0x00400000);
        prog->init_exec_allocator();
        prog->unprotect_pages();
        LOG << "Pages unprotected";

        {
            auto p = prog->patch();
            // Unfortunately, there are issues with the static initialisation
            // order on Linux, so we use this "bootstrap hook" to do anything
            // but hooks.
            // Ideally, this would be done even before this function is called,
            // but there's no simple way I know of.
            p.hook(0x00b5c5d0, reinterpret_cast<void *>(init_post_main));
            p.apply();
        }

        // Some hooks can't be in the post_main init function, because they
        // need to run even for static initialisers
        {
            auto p = prog->patch();
            p.hook(0x00b5c080, reinterpret_cast<void *>(hook_ZApplicationEngineWin32__MainLoop));
            p.apply();
        }

        {
            auto p = prog->patch();
            p.hook(0x02a4a4d0, reinterpret_cast<void *>(hook_ZDebugConsole__AddLine));
            p.apply();
        }

        {
            auto p = prog->patch();
            p.hook(0x00b623d0, reinterpret_cast<void *>(hook_ZEngineAppCommon__GetDefaultSettings));
            p.apply();
        }

        {
            auto p = prog->patch();
            p.hook(0x0113ffb0,
                reinterpret_cast<void *>(hook_ZResourceManager__RegisterResourceInstaller),
                reinterpret_cast<void **>(&orig_ZResourceManager__RegisterResourceInstaller));
            p.apply();
        }

        {
            auto p = prog->patch();
            p.hook(0x00833a20, reinterpret_cast<void *>(hook_Crc32Generator__CaseInsensitiveCrc32_cstr));
            p.hook(0x00833980, reinterpret_cast<void *>(hook_Crc32Generator__CaseInsensitiveCrc32_ZString));
            p.hook(0x00833a70, reinterpret_cast<void *>(hook_Crc32Generator__CaseSensitiveCrc32));
            p.hook(0x00833ab0, reinterpret_cast<void *>(hook_Crc32Generator__Crc32));
            p.apply();
        }

        {
            auto p = prog->patch();
            p.hook(0x0070ae10, reinterpret_cast<void *>(hook_SignalInputPin));
            p.apply();
        }


    },
};
