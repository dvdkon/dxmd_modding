// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "gui.hpp"
#include "imgui.h"
#include <map>
#include <fstream>
#include <string>
#include <pahil/logging.hpp>
#include <pahil.hpp>
#include <imgui_overlay.hpp>
#include "hooker/dehash.hpp"

void TypeRegistryWindow::draw() {
    ImGui::Begin("Type registry");

    auto reg = reinterpret_cast<ZTypeRegistry *>(
            GetGlobalPointer("ioi_typeinforegistry"));

    if(ImGui::Button("Dump")) {
        std::ofstream outfile("typeregistry.txt", std::ios::trunc);
        outfile << "Name"
            << "\t" << "Flags"
            << "\t" << "Property count"
            << "\t" << "Constructor count"
            << "\t" << "Base class count"
            << "\t" << "Component map length"
            << "\t" << "Input pin count"
            << std::endl;
        for(auto& node : reg->types_by_name) {
            outfile << node.a.cstr
                << "\t" << node.b->details->flags
                << "\t" << node.b->details->property_count
                << "\t" << node.b->details->constructor_count
                << "\t" << node.b->details->base_class_count
                << "\t" << node.b->details->component_map_len
                << "\t" << node.b->details->input_pin_count
                << std::endl;
        }
    }

    ImGui::Columns(2);
    ImGui::BeginChild("Type list");

    if(ImGui::BeginTabBar("Type registry tab bar")) {
        if(ImGui::BeginTabItem("Named")) {
            ImGui::Text("Entry count: %ld", reg->types_by_name.node_count);

            // First put everything into a std::map to sort it
            std::map<std::string, STypeID *> types;
            for(auto& type : reg->types_by_name) {
                types[type.a.cstr] = type.b;
            }
            for(auto& [name, type] : types) {
                if(ImGui::Selectable(name.c_str(),
                                     type == this->selected_type)) {
                    this->selected_type = type;
                }
            }
            ImGui::EndTabItem();
        }
        if(ImGui::BeginTabItem("Nameless")) {
            ImGui::Text("Entry count: %d", reg->types_without_name.size);

            ImGui::EndTabItem();
        }
        ImGui::EndTabBar();
    }

    ImGui::EndChild();
    ImGui::NextColumn();
    ImGui::BeginChild("Type description");

    if(this->selected_type != NULL) {
        ImGui::Columns(2);

        auto details = this->selected_type->details;
        if(details != NULL) {
            this->draw_itype(details);
        }

        ImGui::Columns(1);
    }
    ImGui::EndChild();
    ImGui::Columns(1);

    ImGui::End();
}

void TypeRegistryWindow::draw_itype(IType *itype) {
    ImGui::Text("Name:");
    ImGui::NextColumn();
    ImGui::TextWrapped("%s", itype->name);
    ImGui::NextColumn();

    ImGui::Text("Instance size:");
    ImGui::NextColumn();
    ImGui::TextWrapped("%d", itype->instance_size);
    ImGui::NextColumn();

    ImGui::Text("Alignment:");
    ImGui::NextColumn();
    ImGui::TextWrapped("%d", itype->alignment);
    ImGui::NextColumn();

    ImGui::Text("Flags:");
    ImGui::NextColumn();
    ImGui::TextWrapped("%d", itype->flags);
    ImGui::NextColumn();

    if(itype->flags & IType::FLAG_IS_CLASS) {
        ImGui::Text("Properties:");
        ImGui::NextColumn();
        for(size_t i = 0; i < itype->property_count; i++) {
            auto prop = itype->properties[i];

            if(decrc32_map.find(prop.name_crc32) != decrc32_map.end()) {
                ImGui::Text("Name: %s",
                            decrc32_map[prop.name_crc32].c_str());
            } else {
                ImGui::Text("Name: crc32 %x", prop.name_crc32);
            }
            ImGui::Indent(16);
            auto details = prop.stypeid->details;
            if(details != NULL) {
                ImGui::Text("Type: %s", details->name);
            } else {
                ImGui::Text("Type: unnamed");
            }
            ImGui::Unindent(16);
        }
        ImGui::NextColumn();

        ImGui::Text("Input pins:");
        ImGui::NextColumn();
        for(size_t i = 0; i < itype->input_pin_count; i++) {
            auto pin = itype->input_pins[i];

            ImGui::Text("Name: %s", pin.name);
            ImGui::Indent(16);
            auto details = pin.stypeid->details;
            if(details != NULL) {
                ImGui::Text("Type: %s", details->name);
            } else {
                ImGui::Text("Type: unnamed");
            }
            ImGui::Unindent(16);
        }
        ImGui::NextColumn();

        ImGui::Text("Base classes:");
        ImGui::NextColumn();
        for(size_t i = 0; i < itype->base_class_count; i++) {
            auto base_class = itype->base_classes[i];
            if(base_class == NULL) {
                ImGui::Text("NULL");
                continue;
            }
            auto details = base_class->details;
            if(details != NULL) {
                ImGui::Text("Name: %s", details->name);
            } else {
                ImGui::Text("Name: unnamed");
            }
        }
        ImGui::NextColumn();

        ImGui::Text("Component map:");
        ImGui::NextColumn();
        for(size_t i = 0; i < itype->component_map_len; i++) {
            auto details = itype->component_map[i].stypeid->details;
            if(details != NULL) {
                ImGui::Text("Name: %s", details->name);
            } else {
                ImGui::Text("Name: unnamed");
            }
        }
        ImGui::NextColumn();
    }
}

void DeCrc32Window::draw() {
    ImGui::SetNextWindowSizeConstraints(
        ImVec2(200, 200), ImVec2(FLT_MAX, FLT_MAX));
    ImGui::Begin("DeCrc32");

    if(ImGui::Button("Dump")) {
        std::ofstream outfile("decrc32.txt", std::ios::trunc);
        for(auto const& [crc32, str] : decrc32_map) {
            // Don't write out obviously non-text entries
            if(str.find('\0') == std::string::npos) {
                outfile
                    << std::hex << std::setfill('0') << std::setw(8) << crc32
                    << "\t" << str << std::endl;
            }
        }
    }

    ImGui::BeginChild("DeCrc32 map");
    ImGui::Columns(2);
    for(auto const& [crc32, str] : decrc32_map) {
        ImGui::Text("%8x", crc32);
        ImGui::NextColumn();
        ImGui::Text("%s", str.c_str());
        ImGui::NextColumn();
    }
    ImGui::Columns(1);
    ImGui::EndChild();

    ImGui::End();
}

void GameConsoleWindow::draw() {
    ImGui::SetNextWindowSizeConstraints(
        ImVec2(300, 400), ImVec2(FLT_MAX, FLT_MAX));
    ImGui::Begin("Game console");

    if(ImGui::CollapsingHeader("Command reference")) {
        ImGui::BeginChild("Console command reference", ImVec2(0, -100));
        ImGui::Columns(2);
        ZConfigCommand *cmd = static_cast<ZConfigCommand *>(
            GetGlobalPointer("base.config.vars"));
        while(cmd != NULL) {
            ImGui::Text("%s", cmd->name.cstr);
            ImGui::NextColumn();
            ImGui::TextWrapped("%s", cmd->description.cstr);
            ImGui::NextColumn();
            cmd = cmd->next;
        }
        ImGui::Columns(1);
        ImGui::EndChild();
    }

    auto reserved_height = ImGui::GetFrameHeightWithSpacing();
    ImGui::BeginChild("Console output", ImVec2(0, -reserved_height));
    for(auto line : this->output) {
        ImGui::TextWrapped("%s", line.c_str());
    }
    if(this->scroll_to_bottom) {
        ImGui::SetScrollHereY(1.0f);
        this->scroll_to_bottom = false;
    }
    ImGui::EndChild();

    ImGui::InputText("", this->input_line, 256);
    ImGui::SameLine();
    if(ImGui::Button("Execute")) {
        ZDebugConsole__ExecuteCommand(g_pDebugConsoleSingleton,
                                      this->input_line);
        this->input_line[0] = '\0';
    }

    ImGui::End();
}

void GameConsoleWindow::add_line(std::string line) {
    this->output.push_back(line);
    this->scroll_to_bottom = true;
}

static std::vector<std::string> transitions = {
    "DeathMenu_MainScreenToLoadScreen",
    "MainMenu_BreachMainScreenToBreachCreditsScreen",
    "MainMenu_BreachMainScreenToBreachIntroScreen",
    "MainMenu_BreachMainScreenToBreachProfileScreen",
    "MainMenu_BreachMainScreenToControlSchemeSelectionScreen",
    "MainMenu_BreachMainScreenToMainScreen",
    "MainMenu_BreachMainScreentoPCSettingsScreen",
    "MainMenu_BreachMainScreenToSEMScreen",
    "MainMenu_DLCDetailScreenToDLCDifficultyScreen",
    "MainMenu_DLCDetailScreenToDLCLoadGameScreen",
    "MainMenu_DLCListingScreenToDLCDifficultyScreen",
    "MainMenu_DLCListingScreenToDLCLoadGameScreen",
    "MainMenu_MainScreenToBreachMainScreen",
    "MainMenu_MainScreenToDLCListingScreen",
    "MainMenu_MainScreenToExtrasScreen",
    "MainMenu_MainScreenToGymScreen",
    "MainMenu_MainScreenToMapScreen",
    "MainMenu_MainScreenToSEMScreen",
    "MainMenu_MainScreenToShopScreen",
    "MainMenu_MainScreenToStoryScreen",
    "MainMenu_MapScreenToSpawnScreen",
    "MainMenu_NewGameScreenToSchemeSelectionScreen",
    "MainMenu_PCDisplaySettingsScreenToPCSettingsCategoryScreen",
    "MainMenu_ShopScreenToMainScreen",
    "MainMenu_StartScreenToMainScreen",
    "MainMenu_StoryScreenToLoadGameScreen",
    "MainMenu_StoryScreenToNewGameScreen",
    "PauseMenu_CheatScreenToAchievementCheatsScreen",
    "PauseMenu_CheatScreenToGameplayCheatsScreen",
    "PauseMenu_CheatScreenToItemsCheatsScreen",
    "PauseMenu_CheatScreenToProfileCheatsScreen",
    "PauseMenu_MainScreenToCheatScreen",
    "PauseMenu_MainScreenToLoadScreen",
    "PauseMenu_MainScreenToSaveScreen",
    "PauseMenu_MainScreenToShopScreen",
    "PauseMenu_MainScreenToSkinsScreen",
    "PauseMenu_PCDisplaySettingsScreenToPCSettingsCategoryScreen",
    "PauseMenu_ShopScreenToMainScreen",
};

void MenuTransitionWindow::draw() {
    ImGui::Begin("Menu transition");
    if(ImGui::BeginCombo("##menu_transition", this->transition.c_str())) {
        for(auto opt : transitions) {
            if(ImGui::Selectable(opt.c_str())) {
                this->transition = opt;
            }
        }
        ImGui::EndCombo();
    }
    ImGui::SameLine();
    if(ImGui::Button("Transition")) {
        auto zstr = ZString_from_cstr(this->transition.c_str());
        TriggerMenuScreenTransition__string(&zstr);
    }
    ImGui::End();
}

void OverlayGui::init() {
    try {
        ImGui::StyleColorsDark();
    } catch(std::exception& e) {
        LOG << "Initialising overlay failed! " << e.what();
    }
}

void OverlayGui::draw() {
    try {
        this->type_registry_window.draw();
        this->decrc32_window.draw();
        this->game_console_window.draw();
        this->menu_transition_window.draw();
    } catch(std::exception& e) {
        LOG << "Drawing overlay failed! " << e.what();
        throw;
    }
}

OverlayGui overlay_gui;

void init_overlay_hook() {
    imgui_overlay_init(
        "F12",
        [](std::string line) { LOGM("overlay") << line; },
        []() { overlay_gui.init(); },
        []() { overlay_gui.draw(); });
#if defined(__linux__)
    imgui_overlay_opengl3_init();
#elif defined(_WIN32)
    imgui_overlay_d3d_init();
#endif
}
