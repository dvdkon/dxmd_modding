// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "hooks.hpp"
#include <filesystem>
#include <ios>
#include <sstream>
#include <fstream>
#include <pahil/logging.hpp>
#include "hooker/dxmd.hpp"
#include "hooker/gui.hpp"
#include "hooker/version.hpp"
#include "hooker/res_replacement.hpp"
#include "hooker/dehash.hpp"

using namespace std::string_literals;

void(*orig_ZResourceManager__RegisterResourceInstaller)(
    ZResourceManager *self, IResourceInstaller *inst, uint32_t restag);

void *hook_WinMain() {
    LOG << "WinMain() hook called";
    LOG << "MDSuen correctly hooked";

    LOG << "Initialising overlay GUI...";
    init_overlay_hook();

    LOG << "Initialising resource replacement...";
    load_replaced_resources_list();

    return NULL;
}

void *hook_nx__NxDXGIImpl__Present(nx::NxDXGIImpl *self, bool unk1) {
    return NULL;
}

void *hook_ZApplicationEngineWin32__MainLoop(void *self) {
    LOG << "ZApplicationEngineWin32::MainLoop() called";

    if(globalPointerCount != NULL) {
        for(size_t i = 0; i < *globalPointerCount; i++) {
            LOG << "Global pointer: " << globalPointers[i].name;
        }
    }

    return NULL;
}

void *hook_ZEngineAppCommon__MainLoopSequence(void *self) {
    return NULL;
}

void *hook_ZDebugConsole__AddLine(void *self, char *fmtstr, ...) {
    va_list args;
    va_start(args, fmtstr);

    char buf[1024];
    vsnprintf(buf, 1024, fmtstr, args);
    overlay_gui.game_console_window.add_line(buf);

    va_end(args);

    return NULL;
}

void *hook_ZEngineAppCommon__GetDefaultSettings(ZString *out) {
    // TODO: Allow overrinding this config
    return NULL;
}

void *hook_IResourceInstaller__Install(
        IResourceInstaller *self, ZResourcePending *pending_res) {
    auto rrid = pending_res->resource_stub->runtime_resource_id;
    if(replaced_resources.find(rrid) != replaced_resources.end()) {
        LOG << "Caught loading of resource, rrid=" << std::hex << rrid;

        // TODO: Free the previous data, or does the game do it itself?
        std::stringstream rrid_hex;
        rrid_hex << std::hex << rrid;
        auto path = replacement_resource_dir / rrid_hex.str();
        // TODO: file_size or seek&tell&seek trick?
        auto length = std::filesystem::file_size(path);
        std::ifstream replacement_file(path);
        auto buffer = new char[length];
        replacement_file.read(buffer, length);
        
        *pending_res->resource_reader->raw_bytes = buffer;
        pending_res->resource_reader->length = length;
    }

    return NULL;
}

void *hook_ZResourceManager__RegisterResourceInstaller(
        ZResourceManager *self, IResourceInstaller *inst, uint32_t restag) {
    {
        auto p = prog->patch();
        p.hook_real(reinterpret_cast<void *>(inst->vptr->Install),
                    reinterpret_cast<void *>(hook_IResourceInstaller__Install));
        try {
            p.apply();
        } catch(std::exception& e) {
            LOG << "Install() hook failed: " << e.what();
        }
    }

    orig_ZResourceManager__RegisterResourceInstaller(self, inst, restag);

    static int zero = 0;
    return &zero;
}


void *hook_Crc32Generator__CaseInsensitiveCrc32_cstr(char *str) {
    std::string sstr(str);
    for(size_t i = 0; i < sstr.length(); i++) {
        if(sstr[i] >= 'A' && sstr[i] <= 'Z') {
            sstr[i] += 0x20;
        }
    }
    decrc32_add_str(sstr);

    return NULL;
}

void *hook_Crc32Generator__CaseInsensitiveCrc32_ZString(ZString *str) {
    auto sstr = std::string(str->cstr, str->size & 0x00FFFFFF);
    for(size_t i = 0; i < sstr.length(); i++) {
        if(sstr[i] >= 'A' && sstr[i] <= 'Z') {
            sstr[i] += 0x20;
        }
    }
    decrc32_add_str(sstr);

    return NULL;
}

void *hook_Crc32Generator__CaseSensitiveCrc32(char *str) {
    std::string sstr(str);
    decrc32_add_str(sstr);

    return NULL;
}

void *hook_Crc32Generator__Crc32(char *str, size_t len) {
    std::string sstr(str, len);
    decrc32_add_str(sstr);

    return NULL;
}

void *hook_SignalInputPin(ZEntityType **entity_type, uint32_t pin_crc32, ZVariantRef *value) {
    std::string name;
    if(decrc32_map.find(pin_crc32) != decrc32_map.end()) {
        name = decrc32_map[pin_crc32];
    } else {
        name = "crc "s + std::to_string(pin_crc32);
    }

    std::string type_name = "";
    if(value->stypeid->details != NULL) {
        type_name = value->stypeid->details->name;
    }

    LOG << "SignalOutputPin() for " << name << ", value type=" << type_name;

    if((*entity_type)->properties != NULL) {
        for(auto prop : *(*entity_type)->properties) {
            std::string prop_name;
            if(decrc32_map.find(prop.name_crc32) != decrc32_map.end()) {
                prop_name = decrc32_map[prop.name_crc32];
            } else {
                prop_name = "crc "s + std::to_string(prop.name_crc32);
            }

            LOG << "\tPROP name=" << prop_name << " unk1=" << prop.unk1
                << " unk2=" << prop.unk2 << " unk3=" << prop.unk3
                << " inner=" << std::hex << (void *) prop.inner
                << " inner.unk1 deref'd=" << *(uint64_t *) prop.inner->unk1;
        }
    }

    return NULL;
}
