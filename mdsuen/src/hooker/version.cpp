// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2020 David Koňařík
#include "version.hpp"
#include <vector>
#include <pahil.hpp>

std::optional<GameVersion> find_game_version() {
    auto exec_path = pahil::Module::get_exec_path();
    size_t filesize = std::filesystem::file_size(exec_path);

    std::vector<GameVersion> versions = {
        linux_version,
        win_breach_version,
        win_gog_version,
        dbg_release_version,
    };
    for(auto ver : versions) {
        if(ver.filesize == filesize) {
            return ver;
        }
    }

    return std::nullopt;
}

std::unique_ptr<pahil::Module> prog;
