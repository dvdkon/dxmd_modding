// This file is part of MDSuen and is licenced under the MPL 2.0
// (c) 2021 David Koňařík
#include <fstream>
#include <thread>
#include <mutex>
#include <pahil.hpp>
#include <windows.h>
#include "common/dxmd/runtime.resource.hpp"
#include "minhook/MinHook.h"

int (*orig_WinMain)(HINSTANCE, HINSTANCE, LPSTR, int);

int hook_WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {
    LOG << "WinMain() called!";
    return orig_WinMain(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
}

ZResourcePtr (*orig_GetResourcePtrInternal)(ZResourceManager *, const ZRuntimeResourceID *, EResourceLoadPriority, EResourceLoadStatus, bool);
ZResourcePtr hook_GetResourcePtrInternal(ZResourceManager *self,
                                         const ZRuntimeResourceID *ridResource,
                                         EResourceLoadPriority nPriority,
                                         EResourceLoadStatus requiredLoadStatus,
                                         bool loadStreamedDependencies) {
    //while(true);
    LOG << "GetResourcePtrInternal() called,"
        << " ridResource=" << ridResource->ToDebugString()
        << " requiredLoadStatus=" << requiredLoadStatus
        << " loadStreamedDependencies=" << loadStreamedDependencies;
    return orig_GetResourcePtrInternal(self, ridResource, nPriority, requiredLoadStatus, loadStreamedDependencies);
}

ZString *(*orig_CalcResourceFileName)(ZLocalResourceFileMediator *, ZString *, ZRuntimeResourceID *);
ZString *hook_CalcResourceFileName(ZLocalResourceFileMediator *self,
                                   ZString *__return_storage_ptr__,
                                   ZRuntimeResourceID *rrid) {
    auto ret = orig_CalcResourceFileName(self, __return_storage_ptr__, rrid);
    LOG << "CalcResourceFileName(" << rrid->ToDebugString() << ") = " << *ret;
    return ret;
}

void (*orig_GetResourceFileNameAndOffset)(ZLocalResourceFileMediator *, const ZRuntimeResourceID &, ZString &, uint32_t &);
void hook_GetResourceFileNameAndOffset(ZLocalResourceFileMediator *self,
                                       const ZRuntimeResourceID &ridResource,
                                       ZString &outString,
                                       uint32_t &outOffset) {
    orig_GetResourceFileNameAndOffset(self, ridResource, outString, outOffset);
    LOG << "GetResourceFileNameAndOffset(" << ridResource.ToDebugString() << ") = " << outString << " @ " << outOffset;
}

void (*orig_Load)(ZResourceStreamer *, ZResourceStub *, EResourceLoadPriority);
void hook_Load(ZResourceStreamer *self, ZResourceStub *rstub, EResourceLoadPriority prio) {
    LOG << "ZResourceStreamer::Load(" << rstub->m_ridResource.ToDebugString() << ")";
    orig_Load(self, rstub, prio);
}


void (*orig_UpdateResource)(ZLocalResourceFileMediator *,
                            ZResourceStub *, int,
                            ZDelegate<void (ZRuntimeResourceID,ZResourceStub *,int,ZString const &,unsigned __int64,unsigned __int64)> *);
void hook_UpdateResource(ZLocalResourceFileMediator *self, ZResourceStub *rstub, int prio,
                         ZDelegate<void (ZRuntimeResourceID,ZResourceStub *,int,ZString const &,unsigned __int64,unsigned __int64)> *callback) {
    LOG << "UpdateResource(" << rstub->m_ridResource.ToDebugString() << ") type=" << (rstub->m_eType & 0x7f);
    orig_UpdateResource(self, rstub, prio, callback);
}


void (*orig_AddResourceToQueue)(ZResourceStreamer *, const ZRuntimeResourceID, ZResourceStub *, int32_t, const ZString &, uint64_t, uint64_t);
void hook_AddResourceToQueue(ZResourceStreamer *self,
                             const ZRuntimeResourceID id,
                             ZResourceStub *pResource,
                             int32_t nPriority,
                             const ZString &sFilename,
                             uint64_t nFileVersion,
                             uint64_t nFileOffset) {
    LOG << "AddResourceToQueue(" << id.ToDebugString() << " / " <<  pResource->m_ridResource.ToDebugString() << ") filename=" << sFilename << " ver=" << nFileVersion << " offset=" << nFileOffset;
    orig_AddResourceToQueue(self, id, pResource, nPriority, sFilename, nFileVersion, nFileOffset);
}

void (*orig_SetResourceStatus)(ZResourceStub *self, EResourceStatus eResourceStatus);
void hook_SetResourceStatus(ZResourceStub *self, EResourceStatus eResourceStatus) {
    LOG << "SetResourceStatus(" << self->GetRuntimeResourceID().ToDebugString() << ", " << eResourceStatus << ")";
    orig_SetResourceStatus(self, eResourceStatus);
}

void (*orig_ReadFromResource)(ZResourceStreamer *self, int64_t handle, void *pBuffer, int64_t nPosition, uint64_t nSize, const ZDelegate<void __cdecl(__int64,void *,unsigned __int64,unsigned __int64)> &callback);
void hook_ReadFromResource(ZResourceStreamer *self, int64_t handle, void *pBuffer, int64_t nPosition, uint64_t nSize, const ZDelegate<void __cdecl(__int64,void *,unsigned __int64,unsigned __int64)> &callback) {
    LOG << "ReadFromResource(" << handle << ", " << nPosition << ", " << nSize << ")";
    orig_ReadFromResource(self, handle, pBuffer, nPosition, nSize, callback);
}

int64_t (*orig_OpenResource)(ZResourceDevice *self, const ZString &fileName, int32_t priority, ZResourceStreamer::EStreamType streamType, bool shareable, ZResourceStreamer::EOpenResourceFlags flags);
int64_t hook_OpenResource(ZResourceDevice *self, const ZString &fileName, int32_t priority, ZResourceStreamer::EStreamType streamType, bool shareable, ZResourceStreamer::EOpenResourceFlags flags) {
    auto handle = orig_OpenResource(self, fileName, priority, streamType, shareable, flags);
    LOG << "OpenResource(" << fileName << ", type=" << streamType << ", flags=" << flags << ") = " << handle;
    return handle;
}

/** Will be called before the hooked process' main() */
void on_library_load() {
    if(MH_Initialize() != MH_OK) {
        LOG << "MinHook init failed!";
        return;
    }

    pahil::Module engine(0x140000000);
    auto WinMain = reinterpret_cast<void *>(engine.translate_addr(0x140057720));
    if(MH_CreateHook(
        WinMain,
        reinterpret_cast<void *>(hook_WinMain),
        reinterpret_cast<void **>(&orig_WinMain)) != MH_OK) {
        LOG << "Hooking WinMain failed!";
    }
    if(MH_EnableHook(WinMain) != MH_OK) {
        LOG << "Enabling WinMain hook failed!";
    }
    /*engine.init_exec_allocator();
    engine.unprotect_pages();
    {
        auto p = engine.patch();
        p.hook(0x140057720, hook_WinMain);
        p.apply();
    }*/

    pahil::Module runtime_resource(0x180000000, "runtime.resource.dll");

    auto GetResourcePtrInternal = reinterpret_cast<void *>(runtime_resource.translate_addr(0x18008b3d0));
    if(MH_CreateHook(
        GetResourcePtrInternal,
        reinterpret_cast<void *>(hook_GetResourcePtrInternal),
        reinterpret_cast<void **>(&orig_GetResourcePtrInternal)) != MH_OK) {
        LOG << "Hooking GetResourcePtrInternal failed!";
    }
    if(MH_EnableHook(GetResourcePtrInternal) != MH_OK) {
        LOG << "Enabling GetResourcePtrInternal hook failed!";
    }

    auto CalcResourceFileName = reinterpret_cast<void *>(runtime_resource.translate_addr(0x180055590));
    if(MH_CreateHook(
        CalcResourceFileName,
        reinterpret_cast<void *>(hook_CalcResourceFileName),
        reinterpret_cast<void **>(&orig_CalcResourceFileName)) != MH_OK) {
        LOG << "Hooking CalcResourceFileName failed!";
    }
    if(MH_EnableHook(CalcResourceFileName) != MH_OK) {
        LOG << "Enabling CalcResourceFileName hook failed!";
    }

    auto GetResourceFileNameAndOffset = reinterpret_cast<void *>(runtime_resource.translate_addr(0x18008a320));
    if(MH_CreateHook(
        GetResourceFileNameAndOffset,
        reinterpret_cast<void *>(hook_GetResourceFileNameAndOffset),
        reinterpret_cast<void **>(&orig_GetResourceFileNameAndOffset)) != MH_OK) {
        LOG << "Hooking GetResourceFileNameAndOffset failed!";
    }
    if(MH_EnableHook(GetResourceFileNameAndOffset) != MH_OK) {
        LOG << "Enabling GetResourceFileNameAndOffset hook failed!";
    }

    auto Load = reinterpret_cast<void *>(runtime_resource.translate_addr(0x1800aa1e0));
    if(MH_CreateHook(
        Load,
        reinterpret_cast<void *>(hook_Load),
        reinterpret_cast<void **>(&orig_Load)) != MH_OK) {
        LOG << "Hooking Load failed!";
    }
    if(MH_EnableHook(Load) != MH_OK) {
        LOG << "Enabling Load hook failed!";
    }

    auto UpdateResource = reinterpret_cast<void *>(runtime_resource.translate_addr(0x1800f1d60));
    if(MH_CreateHook(
        UpdateResource,
        reinterpret_cast<void *>(hook_UpdateResource),
        reinterpret_cast<void **>(&orig_UpdateResource)) != MH_OK) {
        LOG << "Hooking UpdateResource failed!";
    }
    if(MH_EnableHook(UpdateResource) != MH_OK) {
        LOG << "Enabling UpdateResource hook failed!";
    }

    auto AddResourceToQueue = reinterpret_cast<void *>(runtime_resource.translate_addr(0x18004e680));
    if(MH_CreateHook(
        AddResourceToQueue,
        reinterpret_cast<void *>(hook_AddResourceToQueue),
        reinterpret_cast<void **>(&orig_AddResourceToQueue)) != MH_OK) {
        LOG << "Hooking AddResourceToQueue failed!";
    }
    if(MH_EnableHook(AddResourceToQueue) != MH_OK) {
        LOG << "Enabling AddResourceToQueue hook failed!";
    }

    auto SetResourceStatus = reinterpret_cast<void *>(runtime_resource.translate_addr(0x1800e4ca0));
    if(MH_CreateHook(
        SetResourceStatus,
        reinterpret_cast<void *>(hook_SetResourceStatus),
        reinterpret_cast<void **>(&orig_SetResourceStatus)) != MH_OK) {
        LOG << "Hooking SetResourceStatus failed!";
    }
    if(MH_EnableHook(SetResourceStatus) != MH_OK) {
        LOG << "Enabling SetResourceStatus hook failed!";
    }

    auto ReadFromResource = reinterpret_cast<void *>(runtime_resource.translate_addr(0x1800ce0d0));
    if(MH_CreateHook(
        ReadFromResource,
        reinterpret_cast<void *>(hook_ReadFromResource),
        reinterpret_cast<void **>(&orig_ReadFromResource)) != MH_OK) {
        LOG << "Hooking ReadFromResource failed!";
    }
    if(MH_EnableHook(ReadFromResource) != MH_OK) {
        LOG << "Enabling ReadFromResource hook failed!";
    }

    auto OpenResource = reinterpret_cast<void *>(runtime_resource.translate_addr(0x1800b2070));
    if(MH_CreateHook(
        OpenResource,
        reinterpret_cast<void *>(hook_OpenResource),
        reinterpret_cast<void **>(&orig_OpenResource)) != MH_OK) {
        LOG << "Hooking OpenResource failed!";
    }
    if(MH_EnableHook(OpenResource) != MH_OK) {
        LOG << "Enabling OpenResource hook failed!";
    }

    /*runtime_resource.init_exec_allocator();
    runtime_resource.unprotect_pages();
    {
        auto p = runtime_resource.patch();
        p.hook(0x18008b3d0, reinterpret_cast<void *>(hook_GetResourcePtrInternal));
        p.apply();
    }*/
}


std::ofstream logfile("mdsuen_dbg_hooker.log");
std::mutex log_mutex;

void main_inner() {
    // It's hard to view the stdout/stderr of a GUI process on Windows, so
    // redirect the log into a file
    pahil::logger.msg_handler = [&](std::string msg) {
        log_mutex.lock();
        // Don't create ridiculously long logs
        if(logfile.tellp() < 100000000) {
            //OutputDebugStringA((msg + "\n").c_str());
            logfile << msg << std::endl;
        }
        log_mutex.unlock();
    };

    LOG << "MDSuen debug hooker is starting...";

    try {
        on_library_load();
    } catch(std::exception &e) {
        LOG << "MDSuen exception in init: " << e.what();
        return;
    }

    LOG << "MDSuen init complete!";
}

bool WINAPI DllMain(HINSTANCE dll, int reason, void *reserved) {
    if(reason != DLL_PROCESS_ATTACH) {
        return true;
    }

    __try {
        main_inner();
    } __except(EXCEPTION_EXECUTE_HANDLER) {
        LOG << "MDSuen init failed with structured exception! "
            << GetExceptionCode();
        return false;
    }
    return true;
}