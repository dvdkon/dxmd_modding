cmake_minimum_required(VERSION 3.16)

set(blacklist System.Windows.Interactivity.dll
              System.Net.Http.Formatting.dll
              System.Data.SQLite.dll
              System.Data.SqlServerCe.dll
              Game.AnimationServices.dll)
file(MAKE_DIRECTORY ${outdir})

file(GLOB dlls ${game_location}/g2base.dll
               ${game_location}/basedll.dll
               ${game_location}/nxapp.dll
               ${game_location}/game.*.dll
               ${game_location}/resource.*.dll
               ${game_location}/runtime.*.dll
               ${game_location}/system.*.dll
               ${game_location}/tool.*.dll)
foreach(dll ${dlls})
    get_filename_component(name ${dll} NAME)
    if(NOT ${name} IN_LIST blacklist)
        message("Creating wrapper DLLs for ${dll}")
        string(REPLACE .dll .pdb pdb ${dll})
        execute_process(COMMAND ${generator} ${dll} ${pdb} ${outdir})
    endif()
endforeach()
