cmake_minimum_required(VERSION 3.16)

option (FORCE_COLOURED_OUTPUT "Force clang colour diagnostics" FALSE)
if(${FORCE_COLOURED_OUTPUT})
    # TODO: Don't add the flag for MSVC
    add_compile_options(-fcolor-diagnostics)
endif()

project(MDSuen)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_EXTENSIONS OFF)

option(game_location "Location of DXMD's \"release\" directory")

add_compile_definitions(WIN32_LEAN_AND_MEAN NOMINMAX _CRT_SECURE_NO_WARNINGS _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING)
# Disable some warnings that crop up from included header files
add_compile_options(-Wno-reserved-id-macro -Wno-documentation)

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
add_subdirectory(deps/glfw)
target_compile_options(glfw PRIVATE -Wno-everything)

add_subdirectory(deps/pahil)

add_library(glew STATIC ${PROJECT_SOURCE_DIR}/deps/glew/src/glew.c)
target_include_directories(glew PUBLIC ${PROJECT_SOURCE_DIR}/deps/glew/include)
target_link_libraries(glew PUBLIC opengl32)
target_compile_definitions(glew PUBLIC GLEW_STATIC)

file(GLOB imgui_sources
    ${PROJECT_SOURCE_DIR}/deps/imgui/*.cpp
    ${PROJECT_SOURCE_DIR}/deps/imgui/misc/cpp/imgui_stdlib.cpp
    ${PROJECT_SOURCE_DIR}/deps/imgui/backends/imgui_impl_opengl3.cpp
    ${PROJECT_SOURCE_DIR}/deps/imgui/backends/imgui_impl_glfw.cpp
    ${PROJECT_SOURCE_DIR}/deps/imgui/backends/imgui_impl_win32.cpp)
add_library(imgui STATIC ${imgui_sources})
target_include_directories(imgui PUBLIC
    ${PROJECT_SOURCE_DIR}/deps/imgui
    ${PROJECT_SOURCE_DIR}/deps/imgui/misc/cpp
    ${PROJECT_SOURCE_DIR}/deps/imgui/backends)
target_link_libraries(imgui PUBLIC glew glfw)

add_library(imgui_club INTERFACE)
target_include_directories(imgui_club INTERFACE
    ${PROJECT_SOURCE_DIR}/deps/imgui_club/imgui_memory_editor)

add_executable(wrapper_dll_builder src/wrapper_dll_builder/main.cpp)
target_include_directories(wrapper_dll_builder PRIVATE "$ENV{VSINSTALLDIR}/DIA SDK/include")
add_custom_target(generate_wrapper_dlls
    COMMAND ${CMAKE_COMMAND}
        -Dgame_location=${game_location}
        -Dgenerator=$<TARGET_FILE:wrapper_dll_builder>
        -Doutdir=${PROJECT_SOURCE_DIR}/dxmd_dlls
        -P ${PROJECT_SOURCE_DIR}/generate_wrapper_dlls.cmake 
    DEPENDS wrapper_dll_builder generate_wrapper_dlls.cmake)

file(GLOB mdsuen_common_sources ${PROJECT_SOURCE_DIR}/src/common/*.cpp)
add_library(mdsuen_common STATIC ${mdsuen_common_sources})
target_include_directories(mdsuen_common PUBLIC ${PROJECT_SOURCE_DIR}/src)
target_link_libraries(mdsuen_common PRIVATE pahil imgui)

add_executable(mdsuen_launcher src/launcher/main.cpp)
target_include_directories(mdsuen_launcher PRIVATE deps/args_hxx)
target_link_libraries(mdsuen_launcher PRIVATE pahil)

file(GLOB mdsuen_hooker_sources ${PROJECT_SOURCE_DIR}/src/hooker/*.cpp)
add_library(mdsuen_hooker SHARED ${mdsuen_hooker_sources})
target_include_directories(mdsuen_hooker PUBLIC ${PROJECT_SOURCE_DIR}/src)
target_link_libraries(mdsuen_hooker PRIVATE pahil imgui)

set(dxmd_libs
    basedll.0 nxapp.0 g2base.0
    system.io.0 system.job.0 system.net.0 system.connection.0
    system.serializer.0 system.render.0
    runtime.core.0 runtime.debug.0 runtime.resource.0 runtime.entity.0
    runtime.localization.0 runtime.render.0 runtime.render.1 runtime.sound.0
    runtime.gfx.0 runtime.streaming.0 runtime.activation.0 runtime.physics.0
    runtime.physics.physx.0 runtime.animation.0 runtime.navigation.0
    runtime.facefx.0 runtime.animationgraph.0 runtime.purehair.0
    runtime.lodsystem.0 runtime.video.0 runtime.input.0
    runtime.windowsystem.0 runtime.platformservices.0 runtime.events.0
    resource.core.0
    game.core.0 game.main.0 game.sci.0 game.cover.0 game.camera.0)

file(GLOB_RECURSE mdsuen_editor_sources
    ${PROJECT_SOURCE_DIR}/src/editor/*.cpp
    ${PROJECT_SOURCE_DIR}/src/editor/*.rc)
add_executable(mdsuen_editor WIN32 ${mdsuen_editor_sources})
target_include_directories(mdsuen_editor PUBLIC
    ${PROJECT_SOURCE_DIR}/src
    ${PROJECT_SOURCE_DIR}/deps/DirectXTex)
target_link_directories(mdsuen_editor PUBLIC ${PROJECT_SOURCE_DIR}/dxmd_dlls)
target_link_libraries(mdsuen_editor
    PRIVATE mdsuen_common pahil imgui imgui_club ${dxmd_libs})

file(GLOB mdsuen_dbg_hooker_sources ${PROJECT_SOURCE_DIR}/src/dbg_hooker/*.cpp ${PROJECT_SOURCE_DIR}/src/dbg_hooker/minhook/*.c ${PROJECT_SOURCE_DIR}/src/dbg_hooker/minhook/hde/*.c)
add_library(mdsuen_dbg_hooker SHARED ${mdsuen_dbg_hooker_sources})
target_include_directories(mdsuen_dbg_hooker PUBLIC ${PROJECT_SOURCE_DIR}/src)
target_link_directories(mdsuen_dbg_hooker PUBLIC ${PROJECT_SOURCE_DIR}/dxmd_dlls)
target_link_libraries(mdsuen_dbg_hooker PRIVATE pahil ${dxmd_libs})
target_compile_definitions(mdsuen_dbg_hooker PRIVATE SUBHOOK_STATIC)
