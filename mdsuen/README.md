# MDSuen

MDSuen is a shared library that will hook into the DX:MD executable and provide
some extra functionality, like logging when important functions are called or
allowing access to the game's console.

There are a few showcase videos:

- [Linux demo from 2020-04-12](https://www.youtube.com/watch?v=vMumNti_sPw)
- [1.0.0 demo from 2020-05-08](https://www.youtube.com/watch?v=N4cgQHCiXj0)

## Game versions

MDSuen supports multiple versions of the game:

- The latest Linux version on Steam  
  SHA256 hash: ef4fb752cd423ad615a2d3feca681ed55fb4953e21b4ca0e15d9dae748886e11  
  (only in an old version, support is currently broken)
- Standalone Deus Ex: Breach executable from Steam  
  SHA256 hash: b1b7228169b8e05940ee4ca1a33c4454f8629e59a98f1e5aeb2fa5c526eca044  
  This executable is used instead of the normal DX:MD one because DXB.exe is
  Denuvo-free and therefore not encrypted. It can play the normal game just
  fine.  
  To download it, open the [Steam console](steam://open/console) and run the
  following command:

      download_depot 555450 555452

- GOG executable  
  SHA256 hash: cf4805608f9cc7129a8f04ade8eeefdf1f0cd849a1f702dfe8f0cd06f2f53ee8

## Linux installation

- Build MDSuen using [xmake](https://xmake.io/#/)
- Start Steam
- Go to the game directory (`<steam dir>/steamapps/common/Deus Ex Mankind
  Divided`) and run `env SteamAppId=337000 LD_PRELOAD=<path to
  mdsuen>/build/linux/x86_64/release/libmdsuen.so" ./bin/DeusExMD`

## Windows installation

- Download the latest release from [OSDN](https://osdn.net/users/dvdkon/pf/dxmd_modding/files/)
- Extract the files into your game's `retail` folder
- If you're using the Steam Breach executable, you'll need to create a file in
  this directory named `steam_appid.txt` containing `555450`
- Run `mdsuen_launcher.exe`

## Usage

Right now, MDSuen provides a few functions accessible through an in-game
overlay. Press `F12` to open it.
