# Datautils

Datautils is a collection of Python scripts for exploring DX:MD's data files.
Probably the most interesting functionality is mounting the data as a Linux
FUSE filesystem. It's currently very slow, because it's not optimised at all.
Using [PyPy](https://www.pypy.org/) is not mandatory, but speeds the program up
significantly.

Example usage:

	GAME="<path to game folder>"
	pypy3 read_headers.py "$GAME" metadata
	mkdir -p mountdir
	pypy3 mount.py -o metadata=metadata,datadir="$GAME" mountdir

The paths saved in the game's data files are similar to normal filesystem
paths, but different, so the mount script creates a rudimentary mapping. For
example, `[a:/b/c/d.swf].pc_swf` becomes `a:/b/c/d.swf/.pc_swf`. Note that
files starting with a dot are treated as hidden by most \*NIX utilities.

